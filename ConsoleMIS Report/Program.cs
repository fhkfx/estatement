﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;



namespace ConsoleMIS_Report
{
    class Program
    {
        static private String delimiter = ",";
        static String genfileExt = ".csv";
        static Int32 eStmt_SubscribeAcLstRecord = 0;
        static Int32 eStmt_SubscribeByCatRptRecord = 0;
        static Int32 eStmt_SubscribeRptRecord = 0;
        static Int32 noOfRecord = 0;

        static void Main(string[] args)
        {
            writeLog("\n=========================================================");
            writeLog(String.Format("[Estatement] Generate MIS Report Start" + " --- " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            noOfRecord = generateMISReport();
            writeLog("No. of records generated as follow :");
                writeLog(String.Format("{0} : {1}", ConfigurationManager.AppSettings.Get("eStmt_SubscribeAcLstName") + genfileExt, eStmt_SubscribeAcLstRecord));
            writeLog(String.Format("{0} : {1}", ConfigurationManager.AppSettings.Get("eStmt_SubscribeByCatRptName") + genfileExt, eStmt_SubscribeByCatRptRecord));
            writeLog(String.Format("{0} : {1}", ConfigurationManager.AppSettings.Get("eStmt_SubscribeRptName") + genfileExt, eStmt_SubscribeRptRecord));
            writeLog(String.Format("Total : {0}", noOfRecord));
            writeLog(String.Format("[Estatement] Generate MIS Report Finish" + " --- " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            System.Threading.Thread.Sleep(5000);
        }

        static private Int32 generateMISReport()
        {
            String eStmt_SubscribeAcLstData = "";
            String eStmt_SubscribeByCatRptData = "";
            String eStmt_SubscribeRptData = "";

            String file_eStmt_SubscribeAcLst_Name = ConfigurationManager.AppSettings.Get("eStmt_SubscribeAcLstPath") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "_" + ConfigurationManager.AppSettings.Get("eStmt_SubscribeAcLstName") + genfileExt;
            String file_eStmt_SubscribeByCatRpt_Name = ConfigurationManager.AppSettings.Get("eStmt_SubscribeByCatRptPath") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "_" + ConfigurationManager.AppSettings.Get("eStmt_SubscribeByCatRptName") + genfileExt;
            String file_eStmt_SubscribeRpt_Name = ConfigurationManager.AppSettings.Get("eStmt_SubscribeRptPath") + "\\" + DateTime.Now.ToString("yyyyMMdd") + "_" + ConfigurationManager.AppSettings.Get("eStmt_SubscribeRptName") + genfileExt;

            System.IO.StreamWriter file_eStmt_SubscribeAcLst = new System.IO.StreamWriter(file_eStmt_SubscribeAcLst_Name, false, Encoding.Unicode);
            System.IO.StreamWriter file_eStmt_SubscribeByCatRpt = new System.IO.StreamWriter(file_eStmt_SubscribeByCatRpt_Name, false, Encoding.Unicode);
            System.IO.StreamWriter file_eStmt_SubscribeRpt = new System.IO.StreamWriter(file_eStmt_SubscribeRpt_Name, false, Encoding.Unicode);

            try
            {
                using (EstatementEntities entities = new EstatementEntities())
                {
                    #region eStmt_SubscribeAcLst
                    writeLog(String.Format("Generate Report : {0} start", Path.GetFileName(file_eStmt_SubscribeAcLst_Name)));
                    List<tbAccountList> mis_eStmt_SubscribeAcLst_SubscribeRpt_Data = entities.tbAccountLists.ToList();

                    // Header
                    eStmt_SubscribeAcLstData = @"""RMID""";
                    eStmt_SubscribeAcLstData = eStmt_SubscribeAcLstData + delimiter + @"""StatementType""";
                    eStmt_SubscribeAcLstData = eStmt_SubscribeAcLstData + delimiter + @"""AccountNumber""";
                    eStmt_SubscribeAcLstData = eStmt_SubscribeAcLstData + delimiter + @"""Subscription_Status""";
                    file_eStmt_SubscribeAcLst.WriteLine(eStmt_SubscribeAcLstData);

                    // Body
                    foreach (var item in mis_eStmt_SubscribeAcLst_SubscribeRpt_Data)
                    {
                        eStmt_SubscribeAcLstData = "\"" + item.RMID;      // RMID
                        eStmt_SubscribeAcLstData = eStmt_SubscribeAcLstData + "\"" + delimiter + "\"" + item.StatementType;      // Statement Type
                        eStmt_SubscribeAcLstData = eStmt_SubscribeAcLstData + "\"" + delimiter + "\"" + item.AccountNumber;      // Account Number
                        eStmt_SubscribeAcLstData = eStmt_SubscribeAcLstData + "\"" + delimiter + "\"" + item.RegistrationStatus;      // Subscription_Status
                        file_eStmt_SubscribeAcLst.WriteLine(eStmt_SubscribeAcLstData + "\"");
                        eStmt_SubscribeAcLstRecord++;
                        noOfRecord++;
                    }
                    file_eStmt_SubscribeAcLst.Close();
                    writeLog(String.Format("Generate Report : {0} finish", Path.GetFileName(file_eStmt_SubscribeAcLst_Name)));
                    #endregion

                    #region eStmt_SubscribeRpt
                    writeLog(String.Format("Generate Report : {0} start", Path.GetFileName(file_eStmt_SubscribeRpt_Name)));
                    // Header
                    eStmt_SubscribeRptData = @"""RMID""";
                    eStmt_SubscribeRptData = eStmt_SubscribeRptData + delimiter + @"""APP_CD""";
                    eStmt_SubscribeRptData = eStmt_SubscribeRptData + delimiter + @"""ACCT_NO""";
                    eStmt_SubscribeRptData = eStmt_SubscribeRptData + delimiter + @"""Category""";
                    eStmt_SubscribeRptData = eStmt_SubscribeRptData + delimiter + @"""STMT_CAT""";
                    file_eStmt_SubscribeRpt.WriteLine(eStmt_SubscribeRptData);

                    // Body
                    foreach (var item in mis_eStmt_SubscribeAcLst_SubscribeRpt_Data)
                    {
                        eStmt_SubscribeRptData = "\"" + item.RMID;      // RMID
                        eStmt_SubscribeRptData = eStmt_SubscribeRptData + "\"" + delimiter + "\"" + "";      // APP_CD
                        eStmt_SubscribeRptData = eStmt_SubscribeRptData + "\"" + delimiter + "\"" + item.AccountNumber;      // Account Number
                        eStmt_SubscribeRptData = eStmt_SubscribeRptData + "\"" + delimiter + "\"" + item.Category;      // Category
                        eStmt_SubscribeRptData = eStmt_SubscribeRptData + "\"" + delimiter + "\"" + item.StatementType;      // StatementType

                        file_eStmt_SubscribeRpt.WriteLine(eStmt_SubscribeRptData + "\"");
                        eStmt_SubscribeRptRecord++;
                        noOfRecord++;
                    }
                    file_eStmt_SubscribeRpt.Close();
                    mis_eStmt_SubscribeAcLst_SubscribeRpt_Data.Clear();
                    writeLog(String.Format("Generate Report : {0} finish", Path.GetFileName(file_eStmt_SubscribeRpt_Name)));
                    #endregion

                    #region eStmt_SubscribeByCatRpt
                    writeLog(String.Format("Generate Report : {0} start", Path.GetFileName(file_eStmt_SubscribeByCatRpt_Name)));
                    var mis_eStmt_SubscribeByCatRptData_CF_count = entities.tbAccountLists.Where(a => a.StatementType == Properties.Settings.Default.StatementTypeCF).Count();
                    var mis_eStmt_SubscribeByCatRptData_CreditCard_count = entities.tbAccountLists.Where(a => a.Category == Properties.Settings.Default.CategoryCreditCard).Count();
                    var mis_eStmt_SubscribeByCatRptData_PB_count = entities.tbAccountLists.Where(a => a.StatementType == Properties.Settings.Default.StatementTypePB).Count();
                    var mis_eStmt_SubscribeByCatRptData_SuperBank_count = entities.tbAccountLists.Where(a => a.Category == Properties.Settings.Default.CategorySuperBank || a.Category == Properties.Settings.Default.CategoryContainer).Count();
                    var mis_eStmt_SubscribeByCatRptData_Bank_count = entities.tbAccountLists.Where(a => a.StatementType != Properties.Settings.Default.StatementTypePB
                    && a.StatementType != Properties.Settings.Default.StatementTypeCF
                    && a.Category != Properties.Settings.Default.CategoryCreditCard
                    && a.Category != Properties.Settings.Default.CategorySuperBank
                    && a.Category != Properties.Settings.Default.CategoryContainer).Count();
                    var mis_eStmt_SubscribeByCatRptData_Total_count = mis_eStmt_SubscribeByCatRptData_Bank_count
                        + mis_eStmt_SubscribeByCatRptData_CF_count
                        + mis_eStmt_SubscribeByCatRptData_CreditCard_count
                        + mis_eStmt_SubscribeByCatRptData_PB_count
                        + mis_eStmt_SubscribeByCatRptData_SuperBank_count;

                    // Header
                    eStmt_SubscribeByCatRptData = @"""BANK""";
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + delimiter + @"""CF""";
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + delimiter + @"""CreditCard""";
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + delimiter + @"""PB""";
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + delimiter + @"""SuperBank""";
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + delimiter + @"""Total""";

                    // Body
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + "\n\"" + mis_eStmt_SubscribeByCatRptData_Bank_count;      // Bank count
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + "\"" + delimiter + "\"" + mis_eStmt_SubscribeByCatRptData_CF_count;      // CF count
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + "\"" + delimiter + "\"" + mis_eStmt_SubscribeByCatRptData_CreditCard_count;      // CreditCard count
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + "\"" + delimiter + "\"" + mis_eStmt_SubscribeByCatRptData_PB_count;      // PB count
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + "\"" + delimiter + "\"" + mis_eStmt_SubscribeByCatRptData_SuperBank_count;      // SuperBank count
                    eStmt_SubscribeByCatRptData = eStmt_SubscribeByCatRptData + "\"" + delimiter + "\"" + mis_eStmt_SubscribeByCatRptData_Total_count;      // Total count
                    file_eStmt_SubscribeByCatRpt.WriteLine(eStmt_SubscribeByCatRptData + "\"");
                    eStmt_SubscribeByCatRptRecord++;
                    noOfRecord++;
                    file_eStmt_SubscribeByCatRpt.Close();
                    writeLog(String.Format("Generate Report : {0} finish", Path.GetFileName(file_eStmt_SubscribeByCatRpt_Name)));
                    #endregion

                }
            }
            catch (Exception ex)
            {
                writeLog("Error in Generate MIS Report" + ex.ToString() + ((ex.InnerException == null) ? "" : " (InnerException: " + ex.InnerException.ToString() + ")"));
            }
            return noOfRecord;
        }

        public static void writeLog(String content)
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(Properties.Settings.Default.misLogFilePath, true, Encoding.UTF8))
            {
                try
                {
                    sw.WriteLine(content);
                    sw.Close();
                    Console.WriteLine(content);
                }
                catch (Exception ex)
                {
                    sw.WriteLine(String.Format("Error in writeLog : {0}"), ex.Message);
                    System.Threading.Thread.Sleep(5000);
                }
            }
        }
    }
}
