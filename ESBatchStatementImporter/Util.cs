﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ESBatchStatementImporter
{
    class Util
    {
        static EStatementEntities Entity = new EStatementEntities();
        static String logFilename = String.Format("{0}.log", Properties.Settings.Default.AppName);
        static String logPath = Path.Combine(Directory.GetCurrentDirectory(), "Log", logFilename);

        public static void Log(logType type, String content)
        {
            try
            {
                DateTime now = DateTime.Now;
                tbLog log = new tbLog();
                log.Type = Enum.GetName(typeof(logType), type);
                log.Content = content;
                log.CreateBy = Properties.Settings.Default.AppName;
                log.ModifyBy = Properties.Settings.Default.AppName;
                log.CreateDate = now;
                log.LastUpdate = now;
                Entity.AddTotbLog(log);
                Entity.SaveChanges();

                if (!File.GetLastWriteTime(logPath).ToString("yyyyMMdd").Equals(DateTime.Now.ToString("yyyyMMdd")))
                {
                    String newLogPath = String.Format("{0}_{1:yyyyMMdd}.log", logPath.Replace(".log", ""), File.GetLastWriteTime(logPath).ToString("yyyyMMdd"));
                    File.Delete(newLogPath);
                    File.Move(logPath, newLogPath);
                }

                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    sw.Close();
                }

                if (type != logType.Warning)
                {
                    Console.WriteLine(content);
                }
            }
            catch (Exception) { }
        }

        public static void writeToLogfile(String content)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    sw.Close();
                }
            }
            catch (Exception) { }
        }

        public static void ExecuteLog(String IdxFileName, String Category, DateTime StatementDate, int PdfCnt, int AcctNotFd, int StmtNotFd, int IdxIncorr, int StmtImported)
        {
            string sql = "INSERT INTO [dbo].[tbExecuteLog] " + 
                        "           ([IdxFileName] " + 
                        "           ,[Category] " + 
                        "           ,[StatementDate] " + 
                        "           ,[PdfCnt] " + 
                        "           ,[AcctNotFd] " + 
                        "           ,[StmtNotFd] " + 
                        "           ,[IdxIncorr] " + 
                        "           ,[StmtImported] " + 
                        "           ,[CreateDate] " + 
                        "           ,[CreateBy] " + 
                        "           ,[ModifyBy] " + 
                        "           ,[LastUpdate]) " + 
                        "     VALUES " + 
                        "           (@IdxFileName " + 
                        "           ,@Category " + 
                        "           ,@StatementDate " + 
                        "           ,@PdfCnt " + 
                        "           ,@AcctNotFd " + 
                        "           ,@StmtNotFd " + 
                        "           ,@IdxIncorr " + 
                        "           ,@StmtImported " + 
                        "           ,getdate() " + 
                        "           ,@CreateBy " + 
                        "           ,@ModifyBy " + 
                        "           ,getdate())";
            var args = new System.Data.Common.DbParameter[] { 
                new System.Data.SqlClient.SqlParameter { ParameterName = "IdxFileName", Value = IdxFileName },
                new System.Data.SqlClient.SqlParameter { ParameterName = "Category", Value = Category },
                new System.Data.SqlClient.SqlParameter { ParameterName = "StatementDate", Value = StatementDate },
                new System.Data.SqlClient.SqlParameter { ParameterName = "PdfCnt", Value = PdfCnt },
                new System.Data.SqlClient.SqlParameter { ParameterName = "AcctNotFd", Value = AcctNotFd },
                new System.Data.SqlClient.SqlParameter { ParameterName = "StmtNotFd", Value = StmtNotFd },
                new System.Data.SqlClient.SqlParameter { ParameterName = "IdxIncorr", Value = IdxIncorr },
                new System.Data.SqlClient.SqlParameter { ParameterName = "StmtImported", Value = StmtImported },
                new System.Data.SqlClient.SqlParameter { ParameterName = "CreateBy", Value = Properties.Settings.Default.AppName },
                new System.Data.SqlClient.SqlParameter { ParameterName = "ModifyBy", Value = Properties.Settings.Default.AppName }
            };
            Entity.ExecuteStoreCommand(sql, args);
        }

        public class account
        {
            public List<String> rmid;
            public String priAcctNo;
            public String acctNo;
            public String prodCode;
            public String statementType;
            public String acctCategory;

            public account() { }
            public account(List<String> i_rmid, String i_priAcctNo, String i_acctNo, String i_prodCode, String i_statementType, String i_acctCategory)
            {
                rmid = i_rmid;
                priAcctNo = i_priAcctNo;
                acctNo = i_acctNo;
                prodCode = i_prodCode;
                statementType = i_statementType;
                acctCategory = i_acctCategory;
            }
        }

        public class advice
        {
            public String stmtDate;
            public String acNo;
            public String metaType;
            public String pdfPath;
            public String orderType;
            public String assGroup;
            public String assSubGroup;
            public String assNameEN;
            public String assNameSC;
            public String assNameTC;
            public String bpid;
            public String productType;
            public String productName;
            public String adviceTitle;

            public advice(String i_stmtDate, String i_acNo, String i_metaType, String i_pdfPath, String i_orderType, String i_assGroup, String i_assSubGroup, String i_assNameEN, String i_assNameSC, String i_assNameTC, String i_bpid, String i_productType, String i_productName, String i_adviceTitle)
            {
                stmtDate = i_stmtDate;
                acNo = i_acNo;
                metaType = i_metaType;
                pdfPath = i_pdfPath;
                orderType = i_orderType;
                assGroup = i_assGroup;
                assSubGroup = i_assSubGroup;
                assNameEN = i_assNameEN;
                assNameSC = i_assNameSC;
                assNameTC = i_assNameTC;
                bpid = i_bpid;
                productType = i_productType;
                productName = i_productName;
                adviceTitle = i_adviceTitle;
            }
        }
        public class ProductTypeMapLogic
        {
            public String metaType;
            public String orderType;
            public String citicAssetGroup;
            public String citicAssetSubGroup;
            public String productType;
            public ProductTypeMapLogic(String i_metaType, String i_orderType, String i_citicAssetGroup, String i_citicAssetSubGroup, String i_productType)
            {
                metaType = i_metaType;
                orderType = i_orderType;
                citicAssetGroup = i_citicAssetGroup;
                citicAssetSubGroup = i_citicAssetSubGroup;
                productType = i_productType;
            }
        }
        public class ProductNameMapLogic
        {
            public String metaType;
            public String orderType;
            public String citicAssetGroup;
            public String citicAssetSubGroup;
            public String productName;
            public ProductNameMapLogic(String i_metaType, String i_orderType, String i_citicAssetGroup, String i_citicAssetSubGroup, String i_productName)
            {
                metaType = i_metaType;
                orderType = i_orderType;
                citicAssetGroup = i_citicAssetGroup;
                citicAssetSubGroup = i_citicAssetSubGroup;
                productName = i_productName;
            }
        }
        public class AdviceTitleMapLogic
        {
            public String metaType;
            public String orderType;
            public String citicAssetGroup;
            public String citicAssetSubGroup;
            public String adviceTitle;
            public AdviceTitleMapLogic(String i_metaType, String i_orderType, String i_citicAssetGroup, String i_citicAssetSubGroup, String i_adviceTitle)
            {
                metaType = i_metaType;
                orderType = i_orderType;
                citicAssetGroup = i_citicAssetGroup;
                citicAssetSubGroup = i_citicAssetSubGroup;
                adviceTitle = i_adviceTitle;
            }
        }
        public class productType_productName_adviceTitle_Mapping
        {
            public List<Util.ProductTypeMapLogic> productTypeMapList = new List<Util.ProductTypeMapLogic>();
            public List<Util.ProductNameMapLogic> productNameMapList = new List<Util.ProductNameMapLogic>();
            public List<Util.AdviceTitleMapLogic> adviceTitleMapList = new List<Util.AdviceTitleMapLogic>();
            public productType_productName_adviceTitle_Mapping() { }

            public void init()
            {
                // init list of product type, product name and advice title mapping logic 
                List<tbAdviceMapping> ptypeMap = Entity.ExecuteStoreQuery<tbAdviceMapping>(
                                    "SELECT * FROM [dbo].[tbAdviceMapping] Where MappingCategory = 'ProductType' order by RuleNumber"
                                    ).ToList();
                foreach (var record in ptypeMap)
                    productTypeMapList.Add(new Util.ProductTypeMapLogic(record.Meta_Type, record.Order_Type, record.Asset_Grp, record.Asset_SubGrp, record.Product_Type));

                List<tbAdviceMapping> pnameMap = Entity.ExecuteStoreQuery<tbAdviceMapping>(
                                   "SELECT * FROM [dbo].[tbAdviceMapping] Where MappingCategory = 'ProductName' order by RuleNumber"
                                   ).ToList();
                foreach (var record in pnameMap)
                    productNameMapList.Add(new Util.ProductNameMapLogic(record.Meta_Type, record.Order_Type, record.Asset_Grp, record.Asset_SubGrp, record.Product_Name));

                List<tbAdviceMapping> adviceTitleMap = Entity.ExecuteStoreQuery<tbAdviceMapping>(
                                   "SELECT * FROM [dbo].[tbAdviceMapping] Where MappingCategory = 'AdviceTitle' order by RuleNumber"
                                   ).ToList();
                foreach (var record in adviceTitleMap)
                    adviceTitleMapList.Add(new Util.AdviceTitleMapLogic(record.Meta_Type, record.Order_Type, record.Asset_Grp, record.Asset_SubGrp, record.AdviceTitleEN));
            }

            public String getProductType(String metaType, String orderType, String citicAssetGroup, String citicAssetSubGroup)
            {
                foreach (var productTypeMap in productTypeMapList)
                {
                    if (((productTypeMap.metaType != "any") && (metaType == productTypeMap.metaType)) || (productTypeMap.metaType == "any"))
                        if (((productTypeMap.orderType != "any") && (orderType == productTypeMap.orderType)) || (productTypeMap.orderType == "any"))
                            if (((productTypeMap.citicAssetGroup != "any") && (citicAssetGroup == productTypeMap.citicAssetGroup)) || (productTypeMap.citicAssetGroup == "any"))
                                if (((productTypeMap.citicAssetSubGroup != "any") && (citicAssetSubGroup == productTypeMap.citicAssetSubGroup)) || (productTypeMap.citicAssetSubGroup == "any"))
                                    return productTypeMap.productType;
                }
                return citicAssetGroup;
            }
            public String getProductName(String metaType, String orderType, String citicAssetGroup, String citicAssetSubGroup, String assNameENG)
            {
                String assNameEN = assNameENG;
                foreach (var productNameMap in productNameMapList)
                {
                    if (productNameMap.productName != "(Switch In Fund)")
                    {
                        if (((productNameMap.metaType != "any") && (metaType == productNameMap.metaType)) || (productNameMap.metaType == "any"))
                            if (((productNameMap.orderType != "any") && (orderType == productNameMap.orderType)) || (productNameMap.orderType == "any"))
                                if (((productNameMap.citicAssetGroup != "any") && (citicAssetGroup == productNameMap.citicAssetGroup)) || (productNameMap.citicAssetGroup == "any"))
                                    if (((productNameMap.citicAssetSubGroup != "any") && (citicAssetSubGroup == productNameMap.citicAssetSubGroup)) || (productNameMap.citicAssetSubGroup == "any"))
                                        return productNameMap.productName;
                    }
                    else
                    {
                        if (((productNameMap.metaType != "any") && (metaType == productNameMap.metaType)) || (productNameMap.metaType == "any"))
                            if (((productNameMap.orderType != "any") && (orderType == productNameMap.orderType)) || (productNameMap.orderType == "any"))
                                if (((productNameMap.citicAssetGroup != "any") && (citicAssetGroup == productNameMap.citicAssetGroup)) || (productNameMap.citicAssetGroup == "any"))
                                    if (((productNameMap.citicAssetSubGroup != "any") && (citicAssetSubGroup == productNameMap.citicAssetSubGroup)) || (productNameMap.citicAssetSubGroup == "any"))
                                        return (assNameEN + "(Switch In Fund)");
                    }
                }
                return assNameEN;
            }

            public String getAdviceTitle(String metaType, String orderType, String citicAssetGroup, String citicAssetSubGroup)
            {
                foreach (var adviceTitleMap in adviceTitleMapList)
                {
                    if (((adviceTitleMap.metaType != "any") && (metaType == adviceTitleMap.metaType)) || (adviceTitleMap.metaType == "any"))
                        if (((adviceTitleMap.orderType != "any") && (orderType == adviceTitleMap.orderType)) || (adviceTitleMap.orderType == "any"))
                            if (((adviceTitleMap.citicAssetGroup != "any") && (citicAssetGroup == adviceTitleMap.citicAssetGroup)) || (adviceTitleMap.citicAssetGroup == "any"))
                                if (((adviceTitleMap.citicAssetSubGroup != "any") && (citicAssetSubGroup == adviceTitleMap.citicAssetSubGroup)) || (adviceTitleMap.citicAssetSubGroup == "any"))
                                    return adviceTitleMap.adviceTitle;
                }
                return "No Advice Title Match";
            }

        }
    }
}
