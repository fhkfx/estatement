﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;

namespace ESBatchStatementImporter
{
    public enum logType { Info, Error, Warning };
    public enum StatementCategory { Bank, CiticFirst, CreditCard, InfoCast, SuperBank, Container, Advice };
    public enum statementGetDataColumn
    {
        bpid = 1,
        containerAcno = 2,
        stmtDate = 3,
        pdfPath = 4,
        productRef = 5,
        productInfo = 6,
    };
    public enum adviceGetDataColumn
    {
        stmtDate = 1,
        stmtFreq = 2,
        acNo = 5,
        metaType = 7,
        pdfPath = 8,
        orderType = 10,
        assGroup = 11,
        assSubGroup = 12,
        assNameEN = 13,
        assNameSC = 14,
        assNameTC = 15,
        bpid = 19,
        orderID = 23,
    };
    public enum directBankGetDataColumn
    {
        RM_NUMBER = 0,
        ACC_NO = 1,
        StatementDate = 2,
        FileName = 3,
        ProductInfo = 4,
        ProductRef = 5,
        AddDeleteFlag = 6,
        Category = 7,
        StatementType = 8,
    };

    class Program
    {
        private static EStatementEntities entity = new EStatementEntities();
        private static Random rand = new Random();
        private static Boolean isSuccess = true;
        private static Boolean isIndexFileExist = false;
        public static String foldertimeStamp = DateTime.Now.ToString("yyyyMMdd");
        public static Util.productType_productName_adviceTitle_Mapping Mapper = new Util.productType_productName_adviceTitle_Mapping();
        /************Modified by Jacky on 2018-12-28*****************************/
        private static String InfoArchiveFileName = "InfoArchive=";
        /************************************************************************/

        static void Main(string[] args)
        {
            try
            {
                Util.Log(logType.Info, "" + entity.CommandTimeout);
                Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "Log"));
                Util.Log(logType.Info, "=========================================================");
                Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} ESBatchStatementImporter Start.", DateTime.Now));
                entity.CommandTimeout = Properties.Settings.Default.EntityCommandTimeout;
                Mapper.init();
                StatementImport();
                MoveFileToErrorFolder(Properties.Settings.Default.IndexFilePathBank);
                MoveFileToErrorFolder(Properties.Settings.Default.IndexFilePathCiticFirst);
                MoveFileToErrorFolder(Properties.Settings.Default.IndexFilePathCreditCard);
                MoveFileToErrorFolder(Properties.Settings.Default.IndexFilePathInfoCast);
                MoveFileToErrorFolder(Properties.Settings.Default.IndexFilePathSuperBank);
                MoveFileToErrorFolder(Properties.Settings.Default.IndexFilePathDirectBank);
                MoveFileToErrorFolder(Properties.Settings.Default.StatementRootPathRemoteBank);
                MoveFileToErrorFolder(Properties.Settings.Default.StatementRootPathRemoteCiticFirst);
                MoveFileToErrorFolder(Properties.Settings.Default.StatementRootPathRemoteCreditCard);
                MoveFileToErrorFolder(Properties.Settings.Default.StatementRootPathRemoteSuperBank);
                Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} ESBatchStatementImporter End.", DateTime.Now));
                System.Threading.Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in main. Error message:{0}", ex.Message));
                System.Threading.Thread.Sleep(5000);
            }
        }

        private static void StatementImport()
        {
            try
            {
                Util.Log(logType.Info, "StatementImport(CiticFirst) Start.");
                Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START CITICFIRST STATEMENT PROCESSING", DateTime.Now));
                foreach (var filePath in Directory.GetFiles(Properties.Settings.Default.IndexFilePathCiticFirst, "*.txt"))
                {
                    isIndexFileExist = true;
                    StatementImport(filePath, Properties.Settings.Default.StatementRootPathRemoteCiticFirst, StatementCategory.CiticFirst);
                }
                Util.Log(logType.Info, "StatementImport(CiticFirst) End.");
                if (isIndexFileExist)
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END CITICFIRST STATEMENT PROCESSING with status = {1}", DateTime.Now, isSuccess ? "SUCCESS" : "FAIL"));
                }
                else
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END CITICFIRST STATEMENT PROCESSING with status = N/A", DateTime.Now));
                }
                isSuccess = true;
                isIndexFileExist = false;

                #region E - Statement enhancement(Bank statement Import) updated on 2018-11-28 only in production, do not need to add into InfoArchive
                Util.Log(logType.Info, "StatementImport(Bank) Start.");
                Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START BANK STATEMENT PROCESSING", DateTime.Now));
                foreach (var filePath in Directory.GetFiles(Properties.Settings.Default.IndexFilePathBank, "*.txt"))
                {
                    isIndexFileExist = true;
                    StatementImport(filePath, Properties.Settings.Default.StatementRootPathRemoteBank, StatementCategory.Bank);
                }
                Util.Log(logType.Info, "StatementImport(Bank) End.");
                if (isIndexFileExist)
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END BANK STATEMENT PROCESSING with status = {1}", DateTime.Now, isSuccess ? "SUCCESS" : "FAIL"));
                }
                else
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END BANK STATEMENT PROCESSING with status = N/A", DateTime.Now));
                }
                #endregion
                isSuccess = true;
                isIndexFileExist = false;

                #region  E - Statement enhancement(CreditCard statement Import) updated on 2018-11-28 only in production, do not need to add into InfoArchive
                Util.Log(logType.Info, "StatementImport(CreditCard) Start.");
                Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START CREDIT CARD PROCESSING", DateTime.Now));
                foreach (var filePath in Directory.GetFiles(Properties.Settings.Default.IndexFilePathCreditCard, "*.txt"))
                {
                    isIndexFileExist = true;
                    StatementImport(filePath, Properties.Settings.Default.StatementRootPathRemoteCreditCard, StatementCategory.CreditCard);
                }
                Util.Log(logType.Info, "StatementImport(CreditCard) End.");
                if (isIndexFileExist)
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END CREDIT CARD PROCESSING with status = {1}", DateTime.Now, isSuccess ? "SUCCESS" : "FAIL"));
                }
                else
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END CREDIT CARD PROCESSING with status = N/A", DateTime.Now));
                }
                #endregion
                isSuccess = true;
                isIndexFileExist = false;

                #region E-Statement enhancement (InfoCast statement Import)

                Util.Log(logType.Info, "StatementImport(INFOCAST) Start.");
                Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START INFOCAST STATEMENT PROCESSING", DateTime.Now));
                foreach (var filePath in Directory.GetFiles(Properties.Settings.Default.IndexFilePathInfoCast, "*.txt"))
                {
                    isIndexFileExist = true;
                    StatementImport(filePath, Properties.Settings.Default.StatementRootPathRemoteInfoCast, StatementCategory.InfoCast);
                }
                Util.Log(logType.Info, "StatementImport(INFOCAST) End.");
                if (isIndexFileExist)
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END INFOCAST STATEMENT PROCESSING with status = {1}", DateTime.Now, isSuccess ? "SUCCESS" : "FAIL"));
                }
                else
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END INFOCAST STATEMENT PROCESSING with status = N/A", DateTime.Now));
                }
                #endregion
                isSuccess = true;
                isIndexFileExist = false;

                #region E-Statement enhancement (SuperBank & Container statement Import)
                Util.Log(logType.Info, "StatementImport(SuperBank) Start.");
                Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START SUPERBANK STATEMENT PROCESSING", DateTime.Now));
                foreach (var filePath in Directory.GetFiles(Properties.Settings.Default.IndexFilePathSuperBank, "*.txt"))
                {
                    if (Path.GetFileName(filePath).Contains(Properties.Settings.Default.IndexFileNameSuperBankStatementKeyword))
                    {
                        isIndexFileExist = true;
                        Util.Log(logType.Info, "\n"); Util.Log(logType.Info, "SuperBank statement file import Start");
                        SuperBankAndContainerStatementImport(filePath, Properties.Settings.Default.StatementRootPathRemoteSuperBank, StatementCategory.SuperBank);
                        Util.Log(logType.Info, "SuperBank statement file import finish");
                    }
                    else if (Path.GetFileName(filePath).Contains(Properties.Settings.Default.IndexFileNameContainerStatementKeyword))
                    {
                        isIndexFileExist = true;
                        Util.Log(logType.Info, "\n"); Util.Log(logType.Info, "Container statement file import Start");
                        SuperBankAndContainerStatementImport(filePath, Properties.Settings.Default.StatementRootPathRemoteSuperBank, StatementCategory.Container);
                        Util.Log(logType.Info, "Container statement file import finish");
                    }
                    else if (Path.GetFileName(filePath).Contains(Properties.Settings.Default.IndexFileNameSuperBankOrContainerAdviceKeyword))
                    {
                        isIndexFileExist = true;
                        Util.Log(logType.Info, "\n"); Util.Log(logType.Info, "SuperBank and Container advice file import Start");
                        adviceImport(filePath, Properties.Settings.Default.StatementRootPathRemoteSuperBank, StatementCategory.Advice);
                        Util.Log(logType.Info, "SuperBank and Container advice file import finish");
                    }
                    else if (Path.GetFileName(filePath).Contains(Properties.Settings.Default.IndexFileNameSuperBankOrContainerDailyKeyword))
                    {
                        isIndexFileExist = true;
                        Util.Log(logType.Info, "\n"); Util.Log(logType.Info, "SuperBank and Container daily file import Start");
                        adviceImport(filePath, Properties.Settings.Default.StatementRootPathRemoteSuperBank, StatementCategory.Advice);
                        Util.Log(logType.Info, "SuperBank and Container daily file import finish");
                    }

                }
                Util.Log(logType.Info, "\n"); Util.Log(logType.Info, "StatementImport(SuperBank) End.");

                if (isIndexFileExist)
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END SUPERBANK STATEMENT PROCESSING with status = {1}", DateTime.Now, isSuccess ? "SUCCESS" : "FAIL"));
                }
                else
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END SUPERBANK STATEMENT PROCESSING with status = N/A", DateTime.Now));
                }
                #endregion

                #region E-Statement enhancement (DirectBank statement Import)
                Util.Log(logType.Info, "\n"); Util.Log(logType.Info, "StatementImport(DirectBank) Start.");
                Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START DIRECTBANK STATEMENT PROCESSING", DateTime.Now));
                foreach (var filePath in Directory.GetFiles(Properties.Settings.Default.IndexFilePathDirectBank, "*.txt"))
                {
                    if (Path.GetFileName(filePath).Contains(Properties.Settings.Default.IndexFileNameDirectBankKeyword))
                    {
                        isIndexFileExist = true;
                        Util.Log(logType.Info, "DirectBank file import Start");
                        DirectBankStatementImport(filePath);
                        Util.Log(logType.Info, "DirectBank file import finish");
                    }
                }
                Util.Log(logType.Info, "StatementImport(DirectBank) End.");
                if (isIndexFileExist)
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END DIRECTBANK STATEMENT PROCESSING with status = {1}", DateTime.Now, isSuccess ? "SUCCESS" : "FAIL"));
                }
                else
                {
                    Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END DIRECTBANK STATEMENT PROCESSING with status = N/A", DateTime.Now));
                }
                #endregion
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in StatementImport. Error message:{0}", ex.Message));
                if (ex.InnerException != null)
                    Util.Log(logType.Error, String.Format("Error in StatementImport. Inner Exception:{0}", ex.InnerException.Message));
            }
        }

        private static void StatementImport(String indexFilePath, String statementRootPath, StatementCategory statementCategory)
        {
            DateTime fileStatementDate = DateTime.Now;
            Int32 numOfPDF = 0;
            Int32 numOfAccountNotFound = 0;
            Int32 numOfStatementNotFound = 0;
            Int32 numOfIncorrectFormat = 0;
            Int32 numOfImport = 0;
            Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START processing Indexfile : {1}", DateTime.Now, Path.GetFileName(indexFilePath)));
            if (File.Exists(indexFilePath))
            {
                DateTime snapshotDate = DateTime.Now;
                List<String> lines = new List<String>();

                #region read index file
                using (TextReader tr = File.OpenText(indexFilePath))
                {
                    String eachLine = String.Empty;
                    int numFile;
                    int inputPDF;

                    switch (statementCategory)
                    {
                        case StatementCategory.InfoCast:
                            numFile = 0;
                            inputPDF = 0;
                            while ((eachLine = tr.ReadLine()) != null)
                            {
                                if (eachLine.StartsWith("1"))
                                {
                                    // header record
                                    string format = "yyyyMMdd";
                                    String[] data = eachLine.Split('|');
                                    if (!DateTime.TryParseExact(data[2].Trim(), format, CultureInfo.InvariantCulture,
                                            DateTimeStyles.None, out snapshotDate))
                                    {
                                        Util.Log(logType.Error, String.Format("Invalid SnapshotDate. IndexFile:{0}.", indexFilePath));
                                        return;
                                    }

                                    continue;
                                }
                                else if (eachLine.StartsWith("2"))
                                {
                                    lines.Add(eachLine);
                                    numOfPDF++;
                                    inputPDF++;
                                }
                                else if (eachLine.StartsWith("9"))
                                {
                                    // trailer record
                                    try
                                    {
                                        numFile = Convert.ToInt32(eachLine.Split('|')[1]);
                                    }
                                    catch (Exception e)
                                    {
                                        Util.Log(logType.Error, String.Format("Invalid Trailer count. IndexFile:{0}.", indexFilePath));
                                        return;
                                    }
                                    continue;
                                }
                            }

                            if (numFile != inputPDF)
                            {
                                Util.Log(logType.Error, String.Format("Trailer count not match with actual data. IndexFile:{0}.", indexFilePath));
                                return;
                            }

                            break;
                        default:
                            while ((eachLine = tr.ReadLine()) != null)
                            {
                                if (eachLine.Contains(';'))
                                {
                                    if (eachLine.Trim().Length == 9)
                                    {
                                        string format = "yyyyMMdd";
                                        eachLine = eachLine.Trim();

                                        if (!DateTime.TryParseExact(eachLine.Replace(";", ""), format, CultureInfo.InvariantCulture,
                                            DateTimeStyles.None, out snapshotDate))
                                        {
                                            Util.Log(logType.Error, String.Format("Invalid SnapshotDate. IndexFile:{0}.", indexFilePath));
                                            return;
                                        }
                                    }
                                    continue;
                                }
                                lines.Add(eachLine);
                                numOfPDF++;
                            }
                            break;
                    }

                    tr.Close();
                }
                #endregion

                int counter = 0;
                var SystemSettingList = entity.tbSystemSetting.ToList();
                int numCol = 7;

                foreach (var line in lines)
                {
                    String[] arrColumns = null;
                    arrColumns = line.Split('|');

                    #region validation
                    switch (statementCategory)
                    {
                        case StatementCategory.InfoCast:
                            numCol = 6;
                            break;
                        default:
                            // default 7
                            break;
                    }

                    if (arrColumns.Count() < numCol)
                    {
                        numOfIncorrectFormat++;
                        Util.Log(logType.Error, String.Format("Not enough field. IndexFile:{0} Line:{1}", indexFilePath, line));
                        continue;
                    }

                    String rmid = "";
                    DateTime statementDate = DateTime.Now;
                    String statementRelativePath = "";
                    String statementFilename = "";
                    String category = "";
                    String accountNumber = "";
                    String customerID = "";
                    //String statementType = "S";
                    String statementType = "M";
                    String productInfo = "";
                    String productRef = "";
                    String StatementRemotePath = "";
                    string format = "yyyyMMdd";
                    DateTime dateTime;
                    Boolean EStatementFlag;
                    String SuperBankStmtFootnote = "";
                    String Remarks2 = "";
                    String CustRMNum = "";
                    String CustRMID = "";
                    int StartPDF;
                    int EndPDF;
                    Boolean JoinEntities;
                    String stmtType = "";

                    switch (statementCategory)
                    {
                        case StatementCategory.InfoCast:

                            if (!DateTime.TryParseExact(arrColumns[3], format, CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out dateTime))
                            {
                                numOfIncorrectFormat++;
                                Util.Log(logType.Error, String.Format("Invalid statementDate. IndexFile:{0}. Line:{1}", indexFilePath, line));
                                continue;
                            }

                            //customerID = arrColumns[1].Trim();
                            statementDate = dateTime;
                            statementRelativePath = " ";
                            statementFilename = arrColumns[2].Trim();
                            category = "";
                            //statementType = statementFilename.Substring(4,1);
                            statementType = statementFilename.Split('-')[1];
                            accountNumber = arrColumns[1].Trim();
                            productInfo = arrColumns[5].Trim();
                            productRef = arrColumns[6].Trim();

                            fileStatementDate = statementDate;

                            break;
                        default:

                            if (!DateTime.TryParseExact(arrColumns[1], format, CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out dateTime))
                            {
                                numOfIncorrectFormat++;
                                Util.Log(logType.Error, String.Format("Invalid statementDate. IndexFile:{0}. Line:{1}", indexFilePath, line));
                                continue;
                            }

                            rmid = arrColumns[0].Trim();
                            statementDate = dateTime;
                            statementRelativePath = arrColumns[2];
                            statementFilename = arrColumns[3];
                            category = arrColumns[6];
                            accountNumber = arrColumns[7].Trim();
                            customerID = "";
                            //statementType = "S";
                            statementType = "M";
                            productInfo = "";
                            productRef = "";
                            /***********Modified by Jacky on 2018-01-07*************/
                            //purpose : Jessica needs stmtType checking to avoid duplicate record
                            stmtType = arrColumns[8].Trim();
                            /*****************************************************/

                            fileStatementDate = statementDate;

                            /*******************************Modified by Jacky on 2018-12-19********************************/
                            //purpose : infoarchive do not have pdf file path anymore
                            StatementRemotePath = Path.Combine(statementRootPath, statementRelativePath, statementFilename);
                            /*if (!File.Exists(StatementRemotePath))
                            {
                                numOfStatementNotFound++;
                                Util.Log(logType.Warning, String.Format("Statement file not exist:{0}. IndexFile:{1}. Line:{2}", StatementRemotePath, indexFilePath, line));
                                continue;
                            }*/
                            /**************************************************************************************************/
                            break;
                    }

                    #endregion

                    List<tbAccountListSnapshot> snapshotAccountList = new List<tbAccountListSnapshot>();
                    snapshotAccountList.Clear();
                    List<tbAccountList> accountList = new List<tbAccountList>();
                    accountList.Clear();

                    switch (statementCategory)
                    {
                        case StatementCategory.Bank:
                            /*********Modified by Jacky on 2019-01-02********************************/
                            //purpose : The Bank and credit card insert condition should same as InfoCast now
                            //accountList = entity.tbAccountListSnapshot.Where(a => a.AccountNumber == accountNumber && a.Active.Equals("Y") && a.SnapshotDate.Equals(snapshotDate)).ToList();
                            /*{
                                //accountList = entity.ExecuteStoreQuery<tbAccountListSnapshot>(
                                //    String.Format("SELECT * FROM [dbo].[tbAccountListSnapshot] Where AccountNumber='{0}' and Active ='Y' and SNAPSHOTDATE = (select MAX (SNAPSHOTDATE) from [dbo].[tbAccountListSnapshot] where SNAPSHOTDATE <= '{1}')", accountNumber, snapshotDate.ToString("yyyy-MM-dd"))
                                //    ).ToList();
                                snapshotAccountList = entity.ExecuteStoreQuery<tbAccountListSnapshot>(
                                    String.Format("SELECT * FROM [dbo].[tbAccountListSnapshot] Where AccountNumber='{0}' and (Category='{1}' or Category='{2}') and SNAPSHOTDATE = (select MAX (SNAPSHOTDATE) from [dbo].[tbAccountListSnapshot] where SNAPSHOTDATE <= '{3}')", accountNumber, Properties.Settings.Default.CategoryCombine, Properties.Settings.Default.CategoryIndividual, snapshotDate.ToString("yyyy-MM-dd"))
                                    ).ToList();
                            }*/
                            /*{
                                accountList = entity.ExecuteStoreQuery<tbAccountList>(
                                    String.Format("SELECT * FROM [dbo].[tbAccountList] Where AccountNumber='{0}' and (Category='{1}' or Category='{2}')", accountNumber, Properties.Settings.Default.CategoryCombine, Properties.Settings.Default.CategoryIndividual)
                                    ).ToList();

                                Util.writeToLogfile(">> check SQL : " + String.Format("SELECT * FROM [dbo].[tbAccountList] Where AccountNumber='{0}' and (Category='{1}' or Category='{2}')", accountNumber, Properties.Settings.Default.CategoryCombine, Properties.Settings.Default.CategoryIndividual));
                            }*/
                            /*******************************************************************/
                            /****************************Modified by Jacky on 2019-01-07********************************/
                            //purpose : Statementtype checking to avoid duplicate account Number
                            {
                                accountList = entity.ExecuteStoreQuery<tbAccountList>(
                                    String.Format("SELECT * FROM [dbo].[tbAccountList] Where AccountNumber='{0}' and (Category='{1}' or Category='{2}') and StatementType='{3}'", accountNumber, Properties.Settings.Default.CategoryCombine, Properties.Settings.Default.CategoryIndividual, stmtType)
                                    ).ToList();

                                Util.writeToLogfile(">> check SQL : " + String.Format("SELECT * FROM [dbo].[tbAccountList] Where AccountNumber='{0}' and (Category='{1}' or Category='{2}') and StatementType='{3}'", accountNumber, Properties.Settings.Default.CategoryCombine, Properties.Settings.Default.CategoryIndividual, stmtType));
                            }
                            /***********************************************************************************************/
                            break;
                        case StatementCategory.CiticFirst:
                            //accountList = entity.tbAccountListSnapshot.Where(a => a.RMID == rmid && a.Category == Properties.Settings.Default.CategoryCiticFirst && a.Active.Equals("Y") && a.SnapshotDate.Equals(snapshotDate)).ToList();
                            {
                                //accountList = entity.ExecuteStoreQuery<tbAccountListSnapshot>(
                                //    String.Format("SELECT * FROM [dbo].[tbAccountListSnapshot] Where RMID='{0}' and Category='{1}' and Active ='Y' and SNAPSHOTDATE = (select MAX (SNAPSHOTDATE) from [dbo].[tbAccountListSnapshot] where SNAPSHOTDATE <= '{2}')", rmid, Properties.Settings.Default.CategoryCiticFirst, snapshotDate.ToString("yyyy-MM-dd"))
                                //    ).ToList();
                                snapshotAccountList = entity.ExecuteStoreQuery<tbAccountListSnapshot>(
                                    String.Format("SELECT * FROM [dbo].[tbAccountListSnapshot] Where RMID='{0}' and Category='{1}' and SNAPSHOTDATE = (select MAX (SNAPSHOTDATE) from [dbo].[tbAccountListSnapshot] where SNAPSHOTDATE <= '{2}')", rmid, Properties.Settings.Default.CategoryCiticFirst, snapshotDate.ToString("yyyy-MM-dd"))
                                    ).ToList();
                            }
                            break;
                        case StatementCategory.CreditCard:
                            /*********Modified by Jacky on 2019-01-02********************************/
                            //purpose : The Bank and credit card insert condition should same as InfoCast now
                            /*//accountList = entity.tbAccountListSnapshot.Where(a => a.AccountNumber == accountNumber && a.Active.Equals("Y") && a.SnapshotDate.Equals(snapshotDate)).ToList();
                            {
                                //accountList = entity.ExecuteStoreQuery<tbAccountListSnapshot>(
                                //     String.Format("SELECT * FROM [dbo].[tbAccountListSnapshot] Where AccountNumber='{0}' and Active ='Y' and SNAPSHOTDATE = (select MAX (SNAPSHOTDATE) from [dbo].[tbAccountListSnapshot] where SNAPSHOTDATE <= '{1}')", accountNumber, snapshotDate.ToString("yyyy-MM-dd"))
                                //     ).ToList();

                                snapshotAccountList = entity.ExecuteStoreQuery<tbAccountListSnapshot>(
                                     String.Format("SELECT * FROM [dbo].[tbAccountListSnapshot] Where AccountNumber='{0}' and Category='{1}' and SNAPSHOTDATE = (select MAX (SNAPSHOTDATE) from [dbo].[tbAccountListSnapshot] where SNAPSHOTDATE <= '{2}')", accountNumber, Properties.Settings.Default.CategoryCreditCard, snapshotDate.ToString("yyyy-MM-dd"))
                                     ).ToList();
                            }*/
                            /*{
                                accountList = entity.ExecuteStoreQuery<tbAccountList>(
                                    String.Format("SELECT * FROM [dbo].[tbAccountList] Where AccountNumber='{0}' and Category='{1}'", accountNumber, Properties.Settings.Default.CategoryCreditCard)
                                    ).ToList();

                                Util.writeToLogfile(">> check SQL : " + String.Format("SELECT * FROM [dbo].[tbAccountList] Where AccountNumber='{0}' and Category='{1}'", accountNumber, Properties.Settings.Default.CategoryCreditCard));
                            }*/
                            /**************************************************************************************************/
                            /****************************Modified by Jacky on 2019-01-07********************************/
                            //purpose : statementtype checking to avoid duplicate account Number
                            {
                                accountList = entity.ExecuteStoreQuery<tbAccountList>(
                                    String.Format("SELECT * FROM [dbo].[tbAccountList] Where AccountNumber='{0}' and Category='{1}' and StatementType = '{2}'", accountNumber, Properties.Settings.Default.CategoryCreditCard, stmtType)
                                    ).ToList();

                                Util.writeToLogfile(">> check SQL : " + String.Format("SELECT * FROM [dbo].[tbAccountList] Where AccountNumber='{0}' and Category='{1}' and StatementType = '{2}'", accountNumber, Properties.Settings.Default.CategoryCreditCard, stmtType));
                            }
                            /******************************************************************************************/
                            break;
                        case StatementCategory.InfoCast:
                            {
                                accountList = entity.ExecuteStoreQuery<tbAccountList>(
                                    String.Format("SELECT * FROM [dbo].[tbAccountList] Where AccountNumber='{0}' and Category='{1}'", accountNumber, Properties.Settings.Default.CategoryInfoCast)
                                    ).ToList();

                                Util.writeToLogfile(">> check SQL : " + String.Format("SELECT * FROM [dbo].[tbAccountList] Where AccountNumber='{0}' and Category='{1}'", accountNumber, Properties.Settings.Default.CategoryInfoCast));
                            }
                            break;
                        default:
                            break;
                    }

                    if ((statementCategory == StatementCategory.InfoCast) && accountList.Count > 0)
                    {
                        // InfoCast
                        String StatementLocalPath = statementFilename;

                        foreach (tbAccountList account in accountList)
                        {
                            DateTime now = DateTime.Now;
                            //if (account.RegistrationStatus.Equals("S") && account.Active.Equals("Y"))
                            //{
                            tbStatement record = new tbStatement();
                            record.AccountID = account.ID;
                            record.StatementDate = statementDate;
                            record.StatementPath = StatementLocalPath;
                            record.HeaderPath = " ";
                            record.TermsAndConditionPath = " ";
                            record.HeaderPositionX = 0;
                            record.HeaderPositionY = 0;
                            record.CreateDate = now;
                            record.CreateBy = Properties.Settings.Default.AppName;
                            record.ModifyBy = Properties.Settings.Default.AppName;
                            record.LastUpdate = now;
                            record.statementFreq = statementType;
                            record.productInfo = productInfo;
                            record.productRef = productRef;

                            entity.AddTotbStatement(record);
                            //}


                            tbStatementFullList recordFullList = new tbStatementFullList();
                            recordFullList.StatementDate = statementDate;
                            recordFullList.StatementPath = StatementLocalPath;

                            recordFullList.HeaderPath = " ";
                            recordFullList.HeaderPositionX = 0;
                            recordFullList.HeaderPositionY = 0;
                            recordFullList.TermsAndConditionPath = " ";


                            recordFullList.CreateBy = Properties.Settings.Default.AppName;
                            recordFullList.ModifyBy = Properties.Settings.Default.AppName;
                            recordFullList.CreateDate = now;
                            recordFullList.LastUpdate = now;
                            recordFullList.AccountID = account.ID;
                            recordFullList.statementFreq = statementType;
                            recordFullList.productInfo = productInfo;
                            recordFullList.productRef = productRef;

                            entity.AddTotbStatementFullList(recordFullList);
                            numOfImport++;


                        }

                    }
                    #region Bank & credit card insert into tbStatement and tbStatementFullList
                    /*****************Modified by Jacky on 2019-01-02******************/
                    //purpose : The Bank and credit card insert condition should same as InfoCast now
                    else if (((statementCategory == StatementCategory.Bank) || (statementCategory == StatementCategory.CreditCard)) && accountList.Count > 0) {
                        String StatementLocalPath = "";
                        switch (statementCategory)
                        {                            
                            default:
                                //StatementLocalPath = Path.Combine(Properties.Settings.Default.StatementRootPathLocal, category, String.Format("{0}_{1}_{2}{3}", String.IsNullOrEmpty(rmid) ? accountNumber : replaceSpecialChar(rmid), DateTime.Now.ToString("yyyyMMddHHmmssffff"), rand.Next(10000, 99999).ToString(), Path.GetExtension(StatementRemotePath)));
                                StatementLocalPath = Path.Combine(Properties.Settings.Default.StatementRootPathLocal, Program.foldertimeStamp, category, String.Format("{0}_{1}_{2}{3}", String.IsNullOrEmpty(rmid) ? accountNumber : replaceSpecialChar(rmid), DateTime.Now.ToString("yyyyMMddHHmmssffff"), rand.Next(10000, 99999).ToString(), Path.GetExtension(StatementRemotePath)));
                                Directory.CreateDirectory(Path.Combine(Properties.Settings.Default.StatementRootPathLocal, Program.foldertimeStamp, category));

                                #region move statement from remote folder to local folder
                                if (File.Exists(StatementLocalPath))
                                {
                                    try
                                    {
                                        File.Delete(StatementLocalPath);
                                    }
                                    catch (Exception ex)
                                    {
                                        Util.Log(logType.Warning, String.Format("Cannot delete statement:{0}. Error message:{1}.", StatementLocalPath, ex.Message));
                                    }
                                }
                                #endregion

                                break;
                        }

                        String tcPath = null;
                        var headerSetting = new tbSystemSetting();

                        foreach (tbAccountList account in accountList)
                        {
                            DateTime now = DateTime.Now;
                            switch (statementCategory)
                            {
                                default:
                                    #region get header data
                                    headerSetting = SystemSettingList.Where(a => a.SettingName == "Header" && a.Category == account.Category && a.StatementType == account.StatementType && a.EffectiveFrom <= now && a.EffectiveTo >= now).FirstOrDefault();
                                    if (headerSetting == null)
                                    {
                                        Util.Log(logType.Warning, String.Format("Error in StatementImport. Header Setting cannot found. IndexFile:{0}. Line:{1}", indexFilePath, line));
                                        continue;
                                    }
                                    #endregion

                                    #region get termsAndCondition data
                                    tcPath = null;
                                    if ((!account.Category.Equals(StatementCategory.CiticFirst)) && (!account.Category.Equals(StatementCategory.InfoCast)))
                                    {
                                        var tcSetting = SystemSettingList.Where(a => a.SettingName == "TermsAndCondition" && a.Category == account.Category && a.StatementType == account.StatementType && a.EffectiveFrom <= now && a.EffectiveTo >= now).FirstOrDefault();
                                        if (tcSetting == null)
                                        {
                                            Util.Log(logType.Warning, String.Format("Error in StatementImport. TermsAndCondition Setting cannot found. IndexFile:{0}. Line:{1}", indexFilePath, line));
                                            continue;
                                        }
                                        else
                                        {
                                            tcPath = tcSetting.SettingValue;
                                        }
                                    }
                                    #endregion

                                    #region insert statement record                                    
                                        tbStatement record = new tbStatement();
                                        record.StatementDate = statementDate;
                                        switch (statementCategory)
                                        {
                                            case StatementCategory.Bank:
                                            case StatementCategory.CreditCard:
                                                {
                                                    record.StatementPath = InfoArchiveFileName + StatementLocalPath;
                                                }
                                                break;
                                            default:
                                                record.StatementPath = StatementLocalPath;
                                                break;
                                        }
                                        record.HeaderPath = headerSetting.SettingValue;
                                        record.HeaderPositionX = headerSetting.HeaderPositionX.HasValue ? headerSetting.HeaderPositionX.Value : 0;
                                        record.HeaderPositionY = headerSetting.HeaderPositionY.HasValue ? headerSetting.HeaderPositionY.Value : 0;
                                        record.TermsAndConditionPath = tcPath;
                                        record.CreateBy = Properties.Settings.Default.AppName;
                                        record.ModifyBy = Properties.Settings.Default.AppName;
                                        record.CreateDate = now;
                                        record.LastUpdate = now;
                                        record.AccountID = account.ID;
                                        record.statementFreq = statementType;
                                        record.productInfo = productInfo;
                                        record.productRef = productRef;
                                        entity.AddTotbStatement(record);
                                    #endregion
                                    break;
                            }

                            tbStatementFullList recordFullList = new tbStatementFullList();
                            recordFullList.StatementDate = statementDate;
                            recordFullList.StatementPath = StatementLocalPath;
                            recordFullList.HeaderPath = " ";
                            recordFullList.HeaderPositionX = 0;
                            recordFullList.HeaderPositionY = 0;
                            recordFullList.TermsAndConditionPath = " ";
                            recordFullList.CreateBy = Properties.Settings.Default.AppName;
                            recordFullList.ModifyBy = Properties.Settings.Default.AppName;
                            recordFullList.CreateDate = now;
                            recordFullList.LastUpdate = now;
                            recordFullList.AccountID = account.ID;
                            recordFullList.statementFreq = statementType;
                            recordFullList.productInfo = productInfo;
                            recordFullList.productRef = productRef;

                            entity.AddTotbStatementFullList(recordFullList);
                            numOfImport++;
                        }
                    }
                    /*********************************************************************************/
                    #endregion
                    else if (snapshotAccountList.Count > 0)
                    {
                        String StatementLocalPath = "";
                        switch (statementCategory)
                        {
                            //case StatementCategory.InfoCast:
                            //    StatementLocalPath = statementFilename;
                            //    break;
                            default:
                                //StatementLocalPath = Path.Combine(Properties.Settings.Default.StatementRootPathLocal, category, String.Format("{0}_{1}_{2}{3}", String.IsNullOrEmpty(rmid) ? accountNumber : replaceSpecialChar(rmid), DateTime.Now.ToString("yyyyMMddHHmmssffff"), rand.Next(10000, 99999).ToString(), Path.GetExtension(StatementRemotePath)));
                                StatementLocalPath = Path.Combine(Properties.Settings.Default.StatementRootPathLocal, Program.foldertimeStamp, category, String.Format("{0}_{1}_{2}{3}", String.IsNullOrEmpty(rmid) ? accountNumber : replaceSpecialChar(rmid), DateTime.Now.ToString("yyyyMMddHHmmssffff"), rand.Next(10000, 99999).ToString(), Path.GetExtension(StatementRemotePath)));
                                Directory.CreateDirectory(Path.Combine(Properties.Settings.Default.StatementRootPathLocal, Program.foldertimeStamp, category));

                                #region move statement from remote folder to local folder
                                if (File.Exists(StatementLocalPath))
                                {
                                    try
                                    {
                                        File.Delete(StatementLocalPath);
                                    }
                                    catch (Exception ex)
                                    {
                                        Util.Log(logType.Warning, String.Format("Cannot delete statement:{0}. Error message:{1}.", StatementLocalPath, ex.Message));
                                    }
                                }
                                /*******************************Modified by Jacky on 2018-12-19********************************/
                                //purpose : infoarchive do not have pdf file path anymore
                                /*try
                                {
                                    File.Move(StatementRemotePath, StatementLocalPath);
                                }
                                catch (Exception ex)
                                {
                                    Util.Log(logType.Warning, String.Format("Cannot move statement from:{0} to {1}. Error message:{2}.", StatementRemotePath, StatementLocalPath, ex.Message));
                                }*/
                                /********************************************************************************************/
                                #endregion

                                break;
                        }

                        String tcPath = null;
                        var headerSetting = new tbSystemSetting();

                        foreach (tbAccountListSnapshot account in snapshotAccountList)
                        {
                            DateTime now = DateTime.Now;
                            switch (statementCategory)
                            {
                                //case StatementCategory.InfoCast:
                                //    if (account.RegistrationStatus.Equals("S") && account.Active.Equals("Y"))
                                //    {
                                //        tbStatement record = new tbStatement();
                                //        record.StatementDate = statementDate;
                                //        record.StatementPath = StatementLocalPath;
                                //        record.HeaderPath = " ";
                                //        record.HeaderPositionX = 0;
                                //        record.HeaderPositionY = 0;
                                //        record.TermsAndConditionPath = " ";
                                //        record.CreateBy = Properties.Settings.Default.AppName;
                                //        record.ModifyBy = Properties.Settings.Default.AppName;
                                //        record.CreateDate = now;
                                //        record.LastUpdate = now;
                                //        record.AccountID = account.AccountID;
                                //        record.statementFreq = statementType;
                                //        record.productInfo = productInfo;
                                //        record.productRef = productRef;
                                //        entity.AddTotbStatement(record);
                                //    }
                                //    break;
                                default:
                                    #region get header data
                                    headerSetting = SystemSettingList.Where(a => a.SettingName == "Header" && a.Category == account.Category && a.StatementType == account.StatementType && a.EffectiveFrom <= now && a.EffectiveTo >= now).FirstOrDefault();
                                    if (headerSetting == null)
                                    {
                                        Util.Log(logType.Warning, String.Format("Error in StatementImport. Header Setting cannot found. IndexFile:{0}. Line:{1}", indexFilePath, line));
                                        continue;
                                    }
                                    #endregion

                                    #region get termsAndCondition data
                                    tcPath = null;
                                    if ((!account.Category.Equals(StatementCategory.CiticFirst)) && (!account.Category.Equals(StatementCategory.InfoCast)))
                                    {
                                        var tcSetting = SystemSettingList.Where(a => a.SettingName == "TermsAndCondition" && a.Category == account.Category && a.StatementType == account.StatementType && a.EffectiveFrom <= now && a.EffectiveTo >= now).FirstOrDefault();
                                        if (tcSetting == null)
                                        {
                                            Util.Log(logType.Warning, String.Format("Error in StatementImport. TermsAndCondition Setting cannot found. IndexFile:{0}. Line:{1}", indexFilePath, line));
                                            continue;
                                        }
                                        else
                                        {
                                            tcPath = tcSetting.SettingValue;
                                        }
                                    }
                                    #endregion

                                    #region insert statement record                                    
                                    /*************************Modified by Jacky on 2019-01-02*************/
                                    //purpose : The condition of tbstatementImport of Bank and Credit Card should now similiar to others
                                    /*if (account.RegistrationStatus.Equals("S") && account.Active.Equals("Y"))
                                    { */
                                    if (account.Active.Equals("Y"))
                                    {
                                        /*********************************************************************/
                                        tbStatement record = new tbStatement();
                                        record.StatementDate = statementDate;
                                        switch (statementCategory)
                                        {
                                            case StatementCategory.Bank:
                                            case StatementCategory.CreditCard:
                                                {
                                                    record.StatementPath = InfoArchiveFileName + StatementLocalPath;
                                                }
                                                break;
                                            default:
                                                record.StatementPath = StatementLocalPath;
                                                break;
                                        }
                                        record.HeaderPath = headerSetting.SettingValue;
                                        record.HeaderPositionX = headerSetting.HeaderPositionX.HasValue ? headerSetting.HeaderPositionX.Value : 0;
                                        record.HeaderPositionY = headerSetting.HeaderPositionY.HasValue ? headerSetting.HeaderPositionY.Value : 0;
                                        record.TermsAndConditionPath = tcPath;
                                        record.CreateBy = Properties.Settings.Default.AppName;
                                        record.ModifyBy = Properties.Settings.Default.AppName;
                                        record.CreateDate = now;
                                        record.LastUpdate = now;
                                        record.AccountID = account.AccountID;
                                        record.statementFreq = statementType;
                                        record.productInfo = productInfo;
                                        record.productRef = productRef;
                                        entity.AddTotbStatement(record);
                                    }
                                    /******************************************************/
                                    #endregion
                                    break;
                            }

                            tbStatementFullList recordFullList = new tbStatementFullList();
                            recordFullList.StatementDate = statementDate;
                            recordFullList.StatementPath = StatementLocalPath;

                            //if ((statementCategory != StatementCategory.InfoCast) && (statementCategory != StatementCategory.SuperBank) && (statementCategory != StatementCategory.Container))
                            //{
                            //    recordFullList.HeaderPath = headerSetting.SettingValue;
                            //    recordFullList.HeaderPositionX = headerSetting.HeaderPositionX.HasValue ? headerSetting.HeaderPositionX.Value : 0;
                            //    recordFullList.HeaderPositionY = headerSetting.HeaderPositionY.HasValue ? headerSetting.HeaderPositionY.Value : 0;
                            //    recordFullList.TermsAndConditionPath = tcPath;
                            //}
                            //else
                            //{
                            recordFullList.HeaderPath = " ";
                            recordFullList.HeaderPositionX = 0;
                            recordFullList.HeaderPositionY = 0;
                            recordFullList.TermsAndConditionPath = " ";
                            //}
                            recordFullList.CreateBy = Properties.Settings.Default.AppName;
                            recordFullList.ModifyBy = Properties.Settings.Default.AppName;
                            recordFullList.CreateDate = now;
                            recordFullList.LastUpdate = now;
                            recordFullList.AccountID = account.AccountID;
                            recordFullList.statementFreq = statementType;
                            recordFullList.productInfo = productInfo;
                            recordFullList.productRef = productRef;

                            entity.AddTotbStatementFullList(recordFullList);
                            numOfImport++;
                            //}
                        }
                    }
                    else
                    {
                        numOfAccountNotFound++;
                        Util.Log(logType.Warning, String.Format("Account does not exist. IndexFile:{0}. Line:{1}", indexFilePath, line));
                    }

                    // batch update
                    if (counter++ % 1000 == 0)
                    {
                        entity.SaveChanges();
                    }
                }
                entity.SaveChanges();

            }
            else
            {
                Util.Log(logType.Error, String.Format("Index file does not exist:{0}", indexFilePath));
            }
            Util.Log(logType.Info, String.Format("(a)No. of PDF source files = {0}", numOfPDF.ToString()));
            Util.Log(logType.Info, String.Format("(b)No. of account not found = {0}", numOfAccountNotFound.ToString()));
            Util.Log(logType.Info, String.Format("(c)No. of stmt file not found = {0}", numOfStatementNotFound.ToString()));
            Util.Log(logType.Info, String.Format("(d)Index file format incorrect = {0}", numOfIncorrectFormat.ToString()));
            Util.Log(logType.Info, String.Format("(e)No. of stmt imported in statementfull list = {0}", numOfImport.ToString()));
            if (numOfImport != (numOfPDF - numOfAccountNotFound - numOfStatementNotFound - numOfIncorrectFormat))
            {
                String errorMessage = "";
                if (numOfAccountNotFound > 0) errorMessage = errorMessage + "(b)";
                if (numOfStatementNotFound > 0) errorMessage = errorMessage + "(c)";
                if (numOfIncorrectFormat > 0) errorMessage = errorMessage + "(d)";
                Util.Log(logType.Info, String.Format("ERROR: statement importer with error {0}", errorMessage));
            }

            //Newly added ExecuteLogTable
            try
            {
                Util.ExecuteLog(indexFilePath,
                statementCategory.ToString(),
                fileStatementDate,
                numOfPDF,
                numOfAccountNotFound,
                numOfStatementNotFound,
                numOfIncorrectFormat,
                numOfImport);
            }
            catch (Exception e)
            {

            }

            Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END processing Indexfile : {1}", DateTime.Now, Path.GetFileName(indexFilePath)));

        }
        private static void SuperBankAndContainerStatementImport(String indexFilePath, String statementRootPath, StatementCategory statementCategory)
        {
            Util.Log(logType.Info, String.Format("{0} File : {1}, import start", DateTime.Now.ToString("tt hh:mm:ss"), Path.GetFileName(indexFilePath)));
            try
            {
                DateTime hearderDate;
                DateTime now = DateTime.Now;
                List<String> lines = new List<String>();
                String eachLine = String.Empty;
                int totalFiles = 0;
                Int32 numOfPDF = 0;
                Int32 numOfAccountNotFound = 0;
                Int32 numOfIncorrectFormat = 0;
                Int32 numOfImport = 0;

                #region read index file
                Util.Log(logType.Info, "read index file start");
                using (TextReader tr = File.OpenText(indexFilePath))
                {
                    while ((eachLine = tr.ReadLine()) != null)
                    {
                        if ((lines.Count == 0) && (eachLine.Contains(";")))
                        {
                            // header record
                            string format = "yyyyMMdd";
                            if (!DateTime.TryParseExact(eachLine.Substring(0, 8), format, CultureInfo.InvariantCulture,
                                    DateTimeStyles.None, out hearderDate))
                            {
                                Util.Log(logType.Error, String.Format("Error in read index file, invalid date in header line. IndexFile:{0}.", indexFilePath));
                                return;
                            }

                            continue;
                        }
                        else if (eachLine.Contains("|"))
                        {
                            numOfPDF++;
                            lines.Add(eachLine);
                        }
                        else if ((eachLine.StartsWith("0")) && (eachLine.Contains(";")))
                        {
                            // trailer record
                            try
                            {
                                totalFiles = Convert.ToInt32(eachLine.Split(';')[0]);
                            }
                            catch (Exception ex)
                            {
                                Util.Log(logType.Error, String.Format("Error in read index file, invalid Trailer count. IndexFile:{0}.", indexFilePath));
                                return;
                            }
                            continue;
                        }
                    }
                    tr.Close();

                    if (totalFiles != numOfPDF)
                    {
                        Util.Log(logType.Error, String.Format("Error in read index file, trailer count not match with actual numbers of data. IndexFile:{0}.", indexFilePath));
                        return;
                    }
                }

                DataTable tempDataTable = new DataTable();
                tempDataTable.Columns.Add("StatementDate");
                tempDataTable.Columns.Add("StatementPath");
                tempDataTable.Columns.Add("HeaderPath");
                tempDataTable.Columns.Add("TermsAndConditionPath");
                tempDataTable.Columns.Add("HeaderPositionX");
                tempDataTable.Columns.Add("HeaderPositionY");
                tempDataTable.Columns.Add("CreateDate");
                tempDataTable.Columns.Add("CreateBy");
                tempDataTable.Columns.Add("ModifyBy");
                tempDataTable.Columns.Add("LastUpdate");
                tempDataTable.Columns.Add("statementFreq");
                tempDataTable.Columns.Add("productInfo");
                tempDataTable.Columns.Add("productRef");
                tempDataTable.Columns.Add("bpid");
                tempDataTable.Columns.Add("containerAcno");

                DateTime StatementDate;
                int lineCounter = 1;
                foreach (var line in lines)
                {
                    lineCounter++;
                    // check if enough column in each line
                    if (line.Split('|').Count() < Convert.ToInt32(Properties.Settings.Default.StatementFileColumnCount))
                    {
                        numOfIncorrectFormat++;
                        Util.Log(logType.Warning, String.Format("Not enough field at Line {0} : \n{1}", lineCounter, line));
                        continue;
                    }
                    // check if correct StatementDate format in each line
                    if (!DateTime.TryParseExact(line.Split('|')[(int)statementGetDataColumn.stmtDate].Substring(0, 8), "yyyyMMdd", CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out StatementDate))
                    {
                        numOfIncorrectFormat++;
                        Util.Log(logType.Warning, String.Format("Invalid statementDate at Line {0} : {1}", lineCounter, line.Split('|')[(int)statementGetDataColumn.stmtDate].Substring(0, 8)));
                        continue;
                    }

                    DataRow row = tempDataTable.NewRow();
                    row["StatementDate"] = StatementDate;
                    row["StatementPath"] = line.Split('|')[(int)statementGetDataColumn.pdfPath];
                    row["HeaderPath"] = " ";
                    row["TermsAndConditionPath"] = " ";
                    row["HeaderPositionX"] = 0;
                    row["HeaderPositionY"] = 0;
                    row["CreateDate"] = now;
                    row["CreateBy"] = Properties.Settings.Default.AppName;
                    row["ModifyBy"] = Properties.Settings.Default.AppName;
                    row["LastUpdate"] = now;
                    row["statementFreq"] = "M";
                    row["productInfo"] = line.Split('|')[(int)statementGetDataColumn.productInfo];
                    row["productRef"] = line.Split('|')[(int)statementGetDataColumn.productRef];
                    row["bpid"] = line.Split('|')[(int)statementGetDataColumn.bpid];
                    row["containerAcno"] = line.Split('|')[(int)statementGetDataColumn.containerAcno];

                    tempDataTable.Rows.Add(row);
                }
                Util.Log(logType.Info, "read index file finish");
                #endregion

                Util.Log(logType.Info, String.Format("Truncate {0} start", Properties.Settings.Default.tbStatementTempTableName));
                entity.ExecuteStoreCommand(String.Format("TRUNCATE TABLE {0}", Properties.Settings.Default.tbStatementTempTableName));
                Util.Log(logType.Info, String.Format("Truncate {0} finish", Properties.Settings.Default.tbStatementTempTableName));

                // use bulk insert
                Util.Log(logType.Info, String.Format("Insert record into {0} start", Properties.Settings.Default.tbStatementTempTableName));
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                {
                    connection.Open();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    {
                        bulkCopy.DestinationTableName = Properties.Settings.Default.tbStatementTempTableName;
                        bulkCopy.BatchSize = 10000;
                        bulkCopy.BulkCopyTimeout = Properties.Settings.Default.BulkInsertTimeout;

                        SqlBulkCopyColumnMapping map_StatementDate = new SqlBulkCopyColumnMapping("StatementDate", "StatementDate");
                        bulkCopy.ColumnMappings.Add(map_StatementDate);
                        SqlBulkCopyColumnMapping map_StatementPath = new SqlBulkCopyColumnMapping("StatementPath", "StatementPath");
                        bulkCopy.ColumnMappings.Add(map_StatementPath);
                        SqlBulkCopyColumnMapping map_HeaderPath = new SqlBulkCopyColumnMapping("HeaderPath", "HeaderPath");
                        bulkCopy.ColumnMappings.Add(map_HeaderPath);
                        SqlBulkCopyColumnMapping map_TermsAndConditionPath = new SqlBulkCopyColumnMapping("TermsAndConditionPath", "TermsAndConditionPath");
                        bulkCopy.ColumnMappings.Add(map_TermsAndConditionPath);
                        SqlBulkCopyColumnMapping map_HeaderPositionX = new SqlBulkCopyColumnMapping("HeaderPositionX", "HeaderPositionX");
                        bulkCopy.ColumnMappings.Add(map_HeaderPositionX);
                        SqlBulkCopyColumnMapping map_HeaderPositionY = new SqlBulkCopyColumnMapping("HeaderPositionY", "HeaderPositionY");
                        bulkCopy.ColumnMappings.Add(map_HeaderPositionY);
                        SqlBulkCopyColumnMapping map_CreateDate = new SqlBulkCopyColumnMapping("CreateDate", "CreateDate");
                        bulkCopy.ColumnMappings.Add(map_CreateDate);
                        SqlBulkCopyColumnMapping map_CreateBy = new SqlBulkCopyColumnMapping("CreateBy", "CreateBy");
                        bulkCopy.ColumnMappings.Add(map_CreateBy);
                        SqlBulkCopyColumnMapping map_ModifyBy = new SqlBulkCopyColumnMapping("ModifyBy", "ModifyBy");
                        bulkCopy.ColumnMappings.Add(map_ModifyBy);
                        SqlBulkCopyColumnMapping map_LastUpdate = new SqlBulkCopyColumnMapping("LastUpdate", "LastUpdate");
                        bulkCopy.ColumnMappings.Add(map_LastUpdate);
                        SqlBulkCopyColumnMapping map_statementFreq = new SqlBulkCopyColumnMapping("statementFreq", "statementFreq");
                        bulkCopy.ColumnMappings.Add(map_statementFreq);
                        SqlBulkCopyColumnMapping map_productInfo = new SqlBulkCopyColumnMapping("productInfo", "productInfo");
                        bulkCopy.ColumnMappings.Add(map_productInfo);
                        SqlBulkCopyColumnMapping map_productRef = new SqlBulkCopyColumnMapping("productRef", "productRef");
                        bulkCopy.ColumnMappings.Add(map_productRef);
                        switch (statementCategory)
                        {
                            case StatementCategory.SuperBank:
                                {
                                    SqlBulkCopyColumnMapping map_bpid = new SqlBulkCopyColumnMapping("bpid", "bpid");
                                    bulkCopy.ColumnMappings.Add(map_bpid);
                                }
                                break;
                            case StatementCategory.Container:
                                {
                                    SqlBulkCopyColumnMapping map_containerAcno = new SqlBulkCopyColumnMapping("containerAcno", "containerAcno");
                                    bulkCopy.ColumnMappings.Add(map_containerAcno);
                                }
                                break;
                            default:
                                break;
                        }

                        bulkCopy.WriteToServer(tempDataTable);
                    }
                    connection.Close();
                }
                tempDataTable.Clear();
                Util.Log(logType.Info, String.Format("Insert record into {0} finish", Properties.Settings.Default.tbStatementTempTableName));


                Util.Log(logType.Info, String.Format("Call store pro. {0} to update table tbStatement and tbStatementFullList start", Properties.Settings.Default.spUpdatetbStatementAndtbStatementFullList));
                entity.ExecuteStoreCommand(String.Format("EXECUTE {0}", Properties.Settings.Default.spUpdatetbStatementAndtbStatementFullList));
                Util.Log(logType.Info, String.Format("Call store pro. {0} to update table tbStatement and tbStatementFullList finish", Properties.Settings.Default.spUpdatetbStatementAndtbStatementFullList));

                numOfImport = entity.tbStatementTemp.Where(a => a.AccountID != null).Count();
                numOfAccountNotFound = entity.tbStatementTemp.Where(a => a.AccountID == null).Count();

                Util.Log(logType.Info, String.Format("{0} File : {1}, import finish", DateTime.Now.ToString("tt hh:mm:ss"), Path.GetFileName(indexFilePath)));
                Util.Log(logType.Info, String.Format("(a)No. of PDF source files = {0}", numOfPDF.ToString()));
                Util.Log(logType.Info, String.Format("(b)No. of account not found = {0}", numOfAccountNotFound.ToString()));
                Util.Log(logType.Info, String.Format("(c)No. of stmt file not found = {0}", "0"));
                Util.Log(logType.Info, String.Format("(d)Index file format incorrect = {0}", numOfIncorrectFormat.ToString()));
                Util.Log(logType.Info, String.Format("(e)No. of stmt imported in statementfull list = {0}", numOfImport.ToString()));
                if (numOfImport != (numOfPDF - numOfAccountNotFound - numOfIncorrectFormat))
                {
                    String errorMessage = "";
                    if (numOfAccountNotFound > 0) errorMessage = errorMessage + "(b)";
                    if (numOfIncorrectFormat > 0) errorMessage = errorMessage + "(d)";
                    Util.Log(logType.Info, String.Format("ERROR: statement importer with error {0}", errorMessage));
                }
            }
            catch (Exception ex)
            {
                Util.Log(logType.Info, String.Format("Error in File {0} import : {1}", Path.GetFileName(indexFilePath), ex.Message));
            }
        }
        private static void adviceImport(String indexFilePath, String statementRootPath, StatementCategory statementCategory)
        {
            Util.Log(logType.Info, String.Format("{0} File : {1}, import start", DateTime.Now.ToString("tt hh:mm:ss"), Path.GetFileName(indexFilePath)));
            try
            {
                DateTime now = DateTime.Now;
                List<String> lines = new List<String>();
                List<Util.advice> advices = new List<Util.advice>();
                int totalFiles = 0;
                Int32 numOfPDF = 0;
                Int32 numOfAccountNotFound = 0;
                Int32 numOfIncorrectFormat = 0;
                Int32 numOfImport = 0;

                #region read index file
                Util.Log(logType.Info, "read index file start");
                using (TextReader tr = File.OpenText(indexFilePath))
                {
                    String eachLine = String.Empty;

                    while ((eachLine = tr.ReadLine()) != null)
                    {
                        if (eachLine.StartsWith("H"))
                        {
                            continue;
                        }
                        else if (eachLine.StartsWith("AVALOQ"))
                        {
                            numOfPDF++;
                            lines.Add(eachLine);
                        }
                        else if ((eachLine.StartsWith("0")) && (eachLine.Contains(";")))
                        {
                            // trailer record
                            try
                            {
                                totalFiles = Convert.ToInt32(eachLine.Split(';')[0]);
                            }
                            catch (Exception ex)
                            {
                                Util.Log(logType.Error, String.Format("Error in read index file, invalid Trailer count. IndexFile:{0}.", indexFilePath));
                                return;
                            }
                            continue;
                        }
                    }
                    tr.Close();

                    if (totalFiles != numOfPDF)
                    {
                        Util.Log(logType.Error, String.Format("Error in read index file, trailer count not match with actual numbers of data. IndexFile:{0}.", indexFilePath));
                        return;
                    }
                }

                DataTable tempDataTable = new DataTable();
                tempDataTable.Columns.Add("StatementDate");
                tempDataTable.Columns.Add("StatementPath");
                tempDataTable.Columns.Add("HeaderPath");
                tempDataTable.Columns.Add("TermsAndConditionPath");
                tempDataTable.Columns.Add("HeaderPositionX");
                tempDataTable.Columns.Add("HeaderPositionY");
                tempDataTable.Columns.Add("CreateDate");
                tempDataTable.Columns.Add("CreateBy");
                tempDataTable.Columns.Add("ModifyBy");
                tempDataTable.Columns.Add("LastUpdate");
                tempDataTable.Columns.Add("statementFreq");
                tempDataTable.Columns.Add("productInfo");
                tempDataTable.Columns.Add("productRef");
                tempDataTable.Columns.Add("Meta_Type");
                tempDataTable.Columns.Add("Order_Type");
                tempDataTable.Columns.Add("Asset_Grp");
                tempDataTable.Columns.Add("Asset_SubGrp");
                tempDataTable.Columns.Add("Asset_Name_EN");
                tempDataTable.Columns.Add("Asset_Name_SC");
                tempDataTable.Columns.Add("Asset_Name_TC");
                tempDataTable.Columns.Add("Product_Type");
                tempDataTable.Columns.Add("Product_Name");
                tempDataTable.Columns.Add("Advice_Title");
                tempDataTable.Columns.Add("containerAcno");

                DateTime StatementDate;
                int lineCounter = 1;
                foreach (var line in lines)
                {
                    lineCounter++;
                    // check if correct StatementDate format in each line
                    if (!DateTime.TryParseExact(line.Split(';')[(int)adviceGetDataColumn.stmtDate], "dd-MMM-yyyy", CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out StatementDate))
                    {
                        numOfIncorrectFormat++;
                        Util.Log(logType.Warning, String.Format("Invalid statementDate at Line {0} : {1}", lineCounter, line.Split(';')[(int)adviceGetDataColumn.stmtDate].Substring(0, 8)));
                        continue;
                    }
                    String metaType = line.Split(';')[(int)adviceGetDataColumn.metaType];
                    String orderType = line.Split(';')[(int)adviceGetDataColumn.orderType];
                    String assGroup = line.Split(';')[(int)adviceGetDataColumn.assGroup];
                    String assSubGroup = line.Split(';')[(int)adviceGetDataColumn.assSubGroup];
                    String assNameEN = line.Split(';')[(int)adviceGetDataColumn.assNameEN];

                    DataRow row = tempDataTable.NewRow();
                    row["StatementDate"] = StatementDate;
                    row["StatementPath"] = line.Split(';')[(int)adviceGetDataColumn.pdfPath];
                    row["HeaderPath"] = " ";
                    row["TermsAndConditionPath"] = " ";
                    row["HeaderPositionX"] = 0;
                    row["HeaderPositionY"] = 0;
                    row["CreateDate"] = now;
                    row["CreateBy"] = Properties.Settings.Default.AppName;
                    row["ModifyBy"] = Properties.Settings.Default.AppName;
                    row["LastUpdate"] = now;
                    row["statementFreq"] = line.Split(';')[(int)adviceGetDataColumn.stmtFreq];
                    row["productInfo"] = "";
                    row["productRef"] = line.Split(';')[(int)adviceGetDataColumn.orderID]; ;
                    row["Meta_Type"] = metaType;
                    row["Order_Type"] = orderType;
                    row["Asset_Grp"] = assGroup;
                    row["Asset_SubGrp"] = assSubGroup;
                    row["Asset_Name_EN"] = assNameEN;
                    row["Asset_Name_SC"] = line.Split(';')[(int)adviceGetDataColumn.assNameSC];
                    row["Asset_Name_TC"] = line.Split(';')[(int)adviceGetDataColumn.assNameTC];
                    row["Product_Type"] = Mapper.getProductType(metaType, orderType, assGroup, assSubGroup);
                    row["Product_Name"] = Mapper.getProductName(metaType, orderType, assGroup, assSubGroup, assNameEN);
                    row["Advice_Title"] = Mapper.getAdviceTitle(metaType, orderType, assGroup, assSubGroup);
                    row["containerAcno"] = line.Split(';')[(int)adviceGetDataColumn.acNo];

                    tempDataTable.Rows.Add(row);
                }
                Util.Log(logType.Info, "read index file finish");
                #endregion

                Util.Log(logType.Info, String.Format("Truncate {0} start", Properties.Settings.Default.tbStatementTempTableName));
                entity.ExecuteStoreCommand(String.Format("TRUNCATE TABLE {0}", Properties.Settings.Default.tbStatementTempTableName));
                Util.Log(logType.Info, String.Format("Truncate {0} finish", Properties.Settings.Default.tbStatementTempTableName));

                // use bulk insert
                Util.Log(logType.Info, String.Format("Insert record into {0} start", Properties.Settings.Default.tbStatementTempTableName));
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                {
                    connection.Open();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    {
                        bulkCopy.DestinationTableName = Properties.Settings.Default.tbStatementTempTableName;
                        bulkCopy.BatchSize = 10000;
                        bulkCopy.BulkCopyTimeout = Properties.Settings.Default.BulkInsertTimeout;

                        SqlBulkCopyColumnMapping map_StatementDate = new SqlBulkCopyColumnMapping("StatementDate", "StatementDate");
                        bulkCopy.ColumnMappings.Add(map_StatementDate);
                        SqlBulkCopyColumnMapping map_StatementPath = new SqlBulkCopyColumnMapping("StatementPath", "StatementPath");
                        bulkCopy.ColumnMappings.Add(map_StatementPath);
                        SqlBulkCopyColumnMapping map_HeaderPath = new SqlBulkCopyColumnMapping("HeaderPath", "HeaderPath");
                        bulkCopy.ColumnMappings.Add(map_HeaderPath);
                        SqlBulkCopyColumnMapping map_TermsAndConditionPath = new SqlBulkCopyColumnMapping("TermsAndConditionPath", "TermsAndConditionPath");
                        bulkCopy.ColumnMappings.Add(map_TermsAndConditionPath);
                        SqlBulkCopyColumnMapping map_HeaderPositionX = new SqlBulkCopyColumnMapping("HeaderPositionX", "HeaderPositionX");
                        bulkCopy.ColumnMappings.Add(map_HeaderPositionX);
                        SqlBulkCopyColumnMapping map_HeaderPositionY = new SqlBulkCopyColumnMapping("HeaderPositionY", "HeaderPositionY");
                        bulkCopy.ColumnMappings.Add(map_HeaderPositionY);
                        SqlBulkCopyColumnMapping map_CreateDate = new SqlBulkCopyColumnMapping("CreateDate", "CreateDate");
                        bulkCopy.ColumnMappings.Add(map_CreateDate);
                        SqlBulkCopyColumnMapping map_CreateBy = new SqlBulkCopyColumnMapping("CreateBy", "CreateBy");
                        bulkCopy.ColumnMappings.Add(map_CreateBy);
                        SqlBulkCopyColumnMapping map_ModifyBy = new SqlBulkCopyColumnMapping("ModifyBy", "ModifyBy");
                        bulkCopy.ColumnMappings.Add(map_ModifyBy);
                        SqlBulkCopyColumnMapping map_LastUpdate = new SqlBulkCopyColumnMapping("LastUpdate", "LastUpdate");
                        bulkCopy.ColumnMappings.Add(map_LastUpdate);
                        SqlBulkCopyColumnMapping map_statementFreq = new SqlBulkCopyColumnMapping("statementFreq", "statementFreq");
                        bulkCopy.ColumnMappings.Add(map_statementFreq);
                        SqlBulkCopyColumnMapping map_productInfo = new SqlBulkCopyColumnMapping("productInfo", "productInfo");
                        bulkCopy.ColumnMappings.Add(map_productInfo);
                        SqlBulkCopyColumnMapping map_productRef = new SqlBulkCopyColumnMapping("productRef", "productRef");
                        bulkCopy.ColumnMappings.Add(map_productRef);
                        SqlBulkCopyColumnMapping map_Meta_Type = new SqlBulkCopyColumnMapping("Meta_Type", "Meta_Type");
                        bulkCopy.ColumnMappings.Add(map_Meta_Type);
                        SqlBulkCopyColumnMapping map_Order_Type = new SqlBulkCopyColumnMapping("Order_Type", "Order_Type");
                        bulkCopy.ColumnMappings.Add(map_Order_Type);
                        SqlBulkCopyColumnMapping map_Asset_Grp = new SqlBulkCopyColumnMapping("Asset_Grp", "Asset_Grp");
                        bulkCopy.ColumnMappings.Add(map_Asset_Grp);
                        SqlBulkCopyColumnMapping map_Asset_SubGrp = new SqlBulkCopyColumnMapping("Asset_SubGrp", "Asset_SubGrp");
                        bulkCopy.ColumnMappings.Add(map_Asset_SubGrp);
                        SqlBulkCopyColumnMapping map_Asset_Name_EN = new SqlBulkCopyColumnMapping("Asset_Name_EN", "Asset_Name_EN");
                        bulkCopy.ColumnMappings.Add(map_Asset_Name_EN);
                        SqlBulkCopyColumnMapping map_Asset_Name_SC = new SqlBulkCopyColumnMapping("Asset_Name_SC", "Asset_Name_SC");
                        bulkCopy.ColumnMappings.Add(map_Asset_Name_SC);
                        SqlBulkCopyColumnMapping map_Asset_Name_TC = new SqlBulkCopyColumnMapping("Asset_Name_TC", "Asset_Name_TC");
                        bulkCopy.ColumnMappings.Add(map_Asset_Name_TC);
                        SqlBulkCopyColumnMapping map_Product_Type = new SqlBulkCopyColumnMapping("Product_Type", "Product_Type");
                        bulkCopy.ColumnMappings.Add(map_Product_Type);
                        SqlBulkCopyColumnMapping map_Product_Name = new SqlBulkCopyColumnMapping("Product_Name", "Product_Name");
                        bulkCopy.ColumnMappings.Add(map_Product_Name);
                        SqlBulkCopyColumnMapping map_Advice_Title = new SqlBulkCopyColumnMapping("Advice_Title", "Advice_Title");
                        bulkCopy.ColumnMappings.Add(map_Advice_Title);
                        SqlBulkCopyColumnMapping map_containerAcno = new SqlBulkCopyColumnMapping("containerAcno", "containerAcno");
                        bulkCopy.ColumnMappings.Add(map_containerAcno);

                        bulkCopy.WriteToServer(tempDataTable);
                    }
                    connection.Close();
                }
                tempDataTable.Clear();
                Util.Log(logType.Info, String.Format("Insert record into {0} finish", Properties.Settings.Default.tbStatementTempTableName));


                Util.Log(logType.Info, String.Format("Call store pro. {0} to update table tbStatement and tbStatementFullList start", Properties.Settings.Default.spUpdatetbStatementAndtbStatementFullList));
                entity.ExecuteStoreCommand(String.Format("EXECUTE {0}", Properties.Settings.Default.spUpdatetbStatementAndtbStatementFullList));
                Util.Log(logType.Info, String.Format("Call store pro. {0} to update table tbStatement and tbStatementFullList finish", Properties.Settings.Default.spUpdatetbStatementAndtbStatementFullList));

                numOfImport = entity.tbStatementTemp.Where(a => a.AccountID != null).Count();
                numOfAccountNotFound = entity.tbStatementTemp.Where(a => a.AccountID == null).Count();

                Util.Log(logType.Info, String.Format("{0} File : {1}, import finish", DateTime.Now.ToString("tt hh:mm:ss"), Path.GetFileName(indexFilePath)));
                Util.Log(logType.Info, String.Format("(a)No. of PDF source files = {0}", numOfPDF.ToString()));
                Util.Log(logType.Info, String.Format("(b)No. of account not found = {0}", numOfAccountNotFound.ToString()));
                Util.Log(logType.Info, String.Format("(c)No. of stmt file not found = {0}", "0"));
                Util.Log(logType.Info, String.Format("(d)Index file format incorrect = {0}", numOfIncorrectFormat.ToString()));
                Util.Log(logType.Info, String.Format("(e)No. of stmt imported in statementfull list = {0}", numOfImport.ToString()));
                if (numOfImport != (numOfPDF - numOfAccountNotFound - numOfIncorrectFormat))
                {
                    String errorMessage = "";
                    if (numOfAccountNotFound > 0) errorMessage = errorMessage + "(b)";
                    if (numOfIncorrectFormat > 0) errorMessage = errorMessage + "(d)";
                    Util.Log(logType.Info, String.Format("ERROR: statement importer with error {0}", errorMessage));
                }
            }
            catch (Exception ex)
            {
                Util.Log(logType.Info, String.Format("Error in File {0} import : {1}", Path.GetFileName(indexFilePath), ex.Message));
            }
        }
        private static void DirectBankStatementImport(String indexFilePath)
        {
            Util.Log(logType.Info, String.Format("{0} File : {1}, import start", DateTime.Now.ToString("tt hh:mm:ss"), Path.GetFileName(indexFilePath)));
            try
            {
                DateTime hearderDate;
                DateTime now = DateTime.Now;
                List<String> lines = new List<String>();
                String eachLine = String.Empty;
                int totalFiles = 0;
                Int32 numOfPDF = 0;
                Int32 numOfAccountNotFound = 0;
                Int32 numOfIncorrectFormat = 0;
                Int32 numOfImport = 0;
                List<tbDirectBankMapping> dbMapList = entity.tbDirectBankMapping.Where(a => a.CheckCategory == "Y").ToList();
                Dictionary<String, String> dbMapDict = new Dictionary<String, String>();
                foreach (var dbMap in dbMapList)
                {
                    if (!dbMapDict.ContainsKey(dbMap.StatementType))
                        dbMapDict.Add(dbMap.StatementType, dbMap.CheckAcno);
                }

                #region read index file
                Util.Log(logType.Info, "read index file start");
                using (TextReader tr = File.OpenText(indexFilePath))
                {
                    while ((eachLine = tr.ReadLine()) != null)
                    {
                        if ((lines.Count == 0) && (eachLine.Contains(";")))
                        {
                            // header record
                            string format = "yyyyMMdd";
                            if (!DateTime.TryParseExact(eachLine.Substring(0, 8), format, CultureInfo.InvariantCulture,
                                    DateTimeStyles.None, out hearderDate))
                            {
                                Util.Log(logType.Error, String.Format("Error in read index file, invalid date in header line. IndexFile:{0}.", indexFilePath));
                                return;
                            }

                            continue;
                        }
                        else if (eachLine.Contains("|"))
                        {
                            numOfPDF++;
                            lines.Add(eachLine);
                        }
                        else if ((eachLine.StartsWith("0")) && (eachLine.Contains(";")))
                        {
                            // trailer record
                            try
                            {
                                totalFiles = Convert.ToInt32(eachLine.Split(';')[0]);
                            }
                            catch (Exception ex)
                            {
                                Util.Log(logType.Error, String.Format("Error in read index file, invalid Trailer count. IndexFile:{0}.", indexFilePath));
                                return;
                            }
                            continue;
                        }
                    }
                    tr.Close();

                    if (totalFiles != numOfPDF)
                    {
                        Util.Log(logType.Error, String.Format("Error in read index file, trailer count not match with actual numbers of data. IndexFile:{0}.", indexFilePath));
                        return;
                    }
                }

                DataTable tempDataTable = new DataTable();
                tempDataTable.Columns.Add("StatementDate");
                tempDataTable.Columns.Add("StatementPath");
                tempDataTable.Columns.Add("HeaderPath");
                tempDataTable.Columns.Add("TermsAndConditionPath");
                tempDataTable.Columns.Add("HeaderPositionX");
                tempDataTable.Columns.Add("HeaderPositionY");
                tempDataTable.Columns.Add("CreateDate");
                tempDataTable.Columns.Add("CreateBy");
                tempDataTable.Columns.Add("ModifyBy");
                tempDataTable.Columns.Add("LastUpdate");
                tempDataTable.Columns.Add("statementFreq");
                tempDataTable.Columns.Add("productInfo");
                tempDataTable.Columns.Add("productRef");
                tempDataTable.Columns.Add("RMNumber");
                tempDataTable.Columns.Add("ACC_NO");
                tempDataTable.Columns.Add("Category");
                tempDataTable.Columns.Add("StatementType");

                DateTime StatementDate;
                int lineCounter = 1;
                foreach (var line in lines)
                {
                    lineCounter++;
                    String stmtType = line.Split('|')[(int)directBankGetDataColumn.StatementType];
                    // check if correct StatementDate format in each line
                    if (!DateTime.TryParseExact(line.Split('|')[(int)directBankGetDataColumn.StatementDate], "yyyyMMdd", CultureInfo.InvariantCulture,
                                DateTimeStyles.None, out StatementDate))
                    {
                        numOfIncorrectFormat++;
                        Util.Log(logType.Warning, String.Format("Invalid statementDate at Line {0} : {1}", lineCounter, line.Split('|')[(int)directBankGetDataColumn.StatementDate]));
                        continue;
                    }

                    DataRow row = tempDataTable.NewRow();

                    // check if StatementType valid in each line
                    if (dbMapDict.ContainsKey(stmtType))
                    {
                        if (dbMapDict[stmtType] == "Y")
                            row["ACC_NO"] = line.Split('|')[(int)directBankGetDataColumn.ACC_NO].Replace("-", "");
                        else if (dbMapDict[stmtType] == "N")
                            row["ACC_NO"] = null;
                    }
                    else
                    {
                        numOfIncorrectFormat++;
                        Util.Log(logType.Warning, String.Format("Invalid StatementType (input StatementType does not exist in table tbDirectBankMapping) at Line {0} : {1}", lineCounter, stmtType));
                        continue;
                    }
                    row["StatementDate"] = StatementDate;
                    row["StatementPath"] = line.Split('|')[(int)directBankGetDataColumn.FileName];
                    row["HeaderPath"] = " ";
                    row["TermsAndConditionPath"] = " ";
                    row["HeaderPositionX"] = 0;
                    row["HeaderPositionY"] = 0;
                    row["CreateDate"] = now;
                    row["CreateBy"] = Properties.Settings.Default.AppName;
                    row["ModifyBy"] = Properties.Settings.Default.AppName;
                    row["LastUpdate"] = now;
                    row["statementFreq"] = Properties.Settings.Default.directBankStatementFreq;
                    row["productInfo"] = line.Split('|')[(int)directBankGetDataColumn.ProductInfo]; ;
                    row["productRef"] = line.Split('|')[(int)directBankGetDataColumn.ProductRef];
                    row["RMNumber"] = line.Split('|')[(int)directBankGetDataColumn.RM_NUMBER].Substring(5, 30);
                    row["Category"] = line.Split('|')[(int)directBankGetDataColumn.Category];
                    row["StatementType"] = stmtType;


                    tempDataTable.Rows.Add(row);
                }
                Util.Log(logType.Info, "read index file finish");
                #endregion

                Util.Log(logType.Info, String.Format("Truncate {0} start", Properties.Settings.Default.tbStatementTempTableName));
                entity.ExecuteStoreCommand(String.Format("TRUNCATE TABLE {0}", Properties.Settings.Default.tbStatementTempTableName));
                Util.Log(logType.Info, String.Format("Truncate {0} finish", Properties.Settings.Default.tbStatementTempTableName));

                // use bulk insert
                Util.Log(logType.Info, String.Format("Insert record into {0} start", Properties.Settings.Default.tbStatementTempTableName));
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                {
                    connection.Open();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    {
                        bulkCopy.DestinationTableName = Properties.Settings.Default.tbStatementTempTableName;
                        bulkCopy.BatchSize = 10000;
                        bulkCopy.BulkCopyTimeout = Properties.Settings.Default.BulkInsertTimeout;

                        SqlBulkCopyColumnMapping map_StatementDate = new SqlBulkCopyColumnMapping("StatementDate", "StatementDate");
                        bulkCopy.ColumnMappings.Add(map_StatementDate);
                        SqlBulkCopyColumnMapping map_StatementPath = new SqlBulkCopyColumnMapping("StatementPath", "StatementPath");
                        bulkCopy.ColumnMappings.Add(map_StatementPath);
                        SqlBulkCopyColumnMapping map_HeaderPath = new SqlBulkCopyColumnMapping("HeaderPath", "HeaderPath");
                        bulkCopy.ColumnMappings.Add(map_HeaderPath);
                        SqlBulkCopyColumnMapping map_TermsAndConditionPath = new SqlBulkCopyColumnMapping("TermsAndConditionPath", "TermsAndConditionPath");
                        bulkCopy.ColumnMappings.Add(map_TermsAndConditionPath);
                        SqlBulkCopyColumnMapping map_HeaderPositionX = new SqlBulkCopyColumnMapping("HeaderPositionX", "HeaderPositionX");
                        bulkCopy.ColumnMappings.Add(map_HeaderPositionX);
                        SqlBulkCopyColumnMapping map_HeaderPositionY = new SqlBulkCopyColumnMapping("HeaderPositionY", "HeaderPositionY");
                        bulkCopy.ColumnMappings.Add(map_HeaderPositionY);
                        SqlBulkCopyColumnMapping map_CreateDate = new SqlBulkCopyColumnMapping("CreateDate", "CreateDate");
                        bulkCopy.ColumnMappings.Add(map_CreateDate);
                        SqlBulkCopyColumnMapping map_CreateBy = new SqlBulkCopyColumnMapping("CreateBy", "CreateBy");
                        bulkCopy.ColumnMappings.Add(map_CreateBy);
                        SqlBulkCopyColumnMapping map_ModifyBy = new SqlBulkCopyColumnMapping("ModifyBy", "ModifyBy");
                        bulkCopy.ColumnMappings.Add(map_ModifyBy);
                        SqlBulkCopyColumnMapping map_LastUpdate = new SqlBulkCopyColumnMapping("LastUpdate", "LastUpdate");
                        bulkCopy.ColumnMappings.Add(map_LastUpdate);
                        SqlBulkCopyColumnMapping map_statementFreq = new SqlBulkCopyColumnMapping("statementFreq", "statementFreq");
                        bulkCopy.ColumnMappings.Add(map_statementFreq);
                        SqlBulkCopyColumnMapping map_productInfo = new SqlBulkCopyColumnMapping("productInfo", "productInfo");
                        bulkCopy.ColumnMappings.Add(map_productInfo);
                        SqlBulkCopyColumnMapping map_productRef = new SqlBulkCopyColumnMapping("productRef", "productRef");
                        bulkCopy.ColumnMappings.Add(map_productRef);
                        SqlBulkCopyColumnMapping map_RMNumber = new SqlBulkCopyColumnMapping("RMNumber", "RMNumber");
                        bulkCopy.ColumnMappings.Add(map_RMNumber);
                        SqlBulkCopyColumnMapping map_ACC_NO = new SqlBulkCopyColumnMapping("ACC_NO", "ACC_NO");
                        bulkCopy.ColumnMappings.Add(map_ACC_NO);
                        SqlBulkCopyColumnMapping map_Category = new SqlBulkCopyColumnMapping("Category", "Category");
                        bulkCopy.ColumnMappings.Add(map_Category);
                        SqlBulkCopyColumnMapping map_StatementType = new SqlBulkCopyColumnMapping("StatementType", "StatementType");
                        bulkCopy.ColumnMappings.Add(map_StatementType);

                        bulkCopy.WriteToServer(tempDataTable);
                    }
                    connection.Close();
                }
                tempDataTable.Clear();
                Util.Log(logType.Info, String.Format("Insert record into {0} finish", Properties.Settings.Default.tbStatementTempTableName));

                Util.Log(logType.Info, String.Format("Call store pro. {0} to update table tbStatement and tbStatementFullList start", Properties.Settings.Default.spUpdatetbStatementAndtbStatementFullList));
                entity.ExecuteStoreCommand(String.Format("EXECUTE {0}", Properties.Settings.Default.spUpdatetbStatementAndtbStatementFullList));
                Util.Log(logType.Info, String.Format("Call store pro. {0} to update table tbStatement and tbStatementFullList finish", Properties.Settings.Default.spUpdatetbStatementAndtbStatementFullList));

                numOfImport = entity.tbStatementTemp.Where(a => a.AccountID != null).Count();
                numOfAccountNotFound = entity.tbStatementTemp.Where(a => a.AccountID == null).Count();

                Util.Log(logType.Info, String.Format("{0} File : {1}, import finish", DateTime.Now.ToString("tt hh:mm:ss"), Path.GetFileName(indexFilePath)));
                Util.Log(logType.Info, String.Format("(a)No. of PDF source files = {0}", numOfPDF.ToString()));
                Util.Log(logType.Info, String.Format("(b)No. of account not found = {0}", numOfAccountNotFound.ToString()));
                Util.Log(logType.Info, String.Format("(c)No. of stmt file not found = {0}", "0"));
                Util.Log(logType.Info, String.Format("(d)Index file format incorrect = {0}", numOfIncorrectFormat.ToString()));
                Util.Log(logType.Info, String.Format("(e)No. of stmt imported in statementfull list = {0}", numOfImport.ToString()));
                if (numOfImport != (numOfPDF - numOfAccountNotFound - numOfIncorrectFormat))
                {
                    String errorMessage = "";
                    if (numOfAccountNotFound > 0) errorMessage = errorMessage + "(b)";
                    if (numOfIncorrectFormat > 0) errorMessage = errorMessage + "(d)";
                    Util.Log(logType.Info, String.Format("ERROR: statement importer with error {0}", errorMessage));
                }
            }
            catch (Exception ex)
            {
                Util.Log(logType.Info, String.Format("Error in File {0} import : {1}", Path.GetFileName(indexFilePath), ex.Message));
            }
        }
        private static String replaceSpecialChar(String fileName)
        {
            return Path.GetInvalidFileNameChars().Aggregate(fileName, (current, c) => current.Replace(c.ToString(), "X"));
        }

        

        private static void MoveFileToErrorFolder(String statementRootPath)
        {
            String sourceDir = statementRootPath;
            String destDir = String.Format(Path.Combine(Properties.Settings.Default.ErrorRootDir, DateTime.Now.ToString("yyyyMMdd")));
            Directory.CreateDirectory(destDir);

            foreach (String dirPath in Directory.GetDirectories(sourceDir, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(sourceDir, destDir));

            foreach (String newPath in Directory.GetFiles(sourceDir, "*.*",
                SearchOption.AllDirectories))
            {
                String sourcePath = newPath;
                String destPath = newPath.Replace(sourceDir, destDir);
                try
                {
                    if (File.Exists(destPath))
                    {
                        File.Delete(destPath);
                    }
                    File.Move(sourcePath, destPath);
                }
                catch (Exception ex)
                {
                    Util.Log(logType.Error, String.Format("Cannot move file from {0} to {1}. Error message:{2}", sourcePath, destPath, ex.Message));
                }
            }

            foreach (String dirPath in Directory.GetDirectories(sourceDir, "*",
                SearchOption.TopDirectoryOnly))
            {
                try
                {
                    Directory.Delete(dirPath, true);
                }
                catch (Exception ex)
                {
                    Util.Log(logType.Error, String.Format("Cannot delete directory {0}. Error message:{1}", dirPath, ex.Message));
                }
            }
        }
    }
}
