﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace InfoCastOneMap
{
    class Program
    {
        static String logPath = Path.Combine(Directory.GetCurrentDirectory(), "InfoCastOneMap.log");
        static EStatementEntities entity = new EStatementEntities();
        static void Main(string[] args)
        {
            if (File.Exists(Properties.Settings.Default.FilePathInfoCastOneAccMap))
            {
                Log(String.Format("InfoCastOneMap start {0}", DateTime.Now));
                Log(String.Format("Read InfoCastOneAccMap data file ({0}) start", Path.GetFileName(Properties.Settings.Default.FilePathInfoCastOneAccMap)));
                
                Dictionary<String, String> NewAcnoMapDict = new Dictionary<String, String>();
                Dictionary<String, tbAccountList_b4avq> OldInfoCastAccDict = new Dictionary<String, tbAccountList_b4avq>();
                List<tbAccountList_b4avq> OldInfoCastAccountList = new List<tbAccountList_b4avq>();
                if (Properties.Settings.Default.CheckActive == "Y")
                    OldInfoCastAccountList = entity.tbAccountList_b4avq.Where(a => a.Category == "InfoCast" && a.StatementType == "InfoCast" && a.Active == "Y").ToList();
                else
                    OldInfoCastAccountList = entity.tbAccountList_b4avq.Where(a => a.Category == "InfoCast" && a.StatementType == "InfoCast").ToList();
                foreach (var record in OldInfoCastAccountList)
                {
                    if (!OldInfoCastAccDict.ContainsKey(record.AccountNumber))
                        OldInfoCastAccDict.Add(record.AccountNumber, record);
                    else
                    {
                        Log(String.Format("Old InfoCast Account repeated. Please check table tbAccountList_b4avq record with ID = {0}, AccountNumber = {1}", record.ID, record.AccountNumber));
                        continue;
                    }
                    //Log(String.Format("accNo = {0} , RMID = {1} , status = {2}", record.AccountNumber, record.RMID, record.RegistrationStatus));
                }

                Dictionary<String, tbAccountList> NewInfoCastAccDict = new Dictionary<String, tbAccountList>();
                List<tbAccountList> NewInfoCastAccountList = entity.tbAccountList.Where(a => a.Category == "InfoCast" && a.StatementType == "InfoCast" && a.Active == "Y").ToList();
                foreach (var record in NewInfoCastAccountList)
                {
                    if (!NewInfoCastAccDict.ContainsKey(record.AccountNumber))
                        NewInfoCastAccDict.Add(record.AccountNumber, record);
                    else
                    {
                        Log(String.Format("New InfoCast Account repeated. Please check table tbAccountList record with ID = {0}, AccountNumber = {1}", record.ID, record.AccountNumber));
                        continue;
                    }
                    //Log(String.Format("accNo = {0} , RMID = {1} , status = {2}", record.AccountNumber, record.RMID, record.RegistrationStatus));
                }

                int lineCounter = 0;
                using (TextReader tr = File.OpenText(Properties.Settings.Default.FilePathInfoCastOneAccMap))
                {
                    List<String> lines = new List<String>();
                    String eachLine = String.Empty;

                    while ((eachLine = tr.ReadLine()) != null)
                    {
                        if (eachLine.StartsWith("\""))
                        {
                            lineCounter++;

                            if (!NewAcnoMapDict.ContainsKey(eachLine.Split(',')[0].Substring(1, eachLine.Split(',')[0].Count() - 2)))
                            {
                                if (!NewAcnoMapDict.ContainsValue(eachLine.Split(',')[1].Substring(1, eachLine.Split(',')[1].Count() - 2)))
                                    NewAcnoMapDict.Add(eachLine.Split(',')[0].Substring(1, eachLine.Split(',')[0].Count() - 2), eachLine.Split(',')[1].Substring(1, eachLine.Split(',')[1].Count() - 2));
                                else
                                {
                                    Log(String.Format("New AccountNumber repeated. Please check data file record at Line {0}, repeated new AccountNumber = {1}", lineCounter + 1, eachLine.Split(',')[1].Substring(1, eachLine.Split(',')[1].Count() - 2)));
                                    continue;
                                }
                            }
                            else
                            {
                                Log(String.Format("Old AccountNumber repeated. Please check data file record at Line {0}, repeated old AccountNumber = {1}", lineCounter + 1, eachLine.Split(',')[0].Substring(1, eachLine.Split(',')[0].Count() - 2)));
                                continue;
                            }
                            //Log("old = " + eachLine.Split(',')[0].Substring(1, eachLine.Split(',')[0].Count() - 2)
                            //+ " ; new = " + eachLine.Split(',')[1].Substring(1, eachLine.Split(',')[1].Count() - 2));
                        }
                    }
                    tr.Close();
                }
                Log(String.Format("Read InfoCastOneAccMap data file ({0}) finish", Path.GetFileName(Properties.Settings.Default.FilePathInfoCastOneAccMap)));

                DateTime now = DateTime.Now;
                int statusSuccessUpdate = 0;
                int recordSuccessInsert = 0;
                int recordCannotMapOldAcno = 0;
                int recordCannotMapNewAcno = 0;

                DataTable tempDataTableForUpdateStatus = new DataTable();
                tempDataTableForUpdateStatus.Columns.Add("AccountNumber");
                tempDataTableForUpdateStatus.Columns.Add("RegistrationStatus");

                DataTable tempDataTableForInsert = new DataTable();
                tempDataTableForInsert.Columns.Add("RMID");
                tempDataTableForInsert.Columns.Add("AccountNumber");
                tempDataTableForInsert.Columns.Add("DisplayAccountNumber");
                tempDataTableForInsert.Columns.Add("Category");
                tempDataTableForInsert.Columns.Add("StatementType");
                tempDataTableForInsert.Columns.Add("RegistrationStatus");
                tempDataTableForInsert.Columns.Add("Active");
                tempDataTableForInsert.Columns.Add("CreateDate");
                tempDataTableForInsert.Columns.Add("CreateBy");
                tempDataTableForInsert.Columns.Add("ModifyBy");
                tempDataTableForInsert.Columns.Add("LastUpdate");
                tempDataTableForInsert.Columns.Add("statusUpdateBy");

                Log(String.Format("Map InfoCast record with new AccountNumber start {0}", DateTime.Now));
                foreach (var record in NewAcnoMapDict)
                {
                    if (OldInfoCastAccDict.ContainsKey(record.Key))     // check if old infocast account exist
                    {
                        if (NewInfoCastAccDict.ContainsKey(record.Value))       // check if new infocast account exist, if exist => update status only
                        {
                            DataRow row = tempDataTableForUpdateStatus.NewRow();
                            row["AccountNumber"] = record.Value;
                            row["RegistrationStatus"] = OldInfoCastAccDict[record.Key].RegistrationStatus;

                            tempDataTableForUpdateStatus.Rows.Add(row);
                            statusSuccessUpdate++;
                        }
                        else
                        {
                            recordCannotMapNewAcno++;
                            if (Properties.Settings.Default.InsertNewRecord == "Y")     // if not exist => check if insert new record
                            {
                                DataRow row = tempDataTableForInsert.NewRow();
                                row["RMID"] = OldInfoCastAccDict[record.Key].RMID;
                                row["AccountNumber"] = record.Value;
                                row["DisplayAccountNumber"] = String.Format("{0}-{1}-{2}-{3}", record.Value.Substring(0, 3), record.Value.Substring(3, 1), record.Value.Substring(4, 6), record.Value.Substring(10));
                                row["Category"] = "InfoCast";
                                row["StatementType"] = "InfoCast";
                                row["RegistrationStatus"] = OldInfoCastAccDict[record.Key].RegistrationStatus;
                                row["Active"] = "Y";
                                row["CreateDate"] = now;
                                row["CreateBy"] = OldInfoCastAccDict[record.Key].CreateBy;
                                row["ModifyBy"] = OldInfoCastAccDict[record.Key].ModifyBy;
                                row["LastUpdate"] = now;
                                row["statusUpdateBy"] = OldInfoCastAccDict[record.Key].statusUpdateBy;

                                tempDataTableForInsert.Rows.Add(row);
                                recordSuccessInsert++;
                            }
                        }
                    }
                    else
                    {
                        Log(String.Format("Cannot Map InfoCast account in tbAccountList_b4avq with old AccountNumber = {0}", record.Key));
                        recordCannotMapOldAcno++;
                    }
                }
                Log(String.Format("Map InfoCast record with new AccountNumber finish {0}", DateTime.Now));

                if (tempDataTableForUpdateStatus.Rows.Count > 0)
                {
                    Log("Update tbAccountList InfoCast Accounts status start");
                    Log("Build temp table DataTableForUpdateStatus start");
                    entity.Database.ExecuteSqlCommand("IF OBJECT_ID('[dbo].[tempDataTableForUpdateStatus]', 'U') IS NOT NULL DROP TABLE [dbo].[tempDataTableForUpdateStatus]");
                    entity.Database.ExecuteSqlCommand("CREATE TABLE tempDataTableForUpdateStatus([AccountNumber] [nvarchar](100) NOT NULL,[RegistrationStatus] [nvarchar](1) NOT NULL)");

                    try
                    {
                        using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                        {
                            connection.Open();

                            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                            {
                                bulkCopy.DestinationTableName = "[dbo].[tempDataTableForUpdateStatus]";
                                bulkCopy.BatchSize = 10000;

                                SqlBulkCopyColumnMapping map_AccountNumber = new SqlBulkCopyColumnMapping("AccountNumber", "AccountNumber");
                                bulkCopy.ColumnMappings.Add(map_AccountNumber);
                                SqlBulkCopyColumnMapping map_RegistrationStatus = new SqlBulkCopyColumnMapping("RegistrationStatus", "RegistrationStatus");
                                bulkCopy.ColumnMappings.Add(map_RegistrationStatus);

                                bulkCopy.WriteToServer(tempDataTableForUpdateStatus);
                            }
                            connection.Close();
                        }
                        tempDataTableForUpdateStatus.Clear();
                        Log("Build temp table DataTableForUpdateStatus finish");

                        entity.Database.ExecuteSqlCommand("update aclist set aclist.RegistrationStatus = tmp.RegistrationStatus from [dbo].[tbAccountList] aclist join [dbo].[tempDataTableForUpdateStatus] tmp on aclist.AccountNumber = tmp.AccountNumber");
                        entity.Database.ExecuteSqlCommand("IF OBJECT_ID('[dbo].[tempDataTableForUpdateStatus]', 'U') IS NOT NULL DROP TABLE [dbo].[tempDataTableForUpdateStatus]");
                        Log("Update tbAccountList InfoCast Accounts status finish");
                    }
                    catch (Exception ex)
                    {
                        entity.Database.ExecuteSqlCommand("IF OBJECT_ID('[dbo].[tempDataTableForUpdateStatus]', 'U') IS NOT NULL DROP TABLE [dbo].[tempDataTableForUpdateStatus]");
                        Log(String.Format("Error in Update tbAccountList InfoCast Accounts status : {0}", ex.Message));
                    }
                }

                if (Properties.Settings.Default.InsertNewRecord == "Y")
                {
                    Log("InsertNewRecord Config is set to \"Y\", start insert InfoCast record with new AccountNumber");
                    if (tempDataTableForInsert.Rows.Count > 0)
                    {
                        Log("Insert InfoCast Accounts into tbAccountList start");
                        try
                        {
                            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                            {
                                connection.Open();

                                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                                {
                                    bulkCopy.DestinationTableName = "[dbo].[tbAccountList]";
                                    bulkCopy.BatchSize = 10000;

                                    SqlBulkCopyColumnMapping map_RMID = new SqlBulkCopyColumnMapping("RMID", "RMID");
                                    bulkCopy.ColumnMappings.Add(map_RMID);
                                    SqlBulkCopyColumnMapping map_AccountNumber = new SqlBulkCopyColumnMapping("AccountNumber", "AccountNumber");
                                    bulkCopy.ColumnMappings.Add(map_AccountNumber);
                                    SqlBulkCopyColumnMapping map_DisplayAccountNumber = new SqlBulkCopyColumnMapping("DisplayAccountNumber", "DisplayAccountNumber");
                                    bulkCopy.ColumnMappings.Add(map_DisplayAccountNumber);
                                    SqlBulkCopyColumnMapping map_Category = new SqlBulkCopyColumnMapping("Category", "Category");
                                    bulkCopy.ColumnMappings.Add(map_Category);
                                    SqlBulkCopyColumnMapping map_StatementType = new SqlBulkCopyColumnMapping("StatementType", "StatementType");
                                    bulkCopy.ColumnMappings.Add(map_StatementType);
                                    SqlBulkCopyColumnMapping map_RegistrationStatus = new SqlBulkCopyColumnMapping("RegistrationStatus", "RegistrationStatus");
                                    bulkCopy.ColumnMappings.Add(map_RegistrationStatus);
                                    SqlBulkCopyColumnMapping map_Active = new SqlBulkCopyColumnMapping("Active", "Active");
                                    bulkCopy.ColumnMappings.Add(map_Active);
                                    SqlBulkCopyColumnMapping map_CreateDate = new SqlBulkCopyColumnMapping("CreateDate", "CreateDate");
                                    bulkCopy.ColumnMappings.Add(map_CreateDate);
                                    SqlBulkCopyColumnMapping map_CreateBy = new SqlBulkCopyColumnMapping("CreateBy", "CreateBy");
                                    bulkCopy.ColumnMappings.Add(map_CreateBy);
                                    SqlBulkCopyColumnMapping map_ModifyBy = new SqlBulkCopyColumnMapping("ModifyBy", "ModifyBy");
                                    bulkCopy.ColumnMappings.Add(map_ModifyBy);
                                    SqlBulkCopyColumnMapping map_LastUpdate = new SqlBulkCopyColumnMapping("LastUpdate", "LastUpdate");
                                    bulkCopy.ColumnMappings.Add(map_LastUpdate);
                                    SqlBulkCopyColumnMapping map_statusUpdateBy = new SqlBulkCopyColumnMapping("statusUpdateBy", "statusUpdateBy");
                                    bulkCopy.ColumnMappings.Add(map_statusUpdateBy);

                                    bulkCopy.WriteToServer(tempDataTableForInsert);
                                }
                                connection.Close();
                            }
                            Log("Insert InfoCast Accounts into tbAccountList finish");
                        }
                        catch (Exception ex)
                        {
                            Log(String.Format("Error in Insert InfoCast Accounts into tbAccountList : {0}", ex.Message));
                        }
                    }
                }
                else
                    Log("InsertNewRecord Config does not set to \"Y\", skip insert InfoCast record with new AccountNumber");

                Log(String.Format("Total No. of records cannot map old AccountNumber in tbAccountList_b4avq = {0}", recordCannotMapOldAcno));
                Log(String.Format("Total No. of records cannot map new AccountNumber in tbAccountList = {0}", recordCannotMapNewAcno));
                Log(String.Format("Total No. of records update status in tbAccountList = {0}", statusSuccessUpdate));
                Log(String.Format("Total No. of records inserted into tbAccountList = {0}", recordSuccessInsert));

                Log(String.Format("InfoCastOneMap finish {0}", DateTime.Now));
            }
            else
                Log("InfoCastOneAccMap data file not exist, process skipped");

            System.Threading.Thread.Sleep(5000);
        }

        public static void Log(String content)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    Console.WriteLine(content);
                    sw.Close();
                }
            }
            catch (Exception) { }
        }
    }
}
