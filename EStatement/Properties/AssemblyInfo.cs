﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("EStatement")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("EStatement")]
/*********************Modified by Jacky on 2018-12-07******************/
//purpose : Help Jessica to identify different version
[assembly: AssemblyCopyright("Copyright ©  2019")]
/********************************************************************/
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c488040c-907e-4839-aec7-7a803f49cbc3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

/*********************Modified by Jacky on 2018-12-07******************/
//purpose : Help Jessica to identify different version
//[assembly: AssemblyInformationalVersionAttribute("2018.DEC.01.version_01")]
[assembly: AssemblyInformationalVersionAttribute("2019.JAN.18.version_01.3")]
/************************************************************/
