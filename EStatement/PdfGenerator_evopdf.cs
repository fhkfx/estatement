﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using EvoPdf;
using System.Net;
using System.IO;
using System.Drawing;

namespace EStatement
{
    public class PdfGenerator_evopdf
    {
        /*
        private PdfConverter pdfConverter;
        private string HeaderPath;
        //private string FooterPath;
        private string TcPath;

        public PdfGenerator(string headerPath, string tcPath)
        {
            pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "B4mYiJubiJiInIaYiJuZhpmahpGRkZE=";
            HeaderPath = headerPath;
            //FooterPath = footerPath;
            TcPath = tcPath;

        }

        public byte[] generatePdfFromUrl(string url)
        {
            return (generatePdf(new Document(new MemoryStream(getPdfFromUrl(url)))));
        }

        public byte[] generatePdfFromPath(string path)
        {
            return (generatePdf(new Document(new MemoryStream(getPdfFromPath(path)))));
        }

        private byte[] generatePdf(Document document)
        {
            Document mainDocument = document;
            Document tcDocument = new Document(TcPath);
            //mainDocument.AppendDocument(tcDocument);
            //Document pdfDocument = new Document(new MemoryStream(mainDocument.Save()));

            Image headerImg = Image.FromFile(HeaderPath);
            //Image footerImg = Image.FromFile(FooterPath);

            // set header and footer width and height
            float HeaderFooterWidth = mainDocument.Pages[0].ClientRectangle.Width;
            float DocumentHeight = mainDocument.Pages[0].ClientRectangle.Height;
            float HeaderHeight = headerImg.Height;

            // create the header and footer template
            Template HeaderTemplate = mainDocument.Templates.AddNewTemplate(HeaderFooterWidth, HeaderHeight);
            ImageElement imageElementHeader = new ImageElement(0, 0, HeaderPath);
            HeaderTemplate.AddElement(imageElementHeader);

            foreach (PdfPage pdfPage in mainDocument.Pages)
            {
                //pdfPage.Header = HeaderTemplate;
                pdfPage.AddElement(imageElementHeader);
            }
            
            Document pdfDocument = new Document(new MemoryStream(mainDocument.Save()));
            pdfDocument.AppendDocument(tcDocument);

            byte[] pdfBytes = pdfDocument.Save();
            mainDocument.Close();
            tcDocument.Close();
            pdfDocument.Close();

            return pdfBytes;
        }

        private byte[] getPdfFromUrl(string url)
        {
            WebClient webClient = new WebClient();
            byte[] response = webClient.DownloadData(new Uri(url));
            webClient.Dispose();
            return response;
        }

        private byte[] getPdfFromPath(string path)
        {
            return File.ReadAllBytes(path);
        }
         */
    }
}