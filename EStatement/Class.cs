﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EStatement
{
    public class Class
    {
        #region GetEStatementSummary

        public class getEStatementSummaryResult
        {
            public string hasSubscription;
            public string returnCode;
            public List<statementType> statementTypes = new List<statementType>();
        }

        public class statementType
        {
            public string rmid;
            public string acctNo;
            public string displayAcctNo;
            public string stmtType;
            public DateTime stmtDate;
            public DateTime? viewTime;
            public string stmtID;
            public string marketInsertID;
            public string infoInsertID;
            public string productType;
            public string productRef;
            public string docType;
            public string announcementRef;
            public string bpID;
            public string bpName;
            public string jointFlag;
            public string metaType;
            public string orderType;
            public string citicAssetGroup;
            public string prdouctNameEn;
            public string prdouctNameTc;
            public string prdouctNameSc;

        }

        #endregion

        #region GetEStatementHistory

        public class getEStatementHistoryResult
        {
            public string returnCode;
            public List<statementHistory> statementHistories = new List<statementHistory>();
        }

        public class statementHistory
        {
            public string rmid;
            public string acctNo;
            public string displayAcctNo;
            public string stmtType;
            public DateTime stmtDate;
            public DateTime? viewTime;
            public string stmtID;
            public string marketInsertID;
            public string infoInsertID;
            public string productType;
            public string productRef;
            public string docType;
            public string announcementRef;
            public string bpID;
            public string jointFlag;
            public string bpName;
        }

        #endregion

        #region GetSubscriptionStatus

        public class getSubscriptionStatusResult
        {
            public string returnCode;
            public List<subscriptionStatus> subscriptionStatuses = new List<subscriptionStatus>();
        }

        public class subscriptionStatus
        {
            public string rmid;
            public string acctNo;
            public string displayAcctNo;
            public string stmtType;
            public string subStatus;
            public string actionSys;
            public string actionId;
            public string bpName;
            public string jointFlag;

        }

        #endregion

        #region ChangeSubscriptionStatus

        public class changeSubscriptionStatusResult
        {
            public string rmid;
            public string returnCode;
            public string bpID;
        }

        #endregion

        #region GetViewToken

        public class getViewTokenResponse
        {
            public getViewTokenResult getViewTokenResult = new getViewTokenResult();
        }

        public class getViewTokenResult
        {
            public string returnCode;
            public string rmid;
            public string token;
        }

        #endregion

    }
}