﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using InfoArchiveDemo.ContentService;
using InfoArchiveDemo.QueryService;
using System.ServiceModel;
using System.Globalization;
using System.Xml;
using System.Text;

namespace EStatement
{
    public enum logType { Info, Error, Warning };

    public partial class GetStatement : System.Web.UI.Page
    {
        EStatementEntities Entity = new EStatementEntities();
        Dictionary<String, String> InfoArchiveDict = new Dictionary<String, String>();
        Dictionary<String, String> DirectBankInfoArchiveDict = new Dictionary<String, String>();
        Dictionary<String, String> SuperBankInfoArchiveDict = new Dictionary<String, String>();
        /******************Modified by Jacky on 2018-11-29************************/
        //purpose : production baunch now support credit card and bank statement
        Dictionary<String, String> CreditCardInfoArchiveDict = new Dictionary<String, String>();
        Dictionary<String, String> BankInfoArchiveDict = new Dictionary<String, String>();
        /*************************************************************************/

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.Log(logType.Info, "GetStatement Start.");

            #region Variable
            String token = Request.QueryString["Token"];
            String sessionID = Request.QueryString["SessionID"];
            String isCustomer = Request.QueryString["IsCustomer"];

            if (token == null) token = "";
            if (sessionID == null) sessionID = "";
            if (isCustomer == null) isCustomer = "";

            String token_rmid = null;
            String token_sessionID = null;
            String token_stmtID = null;
            String token_marketInsertID = null;
            String token_bankInfoInsertID = null;
            DateTime token_time = DateTime.Now;
            #endregion

            #region validation
            Util.Log(logType.Info, "GetStatement. Validation Start.");
            if (token == null && sessionID == null && isCustomer == null)
            {
                Util.Log(logType.Warning, "GetStatement. Parameters missing.");
                HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
            }
            if (!isCustomer.Equals("Y") && !isCustomer.Equals("N"))
            {
                Util.Log(logType.Warning, "GetStatement. Invalid IsCustomer.");
                HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
            }

            try
            {
                String key = Util.decrypt(token, Util.GetMD5(Properties.Settings.Default.EncryptionKey));
                String[] elements = key.Split(';');

                token_rmid = elements[0];
                token_sessionID = elements[1];
                token_stmtID = elements[2];
                token_marketInsertID = elements[3];
                token_bankInfoInsertID = elements[4];
                token_time = DateTime.ParseExact(elements[5], "yyyyMMddHHmmss", null);

                Util.Log(logType.Info, String.Format("GetStatement. Token:{0},rmid:{1},sessionID:{2},stmtID:{3},bankInfoInsertID:{4},marketInsertID:{5},datetime:{6}.", token, token_rmid, token_sessionID, token_stmtID, token_bankInfoInsertID, token_marketInsertID, token_time.ToString()));
            }
            catch (Exception ex)
            {
                Util.Log(logType.Warning, String.Format("GetStatement. Error in decoding token. Token:{0},SessionID:{1},IsCustomer:{2}. Error message:{3}", token, sessionID, isCustomer, ex.Message));
                HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
            }

            tbSystemSetting settingTokenTimeout = Entity.tbSystemSetting.Where(a => a.SettingName == "TokenTimeoutSecond" && a.EffectiveFrom <= DateTime.Now && a.EffectiveTo >= DateTime.Now).FirstOrDefault();
            String tokenTimeoutSecond = Properties.Settings.Default.TokenDefaultTimeoutSecond;
            if (settingTokenTimeout != null)
            {
                tokenTimeoutSecond = settingTokenTimeout.SettingValue;
            }
            else
            {
                Util.Log(logType.Warning, String.Format("GetStatement. tokenTimeoutSecond not found. Using default value:{0}.", tokenTimeoutSecond));
            }

            if (token_sessionID != sessionID)
            {
                Util.Log(logType.Warning, String.Format("GetStatement. SessionID mismatch. Token:{0},SessionID:{1},IsCustomer:{2}", token, sessionID, isCustomer));
                HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
            }
            else if (token_time.AddSeconds(Convert.ToInt32(tokenTimeoutSecond)) <= DateTime.Now)
            {
                Util.Log(logType.Warning, String.Format("GetStatement. Token expired. Token:{0},SessionID:{1},IsCustomer:{2}", token, sessionID, isCustomer));
                HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
            }
            Util.Log(logType.Info, "GetStatement. Validation End.");
            #endregion

            if (token_stmtID != "")
            {
                #region Statement
                Int32 stmtID = Convert.ToInt32(token_stmtID);
                tbStatement statement = Entity.tbStatement.Where(a => a.ID == stmtID).FirstOrDefault();

                if (statement != null)
                {
                    /**************************Modified by Jacky on 2018-12-28*********************/
                    //purpose : need to support the old version
                    //if (!(statement.HeaderPath.Trim().Equals("")))
                    if (!(statement.HeaderPath.Trim().Equals("")) && !statement.StatementPath.StartsWith("InfoArchive="))
                    /*************************************************************************/
                    {

                        String statementPath = statement.StatementPath;
                        if (!File.Exists(statementPath))
                        {
                            Util.Log(logType.Warning, String.Format("GetStatement. Statement file not exist. Path:{0}", statementPath));
                            HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                        }
                        else
                        {
                            String headerPath = statement.HeaderPath;
                            String tcPath = statement.TermsAndConditionPath;

                            int positionX = statement.HeaderPositionX;
                            int positionY = statement.HeaderPositionY;

                            #region validation

                            if (!File.Exists(headerPath))
                            {
                                Util.Log(logType.Warning, String.Format("GetStatement. Header file not exist. Path:{0}", headerPath));
                                HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                            }

                            if (!(statement.tbAccountList.Category.Equals("CiticFirst") || statement.tbAccountList.Category.Equals("InfoCast")))
                            {
                                if (!File.Exists(tcPath))
                                {
                                    Util.Log(logType.Warning, String.Format("GetStatement. TC file not exist. Path:{0}", tcPath));
                                    HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                                }
                            }
                            #endregion

                            try
                            {
                                String outputFilename = Properties.Settings.Default.EStatementFileName;
                                Util.Log(logType.Info, "GetStatement. PDF concatenation Start.");
                                PdfGenerator pdfGenerator = new PdfGenerator(headerPath, tcPath, positionX, positionY);
                                if (statement.tbAccountList.Category.Equals("CiticFirst"))
                                {
                                    pdfGenerator.appendTC = false;
                                }

                                byte[] pdfBytes = pdfGenerator.Start(statementPath);
                                Util.Log(logType.Info, "GetStatement. PDF concatenation End.");

                                HttpResponse response = HttpContext.Current.Response;
                                response.Clear();
                                response.ClearContent();
                                response.ContentType = "application/pdf";
                                response.AddHeader("Content-Disposition", String.Format("inline; filename={0}; size={1}", outputFilename, pdfBytes.Length.ToString()));
                                response.BinaryWrite(pdfBytes);

                                if (isCustomer.Equals("Y"))
                                {
                                    statement.ViewTime = DateTime.Now;
                                    statement.ModifyBy = Properties.Settings.Default.AppName;
                                    statement.LastUpdate = DateTime.Now;
                                }
                                Entity.SaveChanges();
                            }
                            catch (Exception ex)
                            {
                                Util.Log(logType.Error, String.Format("Error in GetStatement(Pdf concatenation). Error message:{0}", ex.Message));
                                HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink, false);
                            }

                        }
                    }
                    else {
                        // InfoCast statement
                        // get account number from statement account ID
                        #region query InfoArchive
                        try
                        {
                            List<tbAccountList> accountList = Entity.tbAccountList.Where(a => a.ID == statement.AccountID).ToList();

                            if ((accountList != null) || (accountList.Count > 0))
                            {
                                // initiate InfoArchive config values from database

                                List<tbSystemSetting> InfoArchiveSetting = Entity.tbSystemSetting.Where(a => a.Category == Properties.Settings.Default.CategoryInfoArchive).ToList();
                                foreach (var IAS in InfoArchiveSetting)
                                    InfoArchiveDict.Add(IAS.SettingName, IAS.SettingValue);

                                List<tbSystemSetting> DirectBankInfoArchiveSetting = Entity.tbSystemSetting.Where(a => a.Category == Properties.Settings.Default.CategoryDirectBankInfoArchive).ToList();
                                foreach (var DBIAS in DirectBankInfoArchiveSetting)
                                    DirectBankInfoArchiveDict.Add(DBIAS.SettingName, DBIAS.SettingValue);

                                List<tbSystemSetting> SuperBankInfoArchiveSetting = Entity.tbSystemSetting.Where(a => a.Category == Properties.Settings.Default.CategorySuperBankInfoArchive).ToList();
                                foreach (var SBIAS in SuperBankInfoArchiveSetting)
                                    SuperBankInfoArchiveDict.Add(SBIAS.SettingName, SBIAS.SettingValue);

                                /************************Modified by Jacky on 2018-11-29**********************/
                                List<tbSystemSetting> CreditCardInfoArchiveSetting = Entity.tbSystemSetting.Where(a => a.Category == Properties.Settings.Default.CategoryCreditCardInfoArchive).ToList();
                                foreach (var CC in CreditCardInfoArchiveSetting)
                                    CreditCardInfoArchiveDict.Add(CC.SettingName, CC.SettingValue);

                                List<tbSystemSetting> BankInfoArchiveSetting = Entity.tbSystemSetting.Where(a => a.Category == Properties.Settings.Default.CategoryBankInfoArchive).ToList();
                                foreach (var bank in BankInfoArchiveSetting)
                                    BankInfoArchiveDict.Add(bank.SettingName, bank.SettingValue);
                                /******************************************************************/

                                Util.Log(logType.Info, String.Format("Database config variables validation start - StatementType = \"{0}\" ; statementFreq = \"{1}\"", accountList[0].StatementType, statement.statementFreq));
                                Util.Log(logType.Info, String.Format("Total config variables for Category = \"{0}\"  : {1}", Properties.Settings.Default.CategoryInfoArchive, InfoArchiveDict.Count()));
                                Util.Log(logType.Info, String.Format("Total config variables for Category = \"{0}\"  : {1}", Properties.Settings.Default.CategoryDirectBankInfoArchive, DirectBankInfoArchiveDict.Count()));
                                Util.Log(logType.Info, String.Format("Total config variables for Category = \"{0}\"  : {1}", Properties.Settings.Default.CategorySuperBankInfoArchive, SuperBankInfoArchiveDict.Count()));
                                /************************Modified by Jacky on 2018-11-29***********************************/
                                Util.Log(logType.Info, String.Format("Total config variables for Category = \"{0}\"  : {1}", Properties.Settings.Default.CategoryCreditCardInfoArchive, CreditCardInfoArchiveDict.Count()));
                                Util.Log(logType.Info, String.Format("Total config variables for Category = \"{0}\"  : {1}", Properties.Settings.Default.CategoryBankInfoArchive, BankInfoArchiveDict.Count()));
                                /******************************************************************************************/

                                Util.Log(logType.Info, "profiles validation start");
                                string[] s_daily = { InfoArchiveDict["InfoArchive_profileDaily"] };
                                string[] s_monthly = { InfoArchiveDict["InfoArchive_profileMonthly"] };
                                string[] s_advice = { InfoArchiveDict["InfoArchive_profileAdvice"] };

                                string[] directBank_profile = { DirectBankInfoArchiveDict["DirectBank_InfoArchive_profile"] };

                                string[] sbs_daily = { SuperBankInfoArchiveDict["SuperBank_InfoArchive_ProfileDaily"] };
                                string[] sbs_monthly;
                                if (accountList[0].Category == Properties.Settings.Default.CategorySuperBank)
                                    sbs_monthly = new string[]{ SuperBankInfoArchiveDict["SuperBank_InfoArchive_SBS_ProfileMonthly"] };
                                else
                                    sbs_monthly = new string[] { SuperBankInfoArchiveDict["SuperBank_InfoArchive_AVQ_ProfileMonthly"] };

                                string[] sbs_advice = { SuperBankInfoArchiveDict["SuperBank_InfoArchive_ProfileAdvice"] };

                                /***********************Modified by Jacky on 2018-11-29***************************/
                                //purpose : Add creditcard and bank profile data
                                string[] CC_profile = {CreditCardInfoArchiveDict["CC_InfoArchive_profile"] };
                                string[] Bank_profile = { BankInfoArchiveDict["Bank_InfoArchive_profile"] };
                                /****************************************************************************/
                                Util.Log(logType.Info, "profiles validation finish");

                                // need to call InfoArchive web service to get the pdf file
                                // using query service to get the aip and sequence number

                                //Create resultSchema
                                resultSchema rs = new resultSchema();

                                InfoArchiveDemo.QueryService.serviceContext q_sc1 = new InfoArchiveDemo.QueryService.serviceContext();

                                //Create archiveInformationCollection
                                archiveInformationCollection aic = new archiveInformationCollection();
                                Util.Log(logType.Info, "q_sc1,aic,rs,dc validation start");
                                q_sc1.consumerApplication = InfoArchiveDict["InfoArchive_consumerApplication"];
                                q_sc1.userName = InfoArchiveDict["InfoArchive_userName"];
                                q_sc1.languageCode = InfoArchiveDict["InfoArchive_languageCode"];

                                //Create deliveryChannel
                                deliveryChannel dc = new deliveryChannel();
                                dc.name = InfoArchiveDict["InfoArchive_deliveryChannel"];

                                StringBuilder sb0 = new StringBuilder();
                                if (accountList[0].StatementType == Properties.Settings.Default.StatementTypeDirectBankFD)
                                {
                                    aic.name = DirectBankInfoArchiveDict["DirectBank_InfoArchive_archiveInformation"];
                                    q_sc1.profiles = directBank_profile;
                                    rs.name = DirectBankInfoArchiveDict["DirectBank_InfoArchive_resultSchema"];
                                }/*********************Modified by Jacky on 2018-11-29******************************/
                                else if (accountList[0].Category == Properties.Settings.Default.CategoryCreditCard) {
                                    aic.name = CreditCardInfoArchiveDict["CC_InfoArchive_archiveInformation"];
                                    q_sc1.profiles = CC_profile;
                                    rs.name = CreditCardInfoArchiveDict["CC_InfoArchive_resultSchema"];
                                }
                                else if ((accountList[0].Category == Properties.Settings.Default.CategoryIndividual) || (accountList[0].Category == Properties.Settings.Default.CategoryCombine))
                                {
                                    aic.name = BankInfoArchiveDict["Bank_InfoArchive_archiveInformation"];
                                    q_sc1.profiles = Bank_profile;
                                    rs.name = BankInfoArchiveDict["Bank_InfoArchive_resultSchema"];
                                }
                                /***************************************************/
                                else if ((accountList[0].Category == Properties.Settings.Default.CategorySuperBank) || (accountList[0].Category == Properties.Settings.Default.CategoryContainer))
                                {
                                    if (statement.statementFreq.Equals("D"))
                                    {
                                        aic.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_archiveInformationDaily"];
                                        q_sc1.profiles = sbs_daily;
                                        rs.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_resultDailySchema"];
                                    }
                                    else if (statement.statementFreq.Equals("M"))
                                    {
                                        if (accountList[0].Category == Properties.Settings.Default.CategorySuperBank)
                                            aic.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_SBS_archiveInformationMonthly"];
                                        else
                                            aic.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_AVQ_archiveInformationMonthly"];
                                        q_sc1.profiles = sbs_monthly;
                                        if (accountList[0].Category == Properties.Settings.Default.CategorySuperBank)
                                            rs.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_SBS_resultMonthlySchema"];
                                        else
                                            rs.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_AVQ_resultMonthlySchema"];
                                    }
                                    else if (statement.statementFreq.Equals("A"))
                                    {
                                        aic.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_archiveInformationAdvice"];
                                        q_sc1.profiles = sbs_advice;
                                        rs.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_resultAdviceSchema"];
                                    }
                                }
                                else
                                {
                                    if (statement.statementFreq.Equals("D"))
                                    {
                                        aic.name = InfoArchiveDict["InfoArchive_archiveInformationDaily"];
                                        q_sc1.profiles = s_daily;
                                        rs.name = InfoArchiveDict["InfoArchive_resultDailySchema"];
                                    }
                                    else if (statement.statementFreq.Equals("M"))
                                    {
                                        aic.name = InfoArchiveDict["InfoArchive_archiveInformationMonthly"];
                                        q_sc1.profiles = s_monthly;
                                        rs.name = InfoArchiveDict["InfoArchive_resultMonthlySchema"];
                                    }
                                    else if (statement.statementFreq.Equals("A"))
                                    {
                                        aic.name = InfoArchiveDict["InfoArchive_archiveInformationAdvice"];
                                        q_sc1.profiles = s_advice;
                                        rs.name = InfoArchiveDict["InfoArchive_resultAdviceSchema"];
                                    }
                                }
                                Util.Log(logType.Info, "q_sc1,aic,rs,dc validation finish");

                                sb0.Append("StatementType = " + accountList[0].StatementType + " ; "
                                            + "consumerApplication = " + q_sc1.consumerApplication + " ; "
                                            + "userName = " + q_sc1.userName + " ; "
                                            + "languageCode = " + q_sc1.languageCode + " ; "
                                            + "profiles = " + q_sc1.profiles[0] + " ; "
                                            + "archiveInformationCollection  = " + aic.name + " ; "
                                            + "resultSchema  = " + rs.name + " ; "
                                            + "deliveryChannel  = " + dc.name);

                                StringBuilder sb = new StringBuilder();

                                //Create the searchQueryCriteria
                                relationalOperand ro = new relationalOperand();
                                ro.type = logicalOperator.AND;

                                Util.Log(logType.Info, "operand validation start");
                                if (accountList[0].StatementType == Properties.Settings.Default.StatementTypeDirectBankFD)
                                {
                                    operand Acct_No_0 = new operand();
                                    Acct_No_0.name = DirectBankInfoArchiveDict["DirectBank_InfoArchive_Acct_No_0"];
                                    Acct_No_0.@operator = relationalOperator.Equal;
                                    string[] v1 = { "" + statement.productInfo.Trim() };
                                    Acct_No_0.value = v1;

                                    sb.Append(Acct_No_0.name + " = " + string.Join(" ", v1) + " ; ");

                                    operand Date = new operand();
                                    Date.name = DirectBankInfoArchiveDict["DirectBank_InfoArchive_Date"];
                                    Date.@operator = relationalOperator.GreaterOrEqual;
                                    string[] v2 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                    Date.value = v2;

                                    sb.Append(Date.name + " = " + string.Join(" ", v2) + " ");

                                    operand[] o = { Acct_No_0, Date };
                                    ro.operand = o;
                                }
                                /*************************Modified by Jacky on 2018-12-24******************************/
                                else if ((accountList[0].Category == Properties.Settings.Default.CategoryIndividual) || (accountList[0].Category == Properties.Settings.Default.CategoryCombine))
                                {
                                    operand Acct_No_0 = new operand();
                                    Acct_No_0.name = BankInfoArchiveDict["Bank_InfoArchive_Acct_No_0"];
                                    Acct_No_0.@operator = relationalOperator.Equal;
                                    string[] v1 = { "" + accountList[0].AccountNumber };
                                    Acct_No_0.value = v1;

                                    sb.Append(Acct_No_0.name + " = " + string.Join(" ", v1) + " ; ");

                                    /****************Modified by Jacky on 2019-01-18******************/
                                    /*operand Date = new operand();
                                    Date.name = BankInfoArchiveDict["Bank_InfoArchive_Date"];
                                    Date.@operator = relationalOperator.GreaterOrEqual;
                                    string[] v2 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                    Date.value = v2;

                                    sb.Append(Date.name + " = " + string.Join(" ", v2) + " ");

                                    operand[] o = { Acct_No_0, Date };
                                    ro.operand = o;*/

                                    operand startDate = new operand();
                                    startDate.name = String.IsNullOrEmpty(InfoArchiveDict["InfoArchive_startDate"]) ? "Date" : InfoArchiveDict["InfoArchive_startDate"];
                                    startDate.@operator = relationalOperator.GreaterOrEqual;

                                    string[] v2 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                    startDate.value = v2;

                                    sb.Append(" From Date=" + string.Join(" ", v2));

                                    operand endDate = new operand();
                                    endDate.name = String.IsNullOrEmpty(InfoArchiveDict["InfoArchive_endDate"]) ? "Date" : InfoArchiveDict["InfoArchive_endDate"];
                                    endDate.@operator = relationalOperator.LessOrEqual;
                                    string[] v3 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                    endDate.value = v3;
                                    sb.Append(" To Date=" + string.Join(" ", v3));

                                    operand[] o = { Acct_No_0, startDate, endDate };
                                    ro.operand = o;
                                    /*******************************************************************/
                                }
                                else if (accountList[0].Category == Properties.Settings.Default.CategoryCreditCard)
                                {
                                    operand Acct_No_0 = new operand();
                                    Acct_No_0.name = CreditCardInfoArchiveDict["CC_InfoArchive_Acct_No_0"];
                                    Acct_No_0.@operator = relationalOperator.Equal;
                                    string[] v1 = { "" + accountList[0].AccountNumber };
                                    Acct_No_0.value = v1;

                                    sb.Append(Acct_No_0.name + " = " + string.Join(" ", v1) + " ; ");

                                    /****************Modified by Jacky on 2019-01-18******************/
                                    /*operand Date = new operand();
                                    Date.name = CreditCardInfoArchiveDict["CC_InfoArchive_Date"];
                                    Date.@operator = relationalOperator.GreaterOrEqual;
                                    string[] v2 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                    Date.value = v2;

                                    sb.Append(Date.name + " = " + string.Join(" ", v2) + " ");

                                    operand[] o = { Acct_No_0, Date };
                                    ro.operand = o;*/
                                    operand startDate = new operand();
                                    startDate.name = String.IsNullOrEmpty(InfoArchiveDict["InfoArchive_startDate"]) ? "Date" : InfoArchiveDict["InfoArchive_startDate"];
                                    startDate.@operator = relationalOperator.GreaterOrEqual;

                                    string[] v2 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                    startDate.value = v2;

                                    sb.Append(" From Date=" + string.Join(" ", v2));

                                    operand endDate = new operand();
                                    endDate.name = String.IsNullOrEmpty(InfoArchiveDict["InfoArchive_endDate"]) ? "Date" : InfoArchiveDict["InfoArchive_endDate"];
                                    endDate.@operator = relationalOperator.LessOrEqual;
                                    string[] v3 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                    endDate.value = v3;
                                    sb.Append(" To Date=" + string.Join(" ", v3));

                                    operand[] o = { Acct_No_0, startDate, endDate };
                                    ro.operand = o;
                                    /*******************************************************************/
                                }
                                /**************************************************************************************/
                                else if ((accountList[0].Category == Properties.Settings.Default.CategorySuperBank) || (accountList[0].Category == Properties.Settings.Default.CategoryContainer))
                                {
                                    if (statement.statementFreq.Equals("M"))
                                    {
                                        //operand Cust_Name_0 = new operand();
                                        //Cust_Name_0.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_Cust_Name_0"];
                                        //Cust_Name_0.@operator = relationalOperator.Equal;
                                        //string[] v0 = { SuperBankInfoArchiveDict["SuperBank_InfoArchive_Cust_Name_0_Value"] };
                                        //Cust_Name_0.value = v0;
                                        //sb.Append(Cust_Name_0.name + " = " + string.Join(" ", v0) + " ; ");

                                        //operand Ref_ID_1 = new operand();
                                        //Ref_ID_1.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_Ref_ID_1"];
                                        //Ref_ID_1.@operator = relationalOperator.Equal;
                                        //string[] v1 = { SuperBankInfoArchiveDict["SuperBank_InfoArchive_Ref_ID_1_Value"] };
                                        //Ref_ID_1.value = v1;
                                        //sb.Append(Ref_ID_1.name + " = " + string.Join(" ", v1) + " ; ");

                                        operand Statement_Date = new operand();
                                        Statement_Date.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_Statement_Date"];
                                        Statement_Date.@operator = relationalOperator.Equal;
                                        string[] v2 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                        Statement_Date.value = v2;
                                        sb.Append(Statement_Date.name + " = " + string.Join(" ", v2) + " ; ");

                                        operand Avq_ID_3 = new operand();
                                        Avq_ID_3.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_Avq_ID_3"];
                                        Avq_ID_3.@operator = relationalOperator.Equal;
                                        string[] v3 = { "" + statement.productRef.Trim() };
                                        Avq_ID_3.value = v3;
                                        sb.Append(Avq_ID_3.name + " = " + string.Join(" ", v3) + " ; ");

                                        operand Stat_key_4 = new operand();
                                        Stat_key_4.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_Stat_key_4"];
                                        Stat_key_4.@operator = relationalOperator.Equal;
                                        string[] v4 = { "" + accountList[0].AccountNumber.Trim() };
                                        Stat_key_4.value = v4;
                                        sb.Append(Stat_key_4.name + " = " + string.Join(" ", v4));

                                        //operand[] o = { Cust_Name_0, Ref_ID_1, Statement_Date, Avq_ID_3, Stat_key_4 };
                                        operand[] o = { Statement_Date, Avq_ID_3, Stat_key_4 };
                                        ro.operand = o;
                                    }
                                    else // for statement.statementFreq = "A","D"
                                    {
                                        operand Acct_No_0 = new operand();
                                        Acct_No_0.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_Acct_No_0"];
                                        Acct_No_0.@operator = relationalOperator.Equal;
                                        string[] v0 = { "" + statement.ContainerAcno.Trim() };
                                        Acct_No_0.value = v0;
                                        sb.Append(Acct_No_0.name + " = " + string.Join(" ", v0) + " ; ");

                                        operand Date = new operand();
                                        Date.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_Date"];
                                        Date.@operator = relationalOperator.GreaterOrEqual;

                                        string[] v1 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                        Date.value = v1;

                                        sb.Append("Date = " + string.Join(" ", v1) + " ; ");

                                        operand Ref_No_2 = new operand();
                                        Ref_No_2.name = SuperBankInfoArchiveDict["SuperBank_InfoArchive_Ref_No_2"];
                                        Ref_No_2.@operator = relationalOperator.Equal;
                                        string[] v2 = { "" + statement.productRef.Trim() };
                                        Ref_No_2.value = v2;
                                        sb.Append(Ref_No_2.name + " = " + string.Join(" ", v2));

                                        operand[] o = { Acct_No_0, Date, Ref_No_2 };
                                        ro.operand = o;
                                    }
                                }
                                else
                                {
                                    if (!statement.statementFreq.Equals("A"))
                                    {
                                        operand acctNum = new operand();
                                        acctNum.name = InfoArchiveDict["InfoArchive_acctNum"];
                                        acctNum.@operator = relationalOperator.Equal;
                                        string[] v1 = { "" + accountList[0].AccountNumber.Trim() };
                                        acctNum.value = v1;

                                        sb.Append(" " + acctNum.name + "=" + string.Join(" ", v1));

                                        operand startDate = new operand();
                                        startDate.name = String.IsNullOrEmpty(InfoArchiveDict["InfoArchive_startDate"]) ? "Date" : InfoArchiveDict["InfoArchive_startDate"];
                                        startDate.@operator = relationalOperator.GreaterOrEqual;

                                        string[] v2 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                        startDate.value = v2;

                                        sb.Append(" From Date=" + string.Join(" ", v2));

                                        operand endDate = new operand();
                                        endDate.name = String.IsNullOrEmpty(InfoArchiveDict["InfoArchive_endDate"]) ? "Date" : InfoArchiveDict["InfoArchive_endDate"];
                                        endDate.@operator = relationalOperator.LessOrEqual;
                                        string[] v3 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                        endDate.value = v3;
                                        sb.Append(" To Date=" + string.Join(" ", v3));

                                        operand[] o = { acctNum, startDate, endDate };
                                        ro.operand = o;
                                    }
                                    else
                                    {
                                        operand acctNum = new operand();
                                        acctNum.name = String.IsNullOrEmpty(InfoArchiveDict["InfoArchive_AdviceAcctNum"]) ? "SecurityAcctNum_0" : InfoArchiveDict["InfoArchive_AdviceAcctNum"];
                                        acctNum.@operator = relationalOperator.Equal;
                                        string[] v1 = { "" + accountList[0].AccountNumber.Trim() };
                                        acctNum.value = v1;

                                        sb.Append(" " + acctNum.name + "=" + string.Join(" ", v1));

                                        operand letterDate = new operand();
                                        letterDate.name = String.IsNullOrEmpty(InfoArchiveDict["InfoArchive_letterDate"]) ? "LetterDate" : InfoArchiveDict["InfoArchive_letterDate"];
                                        letterDate.@operator = relationalOperator.Equal;
                                        string[] v2 = { statement.StatementDate.ToString("yyyy-MM-dd") };
                                        letterDate.value = v2;

                                        sb.Append(" " + letterDate.name + "=" + string.Join(" ", v2));

                                        operand announcementRef = new operand();
                                        announcementRef.name = String.IsNullOrEmpty(InfoArchiveDict["InfoArchive_announcementRef"]) ? "AnnouncementRef_3" : InfoArchiveDict["InfoArchive_announcementRef"];
                                        announcementRef.@operator = relationalOperator.Equal;
                                        string[] v4 = { "" + statement.productRef.Trim() };
                                        announcementRef.value = v4;

                                        sb.Append(" " + announcementRef.name + "=" + string.Join(" ", v4));

                                        operand stockCode = new operand();
                                        stockCode.name = String.IsNullOrEmpty(InfoArchiveDict["InfoArchive_stockCode"]) ? "StockCode_4" : InfoArchiveDict["InfoArchive_stockCode"];
                                        stockCode.@operator = relationalOperator.Equal;
                                        string[] v5 = { "" + statement.productInfo.Trim() };
                                        stockCode.value = v5;

                                        sb.Append(" " + stockCode.name + "=" + string.Join(" ", v5));

                                        operand[] o = { acctNum, letterDate, announcementRef, stockCode };
                                        ro.operand = o;
                                    }
                                }
                                Util.Log(logType.Info, "operand validation finish");

                                long l = 1;
                                bool b = false;

                                BasicHttpBinding binding = new BasicHttpBinding();
                                binding.MessageEncoding = WSMessageEncoding.Mtom;
                                Util.Log(logType.Info, "binding,queryEndPoint validation start");
                                try
                                {
                                    binding.MaxReceivedMessageSize = Convert.ToInt64(InfoArchiveDict["InfoArchive_maxPdfSize"]);
                                    binding.MaxBufferSize = Convert.ToInt32(InfoArchiveDict["InfoArchive_maxPdfSize"]);
                                }
                                catch (Exception ex)
                                {
                                    binding.MaxReceivedMessageSize = 20000000; // 20Mb
                                    binding.MaxBufferSize = 20000000;
                                }

                                if (InfoArchiveDict["InfoArchive_queryEndPoint"].StartsWith("https"))
                                {
                                    binding.Security.Mode = BasicHttpSecurityMode.Transport;
                                    System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate
                                    {
                                        return true;
                                    };
                                }

                                System.ServiceModel.EndpointAddress endpoint = new System.ServiceModel.EndpointAddress(InfoArchiveDict["InfoArchive_queryEndPoint"]);
                                Util.Log(logType.Info, "binding,queryEndPoint validation finish");
                                Util.Log(logType.Info, "Database config variables validation finish");

                                Util.Log(logType.Info, "q_sc1,aic,rs,dc values : " + sb0.ToString());
                                Util.Log(logType.Info, "Start query InfoArchive, " + sb.ToString());
                                InfoArchiveDemo.QueryService.QueryServiceSoapPortClient qspc = new QueryServiceSoapPortClient(binding, endpoint);
                                byte[] result = qspc.runQuery(q_sc1, aic, rs, dc, ro, null, null, null, false, out l, out b);
                                Util.Log(logType.Warning, String.Format("GetStatement. InfoArchive query return size = " + l + " for StatementID: {0}", token_stmtID));
                                if (result != null)
                                {
                                    String xml = System.Text.Encoding.Default.GetString(result);
                                    XmlDocument xmldoc = new XmlDocument();
                                    xmldoc.LoadXml(xml);
                                    String contentId = "";

                                    var nodes = xmldoc.GetElementsByTagName("content_file");

                                    if (nodes != null)
                                    {
                                        Util.Log(logType.Info, "Number of data returned = " + nodes.Count);
                                    }
                                    /*******************Modified by Jacky on 2019-01-11*********************/
                                    //purpose : Need back the redirect function in InfoArchive Version
                                    /*int nodeCount = 1;
                                    foreach (XmlNode childrenNode in nodes)
                                    {
                                        Util.Log(logType.Info, nodeCount + ": child node contentId = " + childrenNode.Attributes["ns1:cid"].Value);
                                        nodeCount++;
                                    }*/
                                    int nodeCount = 0;
                                    foreach (XmlNode childrenNode in nodes)
                                    {
                                        Util.Log(logType.Info, nodeCount + ": child node contentId = " + childrenNode.Attributes["ns1:cid"].Value);
                                        nodeCount++;
                                    }

                                    if (nodeCount != 1)
                                    {
                                        Util.Log(logType.Error, String.Format("GetStatement. InfoArchive query return {0} entries StatementID: {1}", nodeCount, token_stmtID));
                                        HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                                    }
                                    /****************************************************************/
                                    nodeCount = 1;
                                    foreach (XmlNode childrenNode in nodes)
                                    {
                                        contentId = childrenNode.Attributes["ns1:cid"].Value;
                                        if (nodeCount == 1)
                                            break;
                                        nodeCount++;
                                    }

                                    if ("".Equals(contentId))
                                    {
                                        Util.Log(logType.Warning, String.Format("GetStatement. InfoArchive query return empty contentId result for StatementID: {0}", token_stmtID));
                                        HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                                    }
                                    else
                                    {
                                        // Get content from InfoArchive
                                        InfoArchiveDemo.ContentService.serviceContext sc1 = new InfoArchiveDemo.ContentService.serviceContext();
                                        sc1.consumerApplication = InfoArchiveDict["InfoArchive_consumerApplication"];
                                        sc1.userName = InfoArchiveDict["InfoArchive_userName"];
                                        sc1.languageCode = InfoArchiveDict["InfoArchive_languageCode"];

                                        if (accountList[0].StatementType == Properties.Settings.Default.StatementTypeDirectBankFD)
                                            sc1.profiles = directBank_profile;
                                        /************************Modified by Jacky on 2018-11-29**************************/
                                        else if(accountList[0].Category == Properties.Settings.Default.CategoryIndividual || accountList[0].Category == Properties.Settings.Default.CategoryCombine)
                                            sc1.profiles = Bank_profile;
                                        else if (accountList[0].Category == Properties.Settings.Default.CategoryCreditCard)
                                            sc1.profiles = CC_profile;
                                        /*******************************************************************/
                                        else if ((accountList[0].Category == Properties.Settings.Default.CategorySuperBank) || (accountList[0].Category == Properties.Settings.Default.CategoryContainer))
                                        {
                                            if (statement.statementFreq.Equals("D"))
                                            {
                                                sc1.profiles = sbs_daily;
                                            }
                                            else if (statement.statementFreq.Equals("M"))
                                            {
                                                sc1.profiles = sbs_monthly;
                                            }
                                            else if (statement.statementFreq.Equals("A"))
                                            {
                                                sc1.profiles = sbs_advice;
                                            }
                                        }
                                        else
                                        {
                                            if (statement.statementFreq.Equals("D"))
                                            {
                                                sc1.profiles = s_daily;
                                            }
                                            else if (statement.statementFreq.Equals("M"))
                                            {
                                                sc1.profiles = s_monthly;
                                            }
                                            else if (statement.statementFreq.Equals("A"))
                                            {
                                                sc1.profiles = s_advice;
                                            }
                                        }

                                        InfoArchiveDemo.ContentService.mimeType m;

                                        string s1 = "";
                                        byte[] data;
                                        InfoArchiveDemo.ContentService.hash[] h;

                                        binding = new BasicHttpBinding();
                                        binding.MessageEncoding = WSMessageEncoding.Mtom;
                                        try
                                        {
                                            binding.MaxReceivedMessageSize = Convert.ToInt64(InfoArchiveDict["InfoArchive_maxPdfSize"]);
                                            binding.MaxBufferSize = Convert.ToInt32(InfoArchiveDict["InfoArchive_maxPdfSize"]);
                                        }
                                        catch (Exception ex)
                                        {
                                            binding.MaxReceivedMessageSize = 20000000; // 20Mb
                                            binding.MaxBufferSize = 20000000;
                                        }

                                        if (InfoArchiveDict["InfoArchive_contentEndPoint"].StartsWith("https"))
                                        {
                                            binding.Security.Mode = BasicHttpSecurityMode.Transport;
                                            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate
                                            {
                                                return true;
                                            };
                                        }

                                        endpoint = new System.ServiceModel.EndpointAddress(InfoArchiveDict["InfoArchive_contentEndPoint"]);
                                        InfoArchiveDemo.ContentService.ContentServiceSoapPortClient cspc = new ContentServiceSoapPortClient(binding, endpoint);

                                        long r = cspc.getContent(sc1, contentId, InfoArchiveDemo.ContentService.mimeType.applicationzip, "pdf", out m, out s1, out data, out s1, out h);

                                        if (r > 0)
                                        {
                                            /**********************************Modified by Jacky on 2018-12-28**************************/
                                            //purpose : add Header and T&C of tbSystemSetting to Bank, CC
                                            if ((accountList[0].Category == Properties.Settings.Default.CategoryIndividual) || (accountList[0].Category == Properties.Settings.Default.CategoryCombine) || (accountList[0].Category == Properties.Settings.Default.CategoryCreditCard))
                                            {
                                                //String statementPath = statement.StatementPath.Replace("InfoArchive=", "");
                                                    String statementpath = Properties.Settings.Default.TempFolder + token_stmtID + ".pdf";
                                                    String headerPath = statement.HeaderPath;
                                                    String tcPath = statement.TermsAndConditionPath;

                                                    int positionX = statement.HeaderPositionX;
                                                    int positionY = statement.HeaderPositionY;

                                                    #region validation

                                                    if (!File.Exists(headerPath))
                                                    {
                                                        Util.Log(logType.Warning, String.Format("GetStatement. Header file not exist. Path:{0}", headerPath));
                                                        HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                                                    }
                                                    
                                                       if (!File.Exists(tcPath))
                                                       {
                                                          Util.Log(logType.Warning, String.Format("GetStatement. TC file not exist. Path:{0}", tcPath));
                                                          HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                                                       }
                                                #endregion
                                                try
                                                {

                                                    #region Create infoArchive file
                                                    // Save IA PDF into temp file
                                                        using (FileStream fs = File.Create(statementpath))
                                                        {
                                                            // Add some information to the file.
                                                                fs.Write(data, 0, data.Length);
                                                        }
                                                    #endregion

                                                    String outputFilename = Properties.Settings.Default.EStatementFileName;
                                                    Util.Log(logType.Info, "GetStatement. PDF concatenation Start.");
                                                    PdfGenerator pdfGenerator = new PdfGenerator(headerPath, tcPath, positionX, positionY);
                                                    byte[] pdfBytes = pdfGenerator.Start(statementpath);
                                                    Util.Log(logType.Info, "GetStatement. PDF concatenation End.");

                                                    #region Remove infoArchive file
                                                    // Remove IA PDF temp folder
                                                    if(File.Exists(statementpath))
                                                        File.Delete(statementpath);
                                                    #endregion

                                                    HttpResponse response = HttpContext.Current.Response;
                                                        response.Clear();
                                                        response.ClearContent();
                                                        response.ContentType = "application/pdf";
                                                        response.AddHeader("Content-Disposition", String.Format("inline; filename={0}; size={1}", outputFilename, pdfBytes.Length.ToString()));
                                                        response.BinaryWrite(pdfBytes);

                                                        if (isCustomer.Equals("Y"))
                                                        {
                                                            statement.ViewTime = DateTime.Now;
                                                            statement.ModifyBy = Properties.Settings.Default.AppName;
                                                            statement.LastUpdate = DateTime.Now;
                                                        }
                                                        Entity.SaveChanges();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Util.Log(logType.Error, String.Format("Error in GetStatement(Pdf concatenation). Error message:{0}", ex.Message));
                                                        HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink, false);
                                                    }
                                                
                                            }
                                            else {
                                            /********************************************************************************/
                                            String outputFilename = Properties.Settings.Default.EStatementFileName;
                                            HttpResponse response = HttpContext.Current.Response;
                                            response.Clear();
                                            response.ClearContent();
                                            response.ContentType = "application/pdf";
                                            response.AddHeader("Content-Disposition", String.Format("inline; filename={0}; size={1}", outputFilename, data.Length.ToString()));
                                            response.BinaryWrite(data);

                                            if (isCustomer.Equals("Y"))
                                            {
                                                statement.ViewTime = DateTime.Now;
                                                statement.ModifyBy = Properties.Settings.Default.AppName;
                                                statement.LastUpdate = DateTime.Now;
                                            }
                                            Entity.SaveChanges();
                                            /******************************************************************************/
                                            }
                                            /******************************************************************************/
                                        }

                                    }

                                }
                                else
                                {
                                    Util.Log(logType.Warning, String.Format("GetStatement. InfoArchive query return null result for StatementID: {0}", token_stmtID));
                                    HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                                }
                            }
                            else
                            {
                                Util.Log(logType.Warning, String.Format("GetStatement. Account not found. StatementID: {0}", token_stmtID));
                                HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                            }
                        }
                        catch (Exception ex1)
                        {
                            Util.Log(logType.Warning, ex1.StackTrace.ToString());
                            Util.Log(logType.Warning, String.Format("GetStatement. Exception (" + ex1.Message + ") found. StatementID: {0}", token_stmtID));
                            HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                        }
                        #endregion
                    }
                }
                else
                {
                    Util.Log(logType.Warning, String.Format("GetStatement. Statement not found. StatementID: {0}", token_stmtID));
                    HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                }
                #endregion
            }
            else if (token_bankInfoInsertID != "")
            {
                #region BankInfoInsert
                Int32 bankInfoInsertID = Convert.ToInt32(token_bankInfoInsertID);
                tbBankInfoInsert bankInfoInsert = Entity.tbBankInfoInsert.Where(a => a.ID == bankInfoInsertID).FirstOrDefault();

                if (bankInfoInsert != null)
                {
                    if (!File.Exists(bankInfoInsert.InsertPath))
                    {
                        Util.Log(logType.Warning, String.Format("GetStatement. BankInfo insert file not exist. Path:{0}", bankInfoInsert.InsertPath));
                        HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                    }

                    String outputFilename = Properties.Settings.Default.BankInfoInsertFileName;
                    byte[] pdfBytes = File.ReadAllBytes(bankInfoInsert.InsertPath);

                    HttpResponse response = HttpContext.Current.Response;
                    response.Clear();
                    response.ClearContent();
                    response.ContentType = "application/pdf";
                    response.AddHeader("Content-Disposition", String.Format("inline; filename={0}; size={1}", outputFilename, pdfBytes.Length.ToString()));
                    response.BinaryWrite(pdfBytes);
                }
                else
                {
                    Util.Log(logType.Warning, String.Format("GetStatement. BankInfoInsert not found. BankInfoInsertID: {0}", token_bankInfoInsertID));
                    HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                }
                #endregion
            }
            else if (token_marketInsertID != "")
            {
                #region MarketInsert
                Int32 marketInsertID = Convert.ToInt32(token_marketInsertID);
                tbMarketInsert marketInsert = Entity.tbMarketInsert.Where(a => a.ID == marketInsertID).FirstOrDefault();

                if (marketInsert != null)
                {
                    if (!File.Exists(marketInsert.InsertPath))
                    {
                        Util.Log(logType.Warning, String.Format("GetStatement. Market insert file not exist. Path:{0}", marketInsert.InsertPath));
                        HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                    }

                    String outputFilename = Properties.Settings.Default.MarketInsertFileName;
                    byte[] pdfBytes = File.ReadAllBytes(marketInsert.InsertPath);

                    HttpResponse response = HttpContext.Current.Response;
                    response.Clear();
                    response.ClearContent();
                    response.ContentType = "application/pdf";
                    response.AddHeader("Content-Disposition", String.Format("inline; filename={0}; size={1}", outputFilename, pdfBytes.Length.ToString()));
                    response.BinaryWrite(pdfBytes);
                }
                else
                {
                    Util.Log(logType.Warning, String.Format("GetStatement. MarketInsert not found. MarketInsertID: {0}", token_marketInsertID));
                    HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
                }
                #endregion
            }
            else
            {
                Util.Log(logType.Warning, "GetStatement. StatementID, BankInfoInsertID, MarketInsertID are empty.");
                HttpContext.Current.Response.Redirect(Properties.Settings.Default.RedirectLink);
            }

            Util.Log(logType.Info, "GetStatement End.");
            HttpContext.Current.Response.End();
        }
    }
}