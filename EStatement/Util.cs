﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace EStatement
{
	// Test
    public class Util
    {
        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public static void Log(logType type, String content)
        {
            try
            {
                EStatementEntities Entity = new EStatementEntities();
                DateTime now = DateTime.Now;
                tbLog log = new tbLog();
                log.Type = Enum.GetName(typeof(logType), type);
                log.Content = content;
                log.CreateBy = Properties.Settings.Default.AppName;
                log.ModifyBy = Properties.Settings.Default.AppName;
                log.CreateDate = now;
                log.LastUpdate = now;
                Entity.AddTotbLog(log);
                Entity.SaveChanges();
            }
            catch (Exception) { }
        }

        public static string encrypt(string strData, string strKey)
        {
            byte[] sourceBytes = UTF8Encoding.UTF8.GetBytes(strData);
            byte[] byte_pwdMD5 = UTF8Encoding.UTF8.GetBytes(strKey);


            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = byte_pwdMD5;
            rDel.Mode = CipherMode.ECB;
            rDel.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = rDel.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(sourceBytes, 0, sourceBytes.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string decrypt(string strData, string strkey)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(strkey);
            byte[] toEncryptArray = Convert.FromBase64String(strData);

            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB;
            rDel.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = rDel.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static string GetMD5(string original)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] b = md5.ComputeHash(Encoding.UTF8.GetBytes(original));
            return BitConverter.ToString(b).Replace("-", string.Empty).ToLower();
        }

    }
}