﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;

namespace EStatement
{
    public class PdfGenerator
    {
        private String headerPath;
        private String tcPath;
        private int positionX;
        private int positionY;
        public Boolean appendHeader = true;
        public Boolean appendTC = true;

        public PdfGenerator(string i_headerPath, string i_tcPath, int i_positionX, int i_positionY)
        {
            headerPath = i_headerPath;
            tcPath = i_tcPath;
            positionX = i_positionX;
            positionY = i_positionY;
        }

        public byte[] Start(string pdfPath)
        {
            byte[] pdfWithHeader;
            using (MemoryStream pdfWithHeaderStream = new MemoryStream())
            {
                var reader = new PdfReader(pdfPath);
                var stamper = new PdfStamper(reader, pdfWithHeaderStream);

                if (appendHeader)
                {
                    #region for png header
                    /*
                    iTextSharp.text.Image headerImage = iTextSharp.text.Image.GetInstance(headerPath);

                    int numberOfPage = reader.NumberOfPages;
                    for (int i = 1; i <= numberOfPage; i++)
                    {
                        var pdfContentByte = stamper.GetUnderContent(i);
                        headerImage.SetAbsolutePosition(positionX, positionY);

                        headerImage.ScaleAbsoluteHeight(reader.GetPageSize(i).Height);
                        headerImage.ScaleAbsoluteWidth(reader.GetPageSize(i).Width);

                        pdfContentByte.AddImage(headerImage);
                    }
                    */
                    #endregion

                    #region for pdf header
                    PdfReader headerReader = new PdfReader(headerPath);
                    PdfImportedPage page = stamper.GetImportedPage(headerReader, 1);
                    for (int i = 1; i <= reader.NumberOfPages; i++)
                    {
                        var pdfContentByte = stamper.GetUnderContent(i);
                        pdfContentByte.AddTemplate(page, positionX, positionY);
                    }
                    #endregion
                }

                stamper.Close();
                pdfWithHeader = pdfWithHeaderStream.ToArray();
                pdfWithHeaderStream.Close();

                /*****************Modified by Jacky on 2018-12-28***********************/
                //purpose : error occur if don't use the following statement
                reader.Close();
                /***************************************************/
            }

            byte[] pdfWithTC;
            using (MemoryStream pdfWithTCStream = new MemoryStream())
            {
                PdfReader readerPDF = new PdfReader(pdfWithHeader);
                
                Document document = new Document(readerPDF.GetPageSize(1));
                document.SetMargins(0f, 0f, 0f, 0f);
                PdfWriter writer = PdfWriter.GetInstance(document, pdfWithTCStream);
                document.Open();

                for (int i = 1; i <= readerPDF.NumberOfPages; i++)
                {
                    PdfImportedPage page = writer.GetImportedPage(readerPDF, i);
                    document.Add(iTextSharp.text.Image.GetInstance(page));
                }

                if (appendTC)
                {
                    #region for png tc
                    /*
                    iTextSharp.text.Image tcImage = iTextSharp.text.Image.GetInstance(tcPath);
                    tcImage.ScaleToFit(558f, 787.5f);
                    tcImage.SpacingBefore = 0f;
                    tcImage.SpacingAfter = 0f;
                    tcImage.Alignment = Element.ALIGN_CENTER;
                    document.Add(tcImage);
                    document.NewPage();
                    */
                    #endregion

                    #region for pdf tc
                    PdfReader tcReader = new PdfReader(tcPath);
                    PdfImportedPage tcpage = writer.GetImportedPage(tcReader, 1);
                    document.Add(iTextSharp.text.Image.GetInstance(tcpage));
                    document.NewPage();
                    #endregion
                }

                document.Close();
                pdfWithTC = pdfWithTCStream.ToArray();
                pdfWithTCStream.Close();
            }

            return pdfWithTC;
        }

        public static bool isAccountNumberExist(String pdfPath, String accountNumber)
        {
            bool isExist = false;

            try
            {
                StringBuilder text = new StringBuilder();
                PdfReader pdfReader = new PdfReader(pdfPath);
                for (int page = 1; page <= pdfReader.NumberOfPages; page++)
                {
                    string currentText = PdfTextExtractor.GetTextFromPage(pdfReader, page, new LocationTextExtractionStrategy());
                    text.Append(currentText);
                }
                pdfReader.Close();

                if (Regex.IsMatch(text.ToString().Replace(" ", ""), accountNumber))
                {
                    isExist = true;
                }
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in GetStatement(Pdf isAccountNumberExist). Error message:{0}", ex.Message));
            }

            return isExist;
        }
    }
}