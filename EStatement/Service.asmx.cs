﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Configuration;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace EStatement
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://www.fujitsu.com/hk/eStatement")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class eStatementWebService : System.Web.Services.WebService
    {
        EStatementEntities Entity = new EStatementEntities();
        //There is already an open DataReader associated with this Command which must be closed first  
        //MultipleActiveResultSets=true and change view 
        SqlDataReader reader = null;

        [WebMethod]
        public Class.getEStatementSummaryResult GetEStatementSummary(String rmid, String isProofPrint, String docType, String AcctNo, String StmtStatus, String StmtPeriodStart, String StmtPeriodEnd, String ProductType)
        {           

            Util.Log(logType.Info, "==================================================================================================================");
            Util.Log(logType.Info, String.Format("GetEStatementSummary Start. RMID=\"{0}\" ; IsProofPrint={1} ; DocType={2} ; AcctNo={3} ; StmtStatus={4} ; StmtPeriodStart={5} ; StmtPeriodEnd={6} ; ProductType={7}.",
                rmid,
                (isProofPrint == null) ? "null" : ("\"" + isProofPrint + "\""),
                (docType == null) ? "null" : ("\"" + docType + "\""),
                (AcctNo == null) ? "null" : ("\"" + AcctNo + "\""),
                (StmtStatus == null) ? "null" : ("\"" + StmtStatus + "\""),
                (StmtPeriodStart == null) ? "null" : ("\"" + StmtPeriodStart + "\""),
                (StmtPeriodEnd == null) ? "null" : ("\"" + StmtPeriodEnd + "\""),
                (ProductType == null) ? "null" : ("\"" + ProductType + "\"")));
            Class.getEStatementSummaryResult response = new Class.getEStatementSummaryResult();
            DateTime now = DateTime.Now;

            // allow null input
            if (isProofPrint == null)
                isProofPrint = "";
            if (docType == null)
                docType = "";
            else
                docType = docType.ToUpper();
            if (AcctNo == null)
                AcctNo = "";
            if (StmtStatus == null)
                StmtStatus = "";
            else
                StmtStatus = StmtStatus.Trim();
            if (StmtPeriodStart == null)
                StmtPeriodStart = "";
            if (StmtPeriodEnd == null)
                StmtPeriodEnd = "";
            if (ProductType == null)
                ProductType = "";
            else
                ProductType = ProductType.ToUpper();

            DateTime startDate;
            DateTime.TryParseExact("2000-01-01", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
            DateTime endDate = now;
            response.hasSubscription = "N";
            response.returnCode = "1";

            if (!String.IsNullOrEmpty(StmtPeriodStart.Trim()))
            {
                if (!DateTime.TryParseExact(StmtPeriodStart, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate))
                {
                    Util.Log(logType.Info, "GetEStatementSummary fail to convert to datetime, StmtPeriodStart = " + StmtPeriodStart);
                    return response;
                }
            }
            if (!String.IsNullOrEmpty(StmtPeriodEnd.Trim()))
            {
                if (!DateTime.TryParseExact(StmtPeriodEnd, "yyyy-MM-dd", CultureInfo.InvariantCulture,
                                    DateTimeStyles.None, out endDate))
                {
                    Util.Log(logType.Info, "GetEStatementSummary fail to convert to datetime, StmtPeriodEnd = " + StmtPeriodEnd);
                    return response;
                }
            }
            if ((!String.IsNullOrEmpty(StmtPeriodStart.Trim())) && (!String.IsNullOrEmpty(StmtPeriodEnd.Trim())))
            {
                if (startDate > endDate)
                {
                    Util.Log(logType.Info, "GetEStatementSummary fail, StmtPeriodStart > StmtPeriodEnd");
                    return response;
                }
            }

            if (!String.IsNullOrEmpty(Properties.Settings.Default.estatementhistory) && Properties.Settings.Default.estatementhistory != "19000101")
            {
                if (!DateTime.TryParseExact(Properties.Settings.Default.estatementhistory, "yyyyMMdd", CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out now))
                {
                    now = DateTime.Now;
                    Util.Log(logType.Warning, String.Format("GetEStatementSummary. Invalid estatementhistory Date Format yyyyMMdd {0} .", Properties.Settings.Default.estatementhistory));
                }
            }
            List<Class.statementType> statementTypeList = new List<Class.statementType>();
            try
            {
                #region normal account statement (citicfirst,infocast,creditcard)
                List<tbAccountList> accountsList = new List<tbAccountList>();
                accountsList.Clear();
                if (String.IsNullOrEmpty(AcctNo.Trim()))
                    accountsList = Entity.tbAccountList.Where(a => a.RMID == rmid && a.StatementType != Properties.Settings.Default.StatementTypeSuperBank && a.StatementType != Properties.Settings.Default.StatementTypeContainer).ToList();
                else
                    accountsList = Entity.tbAccountList.Where(a => a.RMID == rmid && a.StatementType != Properties.Settings.Default.StatementTypeSuperBank && a.StatementType != Properties.Settings.Default.StatementTypeContainer && a.AccountNumber == AcctNo).ToList();

                if (accountsList.Count > 0)
                {
                    response.hasSubscription = "Y";

                    foreach (var account in accountsList)
                    {
                        Util.Log(logType.Info, String.Format("Normal record - start searching statement with AccountID = \"{0}\" ; RMID = \"{1}\" ; AccountNumber = \"{2}\" ; StatementType = \"{3}\"", account.ID, account.RMID, account.AccountNumber, account.StatementType));
                        int predefinedPeriod = 0;

                        if (isProofPrint.Equals("N"))
                        {
                            tbSystemSetting systemSetting = Entity.tbSystemSetting.Where(a => a.SettingName == "PredefinedPeriod" && a.StatementType.ToUpper() == account.StatementType.ToUpper() && a.Category.ToUpper() == account.Category.ToUpper() && a.EffectiveFrom <= DateTime.Now && a.EffectiveTo >= DateTime.Now).FirstOrDefault();
                            if (systemSetting != null)
                            {
                                if (!int.TryParse(systemSetting.SettingValue, out predefinedPeriod))
                                {
                                    predefinedPeriod = 86400;
                                    Util.Log(logType.Warning, String.Format("GetEStatementSummary. Invalid PredefinedPeriod."));
                                }
                            }
                            else
                            {
                                Util.Log(logType.Warning, String.Format("GetEStatementSummary. PredefinedPeriod not found. rmid:{0}.", account.RMID));
                            }
                        }

                        List<tbStatement> statementList = new List<tbStatement>();
                        // 4 checking : grace period 1 day ; statement last for 1 year ; StatementDate >= input StmtPeriodStart ; StatementDate <= input StmtPeriodEnd
                        if (StmtStatus == "Not Read")
                            statementList = account.tbStatement.Where(a => a.CreateDate <= DateTime.Now.AddSeconds(-predefinedPeriod) && a.StatementDate.AddMonths(13) >= now && a.StatementDate >= startDate && a.StatementDate <= endDate && ((a.statementFreq != null && docType.Contains(a.statementFreq)) || docType == "") && a.ViewTime == null).OrderByDescending(a => a.StatementDate).ToList();
                        else if (StmtStatus == "Read")
                            statementList = account.tbStatement.Where(a => a.CreateDate <= DateTime.Now.AddSeconds(-predefinedPeriod) && a.StatementDate.AddMonths(13) >= now && a.StatementDate >= startDate && a.StatementDate <= endDate && ((a.statementFreq != null && docType.Contains(a.statementFreq)) || docType == "") && a.ViewTime != null).OrderByDescending(a => a.StatementDate).ToList();
                        else if ((StmtStatus == "All") || (StmtStatus == ""))
                            statementList = account.tbStatement.Where(a => a.CreateDate <= DateTime.Now.AddSeconds(-predefinedPeriod) && a.StatementDate.AddMonths(13) >= now && a.StatementDate >= startDate && a.StatementDate <= endDate && ((a.statementFreq != null && docType.Contains(a.statementFreq)) || docType == "")).OrderByDescending(a => a.StatementDate).ToList();

                        if (statementList.Count > 0)
                        {
                            Boolean getFirstM = false;
                            Boolean getFirstD = false;
                            foreach (var statement in statementList)
                            {
                                Util.Log(logType.Info, String.Format("get matched statement with ID = \"{0}\" ; AccountID = \"{1}\" ; StatementDate = \"{2}\" ; statementFreq = \"{3}\"", statement.ID, statement.AccountID, statement.StatementDate, statement.statementFreq));

                                if (((statement.statementFreq.ToUpper() == "D") && getFirstD) ||
                                    ((statement.statementFreq.ToUpper() == "M") && getFirstM))
                                {
                                    Util.Log(logType.Info, ("pass statement - not first daily/monthly statement"));
                                    continue;
                                }

                                if ((account.Category.ToUpper() == "INFOCAST") &&
                                    ((statement.statementFreq.ToUpper() == "D") || (statement.statementFreq.ToUpper() == "A")))
                                {
                                    // Daily estatement : 90 days retention, e-Advice : 90 days retention
                                    if (statement.StatementDate.AddDays(90) < now)
                                    {
                                        Util.Log(logType.Info, ("pass statement - InfoCast 'D' or 'A', exceed 90 days retention"));
                                        continue;
                                    }
                                }

                                if (statement.statementFreq.ToUpper() == "D")
                                    getFirstD = true;
                                if (statement.statementFreq.ToUpper() == "M")
                                    getFirstM = true;

                                Class.statementType statementType = new Class.statementType();
                                statementType.rmid = rmid;
                                statementType.acctNo = account.AccountNumber;
                                statementType.displayAcctNo = account.DisplayAccountNumber;
                                statementType.stmtType = account.StatementType;
                                statementType.stmtDate = statement.StatementDate;
                                statementType.viewTime = statement.ViewTime;
                                statementType.stmtID = statement.ID.ToString();
                                statementType.marketInsertID = GetMarketInsertID(account.Category, account.StatementType, statement.StatementDate);
                                statementType.infoInsertID = GetInfoInsertID(account.Category, account.StatementType, statement.StatementDate);

                                // need get product type and product reference for advice type
                                if (statement.productInfo != null)
                                    statementType.productRef = statement.productInfo;
                                else
                                    statementType.productRef = "";

                                Util.Log(logType.Info, "GetEStatementSummary setting productRef end : " + statementType.productRef);
                                if (statement.statementFreq != null)
                                {
                                    if ((statement.statementFreq != "D") &&
                                       (statement.statementFreq != "M") &&
                                       (statement.statementFreq != "A"))
                                        statementType.productType = ""; // Not InfoCast
                                    else if (account.Category.ToUpper() == "INFOCAST")
                                        statementType.productType = "SK"; // InfoCast
                                    else
                                        statementType.productType = ""; // Not InfoCast
                                }
                                else
                                    statementType.productType = "";

                                if (statement.statementFreq != null)
                                    statementType.docType = statement.statementFreq;
                                else
                                    statementType.docType = "";

                                if (statement.productRef != null)
                                    statementType.announcementRef = statement.productRef;
                                else
                                    statementType.announcementRef = "";

                                statementType.jointFlag = Properties.Settings.Default.WSnormalAccountJointFlag;

                                // for InfoCast, if statementFreq = "A", return 3 more fields, Meta_Type & Order_Type & Asset_Grp, hardcode as "SK"
                                if ((account.Category.ToUpper() == "INFOCAST") && (statement.statementFreq == "A"))
                                {
                                    statementType.metaType = "SK";
                                    statementType.orderType = "SK";
                                    statementType.citicAssetGroup = "SK";
                                }

                                // for FixDeposit, return 3 more fields, Meta_Type & Order_Type & Asset_Grp, hardcode as "FD"
                                if (account.Category == "FixDeposit")
                                {
                                    statementType.metaType = "FD";
                                    statementType.orderType = "FD";
                                    statementType.citicAssetGroup = "FD";
                                    statementType.productType = "FD";
                                }

                                if (!String.IsNullOrEmpty(ProductType.Trim()))
                                    if (statementType.productType != ProductType)
                                    {
                                        Util.Log(logType.Info, String.Format("pass statement - statement ProductType = \"{0}\" not matched input ProductType = \"{1}\"", statement.Product_Type, ProductType));
                                        continue;
                                    }

                                statementTypeList.Add(statementType);
                                response.returnCode = "0";
                            }
                            Util.Log(logType.Info, String.Format("Normal record - get matched statement for AccountID = \"{0}\" finish", account.ID));
                        }
                        else
                            Util.Log(logType.Info, ("Normal record - cannot match any statement records in table tbStatement"));
                    }
                }
                else
                {
                    Util.Log(logType.Info, String.Format("Normal record - cannot match any account in table tbAccountList with RMID = \"{0}\"", rmid));
                }
                #endregion

                #region superbank & container statement
                List<tbBPInfo> bpList = new List<tbBPInfo>();
                List<tbBPInfo> containerBpList = new List<tbBPInfo>();
                List<tbBPInfo> superbankORcontainerIndicatorList = new List<tbBPInfo>();
                tbAccountList containerAccIntbAccountList = null;
                tbSubAccountList subAcc = null;
                Boolean isAllAcc = false;
                Boolean isSuperBankAcc = false;
                Boolean isContainerAcc = false;
                Boolean isSuperBankAccWithContainerNo = false;
                bpList.Clear();
                containerBpList.Clear();
                superbankORcontainerIndicatorList.Clear();

                if (String.IsNullOrEmpty(AcctNo.Trim()))
                {
                    bpList = Entity.tbBPInfo.Where(a => a.rmid == rmid).ToList();
                    if (bpList.Count() > 0)
                    {
                        superbankORcontainerIndicatorList = bpList;
                        isAllAcc = true;
                    }
                    else
                        Util.Log(logType.Info, String.Format("SuperBank record - cannot match any account in table tbBPInfo with rmid = \"{0}\"", rmid));
                }
                else
                {
                    bpList = Entity.tbBPInfo.Where(a => a.rmid == rmid && a.bpid == AcctNo).ToList();

                    if (bpList.Count() > 0)   // for superbank all record, AccNo is bpid
                    {
                        superbankORcontainerIndicatorList = bpList;
                        isSuperBankAcc = true;
                    }
                    else   // for superbank/container, AccNo is Container no.
                    {
                        containerAccIntbAccountList = Entity.tbAccountList.Where(a => a.AccountNumber == AcctNo && a.StatementType == Properties.Settings.Default.StatementTypeContainer).FirstOrDefault();
                        if (containerAccIntbAccountList != null)
                        {
                            // for container accounts
                            containerBpList = Entity.tbBPInfo.Where(a => a.rmid == rmid && a.bpid == containerAccIntbAccountList.RMID).ToList();
                            if (containerBpList.Count() > 0)
                            {
                                superbankORcontainerIndicatorList = containerBpList;
                                isContainerAcc = true;
                            }
                            else
                                Util.Log(logType.Info, String.Format("SuperBank record, special error(account exist in table tbAccountList) for container account - cannot match any account in table tbBPInfo with rmid = \"{0}\" ; bpid = \"{1}\"", rmid, containerAccIntbAccountList.RMID));
                        }
                        else
                        {
                            Util.Log(logType.Info, String.Format("SuperBank record - 1) cannot match any account in table tbBPInfo with rmid = \"{0}\" and bpid = \"{1}\"", rmid, AcctNo));
                            Util.Log(logType.Info, String.Format("OR 2) cannot match any account in table tbAccountList with RMID = \"{0}\" and AccountNumber (Container No.) = \"{1}\"", rmid, AcctNo));
                            Util.Log(logType.Info, String.Format("SuperBank record, start checking accounts in table tbSubAccountList with Category = \"{0}\" ; AccountNumber (Container No.) = \"{1}\"", Properties.Settings.Default.CategorySuperBank, AcctNo));
                        }

                        // for superbank accounts
                        List<tbSubAccountList> subAccList = Entity.tbSubAccountList.Where(a => a.Category == Properties.Settings.Default.CategorySuperBank && a.AccountNumber == AcctNo && a.Active == "Y").ToList();
                        if (subAccList.Count == 0)
                            Util.Log(logType.Info, String.Format("SuperBank record - cannot match any ACTIVE account in table tbSubAccountList with Category = \"{0}\" ; AccountNumber = \"{1}\"", Properties.Settings.Default.CategorySuperBank, AcctNo));
                        else if (subAccList.Count > 1)
                            Util.Log(logType.Info, String.Format("SuperBank record - cannot match UNIQUE ACTIVE account in table tbSubAccountList with AccountNumber = \"{0}\"", AcctNo));
                        else if (subAccList.Count == 1)
                        {
                            subAcc = subAccList[0];
                            List<tbBPInfo> superbankAccWithContainerNoBpList = Entity.tbBPInfo.Where(a => a.bpid == subAcc.bpid && a.rmid == rmid).ToList();

                            if (superbankAccWithContainerNoBpList.Count == 0)
                                Util.Log(logType.Info, String.Format("SuperBank record - cannot match any account in table tbBPInfo & tbSubAccountList with rmid = \"{0}\" ; bpid = \"{1}\" and Container No. = \"{2}\"", rmid, subAcc.bpid, AcctNo));
                            else
                            {
                                superbankORcontainerIndicatorList = superbankAccWithContainerNoBpList;
                                isSuperBankAccWithContainerNo = true;
                            }
                        }
                    }
                }

                foreach (var bp in superbankORcontainerIndicatorList)
                {
                    Util.Log(logType.Info, String.Format("SuperBank record - in table tbBPInfo, start processing record with ID = \"{0}\" ; bpid = \"{1}\" ; rmid = \"{2}\"", bp.ID, bp.bpid, bp.rmid));
                    List<tbAccountList> bpAccountsList = new List<tbAccountList>();
                    bpAccountsList.Clear();
                    if (isAllAcc)   // AcctNo is null or empty
                        bpAccountsList = Entity.tbAccountList.Where(a => a.RMID == bp.bpid && ((a.StatementType == Properties.Settings.Default.StatementTypeSuperBank) || (a.StatementType == Properties.Settings.Default.StatementTypeContainer))).ToList();
                    else if (isSuperBankAcc)
                        bpAccountsList = Entity.tbAccountList.Where(a => a.RMID == bp.bpid && a.StatementType == Properties.Settings.Default.StatementTypeSuperBank).ToList();
                    else if ((isContainerAcc) && (!isSuperBankAccWithContainerNo))
                        bpAccountsList = Entity.tbAccountList.Where(a => a.RMID == bp.bpid && a.StatementType == Properties.Settings.Default.StatementTypeContainer && a.AccountNumber == AcctNo).ToList();
                    else if ((!isContainerAcc) && (isSuperBankAccWithContainerNo))
                        bpAccountsList = Entity.tbAccountList.Where(a => a.ID == subAcc.AccountID).ToList();
                    else if ((isContainerAcc) && (isSuperBankAccWithContainerNo))
                    {
                        bpAccountsList = Entity.tbAccountList.Where(a => a.RMID == bp.bpid && a.StatementType == Properties.Settings.Default.StatementTypeContainer && a.AccountNumber == AcctNo || a.ID == subAcc.AccountID).ToList();
                    }

                    foreach (var account in bpAccountsList)
                    {
                        response.hasSubscription = "Y";
                        Util.Log(logType.Info, String.Format("SuperBank record - start searching statement with AccountID = \"{0}\" ; RMID (bpid) = \"{1}\" ; AccountNumber = \"{2}\" ; StatementType = \"{3}\"", account.ID, account.RMID, account.AccountNumber, account.StatementType));
                        int predefinedPeriod = 0;

                        if (isProofPrint.Equals("N"))
                        {
                            tbSystemSetting systemSetting = Entity.tbSystemSetting.Where(a => a.SettingName == "PredefinedPeriod" && a.StatementType.ToUpper() == account.StatementType.ToUpper() && a.Category.ToUpper() == account.Category.ToUpper() && a.EffectiveFrom <= DateTime.Now && a.EffectiveTo >= DateTime.Now).FirstOrDefault();
                            if (systemSetting != null)
                            {
                                if (!int.TryParse(systemSetting.SettingValue, out predefinedPeriod))
                                {
                                    predefinedPeriod = 86400;
                                    Util.Log(logType.Warning, String.Format("GetEStatementSummary. Invalid PredefinedPeriod."));
                                }
                            }
                            else
                            {
                                Util.Log(logType.Warning, String.Format("GetEStatementSummary. PredefinedPeriod not found. rmid:{0}.", account.RMID));
                            }
                        }

                        List<tbStatement> statementList = new List<tbStatement>();
                        // 4 checking : grace period 1 day ; statement last for 1 year ; StatementDate >= input StmtPeriodStart ; StatementDate <= input StmtPeriodEnd
                        if (StmtStatus == "Not Read")
                            statementList = account.tbStatement.Where(a => a.CreateDate <= DateTime.Now.AddSeconds(-predefinedPeriod) && a.StatementDate.AddMonths(13) >= now && a.StatementDate >= startDate && a.StatementDate <= endDate && ((a.statementFreq != null && docType.Contains(a.statementFreq)) || docType == "") && a.ViewTime == null).OrderByDescending(a => a.StatementDate).ToList();
                        else if (StmtStatus == "Read")
                            statementList = account.tbStatement.Where(a => a.CreateDate <= DateTime.Now.AddSeconds(-predefinedPeriod) && a.StatementDate.AddMonths(13) >= now && a.StatementDate >= startDate && a.StatementDate <= endDate && ((a.statementFreq != null && docType.Contains(a.statementFreq)) || docType == "") && a.ViewTime != null).OrderByDescending(a => a.StatementDate).ToList();
                        else if ((StmtStatus == "All") || (StmtStatus == ""))
                            statementList = account.tbStatement.Where(a => a.CreateDate <= DateTime.Now.AddSeconds(-predefinedPeriod) && a.StatementDate.AddMonths(13) >= now && a.StatementDate >= startDate && a.StatementDate <= endDate && ((a.statementFreq != null && docType.Contains(a.statementFreq)) || docType == "")).OrderByDescending(a => a.StatementDate).ToList();

                        if (((!isContainerAcc) && (isSuperBankAccWithContainerNo)) || ((isContainerAcc) && (isSuperBankAccWithContainerNo)))
                            if (account.ID == subAcc.AccountID)
                                statementList = statementList.Where(a => a.ContainerAcno == AcctNo).OrderByDescending(a => a.StatementDate).ToList();

                        if (statementList.Count > 0)
                        {
                            Boolean getFirstM = false;
                            Boolean getFirstD = false;
                            foreach (var statement in statementList)
                            {
                                Util.Log(logType.Info, String.Format("get matched statement with ID = \"{0}\" ; AccountID = \"{1}\" ; StatementDate = \"{2}\" ; statementFreq = \"{3}\"", statement.ID, statement.AccountID, statement.StatementDate, statement.statementFreq));

                                if (!String.IsNullOrEmpty(ProductType.Trim()))
                                    if (statement.Product_Type != ProductType)
                                    {
                                        Util.Log(logType.Info, String.Format("pass statement - statement ProductType = \"{0}\" not matched input ProductType = \"{1}\"", statement.Product_Type, ProductType));
                                        continue;
                                    }

                                if (((statement.statementFreq.ToUpper() == "D") && getFirstD) ||
                                    ((statement.statementFreq.ToUpper() == "M") && getFirstM))
                                {
                                    Util.Log(logType.Info, ("pass statement - not first daily/monthly statement"));
                                    continue;
                                }

                                if ((statement.statementFreq.ToUpper() == "D") || (statement.statementFreq.ToUpper() == "A"))
                                {
                                    // Daily estatement : 90 days retention, e-Advice : 90 days retention
                                    if (statement.StatementDate.AddDays(90) < now)
                                    {
                                        Util.Log(logType.Info, ("pass statement - SuperBank 'D' or 'A', exceed 90 days retention"));
                                        continue;
                                    }
                                }

                                if (statement.statementFreq.ToUpper() == "D")
                                    getFirstD = true;
                                if (statement.statementFreq.ToUpper() == "M")
                                    getFirstM = true;
                                Class.statementType statementType = new Class.statementType();
                                statementType.rmid = rmid;
                                statementType.acctNo = account.AccountNumber;
                                if (account.Category == Properties.Settings.Default.CategorySuperBank)
                                {
                                    if (statement.statementFreq.ToUpper() == "M")   // for SBS 'M', DisplayAccountNumber return one-account-number(10 digits)
                                    {
                                        tbSubAccountList tempSubAcc = Entity.tbSubAccountList.Where(a => a.Category == Properties.Settings.Default.CategorySuperBank && a.bpid == account.AccountNumber && a.Active == "Y").FirstOrDefault();
                                        if (tempSubAcc != null)
                                        {
                                            statementType.displayAcctNo = tempSubAcc.AccountNumber.Substring(0, 10);
                                        }
                                        else
                                        {
                                            statementType.displayAcctNo = account.DisplayAccountNumber;
                                            Util.Log(logType.Info, ("Cannot map corresponding superbank record in tbSubAccountList, displayAcctNo return BPID instead of one account number"));
                                        }
                                    }
                                    else    // for SBS 'D' & 'A', DisplayAccountNumber return container number(12 digits)
                                    {
                                        if (statement.ContainerAcno.Count() == 12)
                                            statementType.displayAcctNo = statement.ContainerAcno.Substring(0, 3) + "-" + statement.ContainerAcno[3] + "-" + statement.ContainerAcno.Substring(4, 6) + "-" + statement.ContainerAcno.Substring(10, 2);
                                        else
                                        {
                                            statementType.displayAcctNo = account.DisplayAccountNumber;
                                            Util.Log(logType.Info, String.Format("Incorrect length of ContainerAcno in tbStatement (required is 12), ContainerAcno = {0} ; displayAcctNo return BPID instead of container number", statement.ContainerAcno));
                                        }
                                    }
                                }
                                else
                                    statementType.displayAcctNo = account.DisplayAccountNumber;
                                statementType.stmtType = account.StatementType;
                                statementType.stmtDate = statement.StatementDate;
                                statementType.viewTime = statement.ViewTime;
                                statementType.stmtID = statement.ID.ToString();
                                statementType.marketInsertID = GetMarketInsertID(account.Category, account.StatementType, statement.StatementDate);
                                statementType.infoInsertID = GetInfoInsertID(account.Category, account.StatementType, statement.StatementDate);

                                // need get product type and product reference for advice type
                                if (statement.productInfo != null)
                                    statementType.productRef = statement.productInfo;
                                else
                                    statementType.productRef = "";

                                Util.Log(logType.Info, "GetEStatementSummary setting productRef end : " + statementType.productRef);
                                if (statement.statementFreq != null)
                                {
                                    if ((statement.statementFreq != "D") &&
                                       (statement.statementFreq != "M") &&
                                       (statement.statementFreq != "A"))
                                        statementType.productType = ""; // Not InfoCast
                                    else
                                    {
                                        if ((statement.statementFreq == "A") || (statement.statementFreq == "D"))
                                        {
                                            if (statement.Product_Type != null)
                                                statementType.productType = statement.Product_Type;
                                            else
                                                statementType.productType = "";
                                        }
                                        else
                                            statementType.productType = "";
                                    }
                                }
                                else
                                    statementType.productType = "";

                                if (statement.statementFreq != null)
                                    statementType.docType = statement.statementFreq;
                                else
                                    statementType.docType = "";

                                if (statement.productRef != null)
                                    statementType.announcementRef = statement.productRef;
                                else
                                    statementType.announcementRef = "";

                                statementType.bpID = bp.bpid;
                                statementType.jointFlag = (bp.AcctType == "Individual") ? "IND" : bp.AcctType;

                                if (bp.BP_name != null)
                                    statementType.bpName = bp.BP_name;
                                else
                                    statementType.bpName = "";

                                if (statement.Meta_Type != null)
                                    statementType.metaType = statement.Meta_Type;
                                else
                                    statementType.metaType = "";

                                if (statement.Order_Type != null)
                                    statementType.orderType = statement.Order_Type;
                                else
                                    statementType.orderType = "";

                                if (statement.Asset_Grp != null)
                                    statementType.citicAssetGroup = statement.Asset_Grp;
                                else
                                    statementType.citicAssetGroup = "";

                                if (statement.Asset_Name_EN != null)
                                    statementType.prdouctNameEn = statement.Asset_Name_EN;
                                else
                                    statementType.prdouctNameEn = "";

                                if (statement.Asset_Name_SC != null)
                                    statementType.prdouctNameSc = statement.Asset_Name_SC;
                                else
                                    statementType.prdouctNameSc = "";

                                if (statement.Asset_Name_TC != null)
                                    statementType.prdouctNameTc = statement.Asset_Name_TC;
                                else
                                    statementType.prdouctNameTc = "";

                                statementTypeList.Add(statementType);
                                response.returnCode = "0";
                            }
                            Util.Log(logType.Info, String.Format("SuperBank record - get matched statement for AccountID = \"{0}\" finish", account.ID));
                        }
                        else
                            Util.Log(logType.Info, ("SuperBank record - cannot match any statement records in table tbStatement"));
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, string.Format("Error in GetEStatementSummary. rmid:{0},isProofPrint:{1}. Error message:{2}.", rmid, isProofPrint, ex.Message));
                response.returnCode = "2";
            }
            Util.Log(logType.Info, "GetEStatementSummary End, returnCode = " + response.returnCode + ", hasSubscription = " + response.hasSubscription);
            response.statementTypes.AddRange(statementTypeList.OrderByDescending(a => a.stmtDate));
            return response;
        }

        [WebMethod]
        public Class.getEStatementHistoryResult GetEStatementHistory(String rmid, String acctNo, String stmtType, String isProofPrint, String docType)
        {
            Util.Log(logType.Info, "==================================================================================================================");
            Util.Log(logType.Info, String.Format("GetEStatementHistory Start. RMID=\"{0}\" ; AcctNo=\"{1}\" ; StmtType=\"{2}\" ; IsProofPrint={3} ; DocType={4}.",
                rmid, acctNo, stmtType,
                (isProofPrint == null) ? "null" : ("\"" + isProofPrint + "\""),
                (docType == null) ? "null" : ("\"" + docType + "\"")));

            Class.getEStatementHistoryResult response = new Class.getEStatementHistoryResult();
            response.returnCode = "1";
            DateTime now = DateTime.Now;
            stmtType = stmtType.ToUpper();

            // allow null input
            if (isProofPrint == null)
                isProofPrint = "";
            if (docType == null)
                docType = "";
            else
                docType = docType.ToUpper();

            // Add debug log for 20170723 production problem 
            Boolean nowConfig = false;
            if (!String.IsNullOrEmpty(Properties.Settings.Default.estatementhistory) && Properties.Settings.Default.estatementhistory != "19000101")
            {
                Util.Log(logType.Warning, String.Format("GetEStatementHistory. (yyyyMMdd)estatementhistory value:{0}.", Properties.Settings.Default.estatementhistory));
                if (!DateTime.TryParseExact(Properties.Settings.Default.estatementhistory, "yyyyMMdd", CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out now))
                {
                    now = DateTime.Now;
                    Util.Log(logType.Warning, String.Format("GetEStatementHistory. Invalid estatementhistory Date Format yyyyMMdd {0} .", Properties.Settings.Default.estatementhistory));
                }
                else
                {
                    // Add debug log for 20170723 production problem 
                    nowConfig = true;
                }
            }

            try
            {
                if (String.IsNullOrEmpty(stmtType.Trim()))
                    Util.Log(logType.Info, String.Format("Invalid StmtType = \"{0}\", please input a valid StmtType", stmtType));
                else if ((stmtType != Properties.Settings.Default.StatementTypeSuperBank) && (stmtType != Properties.Settings.Default.StatementTypeContainer))
                {
                    #region normal account statement (citicfirst,infocast,creditcard)
                    tbAccountList account = Entity.tbAccountList.Where(a => a.RMID == rmid && a.AccountNumber == acctNo && a.StatementType.ToUpper() == stmtType && a.StatementType.ToUpper() != Properties.Settings.Default.StatementTypeSuperBank && a.StatementType.ToUpper() != Properties.Settings.Default.StatementTypeContainer).FirstOrDefault();
                    if (account != null)
                    {
                        Util.Log(logType.Info, String.Format("Normal record - start searching statement with AccountID = \"{0}\" ; RMID = \"{1}\" ; AccountNumber = \"{2}\" ; StatementType = \"{3}\"", account.ID, account.RMID, account.AccountNumber, account.StatementType));
                        int predefinedPeriod = 0;

                        if (isProofPrint.Equals("N"))
                        {
                            tbSystemSetting systemSetting = Entity.tbSystemSetting.Where(a => a.SettingName == "PredefinedPeriod" && a.StatementType.ToUpper() == account.StatementType && a.Category == account.Category && a.EffectiveFrom <= now && a.EffectiveTo >= now).FirstOrDefault();
                            if (systemSetting != null)
                            {
                                if (!int.TryParse(systemSetting.SettingValue, out predefinedPeriod))
                                {
                                    predefinedPeriod = 86400;
                                    Util.Log(logType.Warning, String.Format("GetEStatementSummary. Invalid PredefinedPeriod."));
                                }
                            }
                            else
                            {
                                Util.Log(logType.Warning, String.Format("GetEStatementSummary. PredefinedPeriod not found. rmid:{0}.", account.RMID));
                            }
                        }

                        String statementDate = "";

                        if (now.Month < 10)
                            statementDate = now.Year + "" + "0" + now.Month + "01";
                        else
                            statementDate = now.Year + "" + now.Month + "01";

                        String statementDate1 = "";

                        if (now.Month < 10)
                            statementDate1 = now.Year + "" + "0" + now.Month;
                        else
                            statementDate1 = now.Year + "" + now.Month;

                        if (now.Day < 10)
                            statementDate1 = statementDate1 + "0" + now.Day;
                        else
                            statementDate1 = statementDate1 + now.Day;

                        List<tbStatement> statementList = null;

                        // Add debug log for 20170723 production problem 
                        if (nowConfig)
                            Util.Log(logType.Info, String.Format("GetEStatementHistory(By Config) - now:{0} statementDate:{1} statementDate1:{2}", now.ToString("yyyyMMdd"), statementDate, statementDate1));
                        else
                            Util.Log(logType.Info, String.Format("GetEStatementHistory - now:{0} statementDate:{1} statementDate1:{2}", now.ToString("yyyyMMdd"), statementDate, statementDate1));

                        // for monthly statement, keep 13 months
                        statementList = account.tbStatement.Where(a =>
                            a.CreateDate <= DateTime.Now.AddSeconds(-predefinedPeriod)
                            && (a.StatementDate.AddMonths(13) >= DateTime.ParseExact(statementDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture))
                            && ((a.statementFreq != null && docType.Contains(a.statementFreq)) || docType == "")
                            )
                            .OrderByDescending(c => c.StatementDate).ToList();

                        if (statementList.Count > 0)
                        {
                            foreach (var statement in statementList)
                            {
                                Util.Log(logType.Info, String.Format("get matched statement with ID = \"{0}\" ; AccountID = \"{1}\" ; StatementDate = \"{2}\" ; statementFreq = \"{3}\"", statement.ID, statement.AccountID, statement.StatementDate, statement.statementFreq));

                                Class.statementHistory history = new Class.statementHistory();
                                history.rmid = rmid;
                                history.acctNo = acctNo;
                                history.displayAcctNo = account.DisplayAccountNumber;
                                history.stmtType = account.StatementType;

                                if (account.Category.ToUpper() == "INFOCAST")
                                {
                                    // Daily estatement : 90 days retention, e-Advice : 90 days retention
                                    if ((statement.statementFreq.ToUpper() == "D") || (statement.statementFreq.ToUpper() == "A"))
                                    {
                                        if (statement.StatementDate.AddDays(90) < DateTime.ParseExact(statementDate1, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture))
                                        {
                                            Util.Log(logType.Info, ("pass statement - InfoCast 'D' or 'A', exceed 90 days retention"));
                                            continue;
                                        }
                                    }
                                }

                                history.stmtDate = statement.StatementDate;
                                history.viewTime = statement.ViewTime;
                                history.stmtID = statement.ID.ToString();
                                history.marketInsertID = GetMarketInsertID(account.Category, account.StatementType, statement.StatementDate);
                                history.infoInsertID = GetInfoInsertID(account.Category, account.StatementType, statement.StatementDate);
                                // need get product type and product reference for advice type
                                if (statement.productRef != null)
                                    history.productRef = statement.productRef;
                                else
                                    history.productRef = "";

                                if (statement.productInfo != null)
                                    history.productType = statement.productInfo;
                                else
                                    history.productType = "";

                                history.docType = statement.statementFreq;
                                history.jointFlag = Properties.Settings.Default.WSnormalAccountJointFlag;

                                response.statementHistories.Add(history);
                                response.returnCode = "0";
                            }
                        }
                        else
                            Util.Log(logType.Info, ("Normal record - cannot match any statement records in table tbStatement"));
                    }
                    else
                        Util.Log(logType.Info, String.Format("Normal record - cannot match any account in table tbAccountList with RMID = \"{0}\" ; AcctNo = \"{1}\" ; StmtType = \"{2}\"", rmid, acctNo, stmtType));
                    #endregion
                }
                else
                {
                    #region superbank & container statement
                    tbBPInfo bpAccount = null;
                    tbAccountList account = null;

                    if (stmtType == Properties.Settings.Default.StatementTypeSuperBank)
                    {
                        bpAccount = Entity.tbBPInfo.Where(a => a.rmid == rmid && a.bpid == acctNo).FirstOrDefault();
                        if (bpAccount == null)
                            Util.Log(logType.Info, String.Format("SuperBank record - cannot match any account in table tbBPInfo with rmid = \"{0}\" ; bpid = \"{1}\"", rmid, acctNo));
                        else
                        {
                            account = Entity.tbAccountList.Where(a => a.RMID == acctNo && a.StatementType == stmtType).FirstOrDefault();
                            if (account == null)
                                Util.Log(logType.Info, String.Format("SuperBank record - cannot match any account in table tbAccountList with RMID (bpid) = \"{0}\" ; StmtType = \"{1}\"", acctNo, stmtType));
                        }
                    }
                    else if (stmtType == Properties.Settings.Default.StatementTypeContainer)
                    {
                        account = Entity.tbAccountList.Where(a => a.AccountNumber == acctNo && a.StatementType == stmtType).FirstOrDefault();
                        if (account == null)
                            Util.Log(logType.Info, String.Format("SuperBank record - cannot match any account in table tbAccountList with AccountNumber (Conatiner No.) = \"{0}\" ; StmtType = \"{1}\"", acctNo, stmtType));
                        else
                        {
                            bpAccount = Entity.tbBPInfo.Where(a => a.rmid == rmid && a.bpid == account.RMID).FirstOrDefault();
                            if (bpAccount == null)
                                Util.Log(logType.Info, String.Format("SuperBank record, special error(account exist in table tbAccountList) for container account - cannot match any account in table tbBPInfo with rmid = \"{0}\" ; bpid = \"{1}\"", rmid, account.RMID));
                        }
                    }

                    if ((account != null) && (bpAccount != null))
                    {
                        Util.Log(logType.Info, String.Format("SuperBank record - start searching statement with AccountID = \"{0}\" ; RMID = \"{1}\" ; AccountNumber = \"{2}\" ; StatementType = \"{3}\"", account.ID, account.RMID, account.AccountNumber, account.StatementType));
                        int predefinedPeriod = 0;

                        if (isProofPrint.Equals("N"))
                        {
                            tbSystemSetting systemSetting = Entity.tbSystemSetting.Where(a => a.SettingName == "PredefinedPeriod" && a.StatementType.ToUpper() == account.StatementType && a.Category == account.Category && a.EffectiveFrom <= now && a.EffectiveTo >= now).FirstOrDefault();
                            if (systemSetting != null)
                            {
                                if (!int.TryParse(systemSetting.SettingValue, out predefinedPeriod))
                                {
                                    predefinedPeriod = 86400;
                                    Util.Log(logType.Warning, String.Format("GetEStatementSummary. Invalid PredefinedPeriod."));
                                }
                            }
                            else
                            {
                                Util.Log(logType.Warning, String.Format("GetEStatementSummary. PredefinedPeriod not found. rmid:{0}.", account.RMID));
                            }
                        }

                        String statementDate = "";

                        if (now.Month < 10)
                            statementDate = now.Year + "" + "0" + now.Month + "01";
                        else
                            statementDate = now.Year + "" + now.Month + "01";

                        String statementDate1 = "";

                        if (now.Month < 10)
                            statementDate1 = now.Year + "" + "0" + now.Month;
                        else
                            statementDate1 = now.Year + "" + now.Month;

                        if (now.Day < 10)
                            statementDate1 = statementDate1 + "0" + now.Day;
                        else
                            statementDate1 = statementDate1 + now.Day;


                        List<tbStatement> statementList = null;

                        // Add debug log for 20170723 production problem 
                        if (nowConfig)
                            Util.Log(logType.Info, String.Format("GetEStatementHistory(By Config) - now:{0} statementDate:{1} statementDate1:{2}", now.ToString("yyyyMMdd"), statementDate, statementDate1));
                        else
                            Util.Log(logType.Info, String.Format("GetEStatementHistory - now:{0} statementDate:{1} statementDate1:{2}", now.ToString("yyyyMMdd"), statementDate, statementDate1));

                        // for monthly statement, keep 13 months
                        statementList = account.tbStatement.Where(a =>
                            a.CreateDate <= DateTime.Now.AddSeconds(-predefinedPeriod)
                            && (a.StatementDate.AddMonths(13) >= DateTime.ParseExact(statementDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture))
                            && ((a.statementFreq != null && docType.Contains(a.statementFreq)) || docType == "")
                            )
                            .OrderByDescending(c => c.StatementDate).ToList();

                        if (statementList.Count > 0)
                        {
                            foreach (var statement in statementList)
                            {
                                Util.Log(logType.Info, String.Format("get matched statement with ID = \"{0}\" ; AccountID = \"{1}\" ; StatementDate = \"{2}\" ; statementFreq = \"{3}\"", statement.ID, statement.AccountID, statement.StatementDate, statement.statementFreq));

                                Class.statementHistory history = new Class.statementHistory();
                                history.rmid = rmid;
                                history.acctNo = account.AccountNumber;
                                if (account.Category == Properties.Settings.Default.CategorySuperBank)
                                {
                                    if (statement.statementFreq.ToUpper() == "M")   // for SBS 'M', DisplayAccountNumber return one-account-number(10 digits)
                                    {
                                        tbSubAccountList tempSubAcc = Entity.tbSubAccountList.Where(a => a.Category == Properties.Settings.Default.CategorySuperBank && a.bpid == account.AccountNumber && a.Active == "Y").FirstOrDefault();
                                        if (tempSubAcc != null)
                                        {
                                            history.displayAcctNo = tempSubAcc.AccountNumber.Substring(0, 10);
                                        }
                                        else
                                        {
                                            history.displayAcctNo = account.DisplayAccountNumber;
                                            Util.Log(logType.Info, ("Cannot map corresponding superbank record in tbSubAccountList, displayAcctNo return BPID instead of one account number"));
                                        }
                                    }
                                    else    // for SBS 'D' & 'A', DisplayAccountNumber return container number(12 digits)
                                    {
                                        if (statement.ContainerAcno.Count() == 12)
                                            history.displayAcctNo = statement.ContainerAcno.Substring(0, 3) + "-" + statement.ContainerAcno[3] + "-" + statement.ContainerAcno.Substring(4, 6) + "-" + statement.ContainerAcno.Substring(10, 2);
                                        else
                                        {
                                            history.displayAcctNo = account.DisplayAccountNumber;
                                            Util.Log(logType.Info, String.Format("Incorrect length of ContainerAcno in tbStatement (required is 12), ContainerAcno = {0} ; displayAcctNo return BPID instead of container number", statement.ContainerAcno));
                                        }
                                    }
                                }
                                else
                                    history.displayAcctNo = account.DisplayAccountNumber;
                                history.stmtType = account.StatementType;

                                if ((statement.statementFreq.ToUpper() == "D") || (statement.statementFreq.ToUpper() == "A"))
                                {
                                    // Daily estatement : 90 days retention, e-Advice : 90 days retention
                                    if (statement.StatementDate.AddDays(90) < DateTime.ParseExact(statementDate1, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture))
                                    {
                                        Util.Log(logType.Info, ("pass statement - SuperBank 'D' or 'A', exceed 90 days retention"));
                                        continue;
                                    }
                                }

                                history.stmtDate = statement.StatementDate;
                                history.viewTime = statement.ViewTime;
                                history.stmtID = statement.ID.ToString();
                                history.marketInsertID = GetMarketInsertID(account.Category, account.StatementType, statement.StatementDate);
                                history.infoInsertID = GetInfoInsertID(account.Category, account.StatementType, statement.StatementDate);

                                // need get product type and product reference for advice type
                                if (statement.productRef != null)
                                    history.productRef = statement.productRef;
                                else
                                    history.productRef = "";

                                if ((statement.statementFreq == "A") || (statement.statementFreq == "D"))
                                {
                                    history.productType = statement.Product_Type;
                                }

                                history.docType = statement.statementFreq;
                                history.jointFlag = (bpAccount.AcctType == "Individual") ? "IND" : bpAccount.AcctType;

                                if (bpAccount.BP_name != null)
                                    history.bpName = bpAccount.BP_name;
                                else
                                    history.bpName = "";

                                response.statementHistories.Add(history);
                                response.returnCode = "0";
                            }
                        }
                        else
                            Util.Log(logType.Info, ("SuperBank record - cannot match any statement records in table tbStatement"));
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, string.Format("Error in GetEStatementHistory. rmid:{0},acctNo:{1},stmtType:{2},isProofPrint:{3}. Error message:{4}.", rmid, acctNo, stmtType, isProofPrint, ex.Message));
            }
            Util.Log(logType.Info, "GetEStatementHistory End.");
            return response;
        }

        [WebMethod]
        public Class.getSubscriptionStatusResult GetSubscriptionStatus(String rmid)
        {
            Util.Log(logType.Info, "==================================================================================================================");
            Util.Log(logType.Info, String.Format("GetSubscriptionStatus Start. rmid:{0}.", rmid));
            Class.getSubscriptionStatusResult response = new Class.getSubscriptionStatusResult();
            response.returnCode = "1";
            Dictionary<String, String> categoryCheckedDict = new Dictionary<String, String>();
            Int16 totalAccountReturn = 0;

            try
            {
                List<tbAccountList> recordList = Entity.tbAccountList.Where(a => a.RMID == rmid && ((a.StatementType != Properties.Settings.Default.StatementTypeSuperBank) && (a.StatementType != Properties.Settings.Default.StatementTypeContainer)) && a.Active == "Y").ToList();

                // return normal account info
                if (recordList.Count > 0)
                {
                    Util.Log(logType.Info, String.Format("Normal record - matched {0} account(s) in table tbAccountList with RMID = \"{1}\"", recordList.Count, rmid));
                    foreach (var record in recordList)
                    {
                        Util.Log(logType.Info, String.Format("get matched account with ID = \"{0}\" ; RegistrationStatus = \"{1}\" ; RMID = \"{2}\" ; AccountNumber = \"{3}\" ; Category = \"{4}\" ; StatementType = \"{5}\"", record.ID, record.RegistrationStatus, record.RMID, record.AccountNumber, record.Category, record.StatementType));

                        Class.subscriptionStatus status = new Class.subscriptionStatus();
                        status.rmid = rmid;
                        status.acctNo = record.AccountNumber;
                        status.displayAcctNo = record.DisplayAccountNumber;
                        status.stmtType = record.StatementType;
                        status.subStatus = record.RegistrationStatus;
                        status.jointFlag = Properties.Settings.Default.WSnormalAccountJointFlag;

                        response.subscriptionStatuses.Add(status);
                        totalAccountReturn++;

                        // directbank add account
                        #region Directbank add account
                        if (!categoryCheckedDict.ContainsKey(record.Category))
                            categoryCheckedDict.Add(record.Category, "");
                        else
                            continue;

                        List<tbDirectBankMapping> dbMapList = Entity.tbDirectBankMapping.Where(a => a.CategoryToMap == record.Category && a.CheckCategory == "Y").ToList();
                        if (dbMapList.Count > 0)
                        {
                            foreach (var dbMap in dbMapList)
                            {
                                tbAccountList accountCheck = Entity.tbAccountList.Where(a => a.RMID == rmid && a.Category == dbMap.Category && a.StatementType == dbMap.StatementType).FirstOrDefault();

                                if (accountCheck == null)
                                {
                                    tbAccountList newRecord = new tbAccountList();
                                    newRecord.RMID = rmid;
                                    newRecord.AccountNumber = "";
                                    newRecord.DisplayAccountNumber = "";
                                    newRecord.Category = dbMap.Category;
                                    newRecord.StatementType = dbMap.StatementType;
                                    newRecord.RegistrationStatus = "S";
                                    newRecord.Active = "Y";
                                    newRecord.CreateBy = Properties.Settings.Default.AppName;
                                    newRecord.ModifyBy = Properties.Settings.Default.AppName;
                                    newRecord.CreateDate = DateTime.Now;
                                    newRecord.LastUpdate = DateTime.Now;
                                    newRecord.statusUpdateBy = "WS";
                                    Entity.AddTotbAccountList(newRecord);
                                    Entity.SaveChanges();

                                    Util.Log(logType.Info, String.Format("(GetSubscriptionStatus build account) add account into tbAccountList with RMID = \"{0}\" ; Category = \"{1}\" ; StatementType = \"{2}\"", rmid, dbMap.Category, dbMap.StatementType));
                                }
                                else
                                    Util.Log(logType.Info, String.Format("(GetSubscriptionStatus build account) fail to add account into tbAccountList, an account with RMID = \"{0}\" ; Category = \"{1}\" ; StatementType = \"{2}\" already exist", rmid, dbMap.Category, dbMap.StatementType));
                            }
                        }
                        #endregion
                    }
                    Util.Log(logType.Info, "Normal record - get matched account finish");
                }
                else
                    Util.Log(logType.Info, String.Format("Normal record - cannot match any ACTIVE account in table tbAccountList with RMID = \"{0}\" ; Active = \"Y\"", rmid));

                List<tbBPInfo> tbBPInfoList = Entity.tbBPInfo.Where(a => a.rmid == rmid).ToList();
                List<tbAccountList> bpAccountList = new List<tbAccountList>();

                // return superbank account info
                if (tbBPInfoList.Count > 0)
                {
                    Util.Log(logType.Info, String.Format("SuperBank record - matched {0} account(s) in table tbBPinfo with rmid = \"{1}\"", tbBPInfoList.Count, rmid));
                    foreach (var tbBPInfo in tbBPInfoList)
                    {
                        Util.Log(logType.Info, String.Format("SuperBank record - in table tbBPInfo, start processing record with ID = \"{0}\" ; bpid = \"{1}\" ; rmid = \"{2}\"", tbBPInfo.ID, tbBPInfo.bpid, tbBPInfo.rmid));
                        bpAccountList = Entity.tbAccountList.Where(a => a.RMID == tbBPInfo.bpid && ((a.StatementType == Properties.Settings.Default.StatementTypeSuperBank) || (a.StatementType == Properties.Settings.Default.StatementTypeContainer)) && a.Active == "Y").OrderBy(o => o.Category).ThenBy(o => o.StatementType).ThenBy(o => o.AccountNumber).ToList();

                        if (bpAccountList.Count > 0)
                        {
                            foreach (var account in bpAccountList)
                            {
                                Util.Log(logType.Info, String.Format("get matched account with ID = \"{0}\" ; RegistrationStatus = \"{1}\" ; RMID = \"{2}\" ; AccountNumber = \"{3}\" ; Category = \"{4}\" ; StatementType = \"{5}\"", account.ID, account.RegistrationStatus, account.RMID, account.AccountNumber, account.Category, account.StatementType));

                                Class.subscriptionStatus status = new Class.subscriptionStatus();
                                status.rmid = rmid;
                                status.acctNo = account.AccountNumber;
                                if (account.Category == Properties.Settings.Default.CategorySuperBank)  // for SBS, DisplayAccountNumber return one-account-number(10 digits)
                                {
                                    tbSubAccountList tempSubAcc = Entity.tbSubAccountList.Where(a => a.Category == Properties.Settings.Default.CategorySuperBank && a.bpid == account.AccountNumber && a.Active == "Y").FirstOrDefault();
                                    if (tempSubAcc != null)
                                    {
                                        status.displayAcctNo = tempSubAcc.AccountNumber.Substring(0, 10);
                                    }
                                    else
                                    {
                                        status.displayAcctNo = account.DisplayAccountNumber;
                                        Util.Log(logType.Info, ("Cannot map corresponding superbank record in tbSubAccountList, displayAcctNo return BPID instead of one account number"));
                                    }
                                }
                                else
                                    status.displayAcctNo = account.DisplayAccountNumber;
                                status.stmtType = account.StatementType;
                                status.subStatus = account.RegistrationStatus;
                                status.jointFlag = (tbBPInfo.AcctType == "Individual") ? "IND" : tbBPInfo.AcctType;
                                if (tbBPInfo.BP_name != null)
                                    status.bpName = tbBPInfo.BP_name;
                                else
                                    status.bpName = "";

                                response.subscriptionStatuses.Add(status);
                                totalAccountReturn++;
                            }
                            Util.Log(logType.Info, String.Format("SuperBank record - for RMID (bpid) = {0}, get matched account finish", tbBPInfo.bpid));
                        }
                        else
                            Util.Log(logType.Info, String.Format("SuperBank record - cannot match any ACTIVE account in table tbAccountList with RMID (bpid) = \"{0}\" ; Active = \"Y\"", tbBPInfo.bpid));
                    }
                    Util.Log(logType.Info, "SuperBank record - get matched account finish");
                }
                else
                    Util.Log(logType.Info, String.Format("SuperBank record - cannot match any account in table tbBPinfo with rmid = \"{0}\"", rmid));

                response.returnCode = ((recordList.Count() + bpAccountList.Count()) > 0) ? "0" : "1";
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in GetSubscriptionStatus. rmid:{0}. Error message:{1}.", rmid, ex.Message));
            }
            Util.Log(logType.Info, "GetSubscriptionStatus - Total number of records return = " + totalAccountReturn);
            Util.Log(logType.Info, "GetSubscriptionStatus End.");
            return response;
        }

        [WebMethod]
        public Class.changeSubscriptionStatusResult ChangeSubscriptionStatus(List<Class.subscriptionStatus> changeSubscriptionStatuses)
        {
            Util.Log(logType.Info, "==================================================================================================================");
            Boolean auto_insert = true; // auto insert record to tbAccountList if account number not found
            Class.changeSubscriptionStatusResult Response = new Class.changeSubscriptionStatusResult();
            Response.returnCode = "0";

            try
            {
                foreach (Class.subscriptionStatus request in changeSubscriptionStatuses)
                {
                    Util.Log(logType.Info, String.Format("ChangeSubscriptionStatus Start. RMID=\"{0}\" ; AcctNo=\"{1}\" ; StmtType=\"{2}\" ; SubStatus=\"{3}\" ; ActionSys={4} ; ActionID={5}.",
                        request.rmid, request.acctNo, request.stmtType, request.subStatus,
                        (request.actionSys == null) ? "null" : ("\"" + request.actionSys + "\""),
                        (request.actionId == null) ? "null" : ("\"" + request.actionId + "\"")));

                    Response.rmid = request.rmid;
                    String displayAcno = String.Empty;
                    Int16 acctNoRequireLength = 0;
                    List<String> displayFormatSectionValidList = new List<String>() { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
                    Boolean displayFormatSectionValid = true;
                    Boolean directBankBuildAccount = false;

                    if (String.IsNullOrEmpty(request.stmtType.Trim()))
                    {
                        Util.Log(logType.Warning, String.Format("ChangeSubscriptionStatus stop, invalid input StatementType = \"{0}\"", request.stmtType));
                        Response.returnCode = "1";
                        continue;
                    }

                    if ((request.subStatus != "U") && (request.subStatus != "S"))
                    {
                        Util.Log(logType.Warning, String.Format("ChangeSubscriptionStatus stop, invalid input RegistrationStatus = \"{0}\".", request.subStatus));
                        Response.returnCode = "1";
                        continue;
                    }

                    request.stmtType = request.stmtType.ToUpper();

                    // allow null input
                    if (request.actionId == null)
                        request.actionId = "";
                    if (request.actionSys == null)
                        request.actionSys = "";

                    // input AccountNumber trim '-'
                    if (request.acctNo.Contains("-"))
                    {
                        request.acctNo = request.acctNo.Replace("-", "");
                        Util.Log(logType.Info, String.Format("Normal record - input AccountNumber contains \"-\", auto trim to \"{0}\"", request.acctNo));
                    }

                    if ((request.stmtType != Properties.Settings.Default.StatementTypeSuperBank) && (request.stmtType != Properties.Settings.Default.StatementTypeContainer))
                    {
                        #region normal account
                        tbAccountList record = Entity.tbAccountList.Where(a => a.RMID == request.rmid && a.AccountNumber == request.acctNo && a.StatementType == request.stmtType && a.Active == "Y").FirstOrDefault();
                        String oldRegStatus = "";
                        if (record != null)
                        {
                            oldRegStatus = record.RegistrationStatus;
                            record.RegistrationStatus = request.subStatus;
                            record.statusUpdateBy = "WS";

                            Util.Log(logType.Info, String.Format("Normal record - RegistrationStatus change from \"{0}\" to \"{1}\", account in tbAccountList with ID = \"{2}\" ; RMID = \"{3}\" ; AccountNumber = \"{4}\" ; Category = \"{5}\" ; StatementType = \"{6}\"", oldRegStatus, request.subStatus, record.ID, record.RMID, record.AccountNumber, record.Category, record.StatementType));
                        }
                        else
                        {
                            Response.returnCode = "1";

                            if (auto_insert)
                            {
                                if (request.stmtType == "INFOCAST")
                                {
                                    List<tbSystemSetting> displayFormatList = Entity.tbSystemSetting.Where(a => a.SettingName == "DisplayAccountNumberFormat" && a.StatementType.ToUpper() == request.stmtType).ToList();
                                    tbSystemSetting displayFormat = new tbSystemSetting();

                                    if (displayFormatList.Count() < 1)
                                    {
                                        Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account - InfoCast) fail to add account into tbAccountList, cannot find DisplayAccountNumberFormat logic in table tbSystemSetting with StatementType = \"{0}\"", request.stmtType));
                                        continue;
                                    }
                                    else if (displayFormatList.Count() > 1)
                                    {
                                        Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account - InfoCast) fail to add account into tbAccountList, more than one DisplayAccountNumberFormat logic found in table tbSystemSetting with StatementType = \"{0}\"", request.stmtType));
                                        continue;
                                    }
                                    else
                                        displayFormat = displayFormatList[0];

                                    String[] displayFormatSection = displayFormat.SettingValue.Split('-');
                                    for (int i = 0; i < displayFormatSection.Length - 1; i++)
                                    {
                                        if (displayFormatSectionValidList.Contains(displayFormatSection[i]))
                                            acctNoRequireLength += Convert.ToInt16(displayFormatSection[i]);
                                        else
                                        {
                                            displayFormatSectionValid = false;
                                            break;
                                        }
                                    }
                                    if (!((displayFormatSectionValidList.Contains(displayFormatSection[displayFormatSection.Length - 1])) || (displayFormatSection[displayFormatSection.Length - 1] == "0")))
                                        displayFormatSectionValid = false;
                                    else
                                        acctNoRequireLength += Convert.ToInt16(displayFormatSection[displayFormatSection.Length - 1]);

                                    if (!displayFormatSectionValid)
                                    {
                                        Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account - InfoCast) fail to add account into tbAccountList, incorrect format of DisplayAccountNumberFormat logic found (\"{0}\") in table tbSystemSetting with StatementType = \"{1}\"", displayFormat.SettingValue, request.stmtType));
                                        continue;
                                    }

                                    if (displayFormatSection[displayFormatSection.Length - 1] == "0")
                                        acctNoRequireLength++;

                                    if (request.acctNo.Length < acctNoRequireLength)
                                    {
                                        Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account - InfoCast) fail to add account into tbAccountList, the length of input acctNo (\"{0}\") less than {1}, with StatementType = \"{2}\"", request.acctNo, acctNoRequireLength, request.stmtType));
                                        continue;
                                    }
                                    else if ((request.acctNo.Length > acctNoRequireLength) && (displayFormatSection[displayFormatSection.Length - 1] != "0"))
                                    {
                                        Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account - InfoCast) fail to add account into tbAccountList, the length of input acctNo (\"{0}\") greater than {1}, with StatementType = \"{2}\"", request.acctNo, acctNoRequireLength, request.stmtType));
                                        continue;
                                    }
                                    else
                                    {
                                        displayAcno = request.acctNo.Substring(0, Convert.ToInt16(displayFormatSection[0]));
                                        for (int i = 1; i < displayFormatSection.Length; i++)
                                        {
                                            if (Convert.ToInt16(displayFormatSection[i]) != 0)
                                                displayAcno += ("-" + request.acctNo.Substring(displayAcno.Replace("-", "").Length, Convert.ToInt16(displayFormatSection[i])));
                                            else
                                                displayAcno += ("-" + request.acctNo.Substring(displayAcno.Replace("-", "").Length));
                                        }
                                    }

                                    tbAccountList accountList = new tbAccountList();
                                    accountList.RMID = request.rmid;
                                    accountList.AccountNumber = request.acctNo;
                                    accountList.DisplayAccountNumber = displayAcno;
                                    accountList.Category = "InfoCast";
                                    accountList.StatementType = "InfoCast";
                                    accountList.CreateDate = DateTime.Now;
                                    accountList.LastUpdate = DateTime.Now;
                                    accountList.RegistrationStatus = request.subStatus;
                                    Response.returnCode = "0";
                                    accountList.statusUpdateBy = "WS";
                                    accountList.Active = "Y";
                                    accountList.CreateBy = "WS";
                                    accountList.ModifyBy = "WS";
                                    Entity.AddTotbAccountList(accountList);

                                    Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account - InfoCast) add account into tbAccountList with RMID = \"{0}\" ; AccountNumber = \"{1}\" ; DisplayAccountNumber = \"{2}\" ; Category = \"InfoCast\" ; StatementType = \"InfoCast\" ; RegistrationStatus = \"{3}\"", request.rmid, request.acctNo, displayAcno, request.subStatus));
                                }
                            }

                            // directbank add account
                            #region Directbank add account
                            List<tbDirectBankMapping> dbMapList = Entity.tbDirectBankMapping.Where(a => a.StatementType == request.stmtType).ToList();

                            if (dbMapList.Count > 0)
                            {
                                // only allow one kind of mapping logic with same Category and CheckAcno, i.e. check AccountNumber in tbAccountList or not
                                tbDirectBankMapping firstMap = dbMapList[0];
                                Boolean dbMapConsist = true;
                                foreach (var dbMap in dbMapList)
                                {
                                    if ((dbMap.Category == firstMap.Category) && (dbMap.CheckAcno == firstMap.CheckAcno))
                                        continue;
                                    else
                                    {
                                        dbMapConsist = false;
                                        break;
                                    }
                                }

                                if (dbMapConsist == true)
                                {
                                    tbAccountList accountCheck = new tbAccountList();
                                    if (firstMap.CheckAcno == "Y")
                                        accountCheck = Entity.tbAccountList.Where(a => a.RMID == request.rmid && a.AccountNumber == request.acctNo && a.Category == firstMap.Category && a.StatementType == firstMap.StatementType).FirstOrDefault();
                                    else
                                        accountCheck = Entity.tbAccountList.Where(a => a.RMID == request.rmid && a.Category == firstMap.Category && a.StatementType == firstMap.StatementType).FirstOrDefault();

                                    if (accountCheck == null)
                                    {
                                        tbAccountList newRecord = new tbAccountList();
                                        newRecord.RMID = request.rmid;

                                        if (firstMap.CheckAcno == "N")
                                        {
                                            request.acctNo = "";
                                            displayAcno = "";
                                        }
                                        else
                                        {
                                            List<tbSystemSetting> displayFormatList = Entity.tbSystemSetting.Where(a => a.SettingName == "DisplayAccountNumberFormat" && a.StatementType.ToUpper() == request.stmtType).ToList();
                                            tbSystemSetting displayFormat = new tbSystemSetting();

                                            if (displayFormatList.Count() < 1)
                                            {
                                                Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account) fail to add account into tbAccountList, cannot find DisplayAccountNumberFormat logic in table tbSystemSetting with StatementType = \"{0}\"", request.stmtType));
                                                continue;
                                            }
                                            else if (displayFormatList.Count() > 1)
                                            {
                                                Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account) fail to add account into tbAccountList, more than one DisplayAccountNumberFormat logic found in table tbSystemSetting with StatementType = \"{0}\"", request.stmtType));
                                                continue;
                                            }
                                            else
                                                displayFormat = displayFormatList[0];

                                            String[] displayFormatSection = displayFormat.SettingValue.Split('-');
                                            for (int i = 0; i < displayFormatSection.Length - 1; i++)
                                            {
                                                if (displayFormatSectionValidList.Contains(displayFormatSection[i]))
                                                    acctNoRequireLength += Convert.ToInt16(displayFormatSection[i]);
                                                else
                                                {
                                                    displayFormatSectionValid = false;
                                                    break;
                                                }
                                            }
                                            if (!((displayFormatSectionValidList.Contains(displayFormatSection[displayFormatSection.Length - 1])) || (displayFormatSection[displayFormatSection.Length - 1] == "0")))
                                                displayFormatSectionValid = false;
                                            else
                                                acctNoRequireLength += Convert.ToInt16(displayFormatSection[displayFormatSection.Length - 1]);

                                            if (!displayFormatSectionValid)
                                            {
                                                Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account) fail to add account into tbAccountList, incorrect format of DisplayAccountNumberFormat logic found (\"{0}\") in table tbSystemSetting with StatementType = \"{1}\"", displayFormat.SettingValue, request.stmtType));
                                                continue;
                                            }

                                            if (displayFormatSection[displayFormatSection.Length - 1] == "0")
                                                acctNoRequireLength++;

                                            if (request.acctNo.Length < acctNoRequireLength)
                                            {
                                                Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account) fail to add account into tbAccountList, the length of input acctNo (\"{0}\") less than {1}, with StatementType = \"{2}\"", request.acctNo, acctNoRequireLength, request.stmtType));
                                                continue;
                                            }
                                            else if ((request.acctNo.Length > acctNoRequireLength) && (displayFormatSection[displayFormatSection.Length - 1] != "0"))
                                            {
                                                Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account) fail to add account into tbAccountList, the length of input acctNo (\"{0}\") greater than {1}, with StatementType = \"{2}\"", request.acctNo, acctNoRequireLength, request.stmtType));
                                                continue;
                                            }
                                            else
                                            {
                                                displayAcno = request.acctNo.Substring(0, Convert.ToInt16(displayFormatSection[0]));
                                                for (int i = 1; i < displayFormatSection.Length; i++)
                                                {
                                                    if (Convert.ToInt16(displayFormatSection[i]) != 0)
                                                        displayAcno += ("-" + request.acctNo.Substring(displayAcno.Replace("-", "").Length, Convert.ToInt16(displayFormatSection[i])));
                                                    else
                                                        displayAcno += ("-" + request.acctNo.Substring(displayAcno.Replace("-", "").Length));
                                                }
                                            }
                                        }

                                        newRecord.AccountNumber = request.acctNo;
                                        newRecord.DisplayAccountNumber = displayAcno;
                                        newRecord.Category = firstMap.Category;
                                        newRecord.StatementType = firstMap.StatementType;
                                        newRecord.RegistrationStatus = request.subStatus;
                                        newRecord.Active = "Y";
                                        newRecord.CreateBy = Properties.Settings.Default.AppName;
                                        newRecord.ModifyBy = Properties.Settings.Default.AppName;
                                        newRecord.CreateDate = DateTime.Now;
                                        newRecord.LastUpdate = DateTime.Now;
                                        newRecord.statusUpdateBy = "WS";
                                        Entity.AddTotbAccountList(newRecord);
                                        Entity.SaveChanges();
                                        Response.returnCode = "0";

                                        Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account) add account into tbAccountList with RMID = \"{0}\" ; AccountNumber = \"{1}\" ; DisplayAccountNumber = \"{2}\" ; Category = \"{3}\" ; StatementType = \"{4}\" ; RegistrationStatus = \"{5}\"", request.rmid, request.acctNo, displayAcno, firstMap.Category, firstMap.StatementType, request.subStatus));
                                        directBankBuildAccount = true;
                                    }
                                    else
                                        Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account) fail to add account into tbAccountList, an account with RMID = \"{0}\" ; AccountNumber = \"{1}\" ; Category = \"{2}\" ; StatementType = \"{3}\" already exist", request.rmid, request.acctNo, firstMap.Category, firstMap.StatementType));
                                }
                                else
                                    Util.Log(logType.Warning, ("(ChangeSubscriptionStatus build account) fail to add account into tbAccountList, mapping logic with multiple inconsistent data (same StatementType, different Category/CheckAcno)."));
                            }
                            #endregion
                        }

                        #region add audit log for RegistrationStatus change
                        if (record != null)
                        {
                            tbAuditLog auditLog = new tbAuditLog();
                            auditLog.RMID = record.RMID;
                            auditLog.AccountNumber = record.AccountNumber;
                            auditLog.oldRegStatus = oldRegStatus;
                            auditLog.newRegStatus = request.subStatus;
                            auditLog.actionSys = request.actionSys;
                            auditLog.actionId = request.actionId;
                            auditLog.CreateDate = DateTime.Now;
                            auditLog.StatementType = record.StatementType;
                            Entity.AddTotbAuditLog(auditLog);
                        }
                        else
                        {
                            if (!(((auto_insert) && (request.stmtType == "INFOCAST")) || (directBankBuildAccount)))
                                Util.Log(logType.Warning, String.Format("ChangeSubscriptionStatus account not found for RMID={0}, acctNo={1}, statementType={2}", request.rmid, request.acctNo, request.stmtType));
                        }
                        #endregion

                        #endregion
                    }
                    else
                    {
                        #region superbank & container account
                        if (request.stmtType == Properties.Settings.Default.StatementTypeSuperBank)
                        {
                            // superbank add account
                            tbAccountList accountCheck = Entity.tbAccountList.Where(a => a.RMID == request.acctNo && a.AccountNumber == request.acctNo && a.DisplayAccountNumber == request.acctNo && a.StatementType == request.stmtType).FirstOrDefault();

                            if (accountCheck == null)
                            {
                                Util.Log(logType.Info, String.Format("SuperBank record - cannot match any account in table tbAccountList with RMID (bpid) = \"{0}\" ; AccountNumber = \"{1}\" ; DisplayAccountNumber = \"{2}\" ; StatementType = \"{3}\"", request.acctNo, request.acctNo, request.acctNo, request.stmtType));
                                Util.Log(logType.Info, "(ChangeSubscriptionStatus build account - SuperBank) start to add new record into table tbAccountList");
                                tbAccountList newRecord = new tbAccountList();
                                newRecord.RMID = request.acctNo;
                                newRecord.AccountNumber = request.acctNo;
                                newRecord.DisplayAccountNumber = request.acctNo;
                                newRecord.Category = Properties.Settings.Default.CategorySuperBank;
                                newRecord.StatementType = request.stmtType;
                                newRecord.RegistrationStatus = request.subStatus;
                                newRecord.Active = "Y";
                                newRecord.CreateBy = Properties.Settings.Default.AppName;
                                newRecord.ModifyBy = Properties.Settings.Default.AppName;
                                newRecord.CreateDate = DateTime.Now;
                                newRecord.LastUpdate = DateTime.Now;
                                newRecord.statusUpdateBy = "WS";
                                Entity.AddTotbAccountList(newRecord);
                                Entity.SaveChanges();
                                Response.returnCode = "0";

                                Util.Log(logType.Info, String.Format("(ChangeSubscriptionStatus build account - SuperBank) add account into tbAccountList with RMID = \"{0}\" ; AccountNumber = \"{1}\" ; DisplayAccountNumber = \"{2}\" ; Category = \"{3}\" ; StatementType = \"{4}\" ; RegistrationStatus = \"{5}\"", request.acctNo, request.acctNo, request.acctNo, Properties.Settings.Default.CategorySuperBank, request.stmtType, request.subStatus));
                            }
                            else
                            {
                                Util.Log(logType.Info, String.Format("SKIP (ChangeSubscriptionStatus build account), an account with RMID = \"{0}\" ; AccountNumber = \"{1}\" ; DisplayAccountNumber = \"{2}\" ; Category = \"{3}\" ; StatementType = \"{4}\" already exist", request.acctNo, request.acctNo, request.acctNo, Properties.Settings.Default.CategorySuperBank, request.stmtType));

                                tbBPInfo bpAccount = Entity.tbBPInfo.Where(a => a.rmid == request.rmid && a.bpid == request.acctNo).FirstOrDefault();
                                String tempReturnCode = "0";

                                if (bpAccount != null)
                                {
                                    tbAccountList record = Entity.tbAccountList.Where(a => a.RMID == bpAccount.bpid && a.AccountNumber == request.acctNo && a.StatementType == request.stmtType && a.Active == "Y").FirstOrDefault();
                                    if (record != null)
                                    {
                                        String oldRegStatus = record.RegistrationStatus;
                                        record.RegistrationStatus = request.subStatus;
                                        record.statusUpdateBy = "WS";
                                        Response.bpID = bpAccount.bpid;

                                        Util.Log(logType.Info, String.Format("SuperBank record - RegistrationStatus change from \"{0}\" to \"{1}\", account in tbAccountList with ID = \"{2}\" ; RMID (bpid) = \"{3}\" ; AccountNumber = \"{4}\" ; Category = \"{5}\" ; StatementType = \"{6}\"", oldRegStatus, request.subStatus, record.ID, record.RMID, record.AccountNumber, record.Category, record.StatementType));

                                        tbAuditLog auditLog = new tbAuditLog();
                                        auditLog.RMID = request.rmid;
                                        auditLog.AccountNumber = request.acctNo;
                                        auditLog.oldRegStatus = oldRegStatus;
                                        auditLog.newRegStatus = request.subStatus;
                                        auditLog.actionSys = request.actionSys;
                                        auditLog.actionId = request.actionId;
                                        auditLog.CreateDate = DateTime.Now;
                                        auditLog.StatementType = record.StatementType;
                                        Entity.AddTotbAuditLog(auditLog);
                                        Entity.SaveChanges();

                                        Response.returnCode = tempReturnCode;
                                    }
                                    else
                                    {
                                        Util.Log(logType.Info, String.Format("SuperBank record - cannot match any ACTIVE account in table tbAccountList with RMID (bpid) = \"{0}\" ; AccountNumber = \"{1}\" ; StmtType = \"{2}\" ; Active = \"Y\"", bpAccount.bpid, request.acctNo, request.stmtType));
                                        Response.returnCode = "1";
                                    }
                                }
                                else
                                {
                                    Util.Log(logType.Info, String.Format("SuperBank record - cannot match any account in table tbBPInfo with rmid = \"{0}\" ; bpid = \"{1}\"", request.rmid, request.acctNo));
                                    Response.returnCode = "1";
                                }
                            }
                        }
                        else if (request.stmtType == Properties.Settings.Default.StatementTypeContainer)
                        {
                            tbAccountList record = Entity.tbAccountList.Where(a => a.AccountNumber == request.acctNo && a.StatementType == request.stmtType && a.Active == "Y").FirstOrDefault();
                            String tempReturnCode = "0";

                            if (record != null)
                            {
                                tbBPInfo bpAccount = Entity.tbBPInfo.Where(a => a.rmid == request.rmid && a.bpid == record.RMID).FirstOrDefault();
                                if (bpAccount != null)
                                {
                                    String oldRegStatus = record.RegistrationStatus;
                                    record.RegistrationStatus = request.subStatus;
                                    record.statusUpdateBy = "WS";
                                    Response.bpID = bpAccount.bpid;

                                    Util.Log(logType.Info, String.Format("SuperBank record - RegistrationStatus change from \"{0}\" to \"{1}\", account in tbAccountList with ID = \"{2}\" ; RMID (bpid) = \"{3}\" ; AccountNumber = \"{4}\" ; Category = \"{5}\" ; StatementType = \"{6}\"", oldRegStatus, request.subStatus, record.ID, record.RMID, record.AccountNumber, record.Category, record.StatementType));

                                    tbAuditLog auditLog = new tbAuditLog();
                                    auditLog.RMID = request.rmid;
                                    auditLog.AccountNumber = request.acctNo;
                                    auditLog.oldRegStatus = oldRegStatus;
                                    auditLog.newRegStatus = request.subStatus;
                                    auditLog.actionSys = request.actionSys;
                                    auditLog.actionId = request.actionId;
                                    auditLog.CreateDate = DateTime.Now;
                                    auditLog.StatementType = record.StatementType;
                                    Entity.AddTotbAuditLog(auditLog);
                                    Entity.SaveChanges();

                                    Response.returnCode = tempReturnCode;
                                }
                                else
                                {
                                    Util.Log(logType.Info, String.Format("SuperBank record, special error(account exist in table tbAccountList) for container account - cannot match any account in table tbBPInfo with rmid = \"{0}\" ; bpid = \"{1}\"", request.rmid, record.RMID));
                                    Response.returnCode = "1";
                                }
                            }
                            else
                            {
                                Util.Log(logType.Info, String.Format("SuperBank record - cannot match any ACTIVE account in table tbAccountList with AccountNumber (Container No.) = \"{0}\" ; StmtType = \"{1}\" ; Active = \"Y\"", request.acctNo, request.stmtType));
                                Response.returnCode = "1";
                            }
                        }
                        #endregion
                    }
                }
                Entity.SaveChanges();
            }
            catch (Exception ex)
            {
                String content = "";
                foreach (Class.subscriptionStatus request in changeSubscriptionStatuses)
                {
                    content += String.Format("rmid:{0},acctNo:{1},stmtType:{2},actionSys:{3},actionId:{4}|", request.rmid, request.acctNo, request.stmtType, request.actionSys, request.actionId);
                }
                Util.Log(logType.Error, string.Format("Error in ChangeSubscriptionStatus. {0}. Error message:{1}.", content, ex.Message));
                if (ex.InnerException != null)
                {
                    Util.Log(logType.Error, string.Format("Error in ChangeSubscriptionStatus. Inner exception:{0}.", ex.InnerException));
                }
                Response.returnCode = "1";
            }
            Util.Log(logType.Info, "ChangeSubscriptionStatus End, returnCode = " + Response.returnCode);
            return Response;
        }

        [WebMethod]
        public Class.getViewTokenResult GetViewToken(String rmid, String sessionID, String stmtID, String marketInsertID, String infoInsertID)
        {
            Util.Log(logType.Info, String.Format("GetViewToken Start. rmid:{0},sessionID:{1},stmtID:{2},marketInsertID:{3},bankInfoInsertID:{4}.", rmid, sessionID, stmtID, marketInsertID, infoInsertID));
            Class.getViewTokenResult Response = new Class.getViewTokenResult();

            Response.returnCode = "1";
            Response.token = "";

            if (String.IsNullOrEmpty(rmid))
            {
                Util.Log(logType.Warning, "GetViewToken. rmid is null or empty.");
            }
            else if (String.IsNullOrEmpty(sessionID))
            {
                Util.Log(logType.Warning, "GetViewToken. sessionID is null or empty.");
            }
            else if (String.IsNullOrEmpty(stmtID) && String.IsNullOrEmpty(marketInsertID) && String.IsNullOrEmpty(infoInsertID))
            {
                Util.Log(logType.Warning, "GetViewToken. All statementID, marketInsertID, bankInfoInsertID are null or empty.");
            }
            else
            {
                Response.rmid = rmid;

                stmtID = String.IsNullOrEmpty(stmtID) ? "" : stmtID;
                marketInsertID = String.IsNullOrEmpty(marketInsertID) ? "" : marketInsertID;
                infoInsertID = String.IsNullOrEmpty(infoInsertID) ? "" : infoInsertID;

                try
                {
                    String plaintext = String.Format("{0};{1};{2};{3};{4};{5}", rmid, sessionID, stmtID, marketInsertID, infoInsertID, DateTime.Now.ToString("yyyyMMddHHmmss"));
                    String token = Util.encrypt(plaintext, Util.GetMD5(Properties.Settings.Default.EncryptionKey));

                    Response.returnCode = "0";
                    Response.token = HttpUtility.UrlEncode(token);
                    Util.Log(logType.Info, String.Format("Token generated:{0}", Response.token));
                }
                catch (Exception ex)
                {
                    Util.Log(logType.Error, String.Format("Error in GetViewToken. rmid:{0},sessionID:{1},stmtID:{2},marketInsertID:{3},bankInfoInsertID:{4}. Error message:{5}.", rmid, sessionID, stmtID, marketInsertID, infoInsertID, ex.Message));
                    Response.returnCode = "1";
                    Response.token = "";
                }
            }
            Util.Log(logType.Info, "GetViewToken End, returnCode = " + Response.returnCode + ", token=" + Response.token);
            return Response;
        }

        private String GetMarketInsertID(String category, String statementType, DateTime statementDate)
        {
            tbMarketInsert marketInsert = Entity.tbMarketInsert.Where(a => a.Category == category && a.StatementType == statementType && a.EffectiveFrom <= statementDate && a.EffectiveTo >= statementDate).FirstOrDefault();
            if (marketInsert != null)
            {
                return marketInsert.ID.ToString();
            }
            return "";
        }

        private String GetInfoInsertID(String category, String statementType, DateTime statementDate)
        {
            tbBankInfoInsert infoInsert = Entity.tbBankInfoInsert.Where(a => a.Category == category && a.StatementType == statementType && a.EffectiveFrom <= statementDate && a.EffectiveTo >= statementDate).FirstOrDefault();
            if (infoInsert != null)
            {
                return infoInsert.ID.ToString();
            }
            return "";
        }

    }
}