﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;

namespace ESBatchAccountImporter
{
    public enum logType { Info, Error, Warning };
    public enum SBS_GetData
    {
        bpidStartPos = 0, bpidLength = 14,
        bpAccNoStartPos = bpidStartPos + bpidLength, bpAccNoLength = 10,
        containerAcnoStartPos = bpAccNoStartPos + bpAccNoLength, containerAcnoLength = 12,
        applicationIDStartPos = 96, applicationIDLength = 5,
        control1StartPos = applicationIDStartPos + applicationIDLength, control1Length = 4,
        control2StartPos = control1StartPos + control1Length, control2Length = 4,
        control3StartPos = control2StartPos + control2Length, control3Length = 4,
        control4StartPos = control3StartPos + control3Length, control4Length = 4,
        IMAccNoStartPos = 118, IMAccNoLength = 9,
        STAccNoStartPos = 122, STAccNoLength = 9,
        acctTypeStartPos = 173, acctTypeLength = 10,
        bpSegStartPos = acctTypeStartPos + acctTypeLength, bpSegLength = 1,
        SBSACLLength = bpSegStartPos + bpSegLength
    };
    public enum BPInfo_GetData
    {
        bpidStartPos = 0, bpidLength = 14,
        ckAVQStartPos = 24, ckAVQLength = 10,
        rm_noStartPos = 34, rm_noLength = 30,
        bp_nameStartPos = 262, bp_nameLength = 80,
        no_of_acc_holderStartPos = 71, no_of_acc_holderLength = 2,
        sbs_avp_bp_relLength = no_of_acc_holderStartPos + no_of_acc_holderLength
    };

    class Program
    {
        static EStatementEntities entity = new EStatementEntities();
        //static string[] InfoCastStatementType = { "I-DailyStmt", "I-MthStmt", "I-Advice" };
        static string InfoCastStatementType = "InfoCast";
        static Boolean enhance = true;

        static void Main(string[] args)
        {
            Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "Log"));
            #region update snapshot table
            if (args.Length != 0 && args[0] == "snapshot")
            {
                try
                {
                    Util.Log(logType.Info, "=========================================================");
                    Util.Log(logType.Info, String.Format("Snapshot start --- {0}", DateTime.Now));
                    Util.Log(logType.Info, "Truncate tbAccountListSnapshot start");
                    entity.ExecuteStoreCommand("TRUNCATE TABLE [dbo].[tbAccountListSnapshot]");
                    Util.Log(logType.Info, "Truncate tbAccountListSnapshot finish");

                    Util.Log(logType.Info, "Update table tbAccountListSnapshot start");
                    var tbAccountList = entity.ExecuteStoreQuery<tbAccountList>("SELECT * FROM [dbo].[tbAccountList]").ToList();
                    int counter = 0;
                    /*foreach (var account in tbAccountList)
                    {
                        DateTime yesterday = DateTime.Now.AddDays(-1);
                        tbAccountListSnapshot accountSnapshot = new tbAccountListSnapshot();
                        accountSnapshot.RMID = account.RMID;
                        accountSnapshot.AccountNumber = account.AccountNumber;
                        accountSnapshot.DisplayAccountNumber = account.DisplayAccountNumber;
                        accountSnapshot.Category = account.Category;
                        accountSnapshot.StatementType = account.StatementType;
                        accountSnapshot.RegistrationStatus = "S";
                        accountSnapshot.Active = account.Active;
                        accountSnapshot.CreateDate = account.CreateDate.Value;
                        accountSnapshot.CreateBy = account.CreateBy;
                        accountSnapshot.ModifyDate = account.ModifyDate;
                        accountSnapshot.ModifyBy = account.ModifyBy;
                        accountSnapshot.LastUpdate = account.LastUpdate;
                        accountSnapshot.AccountID = account.ID;
                        accountSnapshot.SnapshotDate = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day);
                        entity.AddTotbAccountListSnapshot(accountSnapshot);

                        // batch update
                        if (counter++ % 20000 == 0)
                        {
                            entity.SaveChanges();
                        }
                    }                    
                    entity.SaveChanges();*/
                    /*****************************Modified by Jacky on 2018-11-28*************************************/
                    //purpose : insert tbAccountList data to tbAccountSnapshot as the above method is too slow
                    #region Create temp data table for host the tbAccountList data
                    DateTime yesterday = DateTime.Now.AddDays(-1);
                        DataTable tempDataTable = new DataTable();
                        tempDataTable.Columns.Add("RMID");
                        tempDataTable.Columns.Add("AccountNumber");
                        tempDataTable.Columns.Add("DisplayAccountNumber");
                        tempDataTable.Columns.Add("Category");
                        tempDataTable.Columns.Add("StatementType");
                        tempDataTable.Columns.Add("RegistrationStatus");
                        tempDataTable.Columns.Add("Active");
                        tempDataTable.Columns.Add("CreateDate");
                        tempDataTable.Columns.Add("CreateBy");
                        tempDataTable.Columns.Add("ModifyDate");
                        tempDataTable.Columns.Add("ModifyBy");
                        tempDataTable.Columns.Add("LastUpdate");
                        tempDataTable.Columns.Add("SnapshotDate");
                        tempDataTable.Columns.Add("AccountID");

                    foreach (var tbAccount in tbAccountList)
                    {
                        DateTime now = DateTime.Now;

                        DataRow row = tempDataTable.NewRow();
                        row["RMID"] = tbAccount.RMID;
                        row["AccountNumber"] = tbAccount.AccountNumber;
                        row["DisplayAccountNumber"] = tbAccount.DisplayAccountNumber;
                        row["Category"] = tbAccount.Category;
                        row["StatementType"] = tbAccount.StatementType;
                        row["RegistrationStatus"] = tbAccount.RegistrationStatus;
                        row["Active"] = tbAccount.Active;
                        row["CreateDate"] = tbAccount.CreateDate;
                        row["CreateBy"] = tbAccount.CreateBy;
                        row["ModifyDate"] = tbAccount.ModifyDate;
                        row["ModifyBy"] = tbAccount.ModifyBy;
                        row["LastUpdate"] = tbAccount.LastUpdate;
                        row["SnapshotDate"] = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day);
                        row["AccountID"] = tbAccount.ID;

                        tempDataTable.Rows.Add(row);
                    }
                    #endregion
                    #region bulk insert to tbAccountSnapshot
                    Util.Log(logType.Info, String.Format("Insert record into {0} start", "[dbo].[tbAccountListSnapshot]"));
                    using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                    {
                        connection.Open();

                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                        {
                            bulkCopy.DestinationTableName = "[dbo].[tbAccountListSnapshot]";
                            bulkCopy.BatchSize = 10000;
                            bulkCopy.BulkCopyTimeout = Properties.Settings.Default.BulkInsertTimeout;

                            SqlBulkCopyColumnMapping map_RMID = new SqlBulkCopyColumnMapping("RMID", "RMID");
                            bulkCopy.ColumnMappings.Add(map_RMID);
                            SqlBulkCopyColumnMapping map_AccountNumber = new SqlBulkCopyColumnMapping("AccountNumber", "AccountNumber");
                            bulkCopy.ColumnMappings.Add(map_AccountNumber);
                            SqlBulkCopyColumnMapping map_DisplayAccountNumber = new SqlBulkCopyColumnMapping("DisplayAccountNumber", "DisplayAccountNumber");
                            bulkCopy.ColumnMappings.Add(map_DisplayAccountNumber);
                            SqlBulkCopyColumnMapping map_Category = new SqlBulkCopyColumnMapping("Category", "Category");
                            bulkCopy.ColumnMappings.Add(map_Category);
                            SqlBulkCopyColumnMapping map_StatementType = new SqlBulkCopyColumnMapping("StatementType", "StatementType");
                            bulkCopy.ColumnMappings.Add(map_StatementType);
                            SqlBulkCopyColumnMapping map_RegistrationStatus = new SqlBulkCopyColumnMapping("RegistrationStatus", "RegistrationStatus");
                            bulkCopy.ColumnMappings.Add(map_RegistrationStatus);
                            SqlBulkCopyColumnMapping map_Active = new SqlBulkCopyColumnMapping("Active", "Active");
                            bulkCopy.ColumnMappings.Add(map_Active);
                            SqlBulkCopyColumnMapping map_CreateDate = new SqlBulkCopyColumnMapping("CreateDate", "CreateDate");
                            bulkCopy.ColumnMappings.Add(map_CreateDate);
                            SqlBulkCopyColumnMapping map_CreateBy = new SqlBulkCopyColumnMapping("CreateBy", "CreateBy");
                            bulkCopy.ColumnMappings.Add(map_CreateBy);
                            SqlBulkCopyColumnMapping map_ModifyDate = new SqlBulkCopyColumnMapping("ModifyDate", "ModifyDate");
                            bulkCopy.ColumnMappings.Add(map_ModifyDate);
                            SqlBulkCopyColumnMapping map_ModifyBy = new SqlBulkCopyColumnMapping("ModifyBy", "ModifyBy");
                            bulkCopy.ColumnMappings.Add(map_ModifyBy);
                            SqlBulkCopyColumnMapping map_LastUpdate = new SqlBulkCopyColumnMapping("LastUpdate", "LastUpdate");
                            bulkCopy.ColumnMappings.Add(map_LastUpdate);
                            SqlBulkCopyColumnMapping map_SnapshotDate = new SqlBulkCopyColumnMapping("SnapshotDate", "SnapshotDate");
                            bulkCopy.ColumnMappings.Add(map_SnapshotDate);
                            SqlBulkCopyColumnMapping map_AccountID = new SqlBulkCopyColumnMapping("AccountID", "AccountID");
                            bulkCopy.ColumnMappings.Add(map_AccountID);

                            bulkCopy.WriteToServer(tempDataTable);
                        }
                        connection.Close();
                    }
                    tempDataTable.Clear();
                    //Util.Log(logType.Info, String.Format("Insert record into {0} Finsih", Properties.Settings.Default.tbAccountListSnapshot));
                    Util.Log(logType.Info, String.Format("Insert record into tbAccountListSnapshot Finsih"));
                    #endregion
                    /***********************************************************************/
                    Util.Log(logType.Info, "Update table tbAccountListSnapshot finish");
                    }
                catch (Exception ex)
                {
                    Util.Log(logType.Error, String.Format("Error in region update snapshot table : {0}", ex.Message));
                }
                Util.Log(logType.Info, String.Format("Snapshot finish --- {0}", DateTime.Now));
                return;
            }
            #endregion

            try
            {
                Util.Log(logType.Info, "=========================================================");
                Util.Log(logType.Info, String.Format("ESBatchAccountImporter start --- {0}", DateTime.Now));
                entity.CommandTimeout = Properties.Settings.Default.EntityCommandTimeout;
                AccountImport();
                Util.Log(logType.Info, String.Format("ESBatchAccountImporter finish --- {0}", DateTime.Now));
                System.Threading.Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in main : {0}", ex.Message));
                System.Threading.Thread.Sleep(5000);
            }
        }

        private static void AccountImport()
        {
            DateTime acImpStartTime = DateTime.Now;
            Dictionary<String, List<Util.account>> newCustomerList = new Dictionary<String, List<Util.account>>();
            List<Util.account> individualAccountIMList = new List<Util.account>();
            Dictionary<String, Util.account> individualAccountIMDict = new Dictionary<String, Util.account>();
            List<Util.account> individualAccountSTList = new List<Util.account>();
            Dictionary<String, Util.account> individualAccountSTDict = new Dictionary<String, Util.account>();
            List<Util.account> combineAccountList = new List<Util.account>();
            List<Util.account> citicFirstAccountList = new List<Util.account>();
            List<Util.account> creditCardAccountList = new List<Util.account>();
            List<Util.account> infoCastAccountList = new List<Util.account>();
            List<Util.account> combineAccountSecondaryList = new List<Util.account>();
            List<Util.account> tbCiticFirstAccountList = new List<Util.account>();
            Dictionary<String, String> srmNumberToRmidMapping = new Dictionary<String, String>();
            Dictionary<String, String> rmNumberToRmidMapping = new Dictionary<String, String>();
            Dictionary<String, String> rmNumber30ToRmidMapping = new Dictionary<String, String>();
            Dictionary<String, List<String>> rmNumberToAccountNumberMapping = new Dictionary<String, List<String>>();
            DateTime startDatetime = DateTime.Now;
            Boolean acImpSucRun = false;
            Boolean tbRmIDAccountNumberSucRun = false;
            List<Util.account> SBSAccountList = new List<Util.account>();
            Dictionary<String, Util.account> tbSuperBankAccountDict = new Dictionary<String, Util.account>();
            Dictionary<String, Util.account> tbContainerAccountDict = new Dictionary<String, Util.account>();
            Dictionary<String, Util.account> tbSubAccountListDict = new Dictionary<String, Util.account>();
            Dictionary<String, Util.BPInfo> tempBPInfoDict = new Dictionary<String, Util.BPInfo>();
            Dictionary<String, String> bpidToCkAvqDict = new Dictionary<String, String>();
            Dictionary<String, String> bpidToAcctTypeDict = new Dictionary<String, String>();
            Dictionary<String, String> bpidToBpnameDict = new Dictionary<String, String>();
            Dictionary<String, String> bpidDefaultSDict = new Dictionary<String, String>();
            int counter = 0;

            Util.Log(logType.Info, "check file existence start");
            if (!isFileExist())
            {
                Util.Log(logType.Error, String.Format("{0:yyyyMMdd HHmmss} END Account Importer with status = FAIL", DateTime.Now));
                return;
            }
            Util.Log(logType.Info, "check file existence finish");

            #region individual account list
            Util.Log(logType.Info, "AccountImport(read individual account list) Start.");
            Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START processing Bank", DateTime.Now));
            try
            {
                Dictionary<String, List<String>> accountNumberToRmidMapping = new Dictionary<String, List<String>>();

                #region init rmNumberToRmidMapping
                if (File.Exists(Properties.Settings.Default.RmNumberRMIDMappingPath))
                {
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.RmNumberRMIDMappingPath))
                    {
                        String eachLine = String.Empty;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            if (eachLine.Length < 998 + 20)
                            {
                                Util.Log(logType.Error, String.Format("AccountImport(read rmNumberToRmidMapping) Incomplete line. File Path:{0}. Line:{1}", Properties.Settings.Default.RmNumberRMIDMappingPath, eachLine));
                                return;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }
                    Util.Log(logType.Info, String.Format("Total No. of records in IADCSRM.TXT = {0}", lines.Count.ToString()));

                    foreach (var line in lines)
                    {
                        String rmNumber = line.Substring(10, 35).Trim();
                        String rmid = line.Substring(998, 20).Trim();

                        if (!rmNumberToRmidMapping.ContainsKey(rmNumber))
                        {
                            rmNumberToRmidMapping.Add(rmNumber, rmid);
                            rmNumber30ToRmidMapping.Add(rmNumber.Substring(5), rmid);
                            srmNumberToRmidMapping.Add(rmNumber, rmid);
                        }
                        else
                        {
                            Util.Log(logType.Warning, String.Format("AccountImport(read rmNumberToRmidMapping) rm number map to multiple rmid. rmNumber:{0}", rmNumber));
                        }
                    }
                }
                else
                {
                    Util.Log(logType.Error, String.Format("RmNumber to RMID mapping file does not existed. File path:{0}", Properties.Settings.Default.RmNumberRMIDMappingPath));
                }
                #endregion

                #region init accountNumberToRmidrMapping
                if (File.Exists(Properties.Settings.Default.AccountNumberRmNumberMappingPath))
                {
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.AccountNumberRmNumberMappingPath))
                    {
                        String eachLine = String.Empty;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            if (eachLine.Length < 45 + 35)
                            {
                                Util.Log(logType.Error, String.Format("AccountImport(read accountNumberToRmNumberMapping) Incomplete line. File Path:{0}. Line:{1}", Properties.Settings.Default.AccountNumberRmNumberMappingPath, eachLine));
                                return;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }
                    Util.Log(logType.Info, String.Format("Total No. of records in IADCARM.TXT = {0}", lines.Count.ToString()));

                    foreach (var line in lines)
                    {
                        String rmNumber = line.Substring(9, 35);
                        String accountNumber = line.Substring(45, 35);

                        if (rmNumberToRmidMapping.ContainsKey(rmNumber))
                        {
                            if (accountNumberToRmidMapping.ContainsKey(accountNumber))
                            {
                                List<String> rmidList = accountNumberToRmidMapping[accountNumber];
                                rmidList.Add(rmNumberToRmidMapping[rmNumber]);
                            }
                            else
                            {
                                accountNumberToRmidMapping.Add(accountNumber, new List<String> { rmNumberToRmidMapping[rmNumber] });
                            }
                        }
                        else
                        {
                            Util.Log(logType.Warning, String.Format("AccountImport(read accountNumberToRmNumberMapping) rm number cannot map to rmid. rmNumber:{0}", rmNumber));
                        }

                        if (rmNumberToAccountNumberMapping.ContainsKey(rmNumber))
                        {
                            List<String> accountList = rmNumberToAccountNumberMapping[rmNumber];
                            accountList.Add(accountNumber);
                        }
                        else
                        {
                            rmNumberToAccountNumberMapping.Add(rmNumber.Trim(), new List<String> { accountNumber });
                        }
                    }
                }
                else
                {
                    Util.Log(logType.Error, String.Format("Account to RmNumber mapping file does not existed. File path:{0}", Properties.Settings.Default.AccountNumberRmNumberMappingPath));
                }
                #endregion

                #region remove account which map to multiple rmid
                List<String> accountWithSameRmid = new List<String>();
                foreach (var item in accountNumberToRmidMapping)
                {
                    if (item.Value.Count() > 1)
                    {
                        accountWithSameRmid.Add(item.Key);
                    }
                }
                foreach (var item in accountWithSameRmid)
                {
                    accountNumberToRmidMapping.Remove(item);
                }
                accountWithSameRmid.Clear();
                #endregion

                #region IM
                if (File.Exists(Properties.Settings.Default.IndividualAccountListPathIM))
                {
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.IndividualAccountListPathIM))
                    {
                        String eachLine = String.Empty;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            if (eachLine.Length < 788 + 3)
                            {
                                Util.Log(logType.Error, String.Format("AccountImport(read Indivdual IM) Incomplete line. File Path:{0}. Line:{1}", Properties.Settings.Default.IndividualAccountListPathIM, eachLine));
                                return;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }
                    Util.Log(logType.Info, String.Format("Total No. of records in IADDPIM.TXT = {0}", lines.Count.ToString()));

                    foreach (var line in lines)
                    {
                        String accountNumber = line.Substring(10, 35);
                        String productCode = line.Substring(788, 3);
                        String IWCode = line.Substring(603, 5).Trim();

                        if (productCode.Equals("100") ||
                            productCode.Equals("101") ||
                            productCode.Equals("151") ||
                            productCode.Substring(0, 1).Equals("3") ||
                            productCode.Substring(0, 1).Equals("4") ||
                            IWCode.Equals("020") ||
                            IWCode.Equals("030") ||
                            IWCode.Equals("060"))
                        {
                            continue;
                        }

                        if (accountNumberToRmidMapping.ContainsKey(accountNumber))
                        {
                            List<String> rmid = accountNumberToRmidMapping[accountNumber];
                            accountNumber = String.Format("{0}{1}", accountNumber.Substring(18, 3), accountNumber.Substring(26, 9));
                            String statementType = productCode.Substring(0, 1).Equals("0") ? "PLOC" : "MONTHLY";
                            individualAccountIMList.Add(new Util.account(rmid, accountNumber, statementType, Properties.Settings.Default.CategoryIndividual));
                            individualAccountIMDict.Add(accountNumber, new Util.account(rmid, accountNumber, statementType, Properties.Settings.Default.CategoryIndividual));
                        }
                        else
                        {
                            Util.Log(logType.Warning, String.Format("RMID not found. Individual Account Number:{0}", accountNumber));
                        }
                    }
                }
                else
                {
                    Util.Log(logType.Error, string.Format("Individual account list IM file does not existed. File path:{0}", Properties.Settings.Default.IndividualAccountListPathIM));
                }
                #endregion

                #region ST
                if (File.Exists(Properties.Settings.Default.IndividualAccountListPathST))
                {
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.IndividualAccountListPathST))
                    {
                        String eachLine = String.Empty;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            if (eachLine.Length < 788 + 3)
                            {
                                Util.Log(logType.Error, String.Format("AccountImport(read Indivdual ST) Incomplete line. File Path:{0}. Line:{1}", Properties.Settings.Default.IndividualAccountListPathST, eachLine));
                                return;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }
                    Util.Log(logType.Info, String.Format("Total No. of records in IADDPST.TXT = {0}", lines.Count.ToString()));

                    foreach (var line in lines)
                    {
                        String accountNumber = line.Substring(10, 35);
                        String productCode = line.Substring(788, 3);
                        String IWCode = line.Substring(603, 5).Trim();

                        if (productCode.Equals("100") ||
                            productCode.Equals("101") ||
                            productCode.Equals("151") ||
                            productCode.Substring(0, 1).Equals("3") ||
                            productCode.Substring(0, 1).Equals("4") ||
                            IWCode.Equals("020") ||
                            IWCode.Equals("030") ||
                            IWCode.Equals("060"))
                        {
                            continue;
                        }

                        if (accountNumberToRmidMapping.ContainsKey(accountNumber))
                        {
                            List<String> rmid = accountNumberToRmidMapping[accountNumber];
                            accountNumber = String.Format("{0}{1}", accountNumber.Substring(15, 3), accountNumber.Substring(26, 9));
                            String statementType = productCode.Substring(0, 1).Equals("0") ? "PLOC" : "MONTHLY";
                            individualAccountSTList.Add(new Util.account(rmid, accountNumber, statementType, Properties.Settings.Default.CategoryIndividual));
                            individualAccountSTDict.Add(accountNumber, new Util.account(rmid, accountNumber, statementType, Properties.Settings.Default.CategoryIndividual));
                        }
                        else
                        {
                            Util.Log(logType.Warning, String.Format("RMID not found. Individual Account Number:{0}", accountNumber));
                        }
                    }
                }
                else
                {
                    Util.Log(logType.Error, string.Format("Individual account list ST file does not existed. File path:{0}", Properties.Settings.Default.IndividualAccountListPathST));
                }
                #endregion

                accountNumberToRmidMapping.Clear();
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, string.Format("Error in AccountImport(reading individual account list). Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, "AccountImport(read individual account list) End.");

            #endregion

            #region combine account list
            Util.Log(logType.Info, "AccountImport(read combine account list) Start.");
            try
            {
                if (File.Exists(Properties.Settings.Default.CombineAccountListPath))
                {
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.CombineAccountListPath))
                    {
                        String eachLine = String.Empty;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            if (eachLine.Contains(';'))
                                continue;
                            if (eachLine.Length < 30 + 3)
                            {
                                Util.Log(logType.Error, String.Format("AccountImport(read Combine) Incomplete line. File Path:{0}. Line:{1}", Properties.Settings.Default.CombineAccountListPath, eachLine));
                                return;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }
                    Util.Log(logType.Info, String.Format("Total No. of records in SCFSTGP.DAT = {0}", lines.Count.ToString()));
                    lines = lines.FindAll(a => a.Substring(14, 2) == "IM" || a.Substring(14, 2) == "ST").ToList();
                    foreach (var line in lines)
                    {
                        String rmTemp = line.Substring(0, 14).Trim();
                        String group = line.Substring(28, 1).Trim();
                        String primarySecondaryIndicator = line.Substring(29, 1).Trim();
                        String accountNumber = line.Substring(16, 12).Trim();
                        String applicationID = line.Substring(14, 2).Trim();
                        String statementType = line.Substring(30, 3).Trim();
                        List<String> rmid = new List<String>();

                        if (applicationID.Equals("IM", StringComparison.OrdinalIgnoreCase))
                        {
                            // Change List to Dictionary for better performance
                            var account = (Util.account)null;
                            if (!enhance)
                            {
                                account = individualAccountIMList.Find(a => a.acctNo == accountNumber);
                                if (account != null)
                                {
                                    rmid = account.rmid;
                                    individualAccountIMDict.Remove(accountNumber);
                                }
                                else
                                {
                                    Util.Log(logType.Warning, String.Format("Account does not exist in IM full account list. Combine Account Number:{0}", accountNumber));
                                    continue;
                                }
                            }

                            else
                            {
                                if (individualAccountIMDict.ContainsKey(accountNumber))
                                {
                                    account = individualAccountIMDict[accountNumber];
                                }

                                if (account != null)
                                {
                                    rmid = account.rmid;
                                    individualAccountIMDict.Remove(accountNumber);
                                }
                                else
                                {
                                    Util.Log(logType.Warning, String.Format("Account does not exist in IM full account list. Combine Account Number:{0}", accountNumber));
                                    continue;
                                }
                            }
                        }
                        else if (applicationID.Equals("ST", StringComparison.OrdinalIgnoreCase))
                        {
                            // Change List to Dictionary for better performance
                            var account = (Util.account)null;
                            if (!enhance)
                            {
                                account = individualAccountSTList.Find(a => a.acctNo == accountNumber);
                                if (account != null)
                                {
                                    rmid = account.rmid;
                                    individualAccountSTDict.Remove(accountNumber);
                                }
                                else
                                {
                                    Util.Log(logType.Warning, String.Format("Account does not exist in ST full account list. Combine Account Number:{0}", accountNumber));
                                    continue;
                                }
                            }
                            else
                            {
                                if (individualAccountSTDict.ContainsKey(accountNumber))
                                {
                                    account = individualAccountSTDict[accountNumber];
                                }

                                if (account != null)
                                {
                                    rmid = account.rmid;
                                    individualAccountSTDict.Remove(accountNumber);
                                }
                                else
                                {
                                    Util.Log(logType.Warning, String.Format("Account does not exist in ST full account list. Combine Account Number:{0}", accountNumber));
                                    continue;
                                }
                            }
                        }

                        if (primarySecondaryIndicator.Equals("P", StringComparison.OrdinalIgnoreCase))
                        {
                            combineAccountList.Add(new Util.account(rmid, accountNumber, statementType, Properties.Settings.Default.CategoryCombine, group));
                        }
                        else if (primarySecondaryIndicator.Equals("S", StringComparison.OrdinalIgnoreCase))
                        {
                            combineAccountSecondaryList.Add(new Util.account(rmid, accountNumber, statementType, Properties.Settings.Default.CategoryCombine, group));
                        }
                    }
                }
                else
                {
                    Util.Log(logType.Error, string.Format("Combine account list file does not existed. File path:{0}", Properties.Settings.Default.CombineAccountListPath));
                }
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, string.Format("Error in AccountImport(reading combine account list). Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, "AccountImport(read combine account list) End.");
            Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END processing Bank", DateTime.Now));
            #endregion

            #region citicFirst account list
            if (Properties.Settings.Default.CFignoreFileCheck == "Y")
            {
                Util.Log(logType.Info, "CFignoreFileCheck = \"Y\", AccountImport(read citicFirst account list) Pass.");
            }
            else
            {
                Util.Log(logType.Info, "AccountImport(read citicFirst account list) Start.");
                Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START processing CiticFirst", DateTime.Now));
                try
                {
                    Dictionary<String, String> rmidToStatementTypeMapping = new Dictionary<String, String>();
                    List<String> rmidCheckExistList = new List<String>();
                    HashSet<String> rmidCheckExistHash = new HashSet<String>();

                    #region init rmidToStatementTypeMapping
                    if (File.Exists(Properties.Settings.Default.RMIDStatementTypeMappingPath))
                    {
                        List<String> lines = new List<String>();
                        using (TextReader tr = File.OpenText(Properties.Settings.Default.RMIDStatementTypeMappingPath))
                        {
                            String eachLine = String.Empty;
                            while ((eachLine = tr.ReadLine()) != null)
                            {
                                if (eachLine.Contains(';'))
                                    continue;
                                if (eachLine.Length < 285 + 2)
                                {
                                    Util.Log(logType.Error, String.Format("AccountImport(read rmidToStatementTypeMapping) Incomplete line. File Path:{0}. Line:{1}", Properties.Settings.Default.RMIDStatementTypeMappingPath, eachLine));
                                    return;
                                }
                                lines.Add(eachLine);
                            }
                            tr.Close();
                        }
                        Util.Log(logType.Info, String.Format("Total No. of records in cafpbinf.dat = {0}", lines.Count.ToString()));
                        foreach (var line in lines)
                        {
                            String rmid = line.Substring(0, 15);
                            String statementType = line.Substring(285, 2);

                            if (!rmidToStatementTypeMapping.ContainsKey(rmid))
                                rmidToStatementTypeMapping.Add(rmid, statementType);
                            else
                                Util.Log(logType.Warning, String.Format("AccountImport(read rmidToStatementTypeMapping) rmid map to multiple statementType. rmid:{0}", rmid));
                        }
                    }
                    else
                    {
                        Util.Log(logType.Error, String.Format("RMID to statementType mapping file does not existed. File path:{0}", Properties.Settings.Default.AccountNumberRmNumberMappingPath));
                    }
                    #endregion

                    if (File.Exists(Properties.Settings.Default.CiticFirstAccountListPath))
                    {
                        List<String> lines = new List<String>();
                        using (TextReader tr = File.OpenText(Properties.Settings.Default.CiticFirstAccountListPath))
                        {
                            String eachLine = String.Empty;
                            while ((eachLine = tr.ReadLine()) != null)
                            {
                                if (eachLine.Contains(';'))
                                    continue;
                                if (eachLine.Length < 41 + 9)
                                {
                                    Util.Log(logType.Error, String.Format("AccountImport(read CiticFirst) Incomplete line. File Path:{0}. Line:{1}", Properties.Settings.Default.CiticFirstAccountListPath, eachLine));
                                    return;
                                }
                                lines.Add(eachLine);
                            }
                            tr.Close();
                        }
                        Util.Log(logType.Info, String.Format("Total No. of records in CAFPBACL.DAT = {0}", lines.Count.ToString()));
                        foreach (var line in lines)
                        {
                            String applicationID = line.Substring(15, 5).Trim();

                            if (applicationID.Equals("IM", StringComparison.OrdinalIgnoreCase) || applicationID.Equals("ST", StringComparison.OrdinalIgnoreCase))
                            {
                                String rmid = line.Substring(0, 15);
                                String accountNumber = applicationID.Equals("IM", StringComparison.OrdinalIgnoreCase) ? (line.Substring(29, 3) + line.Substring(37, 9)).Trim() : (line.Substring(29, 3) + line.Substring(41, 9)).Trim(); //695270023400
                                String statementType = "";

                                if (rmidToStatementTypeMapping.ContainsKey(rmid))
                                {
                                    statementType = rmidToStatementTypeMapping[rmid];
                                }
                                else
                                {
                                    Util.Log(logType.Warning, String.Format("StatementType not found. rmid:{0}", line.Substring(0, 15)));
                                    continue;
                                }

                                if (applicationID.Equals("IM", StringComparison.OrdinalIgnoreCase))
                                {
                                    // Change List to Dictionary for better performance
                                    var account = (Util.account)null;
                                    if (!enhance)
                                        account = individualAccountIMList.Find(a => a.acctNo == accountNumber);
                                    else
                                    {
                                        if (individualAccountIMDict.ContainsKey(accountNumber))
                                            account = individualAccountIMDict[accountNumber];
                                    }

                                    if (account != null)
                                    {
                                        // Not the proper way to remove item from list individualAccountIMList.Remove(account);
                                        individualAccountIMDict.Remove(accountNumber);
                                    }
                                }
                                else if (applicationID.Equals("ST", StringComparison.OrdinalIgnoreCase))
                                {
                                    // Change List to Dictionary for better performance
                                    var account = (Util.account)null;
                                    if (!enhance)
                                        account = individualAccountSTList.Find(a => a.acctNo == accountNumber);
                                    else
                                    {
                                        if (individualAccountSTDict.ContainsKey(accountNumber))
                                            account = individualAccountSTDict[accountNumber];
                                    }

                                    if (account != null)
                                    {
                                        // Not the proper way to remove item from list individualAccountSTList.Remove(account);
                                        individualAccountSTDict.Remove(accountNumber);
                                    }
                                }

                                tbCiticFirstAccountList.Add(new Util.account(new List<String> { rmid }, accountNumber, statementType, Properties.Settings.Default.CategoryCiticFirst));

                                var isAdd = (List<String>)null;

                                if (!enhance)
                                    isAdd = rmidCheckExistList.FindAll(a => a.Equals(rmid)).ToList();
                                else
                                {
                                    if (!rmidCheckExistHash.Contains(rmid))
                                    {
                                        isAdd = new List<String>();
                                    }
                                    else
                                    {
                                        if (isAdd == null)
                                            isAdd = new List<String>();

                                        isAdd.Add(rmid);
                                    }
                                }
                                // Change List to Dictionary for better performance
                                if (isAdd.Count == 0)
                                {
                                    citicFirstAccountList.Add(new Util.account(new List<String> { rmid }, "", statementType, Properties.Settings.Default.CategoryCiticFirst));
                                    rmidCheckExistList.Add(rmid);
                                    rmidCheckExistHash.Add(rmid);
                                }
                            }
                        }
                    }
                    else
                    {
                        Util.Log(logType.Error, string.Format("CiticFirst account list file does not existed. File path:{0}", Properties.Settings.Default.CiticFirstAccountListPath));
                    }

                    rmidCheckExistList.Clear();
                    rmidCheckExistHash.Clear();
                }
                catch (Exception ex)
                {
                    Util.Log(logType.Error, string.Format("Error in AccountImport(reading citicFirst account list). Error message:{0}", ex.Message));
                }
                Util.Log(logType.Info, "AccountImport(read citicFirst account list) End.");
                Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END processing CiticFirst", DateTime.Now));
            }
            #endregion

            #region creditCard accountlist
            Util.Log(logType.Info, "AccountImport(read creditCard account list) Start.");
            Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START processing Card", DateTime.Now));
            try
            {
                if (File.Exists(Properties.Settings.Default.CreditCardAccountListPath))
                {
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.CreditCardAccountListPath))
                    {
                        String eachLine = String.Empty;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            if (eachLine.Contains(';'))
                                continue;
                            if (eachLine.Length < 207 + 1)
                            {
                                Util.Log(logType.Error, String.Format("AccountImport(read CreditCard) Incomplete line. File Path:{0}. Line:{1}", Properties.Settings.Default.CreditCardAccountListPath, eachLine));
                                return;
                            }
                            if ((eachLine.Substring(207, 1) == "0" || eachLine.Substring(207, 1) == "1" || eachLine.Substring(207, 1) == "2") &&
                                (eachLine.Substring(287, 1).Trim() != "Q" && eachLine.Substring(287, 1).Trim() != "X"))
                            {
                                lines.Add(eachLine);
                            }
                        }
                        tr.Close();
                    }

                    List<tbSystemSetting> creditCardTypeList = entity.tbSystemSetting.Where(a => a.SettingName == "CreditCardType" && a.EffectiveFrom <= DateTime.Now && a.EffectiveTo >= DateTime.Now).ToList();
                    Util.Log(logType.Info, String.Format("Total No. of records in ccacctpt.dat = {0}", lines.Count.ToString()));
                    foreach (var line in lines)
                    {
                        String rmid = line.Substring(8, 19).Trim();
                        String cardType = line.Substring(27, 6).Trim();
                        String cardNumber = line.Substring(131, 16).Trim();

                        String blockCode = line.Substring(287, 1).Trim();

                        var setting = creditCardTypeList.Where(a => a.SettingValue == cardType).FirstOrDefault();
                        if (setting != null)
                        {
                            creditCardAccountList.Add(new Util.account(new List<String> { rmid }, cardNumber, setting.StatementType, Properties.Settings.Default.CategoryCreditCard));
                        }
                    }
                }
                else
                {
                    Util.Log(logType.Error, string.Format("Credit card account list file does not existed. File path:{0}", Properties.Settings.Default.CreditCardAccountListPath));
                }
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, string.Format("Error in AccountImport(reading creditCard account list). Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, "AccountImport(read creditCard account list) End.");
            Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END processing Card", DateTime.Now));
            #endregion

            // EStatement enhancement, added E-Advice
            #region InfoCast account list
            Util.Log(logType.Info, "AccountImport(read InfoCast account list) Start.");
            Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} START processing InfoCast", DateTime.Now));
            try
            {
                Dictionary<String, String> rmidToStatementTypeMapping = new Dictionary<String, String>();

                if (File.Exists(Properties.Settings.Default.InfoCastAccountListPath))
                {
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.InfoCastAccountListPath))
                    {
                        String eachLine = String.Empty;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            if (eachLine.Contains(';'))
                                continue;

                            String[] arrColumns = null;
                            arrColumns = eachLine.Split('|');

                            if (arrColumns.Length < 4)
                            {
                                Util.Log(logType.Error, String.Format("AccountImport(read InfoCast) Incomplete line. File Path:{0}. Line:{1}", Properties.Settings.Default.InfoCastAccountListPath, eachLine));
                                return;
                            }
                            else if (!(arrColumns[2].Trim().Equals("A", StringComparison.OrdinalIgnoreCase) ||
                                       arrColumns[2].Trim().Equals("C", StringComparison.OrdinalIgnoreCase) ||
                                       arrColumns[2].Trim().Equals("B", StringComparison.OrdinalIgnoreCase) ||
                                       arrColumns[2].Trim().Equals("D", StringComparison.OrdinalIgnoreCase) ||
                                       arrColumns[2].Trim().Equals("S", StringComparison.OrdinalIgnoreCase)
                                     ))
                            {// account status
                                Util.Log(logType.Error, String.Format("AccountImport(read InfoCast) Invalid account status. File Path:{0}. Line:{1}", Properties.Settings.Default.InfoCastAccountListPath, eachLine));
                                return;
                            }
                            else if (!(arrColumns[3].Trim().Equals("Y", StringComparison.OrdinalIgnoreCase) || (arrColumns[3].Trim().Equals("N", StringComparison.OrdinalIgnoreCase))))
                            {// joint account
                                Util.Log(logType.Error, String.Format("AccountImport(read InfoCast) Invalid joint account type. File Path:{0}. Line:{1}", Properties.Settings.Default.InfoCastAccountListPath, eachLine));
                                return;
                            }

                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }
                    Util.Log(logType.Info, String.Format("Total No. of records in IBKESM001 = {0}", lines.Count.ToString()));
                    List<tbAccountList> tbAccountList = entity.tbAccountList.ToList();

                    foreach (var line in lines)
                    {
                        String[] arrColumns = null;
                        arrColumns = line.Split('|');

                        String customerID = arrColumns[0].Trim();
                        String rmid = "";
                        String accountNumber = arrColumns[1].Trim();
                        String accStatus = arrColumns[2].Trim();
                        String jointAcc = arrColumns[3].Trim();

                        if (accStatus.Trim().Equals("C", StringComparison.OrdinalIgnoreCase))
                        {   // skip to put into temp if account status is closed
                            continue;
                        }
                        // if jointAcc = "Y"
                        if (jointAcc.Equals("Y", StringComparison.OrdinalIgnoreCase))
                        {
                            if (!rmNumber30ToRmidMapping.ContainsKey(customerID))
                            {
                                continue;
                            }
                            rmid = rmNumber30ToRmidMapping[customerID];

                            List<tbAccountList> tbAccountList1 = entity.tbAccountList.Where(a => a.RMID.Equals(rmid) && a.AccountNumber.Equals(accountNumber) && a.Category.Equals(Properties.Settings.Default.CategoryInfoCast, StringComparison.OrdinalIgnoreCase)).ToList();

                            if ((tbAccountList1.Count > 0))
                            {

                                foreach (var account in tbAccountList1)
                                {
                                    account.Active = "N";
                                    account.LastUpdate = DateTime.Now;
                                    entity.SaveChanges();

                                    infoCastAccountList.Add(new Util.account(new List<String> { rmid }, accountNumber, InfoCastStatementType, Properties.Settings.Default.CategoryInfoCast, "", "N"));
                                }
                            }
                            else
                            {
                                infoCastAccountList.Add(new Util.account(new List<String> { rmid }, accountNumber, InfoCastStatementType, Properties.Settings.Default.CategoryInfoCast, "", "N"));
                            }
                        }
                        else
                        {// if jointAcc = "N"
                         // check account number exists or not
                            if (!rmNumber30ToRmidMapping.ContainsKey(customerID))
                            {
                                continue;
                            }

                            rmid = rmNumber30ToRmidMapping[customerID];

                            List<tbAccountList> tbAccountList1 = entity.tbAccountList.Where(a => a.RMID.Equals(rmid) && a.AccountNumber.Equals(accountNumber) && a.Category.Equals(Properties.Settings.Default.CategoryInfoCast, StringComparison.OrdinalIgnoreCase)).ToList();

                            if (tbAccountList1.Count() > 0)
                            {
                                foreach (var account in tbAccountList1)
                                {
                                    String active = "";

                                    if (accStatus.Equals("A", StringComparison.OrdinalIgnoreCase))
                                    {
                                        account.Active = "Y";
                                        active = "Y";
                                    }
                                    else
                                    {
                                        account.Active = "N";
                                        active = "N";
                                    }
                                    account.LastUpdate = DateTime.Now;
                                    entity.SaveChanges();
                                    infoCastAccountList.Add(new Util.account(new List<String> { rmid }, accountNumber, InfoCastStatementType, Properties.Settings.Default.CategoryInfoCast, "", active));
                                }
                            }
                            else
                            {
                                String active = "";
                                if (accStatus.Equals("A", StringComparison.OrdinalIgnoreCase))
                                    active = "Y";
                                else
                                    active = "N";

                                infoCastAccountList.Add(new Util.account(new List<String> { rmid }, accountNumber, InfoCastStatementType, Properties.Settings.Default.CategoryInfoCast, "", active));
                            }
                        }
                    }
                    tbAccountList.Clear();
                }
                else
                {
                    if (!"Y".Equals(Properties.Settings.Default.SuppressInfoCastError))
                    {
                        Util.Log(logType.Error, string.Format("InfoCast account list file does not existed. File path:{0}", Properties.Settings.Default.InfoCastAccountListPath));
                    }
                }
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, string.Format("Error in AccountImport(reading InfoCast account list). Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, "AccountImport(read InfoCast account list) End.");
            Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END processing InfoCast", DateTime.Now));
            #endregion

            #region init BPID BP_name(postal address line 1) Mapping
            Util.Log(logType.Info, String.Format("{0} init BPID BP_name Mapping start", DateTime.Now.ToString("tt hh:mm:ss")));
            try
            {
                int recordCount = 0;
                using (TextReader tr = File.OpenText(Properties.Settings.Default.PostalAddrLine1Path))
                {
                    String eachLine = String.Empty;
                    while ((eachLine = tr.ReadLine()) != null)
                    {
                        if (eachLine.Contains(';'))
                            continue;

                        recordCount++;
                        if (eachLine.Length < 742)
                        {
                            Util.Log(logType.Error, String.Format("Error in reading {0}, Line {1} incorrect :\n{2}", Path.GetFileName(Properties.Settings.Default.PostalAddrLine1Path), recordCount + 1, eachLine));
                            return;
                        }

                        String Addr_BPID = eachLine.Substring((int)BPInfo_GetData.bpidStartPos, (int)BPInfo_GetData.bpidLength).Trim();
                        String PostalAddrLine1 = eachLine.Substring((int)BPInfo_GetData.bp_nameStartPos, (int)BPInfo_GetData.bp_nameLength).Trim();
                        if (!bpidToBpnameDict.ContainsKey(Addr_BPID))
                            bpidToBpnameDict.Add(Addr_BPID, PostalAddrLine1);
                    }
                    tr.Close();
                }
                Util.Log(logType.Info, String.Format("Total No. of records in {0} = {1}", Path.GetFileName(Properties.Settings.Default.PostalAddrLine1Path), bpidToBpnameDict.Count));
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in region init BPID BP_name(postal address line 1) Mapping : {0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("{0} init BPID BP_name Mapping finish", DateTime.Now.ToString("tt hh:mm:ss")));
            #endregion

            #region read BP information
            Util.Log(logType.Info, String.Format("{0} read BP information start", DateTime.Now.ToString("tt hh:mm:ss")));
            try
            {
                int recordCount = 0;
                using (TextReader tr = File.OpenText(Properties.Settings.Default.BPInfoPath))
                {
                    String eachLine = String.Empty;
                    while ((eachLine = tr.ReadLine()) != null)
                    {
                        if (eachLine.Contains(';'))
                            continue;

                        recordCount++;
                        if (eachLine.Length < (int)BPInfo_GetData.sbs_avp_bp_relLength)
                        {
                            Util.Log(logType.Error, String.Format("Error in reading {0}, Line {1} incorrect :\n{2}", Path.GetFileName(Properties.Settings.Default.BPInfoPath), recordCount + 1, eachLine));
                            return;
                        }

                        String bpid = eachLine.Substring((int)BPInfo_GetData.bpidStartPos, (int)BPInfo_GetData.bpidLength).Trim();
                        String ckAVQ = eachLine.Substring((int)BPInfo_GetData.ckAVQStartPos, (int)BPInfo_GetData.ckAVQLength).Trim();
                        String rm_no = eachLine.Substring((int)BPInfo_GetData.rm_noStartPos, (int)BPInfo_GetData.rm_noLength).Trim();
                        if (tempBPInfoDict.ContainsKey(bpid + rm_no))
                            continue;
                        String no_of_acc_holder = eachLine.Substring((int)BPInfo_GetData.no_of_acc_holderStartPos, (int)BPInfo_GetData.no_of_acc_holderLength).Trim();
                        String BP_name = bpidToBpnameDict.ContainsKey(bpid) ? bpidToBpnameDict[bpid] : "";
                        String rmid = rmNumberToRmidMapping.ContainsKey("00000" + rm_no) ? rmNumberToRmidMapping["00000" + rm_no] : "";

                        tempBPInfoDict.Add(bpid + rm_no, new Util.BPInfo(bpid, String.IsNullOrEmpty(ckAVQ.Trim()) ? "N" : "Y", rm_no, BP_name, no_of_acc_holder, rmid));
                        if (!bpidToCkAvqDict.ContainsKey(bpid))
                            bpidToCkAvqDict.Add(bpid, String.IsNullOrEmpty(ckAVQ.Trim()) ? "N" : "Y");
                    }
                    tr.Close();
                    Util.Log(logType.Info, String.Format("Total No. of records in {0} = {1}", Path.GetFileName(Properties.Settings.Default.BPInfoPath), tempBPInfoDict.Count));
                }
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in region read BP information : {0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("{0} read BP information finish", DateTime.Now.ToString("tt hh:mm:ss")));
            bpidToBpnameDict.Clear();
            #endregion

            #region read superbank account list
            Util.Log(logType.Info, String.Format("{0} read superbank account list start", DateTime.Now.ToString("tt hh:mm:ss")));
            try
            {
                int recordCount = 0;
                using (TextReader tr = File.OpenText(Properties.Settings.Default.SuperBankAccountListPath))
                {
                    String eachLine = String.Empty;
                    while ((eachLine = tr.ReadLine()) != null)
                    {
                        if (eachLine.Contains(';'))
                            continue;

                        recordCount++;
                        if (eachLine.Length < (int)SBS_GetData.SBSACLLength)
                        {
                            Util.Log(logType.Error, String.Format("Error in reading {0}, Line {1} incorrect :\n{2}", Path.GetFileName(Properties.Settings.Default.SuperBankAccountListPath), recordCount + 1, eachLine));
                            return;
                        }
                        String BPID = eachLine.Substring((int)SBS_GetData.bpidStartPos, (int)SBS_GetData.bpidLength).Trim();
                        String applicationID = eachLine.Substring((int)SBS_GetData.applicationIDStartPos, (int)SBS_GetData.applicationIDLength).Trim();
                        String AcctType = eachLine.Substring((int)SBS_GetData.acctTypeStartPos, (int)SBS_GetData.acctTypeLength).Trim();

                        if (applicationID.Equals("AVQ", StringComparison.OrdinalIgnoreCase))    // App-ID equal AVQ
                        {
                            String ContainerAcNo = eachLine.Substring((int)SBS_GetData.containerAcnoStartPos, (int)SBS_GetData.containerAcnoLength).Trim();
                            if (!tbSubAccountListDict.ContainsKey(BPID + ContainerAcNo))
                                tbSubAccountListDict.Add(BPID + ContainerAcNo, new Util.account(BPID, ContainerAcNo, ContainerAcNo, Properties.Settings.Default.CategorySuperBank, "Y"));
                            else
                            {
                                Util.Log(logType.Error, String.Format("Error in reading {0}, App-ID = AVQ, Line {1} has same BPID & ContainerNumber as other line :\n{2}", Path.GetFileName(Properties.Settings.Default.SuperBankAccountListPath), recordCount + 1, eachLine));
                                return;
                            }

                            if (tbSuperBankAccountDict.ContainsKey(BPID))
                                continue;

                            tbSuperBankAccountDict.Add(BPID, new Util.account(new List<String> { BPID }, BPID, Properties.Settings.Default.StatementTypeSuperBank, Properties.Settings.Default.CategorySuperBank));

                            if (!bpidToAcctTypeDict.ContainsKey(BPID))
                                bpidToAcctTypeDict.Add(BPID, AcctType);
                        }
                        else    // App-ID equal IM/ST/others
                        {
                            String Control1 = eachLine.Substring((int)SBS_GetData.control1StartPos, (int)SBS_GetData.control1Length).Trim();
                            String Control2 = eachLine.Substring((int)SBS_GetData.control2StartPos, (int)SBS_GetData.control2Length).Trim();
                            String Control3 = eachLine.Substring((int)SBS_GetData.control3StartPos, (int)SBS_GetData.control3Length).Trim();
                            String Control4 = eachLine.Substring((int)SBS_GetData.control4StartPos, (int)SBS_GetData.control4Length).Trim();

                            if (applicationID.Equals("IM", StringComparison.OrdinalIgnoreCase))
                            {
                                String AccNo = eachLine.Substring((int)SBS_GetData.IMAccNoStartPos, (int)SBS_GetData.IMAccNoLength);
                                String imCkAccountNo = Control3 + AccNo;

                                if (individualAccountIMDict.ContainsKey(imCkAccountNo))
                                    individualAccountIMDict.Remove(imCkAccountNo);
                            }
                            else if (applicationID.Equals("ST", StringComparison.OrdinalIgnoreCase))
                            {
                                String AccNo = eachLine.Substring((int)SBS_GetData.STAccNoStartPos, (int)SBS_GetData.STAccNoLength);
                                String stCkAccountNo = Control3 + AccNo;

                                if (individualAccountSTDict.ContainsKey(stCkAccountNo))
                                    individualAccountSTDict.Remove(stCkAccountNo);
                            }

                            if (tbSuperBankAccountDict.ContainsKey(BPID))
                                continue;

                            tbSuperBankAccountDict.Add(BPID, new Util.account(new List<String> { BPID }, BPID, Properties.Settings.Default.StatementTypeSuperBank, Properties.Settings.Default.CategorySuperBank));

                            if (!bpidToAcctTypeDict.ContainsKey(BPID))
                                bpidToAcctTypeDict.Add(BPID, AcctType);
                        }
                    }
                    tr.Close();
                }
                Util.Log(logType.Info, String.Format("Total No. of records in {0} = {1}", Path.GetFileName(Properties.Settings.Default.SuperBankAccountListPath), recordCount));
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in region read superbank account list : {0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("{0} read superbank account list finish", DateTime.Now.ToString("tt hh:mm:ss")));
            #endregion


            #region superbank account default status checking
            /****************************************modified on 2018-10-18*********************************************/
            //purpose : remove the checking of registration status='U', status='Y' account comparing from SBS account list.
            Util.Log(logType.Info, String.Format("{0} superbank account default status checking start", DateTime.Now.ToString("tt hh:mm:ss")));
            //Dictionary<String, String> BankAccStatusDict = new Dictionary<String, String>();
            Dictionary<String, String> bpidExistDict = new Dictionary<String, String>();
            Dictionary<String, List<String>> combinePrimarySecondaryDict = new Dictionary<String, List<String>>();
            Dictionary<String, List<String>> secAccGroupToAcnoDict = new Dictionary<String, List<String>>();
            Dictionary<String, List<String>> bpidToBankAccDict = new Dictionary<String, List<String>>();

            try
            {
                //Util.Log(logType.Info, String.Format("{0} start building SY individual account Dict.", DateTime.Now.ToString("tt hh:mm:ss")));
                //List<tbAccountList> individualAccStatusList = entity.tbAccountList.Where(a => a.Active == "Y" && a.RegistrationStatus == "S" && a.Category == Properties.Settings.Default.CategoryIndividual).ToList();
                /*Util.Log(logType.Info, String.Format("{0} start building UY individual account Dict.", DateTime.Now.ToString("tt hh:mm:ss")));
                List<tbAccountList> individualAccStatusList = entity.tbAccountList.Where(a => a.Active == "Y" && a.RegistrationStatus == "U" && a.Category == Properties.Settings.Default.CategoryIndividual).ToList();


                foreach (var account in individualAccStatusList)
                    if (!BankAccStatusDict.ContainsKey(account.AccountNumber))
                        BankAccStatusDict.Add(account.AccountNumber, "");
                //Util.Log(logType.Info, String.Format("{0} finish building SY individual account Dict.", DateTime.Now.ToString("tt hh:mm:ss")));
                Util.Log(logType.Info, String.Format("{0} finish building UY individual account Dict.", DateTime.Now.ToString("tt hh:mm:ss")));

                //Util.Log(logType.Info, String.Format("{0} start building SY combine account Dict.", DateTime.Now.ToString("tt hh:mm:ss")));
                //List<tbCombineAccountList> secondaryCombineAccList = entity.tbCombineAccountList.Where(a => a.Type == "S" && a.Active == "Y").ToList();
                Util.Log(logType.Info, String.Format("{0} start building UY combine account Dict.", DateTime.Now.ToString("tt hh:mm:ss")));
                List<tbCombineAccountList> secondaryCombineAccList = entity.tbCombineAccountList.Where(a => a.Type == "U" && a.Active == "Y").ToList();


                foreach (var secAcc in secondaryCombineAccList)
                {
                    if (!secAccGroupToAcnoDict.ContainsKey(secAcc.Group))
                        secAccGroupToAcnoDict.Add(secAcc.Group, new List<String> { secAcc.AccountNumber });
                    else
                        secAccGroupToAcnoDict[secAcc.Group].Add(secAcc.AccountNumber);
                }
                Util.Log(logType.Info, "finish building Group-AccountNumber Dict.");
                List<tbCombineAccountList> PrimaryCombineAccList = entity.tbCombineAccountList.Where(a => a.Type == "P").ToList();
                foreach (var primAcc in PrimaryCombineAccList)
                {
                    if (!combinePrimarySecondaryDict.ContainsKey(primAcc.RMID + primAcc.AccountNumber))
                        combinePrimarySecondaryDict.Add(primAcc.RMID + primAcc.AccountNumber, secAccGroupToAcnoDict[primAcc.Group]);
                }
                Util.Log(logType.Info, "finish building combine Pri-Sec Dict.");
                secondaryCombineAccList.Clear();
                PrimaryCombineAccList.Clear();
                secAccGroupToAcnoDict.Clear();

                //List<tbAccountList> combineAccStatusList = entity.tbAccountList.Where(a => a.Active == "Y" && a.RegistrationStatus == "S" && a.Category == Properties.Settings.Default.CategoryCombine).ToList();
                List<tbAccountList> combineAccStatusList = entity.tbAccountList.Where(a => a.Active == "Y" && a.RegistrationStatus == "U" && a.Category == Properties.Settings.Default.CategoryCombine).ToList();
                int combineAccountCounter = 0;
                foreach (var account in combineAccStatusList)
                {
                    if (!BankAccStatusDict.ContainsKey(account.AccountNumber))
                    {
                        BankAccStatusDict.Add(account.AccountNumber, "");
                        combineAccountCounter++;
                        if (combinePrimarySecondaryDict.ContainsKey(account.RMID + account.AccountNumber))
                            foreach (var acno in combinePrimarySecondaryDict[account.RMID + account.AccountNumber])
                                if (!BankAccStatusDict.ContainsKey(acno))
                                    BankAccStatusDict.Add(acno, "");

                        if (combineAccountCounter % 5000 == 0)
                            //Util.Log(logType.Info, String.Format("{0} Check time spent for building every 5000 SY combine account Dict.", DateTime.Now.ToString("tt hh:mm:ss")));
                              Util.Log(logType.Info, String.Format("{0} Check time spent for building every 5000 UY combine account Dict.", DateTime.Now.ToString("tt hh:mm:ss")));
                    }
                }
                combinePrimarySecondaryDict.Clear();
                combineAccStatusList.Clear();
                //Util.Log(logType.Info, String.Format("{0} finish building SY combine account Dict.", DateTime.Now.ToString("tt hh:mm:ss")));
                Util.Log(logType.Info, String.Format("{0} finish building UY combine account Dict.", DateTime.Now.ToString("tt hh:mm:ss")));*/

            /*****************************************************************************************************************************************/

                Util.Log(logType.Info, String.Format("{0} start building bp exist Dict.", DateTime.Now.ToString("tt hh:mm:ss")));
                List<tbAccountList> bpidExistList = entity.tbAccountList.Where(a => a.Category == Properties.Settings.Default.CategorySuperBank).ToList();
                foreach (var account in bpidExistList)
                {
                    if (!bpidExistDict.ContainsKey(account.RMID))
                        bpidExistDict.Add(account.RMID, "");
                }
                bpidExistList.Clear();
                Util.Log(logType.Info, String.Format("{0} finish building bp exist Dict.", DateTime.Now.ToString("tt hh:mm:ss")));               

                Util.Log(logType.Info, String.Format("{0} start building bpid bank accounts mapping", DateTime.Now.ToString("tt hh:mm:ss")));
                using (TextReader tr = File.OpenText(Properties.Settings.Default.SuperBankAccountListPath))
                {
                    String eachLine = String.Empty;
                    while ((eachLine = tr.ReadLine()) != null)
                    {
                        if (eachLine.Contains(';'))
                            continue;

                        String BPID = eachLine.Substring((int)SBS_GetData.bpidStartPos, (int)SBS_GetData.bpidLength).Trim();
                        if (bpidExistDict.ContainsKey(BPID))
                            continue;
                        if (!bpidToCkAvqDict.ContainsKey(BPID))
                            continue;
                        if (bpidToCkAvqDict[BPID] == "Y")
                            continue;

                        String AcctType = eachLine.Substring((int)SBS_GetData.acctTypeStartPos, (int)SBS_GetData.acctTypeLength).Trim();
                        if (AcctType != "Individual")
                            continue;

                        String applicationID = eachLine.Substring((int)SBS_GetData.applicationIDStartPos, (int)SBS_GetData.applicationIDLength).Trim();
                        if (applicationID.Equals("IM", StringComparison.OrdinalIgnoreCase))
                        {
                            String AccNo = eachLine.Substring((int)SBS_GetData.IMAccNoStartPos, (int)SBS_GetData.IMAccNoLength);
                            String Control3 = eachLine.Substring((int)SBS_GetData.control3StartPos, (int)SBS_GetData.control3Length).Trim();
                            String imCkAccountNo = Control3 + AccNo;

                            if (!bpidToBankAccDict.ContainsKey(BPID))
                                bpidToBankAccDict.Add(BPID, new List<String> { imCkAccountNo });
                            else
                                bpidToBankAccDict[BPID].Add(imCkAccountNo);
                        }
                        else if (applicationID.Equals("ST", StringComparison.OrdinalIgnoreCase))
                        {
                            String AccNo = eachLine.Substring((int)SBS_GetData.STAccNoStartPos, (int)SBS_GetData.STAccNoLength);
                            String Control3 = eachLine.Substring((int)SBS_GetData.control3StartPos, (int)SBS_GetData.control3Length).Trim();
                            String stCkAccountNo = Control3 + AccNo;

                            if (!bpidToBankAccDict.ContainsKey(BPID))
                                bpidToBankAccDict.Add(BPID, new List<String> { stCkAccountNo });
                            else
                                bpidToBankAccDict[BPID].Add(stCkAccountNo);
                        }
                    }
                    tr.Close();
                }
                Util.Log(logType.Info, String.Format("{0} finish building bpid bank accounts mapping", DateTime.Now.ToString("tt hh:mm:ss")));            

                Util.Log(logType.Info, String.Format("{0} start bp default status check", DateTime.Now.ToString("tt hh:mm:ss")));
                foreach (var bpid in bpidToBankAccDict)
                {
                    /*Boolean defaultS = true;
                    foreach (var bankAcc in bpid.Value)
                    {
                        if (BankAccStatusDict.ContainsKey(bankAcc))
                        //    continue;
                        //else
                        {
                            defaultS = false;
                            break;
                        }
                    }

                    if (defaultS)*/
                        bpidDefaultSDict.Add(bpid.Key, "");
                }
                Util.Log(logType.Info, String.Format("{0} finish bp default status check", DateTime.Now.ToString("tt hh:mm:ss")));
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in region superbank account default status checking : {0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("{0} superbank account default status checking finish", DateTime.Now.ToString("tt hh:mm:ss")));
            //BankAccStatusDict.Clear();
            bpidExistDict.Clear();
            bpidToBankAccDict.Clear();
            #endregion


            #region read container account list
            Util.Log(logType.Info, String.Format("{0} read container account list start", DateTime.Now.ToString("tt hh:mm:ss")));
            try
            {
                int recordCount = 0;
                using (TextReader tr = File.OpenText(Properties.Settings.Default.ContainerAccountListPath))
                {
                    String eachLine = String.Empty;
                    while ((eachLine = tr.ReadLine()) != null)
                    {
                        if (eachLine.Contains(';'))
                            continue;

                        recordCount++;
                        if (eachLine.Length < (int)SBS_GetData.SBSACLLength)
                        {
                            Util.Log(logType.Error, String.Format("Error in reading {0}, Line {1} incorrect :\n{2}", Path.GetFileName(Properties.Settings.Default.ContainerAccountListPath), recordCount + 1, eachLine));
                            return;
                        }

                        String BPID = eachLine.Substring((int)SBS_GetData.bpidStartPos, (int)SBS_GetData.bpidLength).Trim();
                        String ContainerAcNo = eachLine.Substring((int)SBS_GetData.containerAcnoStartPos, (int)SBS_GetData.containerAcnoLength).Trim();
                        String AcctType = eachLine.Substring((int)SBS_GetData.acctTypeStartPos, (int)SBS_GetData.acctTypeLength).Trim();

                        if (tbContainerAccountDict.ContainsKey(BPID + ContainerAcNo))
                            continue;
                        tbContainerAccountDict.Add(BPID + ContainerAcNo, new Util.account(new List<String> { BPID }, ContainerAcNo, Properties.Settings.Default.StatementTypeContainer, Properties.Settings.Default.CategoryContainer));

                        if (!bpidToAcctTypeDict.ContainsKey(BPID))
                            bpidToAcctTypeDict.Add(BPID, AcctType);

                        if (tbSubAccountListDict.ContainsKey(BPID + ContainerAcNo))
                            continue;
                        tbSubAccountListDict.Add(BPID + ContainerAcNo, new Util.account(BPID, ContainerAcNo, ContainerAcNo, Properties.Settings.Default.CategoryContainer, "Y"));
                    }
                    tr.Close();
                }
                Util.Log(logType.Info, String.Format("Total No. of records in {0} = {1}", Path.GetFileName(Properties.Settings.Default.ContainerAccountListPath), recordCount));
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in region read container account list : {0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("{0} read container account list finish", DateTime.Now.ToString("tt hh:mm:ss")));
            #endregion

            #region update tbSubAccountListTemp table
            Util.Log(logType.Info, String.Format("{0} update table {1} start", DateTime.Now.ToString("tt hh:mm:ss"), Properties.Settings.Default.tbSubAccountListTempTableName));
            try
            {
                DataTable tempDataTable = new DataTable();
                tempDataTable.Columns.Add("bpid");
                tempDataTable.Columns.Add("AccountNumber");
                tempDataTable.Columns.Add("DisplayAccountNumber");
                tempDataTable.Columns.Add("Category");
                tempDataTable.Columns.Add("Active");
                tempDataTable.Columns.Add("ActivateDate");
                tempDataTable.Columns.Add("CreateDate");
                tempDataTable.Columns.Add("CreateBy");
                tempDataTable.Columns.Add("ModifyDate");
                tempDataTable.Columns.Add("ModifyBy");

                foreach (var tbSubAcc in tbSubAccountListDict.Values)
                {
                    DateTime now = DateTime.Now;

                    DataRow row = tempDataTable.NewRow();
                    row["bpid"] = tbSubAcc.bpid;
                    row["AccountNumber"] = tbSubAcc.acctNo;
                    if (!String.IsNullOrEmpty(tbSubAcc.DisplayAccNo))
                        row["DisplayAccountNumber"] = getDisplayAccountNumber(tbSubAcc.DisplayAccNo, tbSubAcc.acctCategory);
                    else
                        row["DisplayAccountNumber"] = "";
                    row["Category"] = tbSubAcc.acctCategory;
                    row["Active"] = tbSubAcc.active;
                    row["ActivateDate"] = now;
                    row["CreateDate"] = now;
                    row["CreateBy"] = Properties.Settings.Default.AppName;
                    row["ModifyDate"] = now;
                    row["ModifyBy"] = Properties.Settings.Default.AppName;

                    tempDataTable.Rows.Add(row);
                }

                Util.Log(logType.Info, String.Format("Truncate {0} start", Properties.Settings.Default.tbSubAccountListTempTableName));
                entity.ExecuteStoreCommand(String.Format("TRUNCATE TABLE {0}", Properties.Settings.Default.tbSubAccountListTempTableName));
                Util.Log(logType.Info, String.Format("Truncate {0} finish", Properties.Settings.Default.tbSubAccountListTempTableName));

                // use bulk insert
                Util.Log(logType.Info, String.Format("Insert record into {0} start", Properties.Settings.Default.tbSubAccountListTempTableName));
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                {
                    connection.Open();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    {
                        bulkCopy.DestinationTableName = Properties.Settings.Default.tbSubAccountListTempTableName;
                        bulkCopy.BatchSize = 10000;
                        bulkCopy.BulkCopyTimeout = Properties.Settings.Default.BulkInsertTimeout;

                        SqlBulkCopyColumnMapping map_bpid = new SqlBulkCopyColumnMapping("bpid", "bpid");
                        bulkCopy.ColumnMappings.Add(map_bpid);
                        SqlBulkCopyColumnMapping map_AccountNumber = new SqlBulkCopyColumnMapping("AccountNumber", "AccountNumber");
                        bulkCopy.ColumnMappings.Add(map_AccountNumber);
                        SqlBulkCopyColumnMapping map_DisplayAccountNumber = new SqlBulkCopyColumnMapping("DisplayAccountNumber", "DisplayAccountNumber");
                        bulkCopy.ColumnMappings.Add(map_DisplayAccountNumber);
                        SqlBulkCopyColumnMapping map_Category = new SqlBulkCopyColumnMapping("Category", "Category");
                        bulkCopy.ColumnMappings.Add(map_Category);
                        SqlBulkCopyColumnMapping map_Active = new SqlBulkCopyColumnMapping("Active", "Active");
                        bulkCopy.ColumnMappings.Add(map_Active);
                        SqlBulkCopyColumnMapping map_ActivateDate = new SqlBulkCopyColumnMapping("ActivateDate", "ActivateDate");
                        bulkCopy.ColumnMappings.Add(map_ActivateDate);
                        SqlBulkCopyColumnMapping map_CreateDate = new SqlBulkCopyColumnMapping("CreateDate", "CreateDate");
                        bulkCopy.ColumnMappings.Add(map_CreateDate);
                        SqlBulkCopyColumnMapping map_CreateBy = new SqlBulkCopyColumnMapping("CreateBy", "CreateBy");
                        bulkCopy.ColumnMappings.Add(map_CreateBy);
                        SqlBulkCopyColumnMapping map_ModifyDate = new SqlBulkCopyColumnMapping("ModifyDate", "ModifyDate");
                        bulkCopy.ColumnMappings.Add(map_ModifyDate);
                        SqlBulkCopyColumnMapping map_ModifyBy = new SqlBulkCopyColumnMapping("ModifyBy", "ModifyBy");
                        bulkCopy.ColumnMappings.Add(map_ModifyBy);

                        bulkCopy.WriteToServer(tempDataTable);
                    }
                    connection.Close();
                }
                tempDataTable.Clear();
            }
            catch (Exception ex)
            {
                Util.Log(logType.Info, String.Format("Error in region update tbSubAccountListTemp table : {0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("Insert record into {0} finish", Properties.Settings.Default.tbSubAccountListTempTableName));
            Util.Log(logType.Info, String.Format("{0} update table {1} finish", DateTime.Now.ToString("tt hh:mm:ss"), Properties.Settings.Default.tbSubAccountListTempTableName));
            tbSubAccountListDict.Clear();
            #endregion

            #region update tbBPInfo table
            Util.Log(logType.Info, String.Format("{0} update table {1} start", DateTime.Now.ToString("tt hh:mm:ss"), Properties.Settings.Default.BPInfoTableName));
            try
            {
                DataTable tempDataTable = new DataTable();
                tempDataTable.Columns.Add("bpid");
                tempDataTable.Columns.Add("rm_no");
                tempDataTable.Columns.Add("BP_name");
                tempDataTable.Columns.Add("AcctType");
                tempDataTable.Columns.Add("no_of_acc_holder");
                tempDataTable.Columns.Add("rmid");
                tempDataTable.Columns.Add("AVQ");

                foreach (Util.BPInfo bpData in tempBPInfoDict.Values)
                {
                    DataRow row = tempDataTable.NewRow();
                    row["bpid"] = bpData.bpid;
                    row["rm_no"] = bpData.rm_no;
                    row["BP_name"] = bpData.BP_name;
                    row["AcctType"] = bpidToAcctTypeDict.ContainsKey(bpData.bpid) ? bpidToAcctTypeDict[bpData.bpid] : "";
                    row["no_of_acc_holder"] = Convert.ToInt16(bpData.no_of_acc_holder);
                    row["rmid"] = bpData.rmid.Trim();
                    row["AVQ"] = bpData.ckAVQ;

                    tempDataTable.Rows.Add(row);
                }

                Util.Log(logType.Info, String.Format("Truncate {0} start", Properties.Settings.Default.BPInfoTableName));
                entity.ExecuteStoreCommand(String.Format("TRUNCATE TABLE {0}", Properties.Settings.Default.BPInfoTableName));
                Util.Log(logType.Info, String.Format("Truncate {0} finish", Properties.Settings.Default.BPInfoTableName));

                // use bulk insert
                Util.Log(logType.Info, String.Format("Insert record into {0} start", Properties.Settings.Default.BPInfoTableName));
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                {
                    connection.Open();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    {
                        bulkCopy.DestinationTableName = Properties.Settings.Default.BPInfoTableName;
                        bulkCopy.BatchSize = 10000;
                        bulkCopy.BulkCopyTimeout = Properties.Settings.Default.BulkInsertTimeout;

                        SqlBulkCopyColumnMapping map_bpid = new SqlBulkCopyColumnMapping("bpid", "bpid");
                        bulkCopy.ColumnMappings.Add(map_bpid);
                        SqlBulkCopyColumnMapping map_rm_no = new SqlBulkCopyColumnMapping("rm_no", "rm_no");
                        bulkCopy.ColumnMappings.Add(map_rm_no);
                        SqlBulkCopyColumnMapping map_BP_name = new SqlBulkCopyColumnMapping("BP_name", "BP_name");
                        bulkCopy.ColumnMappings.Add(map_BP_name);
                        SqlBulkCopyColumnMapping map_AcctType = new SqlBulkCopyColumnMapping("AcctType", "AcctType");
                        bulkCopy.ColumnMappings.Add(map_AcctType);
                        SqlBulkCopyColumnMapping map_no_of_acc_holder = new SqlBulkCopyColumnMapping("no_of_acc_holder", "no_of_acc_holder");
                        bulkCopy.ColumnMappings.Add(map_no_of_acc_holder);
                        SqlBulkCopyColumnMapping map_rmid = new SqlBulkCopyColumnMapping("rmid", "rmid");
                        bulkCopy.ColumnMappings.Add(map_rmid);
                        SqlBulkCopyColumnMapping map_AVQ = new SqlBulkCopyColumnMapping("AVQ", "AVQ");
                        bulkCopy.ColumnMappings.Add(map_AVQ);

                        bulkCopy.WriteToServer(tempDataTable);
                    }
                    connection.Close();
                }
                tempDataTable.Clear();
            }
            catch (Exception ex)
            {
                Util.Log(logType.Info, String.Format("Error in region update tbBPInfo table : {0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("Insert record into {0} finish", Properties.Settings.Default.BPInfoTableName));
            Util.Log(logType.Info, String.Format("{0} update table {1} finish", DateTime.Now.ToString("tt hh:mm:ss"), Properties.Settings.Default.BPInfoTableName));
            tempBPInfoDict.Clear();
            #endregion

            #region merge account list into customer list
            Util.Log(logType.Info, "AccountImport(merge account list) Start.");
            try
            {
                individualAccountIMList.Clear();
                individualAccountIMList = individualAccountIMDict.Values.ToList();
                MergeAccountListIntoCustomerList(individualAccountIMList, newCustomerList);
                individualAccountIMList.Clear();
                individualAccountIMDict.Clear();

                individualAccountSTList.Clear();
                individualAccountSTList = individualAccountSTDict.Values.ToList();
                MergeAccountListIntoCustomerList(individualAccountSTList, newCustomerList);
                individualAccountSTList.Clear();
                individualAccountSTDict.Clear();

                MergeAccountListIntoCustomerList(combineAccountList, newCustomerList);
                //combineAccountList.Clear();
                MergeAccountListIntoCustomerList(citicFirstAccountList, newCustomerList);
                citicFirstAccountList.Clear();
                MergeAccountListIntoCustomerList(creditCardAccountList, newCustomerList);
                creditCardAccountList.Clear();
                MergeAccountListIntoCustomerList(infoCastAccountList, newCustomerList);
                infoCastAccountList.Clear();

                MergeAccountListIntoCustomerList(tbSuperBankAccountDict.Values.ToList(), newCustomerList);
                tbSuperBankAccountDict.Clear();
                MergeAccountListIntoCustomerList(tbContainerAccountDict.Values.ToList(), newCustomerList);
                tbContainerAccountDict.Clear();
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in AccountImport(merge account list). Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, "AccountImport(merge account list) End.");
            #endregion

            #region update account
            try
            {
                #region update by stored procedure
                Util.Log(logType.Info, "AccountImport(update account table) Truncate tbAccountListTemp Start.");
                entity.ExecuteStoreCommand(String.Format("TRUNCATE TABLE {0}", Properties.Settings.Default.tbAccountListTemp));
                Util.Log(logType.Info, "AccountImport(update account table) Truncate tbAccountListTemp End.");
                Util.Log(logType.Info, String.Format("{0} AccountImport(update account table) Insert account into tbAccountListTemp Start.", DateTime.Now.ToString("tt hh:mm:ss")));
                counter = 0;

                DataTable tempDataTable = new DataTable();
                tempDataTable.Columns.Add("RMID");
                tempDataTable.Columns.Add("AccountNumber");
                tempDataTable.Columns.Add("DisplayAccountNumber");
                tempDataTable.Columns.Add("Category");
                tempDataTable.Columns.Add("StatementType");
                tempDataTable.Columns.Add("RegistrationStatus");
                tempDataTable.Columns.Add("Active");
                tempDataTable.Columns.Add("CreateDate");
                tempDataTable.Columns.Add("CreateBy");
                tempDataTable.Columns.Add("ModifyBy");
                tempDataTable.Columns.Add("LastUpdate");

                foreach (var customer in newCustomerList)
                {
                    foreach (var account in customer.Value)
                    {
                        DateTime now = DateTime.Now;

                        DataRow row = tempDataTable.NewRow();
                        row["RMID"] = customer.Key;
                        row["AccountNumber"] = account.acctNo;
                        if (account.acctCategory == Properties.Settings.Default.CategorySuperBank)
                            row["DisplayAccountNumber"] = customer.Key;
                        else
                            row["DisplayAccountNumber"] = getDisplayAccountNumber(account.acctNo, account.acctCategory);
                        row["Category"] = account.acctCategory;
                        row["StatementType"] = account.statementType;
                        if (account.statementType == Properties.Settings.Default.StatementTypeSuperBank)
                        {
                            if (bpidToCkAvqDict.ContainsKey(customer.Key))
                                if (bpidDefaultSDict.ContainsKey(customer.Key))
                                    row["RegistrationStatus"] = Properties.Settings.Default.SuperBankIndividualDefaultRegistrationStatus;
                                else
                                    row["RegistrationStatus"] = Properties.Settings.Default.DefaultRegistrationStatus;
                            else
                            {
                                Util.Log(logType.Warning, String.Format("Cannot check AVQ status for SuperBank Account with BPID = {0}, please ensure that file - {1} has corresponding BP record", customer.Key, Path.GetFileName(Properties.Settings.Default.BPInfoPath)));
                                continue;
                            }
                        }
                        else
                            row["RegistrationStatus"] = Properties.Settings.Default.DefaultRegistrationStatus;

                        if (account.acctCategory.Equals(Properties.Settings.Default.CategoryInfoCast, StringComparison.OrdinalIgnoreCase))
                            row["Active"] = account.active;
                        else
                            row["Active"] = "Y";

                        row["CreateDate"] = now;
                        row["CreateBy"] = Properties.Settings.Default.AppName;
                        row["ModifyBy"] = Properties.Settings.Default.AppName;
                        row["LastUpdate"] = now;

                        tempDataTable.Rows.Add(row);
                    }
                }

                // use bulk insert
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                {
                    connection.Open();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    {
                        bulkCopy.DestinationTableName = Properties.Settings.Default.tbAccountListTemp;
                        bulkCopy.BatchSize = 10000;
                        bulkCopy.BulkCopyTimeout = Properties.Settings.Default.BulkInsertTimeout;

                        SqlBulkCopyColumnMapping map_RMID = new SqlBulkCopyColumnMapping("RMID", "RMID");
                        bulkCopy.ColumnMappings.Add(map_RMID);
                        SqlBulkCopyColumnMapping map_AccountNumber = new SqlBulkCopyColumnMapping("AccountNumber", "AccountNumber");
                        bulkCopy.ColumnMappings.Add(map_AccountNumber);
                        SqlBulkCopyColumnMapping map_DisplayAccountNumber = new SqlBulkCopyColumnMapping("DisplayAccountNumber", "DisplayAccountNumber");
                        bulkCopy.ColumnMappings.Add(map_DisplayAccountNumber);
                        SqlBulkCopyColumnMapping map_Category = new SqlBulkCopyColumnMapping("Category", "Category");
                        bulkCopy.ColumnMappings.Add(map_Category);
                        SqlBulkCopyColumnMapping map_StatementType = new SqlBulkCopyColumnMapping("StatementType", "StatementType");
                        bulkCopy.ColumnMappings.Add(map_StatementType);
                        SqlBulkCopyColumnMapping map_RegistrationStatus = new SqlBulkCopyColumnMapping("RegistrationStatus", "RegistrationStatus");
                        bulkCopy.ColumnMappings.Add(map_RegistrationStatus);
                        SqlBulkCopyColumnMapping map_Active = new SqlBulkCopyColumnMapping("Active", "Active");
                        bulkCopy.ColumnMappings.Add(map_Active);
                        SqlBulkCopyColumnMapping map_CreateDate = new SqlBulkCopyColumnMapping("CreateDate", "CreateDate");
                        bulkCopy.ColumnMappings.Add(map_CreateDate);
                        SqlBulkCopyColumnMapping map_CreateBy = new SqlBulkCopyColumnMapping("CreateBy", "CreateBy");
                        bulkCopy.ColumnMappings.Add(map_CreateBy);
                        SqlBulkCopyColumnMapping map_ModifyBy = new SqlBulkCopyColumnMapping("ModifyBy", "ModifyBy");
                        bulkCopy.ColumnMappings.Add(map_ModifyBy);
                        SqlBulkCopyColumnMapping map_LastUpdate = new SqlBulkCopyColumnMapping("LastUpdate", "LastUpdate");
                        bulkCopy.ColumnMappings.Add(map_LastUpdate);

                        bulkCopy.WriteToServer(tempDataTable);
                    }
                    connection.Close();
                }
                tempDataTable.Clear();

                newCustomerList.Clear();
                bpidToCkAvqDict.Clear();
                bpidDefaultSDict.Clear();
                Util.Log(logType.Info, String.Format("{0} AccountImport(update account table) Insert record into tbAccountListTemp End.", DateTime.Now.ToString("tt hh:mm:ss")));
                Util.Log(logType.Info, String.Format("{0} AccountImport(update account table) Update tbAccountList from tbAccountListTemp Start.", DateTime.Now.ToString("tt hh:mm:ss")));
                entity.ExecuteStoreCommand(String.Format("EXECUTE {0}", Properties.Settings.Default.spUpdatetbAccountList));
                Util.Log(logType.Info, String.Format("{0} AccounImport(update account table) Update tbAccountList from tbAccountListTemp End.", DateTime.Now.ToString("tt hh:mm:ss")));
                #endregion

                #region update tbCombineAccountList
                Util.Log(logType.Info, "AccountImport(update account table) Truncate tbCombineAccountList Start.");
                entity.ExecuteStoreCommand("TRUNCATE TABLE [dbo].[tbCombineAccountList]");
                Util.Log(logType.Info, "AccountImport(update account table) Truncate tbCombineAccountList End.");
                Util.Log(logType.Info, "AccountImport(update account table) Insert account into tbCombineAccountList Start.");

                DataTable combineTempDataTable = new DataTable();
                combineTempDataTable.Columns.Add("RMID");
                combineTempDataTable.Columns.Add("AccountNumber");
                combineTempDataTable.Columns.Add("Type");
                combineTempDataTable.Columns.Add("Group");
                combineTempDataTable.Columns.Add("Active");
                combineTempDataTable.Columns.Add("CreateDate");
                combineTempDataTable.Columns.Add("CreateBy");
                combineTempDataTable.Columns.Add("ModifyBy");
                combineTempDataTable.Columns.Add("LastUpdate");

                foreach (var account in combineAccountList)
                {
                    foreach (var rmid in account.rmid)
                    {
                        DateTime now = DateTime.Now;

                        DataRow row = combineTempDataTable.NewRow();
                        row["RMID"] = rmid;
                        row["AccountNumber"] = account.acctNo;
                        row["Type"] = "P";
                        row["Group"] = account.group;
                        row["Active"] = "Y";
                        row["CreateDate"] = now;
                        row["CreateBy"] = Properties.Settings.Default.AppName;
                        row["ModifyBy"] = Properties.Settings.Default.AppName;
                        row["LastUpdate"] = now;

                        combineTempDataTable.Rows.Add(row);
                    }
                }
                combineAccountList.Clear();

                foreach (var account in combineAccountSecondaryList)
                {
                    foreach (var rmid in account.rmid)
                    {
                        DateTime now = DateTime.Now;

                        DataRow row = combineTempDataTable.NewRow();
                        row["RMID"] = rmid;
                        row["AccountNumber"] = account.acctNo;
                        row["Type"] = "S";
                        row["Group"] = account.group;
                        row["Active"] = "Y";
                        row["CreateDate"] = now;
                        row["CreateBy"] = Properties.Settings.Default.AppName;
                        row["ModifyBy"] = Properties.Settings.Default.AppName;
                        row["LastUpdate"] = now;

                        combineTempDataTable.Rows.Add(row);
                    }
                }
                combineAccountSecondaryList.Clear();

                // use bulk insert
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                {
                    connection.Open();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    {
                        bulkCopy.DestinationTableName = "[dbo].[tbCombineAccountList]";
                        bulkCopy.BatchSize = 10000;
                        bulkCopy.BulkCopyTimeout = Properties.Settings.Default.BulkInsertTimeout;

                        SqlBulkCopyColumnMapping map_RMID = new SqlBulkCopyColumnMapping("RMID", "RMID");
                        bulkCopy.ColumnMappings.Add(map_RMID);
                        SqlBulkCopyColumnMapping map_AccountNumber = new SqlBulkCopyColumnMapping("AccountNumber", "AccountNumber");
                        bulkCopy.ColumnMappings.Add(map_AccountNumber);
                        SqlBulkCopyColumnMapping map_Type = new SqlBulkCopyColumnMapping("Type", "Type");
                        bulkCopy.ColumnMappings.Add(map_Type);
                        SqlBulkCopyColumnMapping map_Group = new SqlBulkCopyColumnMapping("Group", "Group");
                        bulkCopy.ColumnMappings.Add(map_Group);
                        SqlBulkCopyColumnMapping map_Active = new SqlBulkCopyColumnMapping("Active", "Active");
                        bulkCopy.ColumnMappings.Add(map_Active);
                        SqlBulkCopyColumnMapping map_CreateDate = new SqlBulkCopyColumnMapping("CreateDate", "CreateDate");
                        bulkCopy.ColumnMappings.Add(map_CreateDate);
                        SqlBulkCopyColumnMapping map_CreateBy = new SqlBulkCopyColumnMapping("CreateBy", "CreateBy");
                        bulkCopy.ColumnMappings.Add(map_CreateBy);
                        SqlBulkCopyColumnMapping map_ModifyBy = new SqlBulkCopyColumnMapping("ModifyBy", "ModifyBy");
                        bulkCopy.ColumnMappings.Add(map_ModifyBy);
                        SqlBulkCopyColumnMapping map_LastUpdate = new SqlBulkCopyColumnMapping("LastUpdate", "LastUpdate");
                        bulkCopy.ColumnMappings.Add(map_LastUpdate);

                        bulkCopy.WriteToServer(combineTempDataTable);
                    }
                    connection.Close();
                }
                combineTempDataTable.Clear();

                Util.Log(logType.Info, "AccountImport(update account table) Insert record into tbCombineAccountList End.");
                #endregion

                #region update tbCiticFirstAccountList
                Util.Log(logType.Info, "AccountImport(update account table) Truncate tbCiticFirstAccountList Start.");
                entity.ExecuteStoreCommand("TRUNCATE TABLE [dbo].[tbCiticFirstAccountList]");
                Util.Log(logType.Info, "AccountImport(update account table) Truncate tbCiticFirstAccountList End.");
                Util.Log(logType.Info, "AccountImport(update account table) Insert account into tbCiticFirstAccountList Start.");

                DataTable citicTempDataTable = new DataTable();
                citicTempDataTable.Columns.Add("RMID");
                citicTempDataTable.Columns.Add("AccountNumber");
                citicTempDataTable.Columns.Add("Active");
                citicTempDataTable.Columns.Add("CreateDate");
                citicTempDataTable.Columns.Add("CreateBy");
                citicTempDataTable.Columns.Add("ModifyBy");
                citicTempDataTable.Columns.Add("LastUpdate");

                foreach (var account in tbCiticFirstAccountList)
                {
                    foreach (var rmid in account.rmid)
                    {
                        DateTime now = DateTime.Now;

                        DataRow row = citicTempDataTable.NewRow();
                        row["RMID"] = rmid;
                        row["AccountNumber"] = account.acctNo;
                        row["Active"] = "Y";
                        row["CreateDate"] = now;
                        row["CreateBy"] = Properties.Settings.Default.AppName;
                        row["ModifyBy"] = Properties.Settings.Default.AppName;
                        row["LastUpdate"] = now;

                        citicTempDataTable.Rows.Add(row);
                    }
                }
                tbCiticFirstAccountList.Clear();

                // use bulk insert
                using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                {
                    connection.Open();

                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                    {
                        bulkCopy.DestinationTableName = "[dbo].[tbCiticFirstAccountList]";
                        bulkCopy.BatchSize = 10000;
                        bulkCopy.BulkCopyTimeout = Properties.Settings.Default.BulkInsertTimeout;

                        SqlBulkCopyColumnMapping map_RMID = new SqlBulkCopyColumnMapping("RMID", "RMID");
                        bulkCopy.ColumnMappings.Add(map_RMID);
                        SqlBulkCopyColumnMapping map_AccountNumber = new SqlBulkCopyColumnMapping("AccountNumber", "AccountNumber");
                        bulkCopy.ColumnMappings.Add(map_AccountNumber);
                        SqlBulkCopyColumnMapping map_Active = new SqlBulkCopyColumnMapping("Active", "Active");
                        bulkCopy.ColumnMappings.Add(map_Active);
                        SqlBulkCopyColumnMapping map_CreateDate = new SqlBulkCopyColumnMapping("CreateDate", "CreateDate");
                        bulkCopy.ColumnMappings.Add(map_CreateDate);
                        SqlBulkCopyColumnMapping map_CreateBy = new SqlBulkCopyColumnMapping("CreateBy", "CreateBy");
                        bulkCopy.ColumnMappings.Add(map_CreateBy);
                        SqlBulkCopyColumnMapping map_ModifyBy = new SqlBulkCopyColumnMapping("ModifyBy", "ModifyBy");
                        bulkCopy.ColumnMappings.Add(map_ModifyBy);
                        SqlBulkCopyColumnMapping map_LastUpdate = new SqlBulkCopyColumnMapping("LastUpdate", "LastUpdate");
                        bulkCopy.ColumnMappings.Add(map_LastUpdate);

                        bulkCopy.WriteToServer(citicTempDataTable);
                    }
                    connection.Close();
                }
                citicTempDataTable.Clear();
                Util.Log(logType.Info, "AccountImport(update account table) Insert record into tbCiticFirstAccountList End.");
                #endregion

                #region update combine account status
                Util.Log(logType.Info, "AccountImport(update account table) spUpdateCombineAccountStatus Start.");
                entity.ExecuteStoreCommand("EXECUTE [dbo].[spUpdateCombineAccountStatus]");
                Util.Log(logType.Info, "AccountImport(update account table) spUpdateCombineAccountStatus End.");
                #endregion

                #region export eSubscription validation report
                Util.Log(logType.Info, String.Format("{0} eSubscription validation report export start", DateTime.Now.ToString("tt hh:mm:ss")));
                DateTime validDate = DateTime.Today.AddDays(-2);
                List<tbAuditLog> auditLogList = entity.tbAuditLog.Where(a => a.CreateDate >= validDate && a.AutoUnSubscribe != null).ToList();
                try
                {
                    using (var sw = new StreamWriter(Properties.Settings.Default.eSubscribeValidationReportPath))
                    {
                        sw.WriteLine(string.Format("\"Account Number\",\"RMID\",\"Statement Type\",\"Input User\",\"Update Time\""));
                        foreach (var record in auditLogList)
                        {
                            sw.WriteLine(string.Format("\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\"", record.AccountNumber.Trim().ToString(), record.RMID.Trim().ToString(), record.StatementType, record.actionId.Trim().ToString(), record.CreateDate.ToString()));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Util.Log(logType.Error, String.Format("Error in eSubscription validation report export. Cannot write to target file. Error message:{0}", ex.Message));
                }
                Util.Log(logType.Info, String.Format("(eSubscription validation report) No. of records successfully extracted = {0}", auditLogList.Count()));
                Util.Log(logType.Info, String.Format("{0} eSubscription validation report export finish", DateTime.Now.ToString("tt hh:mm:ss")));
                #endregion

                // indicate success insert/update for tables (except tbRmIDAccountNumber)
                acImpSucRun = true;

                #region update tbRmIDAccountNumber table
                Util.Log(logType.Info, String.Format("{0} update table {1} start", DateTime.Now.ToString("tt hh:mm:ss"), Properties.Settings.Default.tbRmIDAccountNumberTableName));
                try
                {
                    Util.Log(logType.Info, String.Format("{0} Build table RmIDRMNumberMap start", DateTime.Now.ToString("tt hh:mm:ss")));
                    entity.ExecuteStoreCommand("IF OBJECT_ID('[dbo].[RmIDRMNumberMap]', 'U') IS NOT NULL DROP TABLE [dbo].[RmIDRMNumberMap]");
                    entity.ExecuteStoreCommand("CREATE TABLE RmIDRMNumberMap([RMID] [nvarchar](100) NOT NULL,[RMNumber] [varchar](100) NOT NULL)");

                    DataTable RmIDRMNumberMapDataTable = new DataTable();
                    RmIDRMNumberMapDataTable.Columns.Add("RMID");
                    RmIDRMNumberMapDataTable.Columns.Add("RMNumber");

                    foreach (var map in rmNumberToRmidMapping)
                    {
                        DataRow RmIDRMNumberMapRow = RmIDRMNumberMapDataTable.NewRow();
                        RmIDRMNumberMapRow["RMID"] = map.Value.Trim();
                        RmIDRMNumberMapRow["RMNumber"] = map.Key.Trim().Substring(5);

                        RmIDRMNumberMapDataTable.Rows.Add(RmIDRMNumberMapRow);
                    }

                    using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.DataTableConnectionString))
                    {
                        connection.Open();

                        using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                        {
                            bulkCopy.DestinationTableName = "[dbo].[RmIDRMNumberMap]";
                            bulkCopy.BatchSize = 10000;
                            bulkCopy.BulkCopyTimeout = Properties.Settings.Default.BulkInsertTimeout;

                            SqlBulkCopyColumnMapping map_RMID = new SqlBulkCopyColumnMapping("RMID", "RMID");
                            bulkCopy.ColumnMappings.Add(map_RMID);
                            SqlBulkCopyColumnMapping map_RMNumber = new SqlBulkCopyColumnMapping("RMNumber", "RMNumber");
                            bulkCopy.ColumnMappings.Add(map_RMNumber);

                            bulkCopy.WriteToServer(RmIDRMNumberMapDataTable);
                        }
                        connection.Close();
                    }
                    RmIDRMNumberMapDataTable.Clear();
                    Util.Log(logType.Info, String.Format("{0} Build table RmIDRMNumberMap finish", DateTime.Now.ToString("tt hh:mm:ss")));

                    Util.Log(logType.Info, String.Format("{0} Build table tbRmIDAccountNumberTemp start", DateTime.Now.ToString("tt hh:mm:ss")));
                    entity.ExecuteStoreCommand("IF OBJECT_ID('[dbo].[tbRmIDAccountNumberTemp]', 'U') IS NOT NULL DROP TABLE [dbo].[tbRmIDAccountNumberTemp]");
                    entity.ExecuteStoreCommand("CREATE TABLE tbRmIDAccountNumberTemp([RMID] [nvarchar](100) NOT NULL,[CreateDate] [datetime] NOT NULL,[RMNumber] [varchar](100) NULL,[AccountNumber] [varchar](100) NULL,[Category] [nvarchar](50) NOT NULL)");
                    entity.ExecuteStoreCommand("INSERT INTO [dbo].[tbRmIDAccountNumberTemp]([RMID],[CreateDate],[AccountNumber],[Category]) SELECT [dbo].[tbAccountList].[RMID],GETDATE(),[dbo].[tbAccountList].[AccountNumber],[dbo].[tbAccountList].[Category] FROM [dbo].[tbAccountList] WHERE [dbo].[tbAccountList].[Category] != 'SBS' and [dbo].[tbAccountList].[Category] != 'AVQ'");
                    entity.ExecuteStoreCommand("INSERT INTO [dbo].[tbRmIDAccountNumberTemp]([RMID],[CreateDate],[AccountNumber],[Category]) SELECT [dbo].[tbBPInfo].[rmid],GETDATE(),[dbo].[tbAccountList].[AccountNumber],[dbo].[tbAccountList].[Category] FROM [dbo].[tbAccountList] INNER JOIN [dbo].[tbBPInfo] ON [dbo].[tbAccountList].RMID = [dbo].[tbBPInfo].bpid WHERE ([dbo].[tbAccountList].[Category] = 'SBS' OR [dbo].[tbAccountList].[Category] = 'AVQ') and [dbo].[tbBPInfo].[rmid] != ''");
                    entity.ExecuteStoreCommand("UPDATE rmAcTemp SET rmAcTemp.RMNumber = rmMap.RMNumber FROM [dbo].[tbRmIDAccountNumberTemp] rmAcTemp INNER JOIN [dbo].[RmIDRMNumberMap] rmMap ON rmAcTemp.RMID = rmMap.RMID");
                    entity.ExecuteStoreCommand("DELETE FROM [dbo].[tbRmIDAccountNumberTemp] WHERE RMNumber IS NULL");
                    entity.ExecuteStoreCommand("UPDATE rmAcTemp SET rmAcTemp.AccountNumber = '' FROM [dbo].[tbRmIDAccountNumberTemp] rmAcTemp WHERE rmAcTemp.AccountNumber IS NULL");
                    Util.Log(logType.Info, String.Format("{0} Build table tbRmIDAccountNumberTemp finish", DateTime.Now.ToString("tt hh:mm:ss")));

                    Util.Log(logType.Info, String.Format("{0} Insert record into tbRmIDAccountNumber start", DateTime.Now.ToString("tt hh:mm:ss")));
                    entity.ExecuteStoreCommand("TRUNCATE TABLE [dbo].[tbRmIDAccountNumber]");
                    entity.ExecuteStoreCommand("INSERT INTO [dbo].[tbRmIDAccountNumber]([RMID],[RMNumber],[CreateDate],[AccountNumber],[Category]) SELECT [RMID],[RMNumber],[CreateDate],[AccountNumber],[Category] FROM [dbo].[tbRmIDAccountNumberTemp] ORDER BY RMNumber,AccountNumber,Category");
                    Util.Log(logType.Info, String.Format("{0} Insert record into tbRmIDAccountNumber finish", DateTime.Now.ToString("tt hh:mm:ss")));

                    entity.ExecuteStoreCommand("IF OBJECT_ID('[dbo].[RmIDRMNumberMap]', 'U') IS NOT NULL DROP TABLE [dbo].[RmIDRMNumberMap]");
                    entity.ExecuteStoreCommand("IF OBJECT_ID('[dbo].[tbRmIDAccountNumberTemp]', 'U') IS NOT NULL DROP TABLE [dbo].[tbRmIDAccountNumberTemp]");
                    Util.Log(logType.Info, String.Format("{0} update table {1} finish", DateTime.Now.ToString("tt hh:mm:ss"), Properties.Settings.Default.tbRmIDAccountNumberTableName));

                    // indicate success update for table tbRmIDAccountNumber
                    tbRmIDAccountNumberSucRun = true;
                }
                catch (Exception ex)
                {
                    entity.ExecuteStoreCommand("IF OBJECT_ID('[dbo].[RmIDRMNumberMap]', 'U') IS NOT NULL DROP TABLE [dbo].[RmIDRMNumberMap]");
                    entity.ExecuteStoreCommand("IF OBJECT_ID('[dbo].[tbRmIDAccountNumberTemp]', 'U') IS NOT NULL DROP TABLE [dbo].[tbRmIDAccountNumberTemp]");
                    Util.Log(logType.Info, String.Format("Error in region update tbRmIDAccountNumber table : {0}", ex.Message));
                }
                #endregion
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in AccountImport(update account table). Error message:{0}", ex.Message));
                if (ex.InnerException != null)
                {
                    Util.Log(logType.Error, String.Format("Error in AccountImport(update account table). Inner exception:{0}", ex.InnerException.Message));
                }
            }
            #endregion

            #region check number of records updated
            Util.Log(logType.Info, String.Format("{0} check number of records updated start", DateTime.Now.ToString("tt hh:mm:ss")));

            List<tbAccountList> bankUpdateList = entity.tbAccountList.Where(
                a => (a.Category.Equals(Properties.Settings.Default.CategoryIndividual, StringComparison.OrdinalIgnoreCase) || a.Category.Equals(Properties.Settings.Default.CategoryCombine, StringComparison.OrdinalIgnoreCase))
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate < startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [Bank] updated in tbAccountList = {0}", bankUpdateList.Count.ToString()));
            bankUpdateList.Clear();
            List<tbAccountList> bankInsertList = entity.tbAccountList.Where(
                a => (a.Category.Equals(Properties.Settings.Default.CategoryIndividual, StringComparison.OrdinalIgnoreCase) || a.Category.Equals(Properties.Settings.Default.CategoryCombine, StringComparison.OrdinalIgnoreCase))
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate >= startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [Bank] inserted in tbAccountList = {0}", bankInsertList.Count.ToString()));
            bankInsertList.Clear();
            List<tbAccountList> citicFirstUpdateList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategoryCiticFirst, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate < startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [CiticFirst] updated in tbAccountList = {0}", citicFirstUpdateList.Count.ToString()));
            citicFirstUpdateList.Clear();
            List<tbAccountList> citicFirstInsertList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategoryCiticFirst, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate >= startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [CiticFirst] inserted in tbAccountList = {0}", citicFirstInsertList.Count.ToString()));
            citicFirstInsertList.Clear();
            List<tbAccountList> creditCardUpdateList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategoryCreditCard, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate < startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [Card] updated in tbAccountList = {0}", creditCardUpdateList.Count.ToString()));
            creditCardUpdateList.Clear();
            List<tbAccountList> creditCardInsertList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategoryCreditCard, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate >= startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [Card] inserted in tbAccountList = {0}", creditCardInsertList.Count.ToString()));
            creditCardInsertList.Clear();
            List<tbAccountList> infoCastUpdateList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategoryInfoCast, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate < startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [InfoCast] updated in tbAccountList = {0}", infoCastUpdateList.Count.ToString()));
            infoCastUpdateList.Clear();
            List<tbAccountList> infoCastInsertList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategoryInfoCast, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate >= startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [InfoCast] inserted in tbAccountList = {0}", infoCastInsertList.Count.ToString()));
            infoCastInsertList.Clear();
            List<tbAccountList> superbankUpdateList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategorySuperBank, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate < startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [SuperBank] updated in tbAccountList = {0}", superbankUpdateList.Count.ToString()));
            superbankUpdateList.Clear();
            List<tbAccountList> superbankInsertList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategorySuperBank, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate >= startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [SuperBank] inserted in tbAccountList = {0}", superbankInsertList.Count.ToString()));
            superbankInsertList.Clear();
            List<tbAccountList> containerUpdateList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategoryContainer, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate < startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [Container] updated in tbAccountList = {0}", containerUpdateList.Count.ToString()));
            containerUpdateList.Clear();
            List<tbAccountList> containerInsertList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategoryContainer, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate >= startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [Container] inserted in tbAccountList = {0}", containerInsertList.Count.ToString()));
            containerInsertList.Clear();
            List<tbAccountList> FixDepositUpdateList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategoryFixDeposit, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate < startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [FixDeposit] updated in tbAccountList = {0}", FixDepositUpdateList.Count.ToString()));
            FixDepositUpdateList.Clear();
            List<tbAccountList> FixDepositInsertList = entity.tbAccountList.Where(
                a => a.Category.Equals(Properties.Settings.Default.CategoryFixDeposit, StringComparison.OrdinalIgnoreCase)
                    && a.LastUpdate >= startDatetime
                    && a.CreateDate >= startDatetime).ToList();
            Util.Log(logType.Info, String.Format("Total No. of records [FixDeposit] inserted in tbAccountList = {0}", FixDepositInsertList.Count.ToString()));
            FixDepositUpdateList.Clear();

            Util.Log(logType.Info, String.Format("{0} check number of records updated finish", DateTime.Now.ToString("tt hh:mm:ss")));
            #endregion

            Util.Log(logType.Info, "AccountImport End.");
            Util.Log(logType.Info, String.Format("{0:yyyyMMdd HHmmss} END Account Importer with status = SUCCESS", DateTime.Now));
            Util.Log(logType.Info, "Account import finish");

        }

        private static void MergeAccountListIntoCustomerList(List<Util.account> accountList, Dictionary<String, List<Util.account>> CustomerList)
        {
            foreach (var account in accountList)
            {
                foreach (var rmid in account.rmid)
                {
                    if (CustomerList.ContainsKey(rmid))
                    {
                        CustomerList[rmid].Add(account);
                    }
                    else
                    {
                        CustomerList.Add(rmid, new List<Util.account> { account });
                    }
                }
            }
        }

        private static Boolean isAccountUpdate(tbAccountList oldAccount, Util.account newAccount)
        {
            if (oldAccount.Category != newAccount.acctCategory) return true;
            if (oldAccount.StatementType != newAccount.statementType) return true;
            if (oldAccount.Active.Equals("N")) return true;
            return false;
        }

        private static String getDisplayAccountNumber(String accountNumber, String Category)
        {
            if (Category.Equals(Properties.Settings.Default.CategoryCombine, StringComparison.OrdinalIgnoreCase) || Category.Equals(Properties.Settings.Default.CategoryIndividual, StringComparison.OrdinalIgnoreCase)
               || Category.Equals(Properties.Settings.Default.CategoryInfoCast, StringComparison.OrdinalIgnoreCase)
            )
                return String.Format("{0}-{1}-{2}", accountNumber.Substring(0, 3), accountNumber.Substring(3, 1), accountNumber.Substring(4));
            else if (Category.Equals(Properties.Settings.Default.CategoryCreditCard, StringComparison.OrdinalIgnoreCase))
                return String.Format("{0}-{1}-{2}-{3}", accountNumber.Substring(0, 4), accountNumber.Substring(4, 4), accountNumber.Substring(8, 4), accountNumber.Substring(12, 4));
            else if ((Category.Equals(Properties.Settings.Default.CategorySuperBank, StringComparison.OrdinalIgnoreCase)) || (Category.Equals(Properties.Settings.Default.CategoryContainer, StringComparison.OrdinalIgnoreCase)))
                return String.Format("{0}-{1}-{2}-{3}", accountNumber.Substring(0, 3), accountNumber.Substring(3, 1), accountNumber.Substring(4, 6), accountNumber.Substring(10, 2));
            else
                return accountNumber;
        }

        private static Boolean isFileExist()
        {
            Boolean allFileExist = true;

            if (!File.Exists(Properties.Settings.Default.AccountNumberRmNumberMappingPath))
            {
                Util.Log(logType.Error, String.Format("AccountNumberRmNumberMappingPath File not exist : {0}", Properties.Settings.Default.AccountNumberRmNumberMappingPath));
                allFileExist = false;
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.AccountNumberRmNumberMappingPath));
            }

            if (!File.Exists(Properties.Settings.Default.RmNumberRMIDMappingPath))
            {
                Util.Log(logType.Error, String.Format("RmNumberRMIDMappingPath File not exist : {0}", Properties.Settings.Default.RmNumberRMIDMappingPath));
                allFileExist = false;
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.RmNumberRMIDMappingPath));
            }

            if (!File.Exists(Properties.Settings.Default.IndividualAccountListPathIM))
            {
                Util.Log(logType.Error, String.Format("IndividualAccountListPathIM File not exist : {0}", Properties.Settings.Default.IndividualAccountListPathIM));
                allFileExist = false;
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.IndividualAccountListPathIM));
            }

            if (!File.Exists(Properties.Settings.Default.IndividualAccountListPathST))
            {
                Util.Log(logType.Error, String.Format("IndividualAccountListPathST File not exist : {0}", Properties.Settings.Default.IndividualAccountListPathST));
                allFileExist = false;
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.IndividualAccountListPathST));
            }

            if (!File.Exists(Properties.Settings.Default.CombineAccountListPath))
            {
                Util.Log(logType.Error, String.Format("CombineAccountListPath File not exist : {0}", Properties.Settings.Default.CombineAccountListPath));
                allFileExist = false;
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.CombineAccountListPath));
            }

            if (!File.Exists(Properties.Settings.Default.CiticFirstAccountListPath))
            {
                if (Properties.Settings.Default.CFignoreFileCheck == "Y")
                {
                    Util.Log(logType.Info, String.Format("CiticFirstAccountListPath File not exist : {0}, but CFignoreFileCheck = \"Y\", AccountImport continue", Properties.Settings.Default.CiticFirstAccountListPath));
                }
                else
                {
                    Util.Log(logType.Error, String.Format("CiticFirstAccountListPath File not exist : {0}", Properties.Settings.Default.CiticFirstAccountListPath));
                    allFileExist = false;
                }
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.CiticFirstAccountListPath));
            }

            if (!File.Exists(Properties.Settings.Default.CreditCardAccountListPath))
            {
                Util.Log(logType.Error, String.Format("CreditCardAccountListPath File not exist : {0}", Properties.Settings.Default.CreditCardAccountListPath));
                allFileExist = false;
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.CreditCardAccountListPath));
            }

            if (!File.Exists(Properties.Settings.Default.RMIDStatementTypeMappingPath))
            {
                if (Properties.Settings.Default.CFignoreFileCheck == "Y")
                {
                    Util.Log(logType.Info, String.Format("RMIDStatementTypeMappingPath File not exist : {0}, but CFignoreFileCheck = \"Y\", AccountImport continue", Properties.Settings.Default.CiticFirstAccountListPath));
                }
                else
                {
                    Util.Log(logType.Error, String.Format("RMIDStatementTypeMappingPath File not exist : {0}", Properties.Settings.Default.RMIDStatementTypeMappingPath));
                    allFileExist = false;
                }
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.RMIDStatementTypeMappingPath));
            }

            if (!File.Exists(Properties.Settings.Default.SuperBankAccountListPath))
            {
                Util.Log(logType.Error, String.Format("SuperBankAccountListPath File not exist : {0}", Properties.Settings.Default.SuperBankAccountListPath));
                allFileExist = false;
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.SuperBankAccountListPath));
            }

            if (!File.Exists(Properties.Settings.Default.ContainerAccountListPath))
            {
                Util.Log(logType.Error, String.Format("ContainerAccountListPath File not exist : {0}", Properties.Settings.Default.ContainerAccountListPath));
                allFileExist = false;
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.ContainerAccountListPath));
            }

            if (!File.Exists(Properties.Settings.Default.BPInfoPath))
            {
                Util.Log(logType.Error, String.Format("BPInfoPath File not exist : {0}", Properties.Settings.Default.BPInfoPath));
                allFileExist = false;
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.BPInfoPath));
            }

            if (!File.Exists(Properties.Settings.Default.PostalAddrLine1Path))
            {
                Util.Log(logType.Error, String.Format("PostalAddrLine1Path File not exist : {0}", Properties.Settings.Default.PostalAddrLine1Path));
                allFileExist = false;
            }
            else
            {
                Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.PostalAddrLine1Path));
            }

            return allFileExist;
        }
    }
}