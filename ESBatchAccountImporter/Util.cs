﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ESBatchAccountImporter
{
    class Util
    {
        static EStatementEntities Entity = new EStatementEntities();
        static String logFilename = String.Format("{0}.log", Properties.Settings.Default.AppName);
        static String logPath = Path.Combine(Directory.GetCurrentDirectory(), "Log", logFilename);

        public static void Log(logType type, String content)
        {
            try
            {
                if (type != logType.Warning)
                {
                    DateTime now = DateTime.Now;
                    tbLog log = new tbLog();
                    log.Type = Enum.GetName(typeof(logType), type);
                    log.Content = content;
                    log.CreateBy = Properties.Settings.Default.AppName;
                    log.ModifyBy = Properties.Settings.Default.AppName;
                    log.CreateDate = now;
                    log.LastUpdate = now;
                    Entity.AddTotbLog(log);
                    Entity.SaveChanges();

                    Console.WriteLine(content);
                }

                if (!File.GetLastWriteTime(logPath).ToString("yyyyMMdd").Equals(DateTime.Now.ToString("yyyyMMdd")))
                {
                    String newLogPath = String.Format("{0}_{1:yyyyMMdd}.log", logPath.Replace(".log", ""), File.GetLastWriteTime(logPath).ToString("yyyyMMdd"));
                    File.Delete(newLogPath);
                    File.Move(logPath, newLogPath);
                }

                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    sw.Close();
                }
            }
            catch (Exception) { }
        }

        public static void writeLog(String content)
        {
            try
            {
                if (File.Exists(logPath))
                {
                    if (!File.GetLastWriteTime(logPath).ToString("yyyyMMdd").Equals(DateTime.Now.ToString("yyyyMMdd")))
                    {
                        String newLogPath = String.Format("{0}_{1:yyyyMMdd}.log", logPath.Replace(".log", ""), File.GetLastWriteTime(logPath).ToString("yyyyMMdd"));
                        File.Delete(newLogPath);
                        File.Move(logPath, newLogPath);
                    }
                }
                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    sw.Close();
                }

                Log(logType.Info, content);
            }
            catch (Exception e)
            {


            }
        }

        public class account
        {
            public List<String> rmid;
            public String acctNo;
            public String statementType;
            public String acctCategory;
            public String group = "";
            public String active = "";
            public String bpid;
            public String DisplayAccNo;
            public String BP_name;

            public account() { }
            public account(String i_bpid, String i_acctNo, String i_DisplayAcctNo, String i_acctCategory, String i_active)
            {
                bpid = i_bpid;
                acctNo = i_acctNo;
                DisplayAccNo = i_DisplayAcctNo;
                acctCategory = i_acctCategory;
                active = i_active;
            }
            public account(List<String> i_rmid, String i_acctNo, List<String> i_DisplayAcctNo, String i_statementType, String i_acctCategory)
            {
                rmid = i_rmid;
                acctNo = i_acctNo;
                DisplayAccNo = i_DisplayAcctNo[0];
                statementType = i_statementType;
                acctCategory = i_acctCategory;
            }
            public account(List<String> i_rmid, String i_acctNo, String i_statementType, String i_acctCategory)
            {
                rmid = i_rmid;
                acctNo = i_acctNo;
                statementType = i_statementType;
                acctCategory = i_acctCategory;
            }
            public account(List<String> i_rmid, String i_acctNo, String i_statementType, String i_acctCategory, String i_group)
            {
                rmid = i_rmid;
                acctNo = i_acctNo;
                statementType = i_statementType;
                acctCategory = i_acctCategory;
                group = i_group;
            }
            public account(List<String> i_rmid, String i_acctNo, String i_statementType, String i_acctCategory, String i_group, String i_active)
            {
                rmid = i_rmid;
                acctNo = i_acctNo;
                statementType = i_statementType;
                acctCategory = i_acctCategory;
                group = i_group;
                active = i_active;
            }
        }

        public class BPInfo
        {
            public String bpid;
            public String ckAVQ;
            public String rm_no;
            public String BP_name;
            public String AcctType;
            public String no_of_acc_holder;
            public String rmid;
            public String jointFlag;

            public BPInfo(String i_bpid, String i_ckAVQ, String i_rm_no, String i_BP_name, String i_no_of_acc_holder, String i_rmid)
            {
                bpid = i_bpid;
                ckAVQ = i_ckAVQ;
                rm_no = i_rm_no;
                BP_name = i_BP_name;
                no_of_acc_holder = i_no_of_acc_holder;
                rmid = i_rmid;
            }
        }

    }
}
