USE [Estatement]
GO
/****** Object:  Table [dbo].[tbAccountList]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbAccountList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[DisplayAccountNumber] [nvarchar](100) NULL,
	[Category] [nvarchar](50) NOT NULL,
	[StatementType] [nvarchar](50) NOT NULL,
	[RegistrationStatus] [nvarchar](1) NOT NULL,
	[Active] [nvarchar](1) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[statusUpdateBy] [char](2) NULL DEFAULT ('SP'),
 CONSTRAINT [PK_tbAccountList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbAccountList_b4avq]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbAccountList_b4avq](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[DisplayAccountNumber] [nvarchar](100) NULL,
	[Category] [nvarchar](50) NOT NULL,
	[StatementType] [nvarchar](50) NOT NULL,
	[RegistrationStatus] [nvarchar](1) NOT NULL,
	[Active] [nvarchar](1) NOT NULL,
	[CreateDate] [datetime] NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[statusUpdateBy] [char](2) NULL,
 CONSTRAINT [PK_tbAccountList_b4avq] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbAccountListSnapshot]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbAccountListSnapshot](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[DisplayAccountNumber] [nvarchar](100) NULL,
	[Category] [nvarchar](50) NOT NULL,
	[StatementType] [nvarchar](50) NOT NULL,
	[RegistrationStatus] [nvarchar](1) NOT NULL,
	[Active] [nvarchar](1) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[SnapshotDate] [datetime] NOT NULL,
	[AccountID] [int] NOT NULL,
 CONSTRAINT [PK_tbAccountListSnapshot] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbAccountListTemp]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbAccountListTemp](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[DisplayAccountNumber] [nvarchar](100) NULL,
	[Category] [nvarchar](50) NOT NULL,
	[StatementType] [nvarchar](50) NOT NULL,
	[RegistrationStatus] [nvarchar](1) NOT NULL,
	[Active] [nvarchar](1) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbAccountListTemp] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbAccountNoticeList]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbAccountNoticeList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[DisplayAccountNumber] [nvarchar](100) NULL,
	[Category] [nvarchar](50) NOT NULL,
	[StatementType] [nvarchar](50) NOT NULL,
	[RegistrationStatusBefore] [nvarchar](1) NULL,
	[RegistrationStatusAfter] [nvarchar](1) NULL,
	[TriggerStatus] [nvarchar](1) NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbAccountNoticeList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbAccountUpdateList]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbAccountUpdateList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_tbAccountUpdateList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbAdviceMapping]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbAdviceMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RuleNumber] [int] NOT NULL,
	[MappingCategory] [nvarchar](100) NOT NULL,
	[Meta_Type] [nvarchar](100) NOT NULL,
	[Order_Type] [nvarchar](100) NOT NULL,
	[Asset_Grp] [nvarchar](100) NOT NULL,
	[Asset_SubGrp] [nvarchar](100) NOT NULL,
	[Product_Type] [nvarchar](100) NULL,
	[Product_Type_Desc_Eng] [nvarchar](500) NULL,
	[Product_Type_Desc_TC] [nvarchar](500) NULL,
	[Product_Name] [nvarchar](500) NULL,
	[AdviceTitleEN] [nvarchar](500) NULL,
	[AdviceTitleTC] [nvarchar](500) NULL,
	[AdviceTitleSC] [nvarchar](500) NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbAdviceMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbAuditLog]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbAuditLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[oldRegStatus] [nvarchar](1) NOT NULL,
	[newRegStatus] [nvarchar](1) NOT NULL,
	[actionSys] [nvarchar](10) NOT NULL,
	[actionId] [nvarchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[StatementType] [nvarchar](50) NOT NULL DEFAULT (''),
	[AutoUnSubscribe] [datetime] NULL,
 CONSTRAINT [PK_tbAuditLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbBankInfoInsert]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbBankInfoInsert](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InsertPath] [nvarchar](100) NOT NULL,
	[Category] [nvarchar](50) NOT NULL,
	[StatementType] [nvarchar](50) NULL,
	[EffectiveFrom] [date] NOT NULL,
	[EffectiveTo] [date] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbBankInformationInsert] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbBounceBackHistory]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbBounceBackHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StatementID] [int] NULL,
	[StatementType] [nvarchar](50) NULL,
	[StatementDocType] [char](1) NULL,
	[RMID] [nvarchar](100) NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[EmailAddress] [varchar](254) NULL,
	[StatementSentDateTime] [datetime] NULL,
	[FailureReportDateTime] [datetime] NULL,
	[FailureReportSubject] [nvarchar](350) NULL,
	[HardSoftBounceIndicator] [char](1) NULL,
	[PreferredLanguage] [nvarchar](30) NULL,
	[MobileNumber] [nvarchar](50) NULL,
	[SMSSentDateTime] [datetime] NULL,
	[SMSContent] [nvarchar](100) NULL,
	[SMSEmailBounceBackIndicator] [char](1) NULL,
	[FirstTimeBounceBackIndicator] [char](1) NULL,
 CONSTRAINT [PK_tbBounceBackHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbBounceBackKeyword]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbBounceBackKeyword](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
	[BounceBackType] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_tbBounceBackKeyword] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbBPInfo]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbBPInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[bpid] [nvarchar](20) NOT NULL,
	[rm_no] [nvarchar](100) NULL,
	[BP_name] [nvarchar](100) NULL,
	[AcctType] [nvarchar](10) NOT NULL,
	[no_of_acc_holder] [int] NOT NULL,
	[rmid] [nvarchar](100) NOT NULL,
	[AVQ] [nvarchar](1) NOT NULL,
 CONSTRAINT [PK_tbBPInfo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbCiticFirstAccountList]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbCiticFirstAccountList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[AccountNumber] [nvarchar](100) NOT NULL,
	[Active] [nvarchar](1) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbCiticFirstAccountList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbCombineAccountList]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbCombineAccountList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[AccountNumber] [nvarchar](100) NOT NULL,
	[Type] [nvarchar](10) NOT NULL,
	[Group] [varchar](1) NOT NULL,
	[Active] [nvarchar](1) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbCombineAccountList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbCustomerEmail]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbCustomerEmail](
	[rmid] [varchar](100) NULL,
	[EMailAddress] [varchar](100) NULL,
	[AcctStatus] [varchar](5) NULL,
	[AcctStatusLastUpdDt] [varchar](20) NULL,
	[PreferredLanguage] [nvarchar](30) NULL,
	[MobileNumber] [varchar](100) NULL,
	[LastUpdateTime] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbDirectBankMapping]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbDirectBankMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](100) NULL,
	[StatementType] [nvarchar](100) NULL,
	[CategoryToMap] [nvarchar](500) NOT NULL,
	[CheckAcno] [nvarchar](1) NOT NULL,
	[CheckCategory] [nvarchar](1) NOT NULL,
 CONSTRAINT [PK_tbDirectBankMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbFormData]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbFormData](
	[PolicyNo] [nvarchar](20) NOT NULL,
	[RMID] [nvarchar](15) NULL,
	[RMNO] [nvarchar](50) NOT NULL,
	[RefNo] [nvarchar](100) NOT NULL,
	[FormID] [nvarchar](50) NOT NULL,
	[VersionNo] [int] NOT NULL,
	[FullName] [nvarchar](80) NOT NULL,
	[Segment] [nvarchar](10) NOT NULL,
	[FNACompDate] [datetime] NOT NULL,
	[InsCompanyName] [nvarchar](50) NOT NULL,
	[PlanName] [nvarchar](200) NOT NULL,
	[BranchName] [nvarchar](50) NOT NULL,
	[ApplyDate] [datetime] NOT NULL,
	[BreakevenYears] [int] NOT NULL,
	[USDRate] [numeric](6, 2) NOT NULL,
	[CNYRate] [numeric](6, 2) NOT NULL,
	[PremiumAmt] [numeric](15, 2) NOT NULL,
	[PremiumCur] [nvarchar](10) NOT NULL,
	[AnnualPFIntParamName] [nvarchar](10) NOT NULL,
	[AnnualPFIntParamValue] [numeric](6, 2) NULL,
	[AnnualPFIntRateOffset] [numeric](6, 2) NULL,
	[AnnualPFIntValue] [numeric](6, 2) NOT NULL,
	[PFPercent] [int] NOT NULL,
	[PFApplyOption] [nvarchar](1) NOT NULL,
	[PFApplyAmt] [numeric](15, 2) NOT NULL,
	[PFApplyCur] [nvarchar](10) NOT NULL,
	[UpfPremiumPay] [numeric](15, 2) NOT NULL,
	[UpfPremiumPayCur] [nvarchar](10) NOT NULL,
	[TotalPFInt] [numeric](15, 2) NOT NULL,
	[TotalPFIntCur] [nvarchar](10) NOT NULL,
	[AnnualPFInt] [numeric](15, 2) NOT NULL,
	[AnnualPFIntCur] [nvarchar](10) NOT NULL,
	[NetLiquidAsset] [numeric](15, 2) NOT NULL,
	[AnnualDispIncome] [numeric](15, 2) NOT NULL,
	[AffordabilityThres] [int] NOT NULL,
	[AssessmentApproach] [nvarchar](max) NOT NULL,
	[Affordability] [int] NOT NULL,
	[ApprovalDate] [datetime] NULL,
	[DrawdownDate] [datetime] NULL,
	[PortfolioCode] [nvarchar](10) NULL,
	[PFStatusID] [int] NOT NULL,
	[isDeleted] [bit] NOT NULL,
	[DeleteReason] [nvarchar](500) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[DeletedBy] [int] NOT NULL,
	[DeletedDate] [datetime] NOT NULL,
	[SubmittedBy] [int] NOT NULL,
	[SubmittionDate] [datetime] NOT NULL,
	[CompletedBy] [int] NOT NULL,
	[CompletionDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbFormData] PRIMARY KEY CLUSTERED 
(
	[PolicyNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbLog]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NULL,
	[Content] [nvarchar](1000) NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbMarketInsert]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbMarketInsert](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InsertPath] [nvarchar](100) NOT NULL,
	[Category] [nvarchar](50) NOT NULL,
	[StatementType] [nvarchar](50) NULL,
	[EffectiveFrom] [date] NOT NULL,
	[EffectiveTo] [date] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbMarketInsert] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbMISReportEmail]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbMISReportEmail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StatementID] [int] NULL,
	[StatementType] [nvarchar](50) NULL,
	[RMID] [nvarchar](100) NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[EmailAddress] [varchar](254) NULL,
	[StatementSentDate] [datetime] NULL,
	[FailureReportDateTime] [datetime] NULL,
	[FailureReportSubject] [nvarchar](100) NULL,
	[HardSoftBounce] [nvarchar](10) NULL,
	[MobileNumber] [varchar](100) NULL,
	[PreferredLanguage] [nvarchar](30) NULL,
	[UNEReferenceNumber] [nvarchar](20) NULL,
 CONSTRAINT [PK_tbMISReportEmail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbMISReportSMS]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbMISReportSMS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [nvarchar](100) NULL,
	[EmailAddress] [varchar](254) NULL,
	[BounceBackCount] [int] NULL,
	[MobileNumber] [varchar](100) NULL,
	[PreferredLanguage] [nvarchar](30) NULL,
	[SMSSentDateTime] [datetime] NULL,
	[UNEReferenceNumber] [nvarchar](20) NULL,
 CONSTRAINT [PK_tbMISReportSMS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbNotification]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbNotification](
	[StatementID] [int] NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[Category] [nvarchar](50) NOT NULL,
	[StatementType] [nvarchar](50) NOT NULL,
	[DisplayAccountNumber] [nvarchar](100) NOT NULL,
	[EMailAddress] [varchar](254) NULL,
	[SentDateTime] [datetime] NULL,
	[Status] [char](1) NOT NULL,
	[EMailReference] [char](36) NULL,
	[EMailSubject] [nvarchar](100) NULL,
	[EMailContent] [nvarchar](max) NULL,
	[ErrorDetail] [nvarchar](max) NULL,
	[statementFreq] [char](1) NULL,
	[UNEReferenceNumber] [nvarchar](20) NULL,
	[TriggerDateTime] [datetime] NULL,
	[HS] [bit] NULL,
	[bounceBackStatus] [nchar](10) NULL,
	[bounceBackCount] [int] NULL,
	[MobileNo] [nvarchar](50) NULL,
	[LastStatusUpdateTime] [datetime] NULL,
	[Ctrl1] [varchar](100) NULL,
	[Ctrl2] [varchar](100) NULL,
	[BPID] [nvarchar](100) NULL,
	[AcctType] [nvarchar](10) NULL,
 CONSTRAINT [PK_tbNotification] PRIMARY KEY CLUSTERED 
(
	[StatementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbNotification.temp]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbNotification.temp](
	[StatementID] [int] NOT NULL,
	[RMID] [nvarchar](100) NOT NULL,
	[Category] [nvarchar](50) NOT NULL,
	[StatementType] [nvarchar](50) NOT NULL,
	[DisplayAccountNumber] [nvarchar](100) NOT NULL,
	[EMailAddress] [varchar](254) NULL,
	[SentDateTime] [datetime] NULL,
	[Status] [char](1) NOT NULL,
	[EMailReference] [char](36) NULL,
	[EMailSubject] [nvarchar](100) NULL,
	[EMailContent] [nvarchar](max) NULL,
	[ErrorDetail] [nvarchar](max) NULL,
	[statementFreq] [char](1) NULL,
	[TriggerDateTime] [datetime] NULL,
	[HS] [bit] NULL,
	[bounceBackStatus] [nchar](10) NULL,
	[MobileNo] [nvarchar](50) NULL,
	[UNEReferenceNumber] [nvarchar](20) NULL,
 CONSTRAINT [PK_tbNotification.temp] PRIMARY KEY CLUSTERED 
(
	[StatementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbRefEstatementTXT]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbRefEstatementTXT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [varchar](100) NULL,
	[BPID] [varchar](20) NULL,
	[RM_Number] [varchar](100) NULL,
	[StatementTypeEngDesc] [nvarchar](100) NOT NULL,
	[AccountNumberOrBPname] [nvarchar](100) NOT NULL,
	[Category] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tbRefEstatementTXT] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbRmIDAccountNumber]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbRmIDAccountNumber](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [nvarchar](100) NULL,
	[RMNumber] [varchar](100) NULL,
	[AccountNumber] [varchar](100) NULL,
	[CreateDate] [datetime] NULL,
	[Category] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tbRmIDAccountNumber] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbStatement]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbStatement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[StatementDate] [datetime] NOT NULL,
	[ViewTime] [datetime] NULL,
	[StatementPath] [nvarchar](500) NOT NULL,
	[HeaderPath] [nvarchar](500) NOT NULL,
	[TermsAndConditionPath] [nvarchar](500) NOT NULL,
	[HeaderPositionX] [int] NOT NULL,
	[HeaderPositionY] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[statementFreq] [nvarchar](1) NULL,
	[productInfo] [char](50) NULL,
	[productRef] [char](50) NULL,
	[Meta_Type] [nvarchar](100) NULL,
	[Order_Type] [nvarchar](100) NULL,
	[Asset_Grp] [nvarchar](100) NULL,
	[Asset_SubGrp] [nvarchar](100) NULL,
	[Asset_Name_EN] [nvarchar](200) NULL,
	[Asset_Name_SC] [nvarchar](200) NULL,
	[Asset_Name_TC] [nvarchar](200) NULL,
	[Product_Type] [nvarchar](100) NULL,
	[Product_Name] [nvarchar](300) NULL,
	[Advice_Title] [nvarchar](500) NULL,
	[ContainerAcno] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbStatement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbStatementFullList]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbStatementFullList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[StatementDate] [datetime] NOT NULL,
	[ViewTime] [datetime] NULL,
	[StatementPath] [nvarchar](500) NOT NULL,
	[HeaderPath] [nvarchar](500) NOT NULL,
	[TermsAndConditionPath] [nvarchar](500) NOT NULL,
	[HeaderPositionX] [int] NOT NULL,
	[HeaderPositionY] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[statementFreq] [nvarchar](1) NULL,
	[productInfo] [char](50) NULL,
	[productRef] [char](50) NULL,
	[Meta_Type] [nvarchar](100) NULL,
	[Order_Type] [nvarchar](100) NULL,
	[Asset_Grp] [nvarchar](100) NULL,
	[Asset_SubGrp] [nvarchar](100) NULL,
	[Asset_Name_EN] [nvarchar](200) NULL,
	[Asset_Name_SC] [nvarchar](200) NULL,
	[Asset_Name_TC] [nvarchar](200) NULL,
	[Product_Type] [nvarchar](100) NULL,
	[Product_Name] [nvarchar](300) NULL,
	[Advice_Title] [nvarchar](500) NULL,
	[ContainerAcno] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbStatementFullList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbStatementTemp]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbStatementTemp](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NULL,
	[StatementDate] [datetime] NOT NULL,
	[ViewTime] [datetime] NULL,
	[StatementPath] [nvarchar](500) NOT NULL,
	[HeaderPath] [nvarchar](500) NOT NULL,
	[TermsAndConditionPath] [nvarchar](500) NOT NULL,
	[HeaderPositionX] [int] NOT NULL,
	[HeaderPositionY] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[statementFreq] [nvarchar](1) NULL,
	[productInfo] [char](50) NULL,
	[productRef] [char](50) NULL,
	[Meta_Type] [nvarchar](100) NULL,
	[Order_Type] [nvarchar](100) NULL,
	[Asset_Grp] [nvarchar](100) NULL,
	[Asset_SubGrp] [nvarchar](100) NULL,
	[Asset_Name_EN] [nvarchar](200) NULL,
	[Asset_Name_SC] [nvarchar](200) NULL,
	[Asset_Name_TC] [nvarchar](200) NULL,
	[Product_Type] [nvarchar](100) NULL,
	[Product_Name] [nvarchar](300) NULL,
	[Advice_Title] [nvarchar](500) NULL,
	[bpid] [nvarchar](100) NULL,
	[containerAcno] [nvarchar](100) NULL,
	[RMNumber] [nvarchar](100) NULL,
	[ACC_NO] [nvarchar](100) NULL,
	[Category] [nvarchar](50) NULL,
	[StatementType] [nvarchar](50) NULL,
	[RMID] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbStatementTemp] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbStatementType]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbStatementType](
	[StatementType] [nvarchar](50) NOT NULL,
	[EngDesc] [nvarchar](100) NOT NULL,
	[TradChiDesc] [nvarchar](100) NOT NULL,
	[SimChiDesc] [nvarchar](100) NOT NULL,
	[statementFreq] [char](1) NOT NULL DEFAULT ('M'),
 CONSTRAINT [PK_statementType_statementFreq] PRIMARY KEY CLUSTERED 
(
	[StatementType] ASC,
	[statementFreq] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbSubAccountList]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSubAccountList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[bpid] [nvarchar](20) NOT NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[DisplayAccountNumber] [nvarchar](100) NULL,
	[Category] [nvarchar](50) NOT NULL,
	[Active] [nvarchar](1) NOT NULL,
	[ActivateDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_tbSubAccountList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbSubAccountListTemp]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSubAccountListTemp](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NULL,
	[bpid] [nvarchar](20) NOT NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[DisplayAccountNumber] [nvarchar](100) NULL,
	[Category] [nvarchar](50) NOT NULL,
	[Active] [nvarchar](1) NOT NULL,
	[ActivateDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_tbSubAccountListTemp] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbSystemSetting]    Script Date: 8/16/2018 10:18:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSystemSetting](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SettingName] [nvarchar](100) NOT NULL,
	[SettingValue] [nvarchar](1000) NOT NULL,
	[Category] [nvarchar](50) NULL,
	[StatementType] [nvarchar](50) NULL,
	[HeaderPositionX] [int] NULL,
	[HeaderPositionY] [int] NULL,
	[EffectiveFrom] [date] NOT NULL,
	[EffectiveTo] [date] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbSystemSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[tbStatement]  WITH NOCHECK ADD  CONSTRAINT [FK_tbStatement_tbAccountList] FOREIGN KEY([AccountID])
REFERENCES [dbo].[tbAccountList] ([ID])
GO
ALTER TABLE [dbo].[tbStatement] CHECK CONSTRAINT [FK_tbStatement_tbAccountList]
GO
ALTER TABLE [dbo].[tbStatementFullList]  WITH NOCHECK ADD  CONSTRAINT [FK_tbStatementFullList_tbAccountList] FOREIGN KEY([AccountID])
REFERENCES [dbo].[tbAccountList] ([ID])
GO
ALTER TABLE [dbo].[tbStatementFullList] CHECK CONSTRAINT [FK_tbStatementFullList_tbAccountList]
GO
ALTER TABLE [dbo].[tbSubAccountList]  WITH NOCHECK ADD  CONSTRAINT [FK_tbSubAccountList_tbAccountList] FOREIGN KEY([AccountID])
REFERENCES [dbo].[tbAccountList] ([ID])
GO
ALTER TABLE [dbo].[tbSubAccountList] CHECK CONSTRAINT [FK_tbSubAccountList_tbAccountList]
GO
ALTER TABLE [dbo].[tbDirectBankMapping]  WITH CHECK ADD  CONSTRAINT [CK_CheckAcno] CHECK  (([CheckAcno]='N' OR [CheckAcno]='Y'))
GO
ALTER TABLE [dbo].[tbDirectBankMapping] CHECK CONSTRAINT [CK_CheckAcno]
GO
ALTER TABLE [dbo].[tbDirectBankMapping]  WITH CHECK ADD  CONSTRAINT [CK_CheckCategory] CHECK  (([CheckCategory]='N' OR [CheckCategory]='Y'))
GO
ALTER TABLE [dbo].[tbDirectBankMapping] CHECK CONSTRAINT [CK_CheckCategory]
GO
