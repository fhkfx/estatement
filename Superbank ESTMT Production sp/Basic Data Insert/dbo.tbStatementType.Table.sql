USE [Estatement]
GO
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'AVQ', N'Consolidated Statement', N'綜合月結單', N'综合月结单', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'BNW', N'Business Now Account', N'劃時商貿理財戶口', N'划时商贸理财户口', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'CF', N'CITICfirst Consolidated Statement', N'CITICfirst 綜合月結單', N'CITICfirst 综合月结单', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'CNCBI', N'Visa Card', N'Visa 卡', N'Visa 卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'CNCBIAIRCHINADCC', N'Air China Dual Currency Credit Card', N'中國國航雙幣信用卡', N'中国国航双币信用卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'CON', N'Consolidated Statement', N'綜合結單', N'综合结单', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'DOLLARSMARTCASH', N'Dollar$mart Cash Card', N'Dollar$mart 現金卡', N'Dollar$mart 现金卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'DOLLARSMARTPLOAN', N'Dollar$mart Personal Installment Loan Card', N'Dollar$mart 分期貸款卡', N'Dollar$mart 分期贷款卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'DOLLARSMARTTAXLOAN', N'Dollar$mart Tax Loan Card', N'Dollar$mart 稅務貸款卡', N'Dollar$mart 税务贷款卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'FD', N'TIME DEPOSIT CONFIRMATION', N'定期存款通知書', N'定期存款通知书', N'A')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'InfoCast', N'Securities Trading Advice', N'證券交易通知書 ', N'证券交易通知书', N'A')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'InfoCast', N'Securities Trading Daily Statement', N'證券交易日結單 ', N'证券交易日结单', N'D')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'InfoCast', N'Securities Trading Monthly Statement', N'證券交易月結單', N'证券交易月结单', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'MASTER-AQUA', N'AQUA MasterCard ', N'AQUA 萬事達卡', N'AQUA 万事达卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'MASTER-AQUAGOLD', N'AQUA Gold MasterCard', N'AQUA 萬事達金卡', N'AQUA 万事达金卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'MASTER-BUSINESS', N'Business Card', N'商務卡', N'商务卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'MASTER-NFC', N'CNCBI NFC Motion Card', N'信銀國際 NFC Motion 信用卡', N'信银国际 NFC Motion 信用卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'MASTER-PAYPASS', N'CNCBI Motion Credit Card', N'信銀國際 Motion 信用卡', N'信银国际 Motion 信用卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'MONTHLY', N'Monthly Statement', N'月結單', N'月结单', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'NOW', N'NOW Account', N'劃時理財戶口', N'划时理财户口', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'PB', N'Private Banking Consolidated Statement', N'私人銀行綜合月結單', N'私人银行综合月结单', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'PLOC', N'Personal Line of Credit', N'支票透支服務', N'支票透支服务', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'RMB', N'RMB Card', N'人民幣卡', N'人民币卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'RMB-GOLD', N'RMB Gold Card', N'人民幣金卡', N'人民币金卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'SBSCON', N'Consolidated Statement', N'綜合月結單', N'综合月结单', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'VISA', N'Visa Card', N'Visa 卡', N'Visa 卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'VISA-AQUA', N'AQUA Visa Card', N'AQUA Visa 卡', N'AQUA Visa 卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'VISA-AQUAGOLD', N'AQUA Visa Gold Card', N'AQUA Visa 金卡', N'AQUA Visa 金卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'VISA-FLEXIRATE', N'Flexi-Rate Visa Card', N'零活息 Visa 卡', N'零活息 Visa 卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'VISAGOLD', N'Visa Gold Card', N'Visa 金卡', N'Visa 金卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'VISAGOLD-FLEXIRATE', N'Flexi-Rate Visa Gold Card', N'零活息 Visa 金卡', N'零活息 Visa 金卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'VISAPLATINUM', N'Visa Platinum Card', N'Visa 白金卡', N'Visa 白金卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'VISAPLATINUM-CF', N'CITICfirst Visa Platinum Card', N'CITICfirst Visa 白金卡', N'CITICfirst Visa 白金卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'VISAPLATINUM-PB', N'Private Banking Visa Platinum Card', N'私人銀行 Visa 白金卡', N'私人银行 Visa 白金卡', N'M')
INSERT [dbo].[tbStatementType] ([StatementType], [EngDesc], [TradChiDesc], [SimChiDesc], [statementFreq]) VALUES (N'VISA-SEED', N'SEED Visa Card', N'SEED Visa 卡', N'SEED Visa 卡', N'M')
