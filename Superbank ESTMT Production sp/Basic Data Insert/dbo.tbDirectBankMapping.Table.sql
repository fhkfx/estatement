USE [Estatement]
GO
SET IDENTITY_INSERT [dbo].[tbDirectBankMapping] ON 

INSERT [dbo].[tbDirectBankMapping] ([ID], [Category], [StatementType], [CategoryToMap], [CheckAcno], [CheckCategory]) VALUES (1, N'Combine', N'CON', N'Combine', N'Y', N'N')
INSERT [dbo].[tbDirectBankMapping] ([ID], [Category], [StatementType], [CategoryToMap], [CheckAcno], [CheckCategory]) VALUES (4, N'FixDeposit', N'FD', N'individual', N'N', N'Y')
INSERT [dbo].[tbDirectBankMapping] ([ID], [Category], [StatementType], [CategoryToMap], [CheckAcno], [CheckCategory]) VALUES (5, N'FixDeposit', N'FD', N'Combine', N'N', N'Y')
SET IDENTITY_INSERT [dbo].[tbDirectBankMapping] OFF
