USE [Estatement]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateCombineAccountStatus]    Script Date: 7/4/2018 3:23:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateCombineAccountStatus] 
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE target SET target.RegistrationStatus = 'S',
	target.ModifyBy = target.ModifyBy + ' - spUpdateCombineAccountStatus'
	FROM tbAccountList target
	WHERE target.Active = 'Y'
	AND target.Category = 'Combine'
	AND target.RegistrationStatus = 'U'
	AND EXISTS (
		select * from tbCombineAccountList c inner join tbCombineAccountList d
		on c.RMID = d.RMID and c.[Group] = d.[Group] and d.[Type] = 'S'
		inner join tbAccountList e
		on e.RMID = d.RMID and e.AccountNumber = d.AccountNumber and e.LastUpdate = (select MAX(LastUpdate) from tbAccountList where RMID = e.RMID and AccountNumber = e.AccountNumber)
		where c.RMID = target.RMID and c.AccountNumber = target.AccountNumber
		and e.RegistrationStatus = 'S'
	)
	and NOT EXISTS (
		select source.ID from tbAccountListTemp source
		where target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category
		AND target.statementType = source.statementType
	)
END






GO
