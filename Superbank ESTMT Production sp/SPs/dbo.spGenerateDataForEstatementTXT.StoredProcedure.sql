USE [Estatement]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateDataForEstatementTXT]    Script Date: 7/4/2018 3:23:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGenerateDataForEstatementTXT]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Start insert records into tbRefEstatementTXT', GETDATE(), 'ESBatchRegistrationStatusExporter', 'ESBatchRegistrationStatusExporter', GETDATE())

insert into [dbo].[tbRefEstatementTXT] ([RMID],[RM_Number],[StatementTypeEngDesc],[AccountNumberOrBPname],[Category])
(
select distinct aclist.RMID,rmMap.RMNumber,aclist.StatementType,aclist.AccountNumber,aclist.Category 
from 
	[dbo].[tbAccountList] aclist
left join
	[dbo].[tbRmIDAccountNumber] rmMap
on 
	aclist.RMID = rmMap.RMID
where 
	aclist.RegistrationStatus = 'S' and aclist.Active = 'Y'
and 
	aclist.Category != 'SBS'
and 
	aclist.Category != 'AVQ'
)

insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Finish insert non superbank records', GETDATE(), 'ESBatchRegistrationStatusExporter', 'ESBatchRegistrationStatusExporter', GETDATE())

insert into [dbo].[tbRefEstatementTXT] ([RMID],[BPID],[StatementTypeEngDesc],[AccountNumberOrBPname],[Category])
(
select bpinfo.rmid,bpinfo.bpid,aclist.StatementType,
(
CASE 
WHEN (aclist.Category = 'SBS')
THEN bpinfo.BP_name
WHEN (aclist.Category = 'AVQ')
THEN aclist.AccountNumber
END
)
,aclist.Category
from 
	[dbo].[tbAccountList] aclist
inner join
	[dbo].[tbBPInfo] bpinfo
on 
	aclist.RMID = bpinfo.bpid
where 
	aclist.RegistrationStatus = 'S' and aclist.Active = 'Y'
and 
(
	aclist.Category = 'SBS'
or 
	aclist.Category = 'AVQ'
)
)

insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Finish insert superbank records', GETDATE(), 'ESBatchRegistrationStatusExporter', 'ESBatchRegistrationStatusExporter', GETDATE())

update estmtTXT
set 
	estmtTXT.RM_Number = rmMap.RMNumber
from 
	[dbo].[tbRefEstatementTXT] estmtTXT
inner join
	[dbo].[tbRmIDAccountNumber] rmMap
on 
	estmtTXT.RMID = rmMap.rmid
where 
	estmtTXT.StatementTypeEngDesc = 'SBSCON'
or
	estmtTXT.StatementTypeEngDesc = 'AVQ'

insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Finish update RMNumber', GETDATE(), 'ESBatchRegistrationStatusExporter', 'ESBatchRegistrationStatusExporter', GETDATE())

insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End insert records into tbRefEstatementTXT', GETDATE(), 'ESBatchRegistrationStatusExporter', 'ESBatchRegistrationStatusExporter', GETDATE())

END





GO
