USE [Estatement]
GO
/****** Object:  StoredProcedure [dbo].[spUpdatetbAccountList]    Script Date: 7/12/2018 11:28:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,> - 201807024 superbank production ver 1.2
-- =============================================
ALTER PROCEDURE [dbo].[spUpdatetbAccountList]
AS
BEGIN
	SET NOCOUNT ON;
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Start [dbo].[spUpdatetbAccountList] ver 1.2', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- Insert FixDeposit Account into tbAccountListTemp
	insert into tbAccountListTemp ([RMID],[AccountNumber],[DisplayAccountNumber],[Category],[StatementType],[RegistrationStatus],[Active],[CreateDate],[CreateBy],[ModifyBy],[LastUpdate])
	select distinct TmpAclist.[RMID],'','','FixDeposit','FD','U','Y',GETDATE(),'ESBatchAccountImporter','ESBatchAccountImporter',GETDATE() 
	from tbAccountListTemp TmpAclist where Category in
	(select distinct CategoryToMap from tbDirectBankMapping where Category = 'FixDeposit')

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Insert FixDeposit Account into tbAccountListTemp', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	-- DollarSmart Ploan and tax loan ineligible for eStmt subscription
	DELETE FROM [dbo].[tbAccountListTemp]
	WHERE Category = 'CreditCard'
	and (StatementType = 'DOLLARSMARTPLOAN' or StatementType = 'DOLLARSMARTTAXLOAN')
	
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End delete CreditCard DOLLARSMARTPLOAN, DOLLARSMARTTAXLOAN from tbAccountListTemp', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- update source registration status from target
	UPDATE 
		source
	SET 
		source.RegistrationStatus = target.RegistrationStatus
	FROM 
		[dbo].[tbAccountList] AS target
		INNER JOIN [dbo].[tbAccountListTemp] AS source
	ON 
		(target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category
		AND target.StatementType = source.StatementType)

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End update registration status from tbAccountList to tbAccountListTemp', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
		
	-- Insert Record with RegistrationStatusBefore to tbAccountNoticeList (New Case)
	INSERT INTO [dbo].[tbAccountNoticeList]
	([AccountID], [RMID], [AccountNumber], [DisplayAccountNumber], [Category],	[StatementType], [RegistrationStatusBefore], [CreateDate], [CreateBy], [ModifyBy], [LastUpdate], [TriggerStatus]) 
		SELECT tempAccountList.ID, tempAccountList.RMID, tempAccountList.AccountNumber, tempAccountList.DisplayAccountNumber, tempAccountList.Category,
		tempAccountList.StatementType, tempAccountList.RegistrationStatus, GETDATE(), tempAccountList.CreateBy,
		tempAccountList.ModifyBy, GETDATE(), 'P'
			FROM [dbo].[tbAccountListTemp] tempAccountList 
			WHERE tempAccountList.Category not in ('CiticFirst', 'InfoCast', 'SBS', 'AVQ')
			and EXISTS (
				select *
				from tbCustomerEmail as a
				inner join
				(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
				on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
				where a.AcctStatus = 'ACT' and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(tempAccountList.RMID)) and a.EMailAddress is not null
			) 	
			and NOT EXISTS (
				select id from tbAccountList c 
				where LTRIM(RTRIM(c.rmid)) = LTRIM(RTRIM(tempAccountList.RMID))
				and LTRIM(RTRIM(c.AccountNumber)) = LTRIM(RTRIM(tempAccountList.AccountNumber))
				and c.Category = tempAccountList.Category
				and c.StatementType = tempAccountList.StatementType
			)
			
	INSERT INTO [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	VALUES ('Info', '(sp) End Insert Record with RegistrationStatusBefore to tbAccountNoticeList (New Case)', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	-- CF ineligible for auto subscription
	-- non-CF and latest AcctStatus is 'ACT' is eligible for autosubscription
	UPDATE source 
	SET source.[RegistrationStatus] = 'S'
	FROM [dbo].[tbAccountListTemp] source
	-- autosubscription ignore SBS & AVQ
	WHERE source.Category not in ('CiticFirst', 'InfoCast', 'FixDeposit', 'SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where a.AcctStatus = 'ACT' and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(source.RMID)) and a.EMailAddress is not null
	) 	
	and NOT EXISTS (
		select id from tbAccountList c 
		where LTRIM(RTRIM(c.rmid)) = LTRIM(RTRIM(source.RMID))
		and LTRIM(RTRIM(c.AccountNumber)) = LTRIM(RTRIM(source.AccountNumber))
		and c.Category = source.Category
		and c.StatementType = source.StatementType
	)

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End auto subscription (SBS/AVQ accounts excepted)', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- latest AcctStatus is 'CLS' or 'CAP' is eligible for auto-Un-Subscription
	-- EMailAddress is null and length = 0, are also for auto-Un-Subscription ; 2015-09-18	
	UPDATE source
	SET source.[RegistrationStatus] = 'U'
	FROM [dbo].[tbAccountListTemp] source 
	WHERE source.RegistrationStatus <> 'U'
	and source.Category not in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(source.RMID))
	)
	
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End auto unsubscription (for Normal accounts)', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- auto-Un-Subscription for SBS & AVQ
	UPDATE source
	SET source.[RegistrationStatus] = 'U'
	FROM [dbo].[tbAccountListTemp] source 
	inner join [dbo].[tbBPInfo] bp
	on source.RMID = bp.bpid
	WHERE source.RegistrationStatus <> 'U'
	and source.Category in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(bp.rmid))
	)
	
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End auto unsubscription (for SBS/AVQ accounts)', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- Update Record with RegistrationStatusAfter to tbAccountNoticeList
	UPDATE 
		source
	SET 
		source.RegistrationStatusAfter = target.RegistrationStatus
	FROM 
		[dbo].[tbAccountNoticeList] AS source
		INNER JOIN [dbo].[tbAccountListTemp] AS target
	ON 
		(target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category
		AND target.StatementType = source.StatementType) 
	
	INSERT INTO [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	VALUES ('Info', '(sp) End Update Record with RegistrationStatusAfter to tbAccountNoticeList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	-- Merge tbAccountList and tbAccountListTemp
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Start Merge tbAccountListTemp to tbAccountList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	MERGE [dbo].[tbAccountList] AS target
	USING [dbo].[tbAccountListTemp] AS source
	ON 
		(target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category
		AND target.statementType = source.statementType)
	WHEN NOT MATCHED BY target THEN

		INSERT([RMID],[AccountNumber],[DisplayAccountNumber],[Category],[StatementType],[RegistrationStatus],[Active],[CreateDate],[CreateBy],[ModifyBy],[LastUpdate])
		VALUES(source.[RMID],source.[AccountNumber],source.[DisplayAccountNumber],source.[Category],source.[StatementType],source.[RegistrationStatus],source.[Active],source.[CreateDate],source.[CreateBy],source.[ModifyBy],source.[LastUpdate])

	WHEN NOT MATCHED BY source THEN
		UPDATE 	SET target.[Active] = 'N'
	;

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Merge tbAccountListTemp to tbAccountList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- Update Active = "Y" and RegistrationStatus from tbAccountListTemp
	UPDATE 
		target
	SET 
		target.Active = 'Y',
		target.LastUpdate = GETDATE(),
		target.ModifyBy = 'ESBatchAccountImporter',
		target.RegistrationStatus = source.RegistrationStatus
	FROM 
		[dbo].[tbAccountList] AS target
		INNER JOIN [dbo].[tbAccountListTemp] AS source
	ON 
		(target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category
		AND target.StatementType = source.StatementType
		AND UPPER(target.Category) != 'INFOCAST'
		)

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Update Active to Y', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	UPDATE 
		target
	SET 
		target.Active = 'N',
		target.LastUpdate = GETDATE(),
		target.ModifyBy = 'ESBatchAccountImporter'
	FROM 
		[dbo].[tbAccountList] AS target
		INNER JOIN [dbo].[tbAccountListTemp] AS source
	ON 
		(target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND (target.Category != source.Category OR target.StatementType != source.StatementType)
		AND target.Category in ('individual', 'combine', 'CiticFirst') 
		AND source.Category in ('individual', 'combine', 'CiticFirst')
		)

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Update Active to N', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- Update tbAccountUpdateList
	TRUNCATE TABLE [dbo].[tbAccountUpdateList];
	insert into [dbo].[tbAccountUpdateList] ([AccountID],[RMID],[AccountNumber],[CreateDate])
	SELECT [ID],[RMID],[AccountNumber],[CreateDate]
	FROM [dbo].[tbAccountList]
	WHERE CAST([dbo].[tbAccountList].CreateDate as DATE) = CAST(GETDATE() as DATE);

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Update tbAccountUpdateList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- Merge tbSubAccountListTemp AccountID
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Start Merge tbSubAccountListTemp to tbSubAccountList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	update [dbo].[tbSubAccountListTemp]
	set [dbo].[tbSubAccountListTemp].[AccountID] = [dbo].[tbAccountList].[ID]
	from
		[dbo].[tbSubAccountListTemp]
	inner join
		[dbo].[tbAccountList]
	on
		[dbo].[tbAccountList].[RMID] = [dbo].[tbSubAccountListTemp].[bpid]
	and
		[dbo].[tbAccountList].[Category] = [dbo].[tbSubAccountListTemp].[Category]
	and
		[dbo].[tbAccountList].[AccountNumber] = 
		CASE
		WHEN ([dbo].[tbSubAccountListTemp].[Category] = 'SBS')
		THEN [dbo].[tbSubAccountListTemp].[bpid]
		WHEN ([dbo].[tbSubAccountListTemp].[Category] = 'AVQ')
		THEN [dbo].[tbSubAccountListTemp].[AccountNumber]
		END

	delete from [dbo].[tbSubAccountListTemp] where AccountID is null

	-- Merge tbSubAccountList and tbSubAccountListTemp
	MERGE [dbo].[tbSubAccountList] AS target
	USING [dbo].[tbSubAccountListTemp] AS source
	ON 
		(target.bpid = source.bpid
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category)
	WHEN NOT MATCHED BY target THEN
		INSERT ([AccountID],[bpid],[AccountNumber],[DisplayAccountNumber],[Category],[Active],[ActivateDate],[CreateDate],[CreateBy],[ModifyDate],[ModifyBy])
		VALUES(
		source.[AccountID], source.[bpid],source.[AccountNumber],source.[DisplayAccountNumber],source.[Category],source.[Active],source.[ActivateDate],source.[CreateDate],source.[CreateBy],source.[ModifyDate],source.[ModifyBy])
	WHEN NOT MATCHED BY source THEN
		UPDATE SET target.[Active] = 'N'
	WHEN MATCHED THEN
		UPDATE SET target.[Active] = 'Y'
	;

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Merge tbSubAccountListTemp to tbSubAccountList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	UPDATE
		aclist
	SET aclist.Active = 'Y',
		aclist.LastUpdate = GETDATE(),
		aclist.ModifyBy = 'ESBatchAccountImporter'
	FROM
		[dbo].[tbAccountList] AS aclist
	WHERE aclist.Category in ('FixDeposit')

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Update Active to Y (DirectBank)', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End [dbo].[spUpdatetbAccountList]', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
END




