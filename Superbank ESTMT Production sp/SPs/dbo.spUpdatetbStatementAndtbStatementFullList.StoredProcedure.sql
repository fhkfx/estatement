USE [Estatement]
GO
/****** Object:  StoredProcedure [dbo].[spUpdatetbStatementAndtbStatementFullList]    Script Date: 7/4/2018 3:23:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdatetbStatementAndtbStatementFullList]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Superbank statement records
	if 
		(select 
			count(*) 
		from 
			[dbo].[tbStatementTemp] stmtTemp
		where 
			stmtTemp.Advice_Title is null
		and 
			stmtTemp.containerAcno is null
		and
			stmtTemp.bpid is not null)
		> 0
	begin
		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert superbank statement records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		update [dbo].[tbStatementTemp]
		set [dbo].[tbStatementTemp].[AccountID] = [dbo].[tbAccountList].[ID]
		from
			[dbo].[tbStatementTemp]
		inner join
			[dbo].[tbAccountList] 
		on
			[dbo].[tbStatementTemp].bpid = [dbo].[tbAccountList].RMID
			and [dbo].[tbAccountList].Category = 'SBS'

		insert into [dbo].[tbStatement] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert superbank statement records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert superbank statement records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbStatementFullList] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert superbank statement records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())
	end
	-- Container statement records
	else if 
		(select 
			count(*) 
		from 
			[dbo].[tbStatementTemp] stmtTemp
		where 
			stmtTemp.Advice_Title is null
		and 
			stmtTemp.bpid is null
		and
			stmtTemp.containerAcno is not null)
		> 0
	begin
		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert container statement records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		update [dbo].[tbStatementTemp]
		set [dbo].[tbStatementTemp].[AccountID] = [dbo].[tbAccountList].[ID]
		from
			[dbo].[tbStatementTemp]
		inner join
			[dbo].[tbAccountList]
		on
			([dbo].[tbStatementTemp].containerAcno = [dbo].[tbAccountList].AccountNumber
			and [dbo].[tbAccountList].Category = 'AVQ')

		insert into [dbo].[tbStatement] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert container statement records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert container statement records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbStatementFullList] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert container statement records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())
	end
	-- Superbank/Container daily/advice records
	else if 
		(select 
			count(*) 
		from 
			[dbo].[tbStatementTemp] stmtTemp
		where 
			stmtTemp.bpid is null
		and 
			stmtTemp.Advice_Title is not null
		and
			stmtTemp.containerAcno is not null)
		> 0
	begin
		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert daily/advice records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		update [dbo].[tbStatementTemp]
		set [dbo].[tbStatementTemp].[AccountID] = [dbo].[tbSubAccountList].[AccountID]
		from
			[dbo].[tbStatementTemp]
		inner join
			[dbo].[tbSubAccountList] 
		on
			([dbo].[tbStatementTemp].containerAcno = [dbo].[tbSubAccountList].AccountNumber
			and [dbo].[tbSubAccountList].Active = 'Y')
		where
			[tbSubAccountList].AccountID in 
			(
				SELECT aclist_id.[ID]
				FROM [dbo].[tbAccountList] aclist_id
			)

		insert into [dbo].[tbStatement] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title],[ContainerAcno])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title],stmtTemp.[containerAcno]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert daily/advice records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert daily/advice records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbStatementFullList] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title],[ContainerAcno])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title],stmtTemp.[containerAcno]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert daily/advice records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())
	end
	-- FixDeposit records
	else if 
		(select 
			count(*) 
		from 
			[dbo].[tbStatementTemp] stmtTemp
		where 
			stmtTemp.RMNumber is not null)
		> 0
	begin
		update stmtTemp
		set stmtTemp.[RMID] = LTRIM(RTRIM(rmMap.[RMID]))
		from
			[dbo].[tbStatementTemp] stmtTemp
		inner join
			[dbo].[tbRmIDAccountNumber] rmMap
		on
			stmtTemp.RMNumber = rmMap.RMNumber

		update stmtTemp
		set stmtTemp.[AccountID] = aclist.[ID]
		from
			[dbo].[tbStatementTemp] stmtTemp
		inner join
			[dbo].[tbAccountList] aclist
		on
			stmtTemp.RMID = LTRIM(RTRIM(aclist.RMID))
		and
			stmtTemp.Category = aclist.Category
		and
			stmtTemp.StatementType = aclist.StatementType
		where 
			stmtTemp.ACC_NO is null

		update stmtTemp
		set stmtTemp.[AccountID] = aclist.[ID]
		from
			[dbo].[tbStatementTemp] stmtTemp
		inner join
			[dbo].[tbAccountList] aclist
		on
			stmtTemp.RMID = LTRIM(RTRIM(aclist.RMID))
		and
			stmtTemp.ACC_NO = aclist.AccountNumber
		and
			stmtTemp.Category = aclist.Category
		and
			stmtTemp.StatementType = aclist.StatementType
		where 
			stmtTemp.ACC_NO is not null

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert directbank records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbStatement] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert directbank records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert directbank records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbStatementFullList] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert directbank records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

	end

	END






GO
