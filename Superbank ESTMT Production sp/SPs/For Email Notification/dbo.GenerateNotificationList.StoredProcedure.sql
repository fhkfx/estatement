USE [Estatement]
GO
/****** Object:  StoredProcedure [dbo].[GenerateNotificationList]    Script Date: 7/4/2018 3:23:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GenerateNotificationList]
AS
BEGIN
	--INSERT INTO tbNotification (StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, Status)
		--SELECT s.ID, a.RMID, a.Category, a.StatementType, a.DisplayAccountNumber, c.EMailAddress, 'P'
	INSERT INTO tbNotification (StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, MobileNo, Status, statementFreq, LastStatusUpdateTime)
		SELECT s.ID, a.RMID, a.Category, a.StatementType, a.DisplayAccountNumber, c.EMailAddress, c.MobileNumber,'P', s.statementFreq, getdate()
			FROM tbStatement s JOIN tbAccountList a ON s.AccountID = a.ID
				JOIN tbSystemSetting ss ON a.Category = ss.Category AND a.StatementType = ss.StatementType AND ss.SettingName = 'PredefinedPeriod'
				LEFT OUTER JOIN 
					(select a.RMID, max(a.EMailAddress) as Emailaddress, max(a.MobileNumber) as Mobilenumber
					from tbCustomerEmail as a
					inner join
					(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
					on a.rmid = b.rmid 
					and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt] 
					group by a.RMID
					) c ON a.RMID = c.RMID
			WHERE DATEADD(SECOND, CONVERT(int, ss.SettingValue), s.CreateDate) < GETDATE()
			AND NOT EXISTS (SELECT * FROM tbNotification n WHERE n.StatementID = s.ID)
			AND a.Category NOT IN ('SBS','AVQ') 

	INSERT INTO tbNotification (StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, MobileNo, Status, statementFreq, LastStatusUpdateTime, BPID, AcctType)
		SELECT DISTINCT s.ID, bp.RMID, a.Category, a.StatementType, 
		(CASE WHEN s.ContainerAcno IS NULL THEN '' ELSE (LEFT(s.ContainerAcno,3)+'-'+SUBSTRING(s.ContainerAcno,4,1)+'-'+SUBSTRING(s.ContainerAcno,5,6)+'-'++SUBSTRING(s.ContainerAcno,11,2)) END), 
		c.EMailAddress, c.MobileNumber,'P', s.statementFreq, getdate(), a.RMID, bp.AcctType
			FROM tbStatement s JOIN tbAccountList a ON s.AccountID = a.ID
				JOIN tbSystemSetting ss ON a.Category = ss.Category AND a.StatementType = ss.StatementType AND ss.SettingName = 'PredefinedPeriod'

				INNER JOIN 
					(Select bpid, rmid, AcctType From tbBPInfo where AcctType = 'individual') bp on a.rmid = bp.bpid

				LEFT OUTER JOIN 
					(select a.RMID, max(a.EMailAddress) as Emailaddress, max(a.MobileNumber) as Mobilenumber
					from tbCustomerEmail as a
					inner join
					(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
					on a.rmid = b.rmid 
					and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt] 
					group by a.RMID
					) c ON bp.RMID = c.RMID

			WHERE DATEADD(SECOND, CONVERT(int, ss.SettingValue), s.CreateDate) < GETDATE()
			AND NOT EXISTS (SELECT * FROM tbNotification n WHERE n.StatementID = s.ID)
			AND a.Category IN('SBS', 'AVQ') 

END







GO
