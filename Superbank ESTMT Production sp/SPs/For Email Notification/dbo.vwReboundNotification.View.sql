USE [Estatement]
GO

/****** Object:  View [dbo].[vwReboundNotification]    Script Date: 4/13/2018 10:58:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER VIEW [dbo].[vwReboundNotification]
AS
SELECT DISTINCT 
                         TOP (100) PERCENT noti.StatementID, noti.RMID, noti.Category, noti.StatementType, noti.DisplayAccountNumber, noti.EMailAddress, 
                         stmtType.EngDesc, stmtType.TradChiDesc, stmtType.SimChiDesc, noti.statementFreq, noti.TriggerDateTime, noti.HS, 
                         noti.bounceBackStatus, noti.bounceBackCount, noti.MobileNo, noti.UNEReferenceNumber, dbo.tbCustomerEmail.PreferredLanguage, 
                         stmt.Product_Type, prodTypeMap.Product_Type_Desc_Eng, prodTypeMap.Product_Type_Desc_TC, stmt.Advice_Title AS AdviceTitleEN, advTitleMap.AdviceTitleTC
FROM					(SELECT StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, statementFreq, Status, TriggerDateTime, HS, 
                         bounceBackStatus, bounceBackCount, MobileNo, UNEReferenceNumber
                         FROM dbo.tbNotification) noti 
						 INNER JOIN dbo.tbStatement stmt 
						 ON noti.StatementID = stmt.ID 
						 LEFT JOIN dbo.tbStatementType stmtType 
						 ON noti.StatementType = stmtType.StatementType AND noti.statementFreq = stmtType.statementFreq 
						 LEFT JOIN (SELECT Product_Type, Product_Type_Desc_Eng, Product_Type_Desc_TC FROM dbo.tbAdviceMapping WHERE MappingCategory = 'ProductType') prodTypeMap 
						 ON stmt.Product_Type = prodTypeMap.Product_Type 
						 LEFT JOIN (SELECT AdviceTitleEN, AdviceTitleTC FROM dbo.tbAdviceMapping WHERE MappingCategory = 'AdviceTitle') advTitleMap 
						 ON stmt.Advice_Title = advTitleMap.AdviceTitleEN 
						 INNER JOIN dbo.tbCustomerEmail 
						 ON noti.RMID = dbo.tbCustomerEmail.rmid 

WHERE					(noti.Status = 'R') AND (noti.TriggerDateTime < GETDATE()) 
ORDER BY noti.StatementID







GO


