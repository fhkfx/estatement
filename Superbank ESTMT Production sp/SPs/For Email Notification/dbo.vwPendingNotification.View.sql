USE [Estatement]
GO

/****** Object:  View [dbo].[vwPendingNotification]    Script Date: 4/12/2018 4:19:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







ALTER VIEW [dbo].[vwPendingNotification]
AS
SELECT DISTINCT 
                         TOP (100) PERCENT noti.StatementID, noti.RMID, noti.Category, noti.StatementType, noti.DisplayAccountNumber, 
						 noti.EMailAddress, (CASE WHEN stmtType.EngDesc IS NULL THEN '' ELSE stmtType.EngDesc END) AS EngDesc, 
						 (CASE WHEN stmtType.TradChiDesc IS NULL THEN '' ELSE stmtType.TradChiDesc END) AS TradChiDesc, 
						 stmtType.SimChiDesc, noti.statementFreq, stmt.Product_Type, 
						 prodTypeMap.Product_Type_Desc_Eng, prodTypeMap.Product_Type_Desc_TC, stmt.Advice_Title AS AdviceTitleEN, advTitleMap.AdviceTitleTC
FROM					(SELECT StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, statementFreq, Status 
                         FROM dbo.tbNotification) noti 
						 INNER JOIN dbo.tbStatement stmt 
						 ON noti.StatementID = stmt.ID 
						 LEFT JOIN dbo.tbStatementType stmtType 
						 ON noti.StatementType = stmtType.StatementType AND noti.statementFreq = stmtType.statementFreq
						 LEFT JOIN (SELECT Product_Type, Product_Type_Desc_Eng, Product_Type_Desc_TC FROM dbo.tbAdviceMapping WHERE MappingCategory = 'ProductType') prodTypeMap 
						 ON stmt.Product_Type = prodTypeMap.Product_Type 
						 LEFT JOIN (SELECT AdviceTitleEN, AdviceTitleTC FROM dbo.tbAdviceMapping WHERE MappingCategory = 'AdviceTitle') advTitleMap 
						 ON stmt.Advice_Title = advTitleMap.AdviceTitleEN 
WHERE					(noti.Status = 'P')
ORDER BY noti.StatementID







GO


