USE [Estatement]
GO

/****** Object:  StoredProcedure [dbo].[spAutoUnsubscribe]    Script Date: 7/24/2018 10:16:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAutoUnsubscribe]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- update tbAuditLog column AutoUnSubscribe for generation of eSubscription validation report "eSubscribeValidationFail.csv"
	update adlog1
	set adlog1.[AutoUnSubscribe] = GETDATE()
	from [dbo].[tbAccountListTemp] source 
	inner join [dbo].[tbAuditLog] adlog1 
	on source.RMID = adlog1.RMID
	and source.AccountNumber = adlog1.AccountNumber
	and source.StatementType = adlog1.StatementType
	inner join 
	(select RMID, AccountNumber, StatementType, MAX(CreateDate) as LastestUpdateDate from [dbo].[tbAuditLog] 
	where oldRegStatus = 'U' and newRegStatus = 'S' 
	and cast(CreateDate as DATE) >= cast(dateadd(day, -2, GETDATE()) as DATE)
	group by RMID, AccountNumber, StatementType) as adlog2
	on adlog1.RMID = adlog2.RMID
	and adlog1.AccountNumber = adlog2.AccountNumber
	and adlog1.StatementType = adlog2.StatementType
	and adlog1.CreateDate = adlog2.LastestUpdateDate
	WHERE source.RegistrationStatus <> 'U'
	and source.Category not in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(source.RMID))
	)
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End update corresponding auto unsubscription record in tbAuditLog (for Normal accounts)', GETDATE(), 'importIBAccount', 'importIBAccount', GETDATE())

	update adlog1
	set adlog1.[AutoUnSubscribe] = GETDATE()
	from [dbo].[tbAccountListTemp] source 
	inner join [dbo].[tbBPInfo] bp 
	on source.RMID = bp.bpid
	inner join [dbo].[tbAuditLog] adlog1 
	on bp.rmid = adlog1.RMID
	and source.AccountNumber = adlog1.AccountNumber
	and source.StatementType = adlog1.StatementType
	inner join 
	(select RMID, AccountNumber, StatementType, MAX(CreateDate) as LastestUpdateDate from [dbo].[tbAuditLog] 
	where oldRegStatus = 'U' and newRegStatus = 'S' 
	and cast(CreateDate as DATE) >= cast(dateadd(day, -2, GETDATE()) as DATE)
	group by RMID, AccountNumber, StatementType) as adlog2
	on adlog1.RMID = adlog2.RMID
	and adlog1.AccountNumber = adlog2.AccountNumber
	and adlog1.StatementType = adlog2.StatementType
	and adlog1.CreateDate = adlog2.LastestUpdateDate
	WHERE source.RegistrationStatus <> 'U'
	and source.Category in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(bp.rmid))
	)
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End update corresponding auto unsubscription record in tbAuditLog (for SBS/AVQ accounts)', GETDATE(), 'importIBAccount', 'importIBAccount', GETDATE())

	-- latest AcctStatus is 'CLS' or 'CAP' is eligible for auto-Un-Subscription
	-- EMailAddress is null and length = 0, are also for auto-Un-Subscription ; 2015-09-18	
	UPDATE source
	SET source.[RegistrationStatus] = 'U'
	FROM [dbo].[tbAccountListTemp] source 
	WHERE source.RegistrationStatus <> 'U'
	and source.Category not in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(source.RMID))
	)
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End auto unsubscription (for Normal accounts)', GETDATE(), 'importIBAccount', 'importIBAccount', GETDATE())

	-- auto-Un-Subscription for SBS & AVQ
	UPDATE source
	SET source.[RegistrationStatus] = 'U'
	FROM [dbo].[tbAccountListTemp] source 
	inner join [dbo].[tbBPInfo] bp
	on source.RMID = bp.bpid
	WHERE source.RegistrationStatus <> 'U'
	and source.Category in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(bp.rmid))
	)
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End auto unsubscription (for SBS/AVQ accounts)', GETDATE(), 'importIBAccount', 'importIBAccount', GETDATE())

END

GO


