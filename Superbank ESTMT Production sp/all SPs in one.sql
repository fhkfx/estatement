USE [Estatement]
GO
/****** Object:  View [dbo].[vwPendingNotification]    Script Date: 8/16/2018 10:45:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[vwPendingNotification]
AS
SELECT DISTINCT 
                         TOP (100) PERCENT noti.StatementID, noti.RMID, noti.Category, noti.StatementType, noti.DisplayAccountNumber, 
						 noti.EMailAddress, (CASE WHEN stmtType.EngDesc IS NULL THEN '' ELSE stmtType.EngDesc END) AS EngDesc, 
						 (CASE WHEN stmtType.TradChiDesc IS NULL THEN '' ELSE stmtType.TradChiDesc END) AS TradChiDesc, 
						 stmtType.SimChiDesc, noti.statementFreq, stmt.Product_Type, 
						 prodTypeMap.Product_Type_Desc_Eng, prodTypeMap.Product_Type_Desc_TC, stmt.Advice_Title AS AdviceTitleEN, advTitleMap.AdviceTitleTC
FROM					(SELECT StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, statementFreq, Status 
                         FROM dbo.tbNotification) noti 
						 INNER JOIN dbo.tbStatement stmt 
						 ON noti.StatementID = stmt.ID 
						 LEFT JOIN dbo.tbStatementType stmtType 
						 ON noti.StatementType = stmtType.StatementType AND noti.statementFreq = stmtType.statementFreq
						 LEFT JOIN (SELECT Product_Type, Product_Type_Desc_Eng, Product_Type_Desc_TC FROM dbo.tbAdviceMapping WHERE MappingCategory = 'ProductType') prodTypeMap 
						 ON stmt.Product_Type = prodTypeMap.Product_Type 
						 LEFT JOIN (SELECT AdviceTitleEN, AdviceTitleTC FROM dbo.tbAdviceMapping WHERE MappingCategory = 'AdviceTitle') advTitleMap 
						 ON stmt.Advice_Title = advTitleMap.AdviceTitleEN 
WHERE					(noti.Status = 'P')
ORDER BY noti.StatementID









GO
/****** Object:  View [dbo].[vwReboundNotification]    Script Date: 8/16/2018 10:45:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[vwReboundNotification]
AS
SELECT DISTINCT 
                         TOP (100) PERCENT noti.StatementID, noti.RMID, noti.Category, noti.StatementType, noti.DisplayAccountNumber, noti.EMailAddress, 
                         stmtType.EngDesc, stmtType.TradChiDesc, stmtType.SimChiDesc, noti.statementFreq, noti.TriggerDateTime, noti.HS, 
                         noti.bounceBackStatus, noti.bounceBackCount, noti.MobileNo, noti.UNEReferenceNumber, dbo.tbCustomerEmail.PreferredLanguage, 
                         stmt.Product_Type, prodTypeMap.Product_Type_Desc_Eng, prodTypeMap.Product_Type_Desc_TC, stmt.Advice_Title AS AdviceTitleEN, advTitleMap.AdviceTitleTC
FROM					(SELECT StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, statementFreq, Status, TriggerDateTime, HS, 
                         bounceBackStatus, bounceBackCount, MobileNo, UNEReferenceNumber
                         FROM dbo.tbNotification) noti 
						 INNER JOIN dbo.tbStatement stmt 
						 ON noti.StatementID = stmt.ID 
						 LEFT JOIN dbo.tbStatementType stmtType 
						 ON noti.StatementType = stmtType.StatementType AND noti.statementFreq = stmtType.statementFreq 
						 LEFT JOIN (SELECT Product_Type, Product_Type_Desc_Eng, Product_Type_Desc_TC FROM dbo.tbAdviceMapping WHERE MappingCategory = 'ProductType') prodTypeMap 
						 ON stmt.Product_Type = prodTypeMap.Product_Type 
						 LEFT JOIN (SELECT AdviceTitleEN, AdviceTitleTC FROM dbo.tbAdviceMapping WHERE MappingCategory = 'AdviceTitle') advTitleMap 
						 ON stmt.Advice_Title = advTitleMap.AdviceTitleEN 
						 INNER JOIN dbo.tbCustomerEmail 
						 ON noti.RMID = dbo.tbCustomerEmail.rmid 

WHERE					(noti.Status = 'R') AND (noti.TriggerDateTime < GETDATE()) 
ORDER BY noti.StatementID








GO
/****** Object:  StoredProcedure [dbo].[GenerateNotificationList]    Script Date: 8/16/2018 10:45:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GenerateNotificationList]
AS
BEGIN
	--INSERT INTO tbNotification (StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, Status)
		--SELECT s.ID, a.RMID, a.Category, a.StatementType, a.DisplayAccountNumber, c.EMailAddress, 'P'
	INSERT INTO tbNotification (StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, MobileNo, Status, statementFreq, LastStatusUpdateTime)
		SELECT s.ID, a.RMID, a.Category, a.StatementType, a.DisplayAccountNumber, c.EMailAddress, c.MobileNumber,'P', s.statementFreq, getdate()
			FROM tbStatement s JOIN tbAccountList a ON s.AccountID = a.ID
				JOIN tbSystemSetting ss ON a.Category = ss.Category AND a.StatementType = ss.StatementType AND ss.SettingName = 'PredefinedPeriod'
				LEFT OUTER JOIN 
					(select a.RMID, max(a.EMailAddress) as Emailaddress, max(a.MobileNumber) as Mobilenumber
					from tbCustomerEmail as a
					inner join
					(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
					on a.rmid = b.rmid 
					and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt] 
					group by a.RMID
					) c ON a.RMID = c.RMID
			WHERE DATEADD(SECOND, CONVERT(int, ss.SettingValue), s.CreateDate) < GETDATE()
			AND NOT EXISTS (SELECT * FROM tbNotification n WHERE n.StatementID = s.ID)
			AND a.Category NOT IN ('SBS','AVQ') 

	INSERT INTO tbNotification (StatementID, RMID, Category, StatementType, DisplayAccountNumber, EMailAddress, MobileNo, Status, statementFreq, LastStatusUpdateTime, BPID, AcctType)
		SELECT DISTINCT s.ID, bp.RMID, a.Category, a.StatementType, 
		(CASE WHEN s.ContainerAcno IS NULL THEN '' ELSE (LEFT(s.ContainerAcno,3)+'-'+SUBSTRING(s.ContainerAcno,4,1)+'-'+SUBSTRING(s.ContainerAcno,5,6)+'-'++SUBSTRING(s.ContainerAcno,11,2)) END), 
		c.EMailAddress, c.MobileNumber,'P', s.statementFreq, getdate(), a.RMID, bp.AcctType
			FROM tbStatement s JOIN tbAccountList a ON s.AccountID = a.ID
				JOIN tbSystemSetting ss ON a.Category = ss.Category AND a.StatementType = ss.StatementType AND ss.SettingName = 'PredefinedPeriod'

				INNER JOIN 
					(Select bpid, rmid, AcctType From tbBPInfo where AcctType = 'individual') bp on a.rmid = bp.bpid

				LEFT OUTER JOIN 
					(select a.RMID, max(a.EMailAddress) as Emailaddress, max(a.MobileNumber) as Mobilenumber
					from tbCustomerEmail as a
					inner join
					(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
					on a.rmid = b.rmid 
					and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt] 
					group by a.RMID
					) c ON bp.RMID = c.RMID

			WHERE DATEADD(SECOND, CONVERT(int, ss.SettingValue), s.CreateDate) < GETDATE()
			AND NOT EXISTS (SELECT * FROM tbNotification n WHERE n.StatementID = s.ID)
			AND a.Category IN('SBS', 'AVQ') 

END





GO
/****** Object:  StoredProcedure [dbo].[spAutoUnsubscribe]    Script Date: 8/16/2018 10:45:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAutoUnsubscribe]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- update tbAuditLog column AutoUnSubscribe for generation of eSubscription validation report "eSubscribeValidationFail.csv"
	update adlog1
	set adlog1.[AutoUnSubscribe] = GETDATE()
	from [dbo].[tbAccountListTemp] source 
	inner join [dbo].[tbAuditLog] adlog1 
	on source.RMID = adlog1.RMID
	and source.AccountNumber = adlog1.AccountNumber
	and source.StatementType = adlog1.StatementType
	inner join 
	(select RMID, AccountNumber, StatementType, MAX(CreateDate) as LastestUpdateDate from [dbo].[tbAuditLog] 
	where oldRegStatus = 'U' and newRegStatus = 'S' 
	and cast(CreateDate as DATE) >= cast(dateadd(day, -2, GETDATE()) as DATE)
	group by RMID, AccountNumber, StatementType) as adlog2
	on adlog1.RMID = adlog2.RMID
	and adlog1.AccountNumber = adlog2.AccountNumber
	and adlog1.StatementType = adlog2.StatementType
	and adlog1.CreateDate = adlog2.LastestUpdateDate
	WHERE source.RegistrationStatus <> 'U'
	and source.Category not in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(source.RMID))
	)
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End update corresponding auto unsubscription record in tbAuditLog (for Normal accounts)', GETDATE(), 'importIBAccount', 'importIBAccount', GETDATE())

	update adlog1
	set adlog1.[AutoUnSubscribe] = GETDATE()
	from [dbo].[tbAccountListTemp] source 
	inner join [dbo].[tbBPInfo] bp 
	on source.RMID = bp.bpid
	inner join [dbo].[tbAuditLog] adlog1 
	on bp.rmid = adlog1.RMID
	and source.AccountNumber = adlog1.AccountNumber
	and source.StatementType = adlog1.StatementType
	inner join 
	(select RMID, AccountNumber, StatementType, MAX(CreateDate) as LastestUpdateDate from [dbo].[tbAuditLog] 
	where oldRegStatus = 'U' and newRegStatus = 'S' 
	and cast(CreateDate as DATE) >= cast(dateadd(day, -2, GETDATE()) as DATE)
	group by RMID, AccountNumber, StatementType) as adlog2
	on adlog1.RMID = adlog2.RMID
	and adlog1.AccountNumber = adlog2.AccountNumber
	and adlog1.StatementType = adlog2.StatementType
	and adlog1.CreateDate = adlog2.LastestUpdateDate
	WHERE source.RegistrationStatus <> 'U'
	and source.Category in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(bp.rmid))
	)
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End update corresponding auto unsubscription record in tbAuditLog (for SBS/AVQ accounts)', GETDATE(), 'importIBAccount', 'importIBAccount', GETDATE())

	-- latest AcctStatus is 'CLS' or 'CAP' is eligible for auto-Un-Subscription
	-- EMailAddress is null and length = 0, are also for auto-Un-Subscription ; 2015-09-18	
	UPDATE source
	SET source.[RegistrationStatus] = 'U'
	FROM [dbo].[tbAccountListTemp] source 
	WHERE source.RegistrationStatus <> 'U'
	and source.Category not in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(source.RMID))
	)
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End auto unsubscription (for Normal accounts)', GETDATE(), 'importIBAccount', 'importIBAccount', GETDATE())

	-- auto-Un-Subscription for SBS & AVQ
	UPDATE source
	SET source.[RegistrationStatus] = 'U'
	FROM [dbo].[tbAccountListTemp] source 
	inner join [dbo].[tbBPInfo] bp
	on source.RMID = bp.bpid
	WHERE source.RegistrationStatus <> 'U'
	and source.Category in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(bp.rmid))
	)
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End auto unsubscription (for SBS/AVQ accounts)', GETDATE(), 'importIBAccount', 'importIBAccount', GETDATE())

END

GO
/****** Object:  StoredProcedure [dbo].[spGenerateDataForEstatementTXT]    Script Date: 8/16/2018 10:45:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spGenerateDataForEstatementTXT]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Start insert records into tbRefEstatementTXT', GETDATE(), 'ESBatchRegistrationStatusExporter', 'ESBatchRegistrationStatusExporter', GETDATE())

insert into [dbo].[tbRefEstatementTXT] ([RMID],[RM_Number],[StatementTypeEngDesc],[AccountNumberOrBPname],[Category])
(
select distinct aclist.RMID,rmMap.RMNumber,aclist.StatementType,aclist.AccountNumber,aclist.Category 
from 
	[dbo].[tbAccountList] aclist
left join
	[dbo].[tbRmIDAccountNumber] rmMap
on 
	aclist.RMID = rmMap.RMID
where 
	aclist.RegistrationStatus = 'S' and aclist.Active = 'Y'
and 
	aclist.Category != 'SBS'
and 
	aclist.Category != 'AVQ'
)

insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Finish insert non superbank records', GETDATE(), 'ESBatchRegistrationStatusExporter', 'ESBatchRegistrationStatusExporter', GETDATE())

insert into [dbo].[tbRefEstatementTXT] ([RMID],[BPID],[StatementTypeEngDesc],[AccountNumberOrBPname],[Category])
(
select bpinfo.rmid,bpinfo.bpid,aclist.StatementType,
(
CASE 
WHEN (aclist.Category = 'SBS')
THEN bpinfo.BP_name
WHEN (aclist.Category = 'AVQ')
THEN aclist.AccountNumber
END
)
,aclist.Category
from 
	[dbo].[tbAccountList] aclist
inner join
	[dbo].[tbBPInfo] bpinfo
on 
	aclist.RMID = bpinfo.bpid
where 
	aclist.RegistrationStatus = 'S' and aclist.Active = 'Y'
and 
(
	aclist.Category = 'SBS'
or 
	aclist.Category = 'AVQ'
)
)

insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Finish insert superbank records', GETDATE(), 'ESBatchRegistrationStatusExporter', 'ESBatchRegistrationStatusExporter', GETDATE())

update estmtTXT
set 
	estmtTXT.RM_Number = rmMap.RMNumber
from 
	[dbo].[tbRefEstatementTXT] estmtTXT
inner join
	[dbo].[tbRmIDAccountNumber] rmMap
on 
	estmtTXT.RMID = rmMap.rmid
where 
	estmtTXT.StatementTypeEngDesc = 'SBSCON'
or
	estmtTXT.StatementTypeEngDesc = 'AVQ'

insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Finish update RMNumber', GETDATE(), 'ESBatchRegistrationStatusExporter', 'ESBatchRegistrationStatusExporter', GETDATE())

insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End insert records into tbRefEstatementTXT', GETDATE(), 'ESBatchRegistrationStatusExporter', 'ESBatchRegistrationStatusExporter', GETDATE())

END






GO
/****** Object:  StoredProcedure [dbo].[spGenerateDBDictionary]    Script Date: 8/16/2018 10:45:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[spGenerateDBDictionary] 
AS
BEGIN

select a.name [Table],b.name [Attribute],c.name [DataType],b.isnullable [Allow Nulls?],CASE WHEN 
d.name is null THEN 0 ELSE 1 END [PKey?],
CASE WHEN e.parent_object_id is null THEN 0 ELSE 1 END [FKey?],CASE WHEN e.parent_object_id 
is null THEN '-' ELSE g.name  END [Ref Table],
CASE WHEN h.value is null THEN '-' ELSE h.value END [Description]
from sysobjects as a
join syscolumns as b on a.id = b.id
join systypes as c on b.xtype = c.xtype 
left join (SELECT  so.id,sc.colid,sc.name 
      FROM    syscolumns sc
      JOIN sysobjects so ON so.id = sc.id
      JOIN sysindexkeys si ON so.id = si.id 
                    and sc.colid = si.colid
      WHERE si.indid = 1) d on a.id = d.id and b.colid = d.colid
left join sys.foreign_key_columns as e on a.id = e.parent_object_id and b.colid = e.parent_column_id    
left join sys.objects as g on e.referenced_object_id = g.object_id  
left join sys.extended_properties as h on a.id = h.major_id and b.colid = h.minor_id
where a.type = 'U' order by a.name

END




GO
/****** Object:  StoredProcedure [dbo].[spUpdateCombineAccountStatus]    Script Date: 8/16/2018 10:45:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdateCombineAccountStatus] 
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE target SET target.RegistrationStatus = 'S',
	target.ModifyBy = target.ModifyBy + ' - spUpdateCombineAccountStatus'
	FROM tbAccountList target
	WHERE target.Active = 'Y'
	AND target.Category = 'Combine'
	AND target.RegistrationStatus = 'U'
	AND EXISTS (
		select * from tbCombineAccountList c inner join tbCombineAccountList d
		on c.RMID = d.RMID and c.[Group] = d.[Group] and d.[Type] = 'S'
		inner join tbAccountList e
		on e.RMID = d.RMID and e.AccountNumber = d.AccountNumber and e.LastUpdate = (select MAX(LastUpdate) from tbAccountList where RMID = e.RMID and AccountNumber = e.AccountNumber)
		where c.RMID = target.RMID and c.AccountNumber = target.AccountNumber
		and e.RegistrationStatus = 'S'
	)
	and NOT EXISTS (
		select source.ID from tbAccountListTemp source
		where target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category
		AND target.statementType = source.statementType
	)
END




GO
/****** Object:  StoredProcedure [dbo].[spUpdatetbAccountList]    Script Date: 8/16/2018 10:45:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,> - 201807024 superbank production ver 1.2
-- =============================================
CREATE PROCEDURE [dbo].[spUpdatetbAccountList]
AS
BEGIN
	SET NOCOUNT ON;
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Start [dbo].[spUpdatetbAccountList] ver 1.2', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- Insert FixDeposit Account into tbAccountListTemp
	insert into tbAccountListTemp ([RMID],[AccountNumber],[DisplayAccountNumber],[Category],[StatementType],[RegistrationStatus],[Active],[CreateDate],[CreateBy],[ModifyBy],[LastUpdate])
	select distinct TmpAclist.[RMID],'','','FixDeposit','FD','U','Y',GETDATE(),'ESBatchAccountImporter','ESBatchAccountImporter',GETDATE() 
	from tbAccountListTemp TmpAclist where Category in
	(select distinct CategoryToMap from tbDirectBankMapping where Category = 'FixDeposit')

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Insert FixDeposit Account into tbAccountListTemp', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	-- DollarSmart Ploan and tax loan ineligible for eStmt subscription
	DELETE FROM [dbo].[tbAccountListTemp]
	WHERE Category = 'CreditCard'
	and (StatementType = 'DOLLARSMARTPLOAN' or StatementType = 'DOLLARSMARTTAXLOAN')
	
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End delete CreditCard DOLLARSMARTPLOAN, DOLLARSMARTTAXLOAN from tbAccountListTemp', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- update source registration status from target
	UPDATE 
		source
	SET 
		source.RegistrationStatus = target.RegistrationStatus
	FROM 
		[dbo].[tbAccountList] AS target
		INNER JOIN [dbo].[tbAccountListTemp] AS source
	ON 
		(target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category
		AND target.StatementType = source.StatementType)

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End update registration status from tbAccountList to tbAccountListTemp', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
		
	-- Insert Record with RegistrationStatusBefore to tbAccountNoticeList (New Case)
	INSERT INTO [dbo].[tbAccountNoticeList]
	([AccountID], [RMID], [AccountNumber], [DisplayAccountNumber], [Category],	[StatementType], [RegistrationStatusBefore], [CreateDate], [CreateBy], [ModifyBy], [LastUpdate], [TriggerStatus]) 
		SELECT tempAccountList.ID, tempAccountList.RMID, tempAccountList.AccountNumber, tempAccountList.DisplayAccountNumber, tempAccountList.Category,
		tempAccountList.StatementType, tempAccountList.RegistrationStatus, GETDATE(), tempAccountList.CreateBy,
		tempAccountList.ModifyBy, GETDATE(), 'P'
			FROM [dbo].[tbAccountListTemp] tempAccountList 
			WHERE tempAccountList.Category not in ('CiticFirst', 'InfoCast', 'SBS', 'AVQ')
			and EXISTS (
				select *
				from tbCustomerEmail as a
				inner join
				(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
				on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
				where a.AcctStatus = 'ACT' and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(tempAccountList.RMID)) and a.EMailAddress is not null
			) 	
			and NOT EXISTS (
				select id from tbAccountList c 
				where LTRIM(RTRIM(c.rmid)) = LTRIM(RTRIM(tempAccountList.RMID))
				and LTRIM(RTRIM(c.AccountNumber)) = LTRIM(RTRIM(tempAccountList.AccountNumber))
				and c.Category = tempAccountList.Category
				and c.StatementType = tempAccountList.StatementType
			)
			
	INSERT INTO [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	VALUES ('Info', '(sp) End Insert Record with RegistrationStatusBefore to tbAccountNoticeList (New Case)', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	-- CF ineligible for auto subscription
	-- non-CF and latest AcctStatus is 'ACT' is eligible for autosubscription
	UPDATE source 
	SET source.[RegistrationStatus] = 'S'
	FROM [dbo].[tbAccountListTemp] source
	-- autosubscription ignore SBS & AVQ
	WHERE source.Category not in ('CiticFirst', 'InfoCast', 'FixDeposit', 'SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where a.AcctStatus = 'ACT' and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(source.RMID)) and a.EMailAddress is not null
	) 	
	and NOT EXISTS (
		select id from tbAccountList c 
		where LTRIM(RTRIM(c.rmid)) = LTRIM(RTRIM(source.RMID))
		and LTRIM(RTRIM(c.AccountNumber)) = LTRIM(RTRIM(source.AccountNumber))
		and c.Category = source.Category
		and c.StatementType = source.StatementType
	)

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End auto subscription (SBS/AVQ accounts excepted)', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- latest AcctStatus is 'CLS' or 'CAP' is eligible for auto-Un-Subscription
	-- EMailAddress is null and length = 0, are also for auto-Un-Subscription ; 2015-09-18	
	UPDATE source
	SET source.[RegistrationStatus] = 'U'
	FROM [dbo].[tbAccountListTemp] source 
	WHERE source.RegistrationStatus <> 'U'
	and source.Category not in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(source.RMID))
	)
	
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End auto unsubscription (for Normal accounts)', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- auto-Un-Subscription for SBS & AVQ
	UPDATE source
	SET source.[RegistrationStatus] = 'U'
	FROM [dbo].[tbAccountListTemp] source 
	inner join [dbo].[tbBPInfo] bp
	on source.RMID = bp.bpid
	WHERE source.RegistrationStatus <> 'U'
	and source.Category in ('SBS', 'AVQ')
	and EXISTS (
		select *
		from tbCustomerEmail as a
		inner join
		(select rmid, MAX(AcctStatusLastUpdDt) as [AcctStatusLastUpdDt] from tbCustomerEmail group by rmid) as b
		on a.rmid = b.rmid and a.[AcctStatusLastUpdDt] = b.[AcctStatusLastUpdDt]
		where (a.AcctStatus = 'CLS' or a.AcctStatus = 'CAP' or a.EMailAddress is null or LEN(a.EMailAddress) = 0) and LTRIM(RTRIM(a.rmid)) = LTRIM(RTRIM(bp.rmid))
	)
	
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End auto unsubscription (for SBS/AVQ accounts)', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- Update Record with RegistrationStatusAfter to tbAccountNoticeList
	UPDATE 
		source
	SET 
		source.RegistrationStatusAfter = target.RegistrationStatus
	FROM 
		[dbo].[tbAccountNoticeList] AS source
		INNER JOIN [dbo].[tbAccountListTemp] AS target
	ON 
		(target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category
		AND target.StatementType = source.StatementType) 
	
	INSERT INTO [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	VALUES ('Info', '(sp) End Update Record with RegistrationStatusAfter to tbAccountNoticeList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	-- Merge tbAccountList and tbAccountListTemp
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Start Merge tbAccountListTemp to tbAccountList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	MERGE [dbo].[tbAccountList] AS target
	USING [dbo].[tbAccountListTemp] AS source
	ON 
		(target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category
		AND target.statementType = source.statementType)
	WHEN NOT MATCHED BY target THEN

		INSERT([RMID],[AccountNumber],[DisplayAccountNumber],[Category],[StatementType],[RegistrationStatus],[Active],[CreateDate],[CreateBy],[ModifyBy],[LastUpdate])
		VALUES(source.[RMID],source.[AccountNumber],source.[DisplayAccountNumber],source.[Category],source.[StatementType],source.[RegistrationStatus],source.[Active],source.[CreateDate],source.[CreateBy],source.[ModifyBy],source.[LastUpdate])

	WHEN NOT MATCHED BY source THEN
		UPDATE 	SET target.[Active] = 'N'
	;

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Merge tbAccountListTemp to tbAccountList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- Update Active = "Y" and RegistrationStatus from tbAccountListTemp
	UPDATE 
		target
	SET 
		target.Active = 'Y',
		target.LastUpdate = GETDATE(),
		target.ModifyBy = 'ESBatchAccountImporter',
		target.RegistrationStatus = source.RegistrationStatus
	FROM 
		[dbo].[tbAccountList] AS target
		INNER JOIN [dbo].[tbAccountListTemp] AS source
	ON 
		(target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category
		AND target.StatementType = source.StatementType
		AND UPPER(target.Category) != 'INFOCAST'
		)

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Update Active to Y', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	UPDATE 
		target
	SET 
		target.Active = 'N',
		target.LastUpdate = GETDATE(),
		target.ModifyBy = 'ESBatchAccountImporter'
	FROM 
		[dbo].[tbAccountList] AS target
		INNER JOIN [dbo].[tbAccountListTemp] AS source
	ON 
		(target.rmid = source.rmid 
		AND target.AccountNumber = source.AccountNumber
		AND (target.Category != source.Category OR target.StatementType != source.StatementType)
		AND target.Category in ('individual', 'combine', 'CiticFirst') 
		AND source.Category in ('individual', 'combine', 'CiticFirst')
		)

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Update Active to N', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- Update tbAccountUpdateList
	TRUNCATE TABLE [dbo].[tbAccountUpdateList];
	insert into [dbo].[tbAccountUpdateList] ([AccountID],[RMID],[AccountNumber],[CreateDate])
	SELECT [ID],[RMID],[AccountNumber],[CreateDate]
	FROM [dbo].[tbAccountList]
	WHERE CAST([dbo].[tbAccountList].CreateDate as DATE) = CAST(GETDATE() as DATE);

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Update tbAccountUpdateList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	-- Merge tbSubAccountListTemp AccountID
	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) Start Merge tbSubAccountListTemp to tbSubAccountList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
	
	update [dbo].[tbSubAccountListTemp]
	set [dbo].[tbSubAccountListTemp].[AccountID] = [dbo].[tbAccountList].[ID]
	from
		[dbo].[tbSubAccountListTemp]
	inner join
		[dbo].[tbAccountList]
	on
		[dbo].[tbAccountList].[RMID] = [dbo].[tbSubAccountListTemp].[bpid]
	and
		[dbo].[tbAccountList].[Category] = [dbo].[tbSubAccountListTemp].[Category]
	and
		[dbo].[tbAccountList].[AccountNumber] = 
		CASE
		WHEN ([dbo].[tbSubAccountListTemp].[Category] = 'SBS')
		THEN [dbo].[tbSubAccountListTemp].[bpid]
		WHEN ([dbo].[tbSubAccountListTemp].[Category] = 'AVQ')
		THEN [dbo].[tbSubAccountListTemp].[AccountNumber]
		END

	delete from [dbo].[tbSubAccountListTemp] where AccountID is null

	-- Merge tbSubAccountList and tbSubAccountListTemp
	MERGE [dbo].[tbSubAccountList] AS target
	USING [dbo].[tbSubAccountListTemp] AS source
	ON 
		(target.bpid = source.bpid
		AND target.AccountNumber = source.AccountNumber
		AND target.Category = source.Category)
	WHEN NOT MATCHED BY target THEN
		INSERT ([AccountID],[bpid],[AccountNumber],[DisplayAccountNumber],[Category],[Active],[ActivateDate],[CreateDate],[CreateBy],[ModifyDate],[ModifyBy])
		VALUES(
		source.[AccountID], source.[bpid],source.[AccountNumber],source.[DisplayAccountNumber],source.[Category],source.[Active],source.[ActivateDate],source.[CreateDate],source.[CreateBy],source.[ModifyDate],source.[ModifyBy])
	WHEN NOT MATCHED BY source THEN
		UPDATE SET target.[Active] = 'N'
	WHEN MATCHED THEN
		UPDATE SET target.[Active] = 'Y'
	;

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Merge tbSubAccountListTemp to tbSubAccountList', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	UPDATE
		aclist
	SET aclist.Active = 'Y',
		aclist.LastUpdate = GETDATE(),
		aclist.ModifyBy = 'ESBatchAccountImporter'
	FROM
		[dbo].[tbAccountList] AS aclist
	WHERE aclist.Category in ('FixDeposit')

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End Update Active to Y (DirectBank)', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())

	insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
	values ('Info', '(sp) End [dbo].[spUpdatetbAccountList]', GETDATE(), 'ESBatchAccountImporter', 'ESBatchAccountImporter', GETDATE())
END





GO
/****** Object:  StoredProcedure [dbo].[spUpdatetbStatementAndtbStatementFullList]    Script Date: 8/16/2018 10:45:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spUpdatetbStatementAndtbStatementFullList]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- Superbank statement records
	if 
		(select 
			count(*) 
		from 
			[dbo].[tbStatementTemp] stmtTemp
		where 
			stmtTemp.Advice_Title is null
		and 
			stmtTemp.containerAcno is null
		and
			stmtTemp.bpid is not null)
		> 0
	begin
		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert superbank statement records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		update [dbo].[tbStatementTemp]
		set [dbo].[tbStatementTemp].[AccountID] = [dbo].[tbAccountList].[ID]
		from
			[dbo].[tbStatementTemp]
		inner join
			[dbo].[tbAccountList] 
		on
			[dbo].[tbStatementTemp].bpid = [dbo].[tbAccountList].RMID
			and [dbo].[tbAccountList].Category = 'SBS'

		insert into [dbo].[tbStatement] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert superbank statement records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert superbank statement records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbStatementFullList] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert superbank statement records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())
	end
	-- Container statement records
	else if 
		(select 
			count(*) 
		from 
			[dbo].[tbStatementTemp] stmtTemp
		where 
			stmtTemp.Advice_Title is null
		and 
			stmtTemp.bpid is null
		and
			stmtTemp.containerAcno is not null)
		> 0
	begin
		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert container statement records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		update [dbo].[tbStatementTemp]
		set [dbo].[tbStatementTemp].[AccountID] = [dbo].[tbAccountList].[ID]
		from
			[dbo].[tbStatementTemp]
		inner join
			[dbo].[tbAccountList]
		on
			([dbo].[tbStatementTemp].containerAcno = [dbo].[tbAccountList].AccountNumber
			and [dbo].[tbAccountList].Category = 'AVQ')

		insert into [dbo].[tbStatement] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert container statement records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert container statement records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbStatementFullList] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert container statement records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())
	end
	-- Superbank/Container daily/advice records
	else if 
		(select 
			count(*) 
		from 
			[dbo].[tbStatementTemp] stmtTemp
		where 
			stmtTemp.bpid is null
		and 
			stmtTemp.Advice_Title is not null
		and
			stmtTemp.containerAcno is not null)
		> 0
	begin
		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert daily/advice records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		update [dbo].[tbStatementTemp]
		set [dbo].[tbStatementTemp].[AccountID] = [dbo].[tbSubAccountList].[AccountID]
		from
			[dbo].[tbStatementTemp]
		inner join
			[dbo].[tbSubAccountList] 
		on
			([dbo].[tbStatementTemp].containerAcno = [dbo].[tbSubAccountList].AccountNumber
			and [dbo].[tbSubAccountList].Active = 'Y')
		where
			[tbSubAccountList].AccountID in 
			(
				SELECT aclist_id.[ID]
				FROM [dbo].[tbAccountList] aclist_id
			)

		insert into [dbo].[tbStatement] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title],[ContainerAcno])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title],stmtTemp.[containerAcno]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert daily/advice records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert daily/advice records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbStatementFullList] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef],[Meta_Type],[Order_Type],[Asset_Grp],[Asset_SubGrp],
			[Asset_Name_EN],[Asset_Name_SC],[Asset_Name_TC],[Product_Type],[Product_Name],[Advice_Title],[ContainerAcno])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef],stmtTemp.[Meta_Type],stmtTemp.[Order_Type],stmtTemp.[Asset_Grp],stmtTemp.[Asset_SubGrp],
					stmtTemp.[Asset_Name_EN],stmtTemp.[Asset_Name_SC],stmtTemp.[Asset_Name_TC],stmtTemp.[Product_Type],stmtTemp.[Product_Name],stmtTemp.[Advice_Title],stmtTemp.[containerAcno]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert daily/advice records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())
	end
	-- FixDeposit records
	else if 
		(select 
			count(*) 
		from 
			[dbo].[tbStatementTemp] stmtTemp
		where 
			stmtTemp.RMNumber is not null)
		> 0
	begin
		update stmtTemp
		set stmtTemp.[RMID] = LTRIM(RTRIM(rmMap.[RMID]))
		from
			[dbo].[tbStatementTemp] stmtTemp
		inner join
			[dbo].[tbRmIDAccountNumber] rmMap
		on
			stmtTemp.RMNumber = rmMap.RMNumber

		update stmtTemp
		set stmtTemp.[AccountID] = aclist.[ID]
		from
			[dbo].[tbStatementTemp] stmtTemp
		inner join
			[dbo].[tbAccountList] aclist
		on
			stmtTemp.RMID = LTRIM(RTRIM(aclist.RMID))
		and
			stmtTemp.Category = aclist.Category
		and
			stmtTemp.StatementType = aclist.StatementType
		where 
			stmtTemp.ACC_NO is null

		update stmtTemp
		set stmtTemp.[AccountID] = aclist.[ID]
		from
			[dbo].[tbStatementTemp] stmtTemp
		inner join
			[dbo].[tbAccountList] aclist
		on
			stmtTemp.RMID = LTRIM(RTRIM(aclist.RMID))
		and
			stmtTemp.ACC_NO = aclist.AccountNumber
		and
			stmtTemp.Category = aclist.Category
		and
			stmtTemp.StatementType = aclist.StatementType
		where 
			stmtTemp.ACC_NO is not null

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert directbank records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbStatement] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert directbank records from tbStatementTemp to tbStatement', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) Start insert directbank records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

		insert into [dbo].[tbStatementFullList] ([AccountID],[StatementDate],[ViewTime],[StatementPath],[HeaderPath],[TermsAndConditionPath],[HeaderPositionX],[HeaderPositionY],
			[CreateDate],[CreateBy],[ModifyBy],[LastUpdate],[statementFreq],[productInfo],[productRef])
			(
				select 
					stmtTemp.[AccountID],stmtTemp.[StatementDate],stmtTemp.[ViewTime],stmtTemp.[StatementPath],stmtTemp.[HeaderPath],stmtTemp.[TermsAndConditionPath],
					stmtTemp.[HeaderPositionX],stmtTemp.[HeaderPositionY],stmtTemp.[CreateDate],stmtTemp.[CreateBy],stmtTemp.[ModifyBy],stmtTemp.[LastUpdate],
					stmtTemp.[statementFreq],stmtTemp.[productInfo],stmtTemp.[productRef]
				from 
					[dbo].[tbStatementTemp] stmtTemp,[dbo].[tbAccountList] aclist
				where 
					stmtTemp.[AccountID] = aclist.[ID]
				and
					stmtTemp.[AccountID] is not null 
			)

		insert into [dbo].[tbLog] ([Type] ,[Content], [CreateDate], [CreateBy],  [ModifyBy], [LastUpdate])
		values ('Info', '(sp) End insert directbank records from tbStatementTemp to tbStatementFullList', GETDATE(), 'ESBatchStatementImporter', 'ESBatchStatementImporter', GETDATE())

	end

	END




GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "result"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbStatementType"
            Begin Extent = 
               Top = 7
               Left = 321
               Bottom = 148
               Right = 505
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbStatement"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbAdviceMapping"
            Begin Extent = 
               Top = 6
               Left = 543
               Bottom = 135
               Right = 760
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwPendingNotification'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwPendingNotification'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "result"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbStatementType"
            Begin Extent = 
               Top = 7
               Left = 321
               Bottom = 148
               Right = 505
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbCustomerEmail"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 267
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbStatement"
            Begin Extent = 
               Top = 270
               Left = 38
               Bottom = 399
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tbAdviceMapping"
            Begin Extent = 
               Top = 6
               Left = 543
               Bottom = 135
               Right = 760
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or =' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwReboundNotification'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwReboundNotification'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwReboundNotification'
GO
