USE [Estatement]
GO
/****** Object:  Table [dbo].[tbStatementTemp]    Script Date: 7/4/2018 3:52:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbStatementTemp](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NULL,
	[StatementDate] [datetime] NOT NULL,
	[ViewTime] [datetime] NULL,
	[StatementPath] [nvarchar](500) NOT NULL,
	[HeaderPath] [nvarchar](500) NOT NULL,
	[TermsAndConditionPath] [nvarchar](500) NOT NULL,
	[HeaderPositionX] [int] NOT NULL,
	[HeaderPositionY] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [timestamp] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[statementFreq] [nvarchar](1) NULL,
	[productInfo] [nvarchar](50) NULL,
	[productRef] [nvarchar](50) NULL,
	[Meta_Type] [nvarchar](100) NULL,
	[Order_Type] [nvarchar](100) NULL,
	[Asset_Grp] [nvarchar](100) NULL,
	[Asset_SubGrp] [nvarchar](100) NULL,
	[Asset_Name_EN] [nvarchar](200) NULL,
	[Asset_Name_SC] [nvarchar](200) NULL,
	[Asset_Name_TC] [nvarchar](200) NULL,
	[Product_Type] [nvarchar](100) NULL,
	[Product_Name] [nvarchar](300) NULL,
	[Advice_Title] [nvarchar](500) NULL,
	[bpid] [nvarchar](100) NULL,
	[containerAcno] [nvarchar](100) NULL,
	[RMNumber] [nvarchar](100) NULL,
	[ACC_NO] [nvarchar](100) NULL,
	[Category] [nvarchar](50) NULL,
	[StatementType] [nvarchar](50) NULL,
	[RMID] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbStatementTemp] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
