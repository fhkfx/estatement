USE [Estatement]
GO
/****** Object:  Table [dbo].[tbBPInfo]    Script Date: 7/4/2018 4:02:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbBPInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[bpid] [nvarchar](20) NOT NULL,
	[rm_no] [nvarchar](100) NULL,
	[BP_name] [nvarchar](100) NULL,
	[AcctType] [nvarchar](10) NOT NULL,
	[no_of_acc_holder] [int] NOT NULL,
	[rmid] [nvarchar](100) NOT NULL,
	[AVQ] [nvarchar](1) NOT NULL,
 CONSTRAINT [PK_tbBPInfo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
