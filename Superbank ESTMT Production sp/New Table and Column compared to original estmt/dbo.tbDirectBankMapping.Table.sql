USE [Estatement]
GO
/****** Object:  Table [dbo].[tbDirectBankMapping]    Script Date: 7/4/2018 4:02:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbDirectBankMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](100) NULL,
	[StatementType] [nvarchar](100) NULL,
	[CategoryToMap] [nvarchar](500) NOT NULL,
	[CheckAcno] [nvarchar](1) NOT NULL,
	[CheckCategory] [nvarchar](1) NOT NULL,
 CONSTRAINT [PK_tbDirectBankMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[tbDirectBankMapping]  WITH CHECK ADD  CONSTRAINT [CK_CheckAcno] CHECK  (([CheckAcno]='N' OR [CheckAcno]='Y'))
GO
ALTER TABLE [dbo].[tbDirectBankMapping] CHECK CONSTRAINT [CK_CheckAcno]
GO
ALTER TABLE [dbo].[tbDirectBankMapping]  WITH CHECK ADD  CONSTRAINT [CK_CheckCategory] CHECK  (([CheckCategory]='N' OR [CheckCategory]='Y'))
GO
ALTER TABLE [dbo].[tbDirectBankMapping] CHECK CONSTRAINT [CK_CheckCategory]
GO
