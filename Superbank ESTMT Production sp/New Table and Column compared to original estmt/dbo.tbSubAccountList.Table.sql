USE [Estatement]
GO
/****** Object:  Table [dbo].[tbSubAccountList]    Script Date: 7/4/2018 3:42:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSubAccountList](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[bpid] [nvarchar](20) NOT NULL,
	[AccountNumber] [nvarchar](100) NULL,
	[DisplayAccountNumber] [nvarchar](100) NULL,
	[Category] [nvarchar](50) NOT NULL,
	[Active] [nvarchar](1) NOT NULL,
	[ActivateDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](100) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyBy] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_tbSubAccountList] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[tbSubAccountList]  WITH NOCHECK ADD  CONSTRAINT [FK_tbSubAccountList_tbAccountList] FOREIGN KEY([AccountID])
REFERENCES [dbo].[tbAccountList] ([ID])
GO
ALTER TABLE [dbo].[tbSubAccountList] CHECK CONSTRAINT [FK_tbSubAccountList_tbAccountList]
GO
