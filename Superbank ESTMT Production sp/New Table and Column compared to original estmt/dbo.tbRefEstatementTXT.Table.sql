USE [Estatement]
GO
/****** Object:  Table [dbo].[tbRefEstatementTXT]    Script Date: 7/4/2018 4:02:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbRefEstatementTXT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RMID] [varchar](100) NULL,
	[BPID] [varchar](20) NULL,
	[RM_Number] [varchar](100) NULL,
	[StatementTypeEngDesc] [nvarchar](100) NOT NULL,
	[AccountNumberOrBPname] [nvarchar](100) NOT NULL,
	[Category] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tbRefEstatementTXT] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
