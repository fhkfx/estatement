USE [Estatement]
GO
/****** Object:  Table [dbo].[tbAdviceMapping]    Script Date: 7/4/2018 3:42:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbAdviceMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RuleNumber] [int] NOT NULL,
	[MappingCategory] [nvarchar](100) NOT NULL,
	[Meta_Type] [nvarchar](100) NOT NULL,
	[Order_Type] [nvarchar](100) NOT NULL,
	[Asset_Grp] [nvarchar](100) NOT NULL,
	[Asset_SubGrp] [nvarchar](100) NOT NULL,
	[Product_Type] [nvarchar](100) NULL,
	[Product_Type_Desc_Eng] [nvarchar](500) NULL,
	[Product_Type_Desc_TC] [nvarchar](500) NULL,
	[Product_Name] [nvarchar](500) NULL,
	[AdviceTitleEN] [nvarchar](500) NULL,
	[AdviceTitleTC] [nvarchar](500) NULL,
	[AdviceTitleSC] [nvarchar](500) NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tbAdviceMapping] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
