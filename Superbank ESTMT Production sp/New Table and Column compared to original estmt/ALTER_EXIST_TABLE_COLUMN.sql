USE [Estatement]
GO

--1
ALTER TABLE tbStatement
ADD [Meta_Type] [nvarchar](100) NULL,
	[Order_Type] [nvarchar](100) NULL,
	[Asset_Grp] [nvarchar](100) NULL,
	[Asset_SubGrp] [nvarchar](100) NULL,
	[Asset_Name_EN] [nvarchar](200) NULL,
	[Asset_Name_SC] [nvarchar](200) NULL,
	[Asset_Name_TC] [nvarchar](200) NULL,
	[Product_Type] [nvarchar](100) NULL,
	[Product_Name] [nvarchar](300) NULL,
	[Advice_Title] [nvarchar](500) NULL,
	[ContainerAcno] [nvarchar](100) NULL;


ALTER TABLE tbStatementFullList
ADD [Meta_Type] [nvarchar](100) NULL,
	[Order_Type] [nvarchar](100) NULL,
	[Asset_Grp] [nvarchar](100) NULL,
	[Asset_SubGrp] [nvarchar](100) NULL,
	[Asset_Name_EN] [nvarchar](200) NULL,
	[Asset_Name_SC] [nvarchar](200) NULL,
	[Asset_Name_TC] [nvarchar](200) NULL,
	[Product_Type] [nvarchar](100) NULL,
	[Product_Name] [nvarchar](300) NULL,
	[Advice_Title] [nvarchar](500) NULL,
	[ContainerAcno] [nvarchar](100) NULL;

--27
ALTER TABLE [tbStatement]
ALTER COLUMN [productInfo] [nvarchar](50) NULL

ALTER TABLE [tbStatementFullList]
ALTER COLUMN [productInfo] [nvarchar](50) NULL

ALTER TABLE [tbStatement]
ALTER COLUMN [productRef] [nvarchar](50) NULL

ALTER TABLE [tbStatementFullList]
ALTER COLUMN [productRef] [nvarchar](50) NULL


--34
ALTER TABLE [dbo].[tbCustomerEmail] ADD [LastUpdateTime] [datetime] NULL;

--35
ALTER TABLE [dbo].[tbNotification] ADD [Ctrl1] [varchar](100) NULL;
ALTER TABLE [dbo].[tbNotification] ADD [Ctrl2] [varchar](100) NULL;
ALTER TABLE [dbo].[tbNotification] ADD [BPID] [nvarchar](100) NULL;
ALTER TABLE [dbo].[tbNotification] ADD [AcctType] [nvarchar](10) NULL;


--45
alter table dbo.tbAuditLog
add StatementType [nvarchar](50) NOT NULL default '',
	AutoUnSubscribe [datetime] NULL
