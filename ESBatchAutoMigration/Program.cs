﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Data;

namespace ESBatchAutoMigration
{
    public enum logType { Info, Error};
    class Program
    {
        static EStatementEntities entity = new EStatementEntities();
        static void Main(string[] args)
        {
            Util.Log(logType.Info, "=========================================================");
            Util.Log(logType.Info, String.Format("ESBatchAutoMigration start --- {0}", DateTime.Now));

            try
            {
                if (!File.Exists(Properties.Settings.Default.EstmtOptinFilePath))
                {
                    Util.Log(logType.Info, String.Format("EstmtOptinFilePath File not exist : {0}", Properties.Settings.Default.EstmtOptinFilePath));
                }
                else
                {
                    Util.Log(logType.Info, String.Format("FILE {0} FOUND", Properties.Settings.Default.EstmtOptinFilePath));

                    int recordCount = 0;
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.EstmtOptinFilePath))
                    {
                        String eachLine = String.Empty;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            recordCount++;
                            if (eachLine.Length < 14)
                            {
                                Util.Log(logType.Info, String.Format("Error in reading {0}, Line {1} incorrect :\n{2}", Path.GetFileName(Properties.Settings.Default.EstmtOptinFilePath), recordCount, eachLine));
                                System.Threading.Thread.Sleep(5000);
                                return;
                            }

                            String rmid = eachLine.Substring(0, 14).Trim();

                            List<tbBPInfo> bpList = entity.tbBPInfoes.Where(a => a.rmid == rmid && a.AcctType != Properties.Settings.Default.bpAccountTypeJOR && a.AcctType != Properties.Settings.Default.bpAccountTypeJAN && a.AcctType != "").ToList();
                            List<tbBPInfo> jointBpList = entity.tbBPInfoes.Where(a => a.rmid == rmid && (a.AcctType == Properties.Settings.Default.bpAccountTypeJOR || a.AcctType == Properties.Settings.Default.bpAccountTypeJAN || a.AcctType == "")).ToList();
                            foreach (var bpAccount in bpList)
                            {
                                tbAccountList account = entity.tbAccountLists.Where(a => a.RMID == bpAccount.bpid && a.Category == Properties.Settings.Default.CategorySuperBank).FirstOrDefault();
                                account.RegistrationStatus = "S";
                            }

                            // batch update
                            if (recordCount % 20000 == 0)
                            {
                                entity.SaveChanges();
                            }

                            foreach (var bpAccount in jointBpList)
                                Util.Log(logType.Error, String.Format("RegistrationStatus of account with RMID = {0}, BPID = {1}, Account Type = {2}, remain unchanged", bpAccount.rmid, bpAccount.bpid, bpAccount.AcctType));
                        }
                        tr.Close();
                        entity.SaveChanges();
                        Util.Log(logType.Info, String.Format("Total No. of records in {0} = {1}", Path.GetFileName(Properties.Settings.Default.EstmtOptinFilePath), recordCount));
                    }
                }
            }
            catch (Exception ex)
            {
                Util.Log(logType.Info, String.Format("Error in main : {0}", ex.Message));
                System.Threading.Thread.Sleep(5000);
            }

            Util.Log(logType.Info, String.Format("ESBatchAutoMigration finish --- {0}", DateTime.Now));
            System.Threading.Thread.Sleep(5000);
        }
    }
}
