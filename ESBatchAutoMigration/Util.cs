﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESBatchAutoMigration
{
    class Util
    {
        public static void Log(logType type, String content)
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(Properties.Settings.Default.EstmtOptinLogFilePath, true, Encoding.UTF8))
            {
                try
                {
                    sw.WriteLine(content);
                    sw.Close();

                    if (type != logType.Error)
                    {
                        Console.WriteLine(content);
                    }
                }
                catch (Exception ex)
                {
                    sw.WriteLine(String.Format("Error in writeLog : {0}"), ex.Message);
                    System.Threading.Thread.Sleep(5000);
                }
            }
        }
    }
}
