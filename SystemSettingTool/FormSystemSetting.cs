﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SystemSettingTool
{
    public partial class FormSystemSetting : Form
    {
        #region variable
        private Boolean bankLogo_isUpdate = false;
        private Boolean termsAndCondition_isUpdate = false;
        private Boolean marketInsert_isUpdate = false;
        private Boolean bankInfoInsert_isUpdate = false;
        private int bankLogo_recordID = 0;
        private int termsAndCondition_recordID = 0;
        private int marketInsert_recordID = 0;
        private int bankInfoInsert_recordID = 0;
        #endregion

        #region Initialization
        public FormSystemSetting()
        {
            InitializeComponent();
        }

        private void FormSystemSetting_Load(object sender, EventArgs e)
        {
            refreshGridView("bankLogo");
            refreshGridView("termsAndCondition");
            refreshGridView("marketInsert");
            refreshGridView("bankInfoInsert");
            EnableControls("bankLogo", false);
            EnableControls("termsAndCondition", false);
            EnableControls("marketInsert", false);
            EnableControls("bankInfoInsert", false);
        }
        #endregion

        #region Control Handler

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #region Bank Logo

        private void bankLogo_btn_pathSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bankLogo_tb_path.Text = openFileDialog.InitialDirectory + openFileDialog.FileName;
            }
        }

        private void bankLogo_bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            EnableControls("bankLogo", true);
        }

        private void bankLogo_bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to delete this record?", "Warning", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    int ID = ((SystemSettingTool.tbSystemSetting)(bankLogoBindingSource.Current)).ID;
                    EStatementEntities entities = new EStatementEntities();
                    tbSystemSetting SystemSetting = entities.tbSystemSetting.FirstOrDefault(x => x.ID == ID);

                    SystemSetting.Active = "N";
                    entities.SaveChanges();
                    refreshGridView("bankLogo");
                }
                catch (Exception ex)
                {
                    Util.Log(string.Format("Error in deleting bank logo. Error message:{0}", ex.Message));
                }
            }
        }

        private void bankLogo_bindingNavigatorEditItem_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = ((SystemSettingTool.tbSystemSetting)(bankLogoBindingSource.Current)).ID;
                EStatementEntities entities = new EStatementEntities();
                tbSystemSetting SystemSetting = entities.tbSystemSetting.FirstOrDefault(x => x.ID == ID);

                bankLogo_isUpdate = true;
                bankLogo_recordID = SystemSetting.ID;
                bankLogo_tb_path.Text = SystemSetting.SettingValue;
                bankLogo_tb_statementType.Text = SystemSetting.StatementType;
                bankLogo_dtp_effectiveFrom.Value = SystemSetting.EffectiveFrom;
                bankLogo_dtp_effectiveTo.Value = SystemSetting.EffectiveTo;
                EnableControls("bankLogo", true);
            }
            catch (Exception ex)
            {
                Util.Log(string.Format("Error in editing bank logo. Error message:{0}", ex.Message));
            }
        }

        private void bankLogo_btn_save_Click(object sender, EventArgs e)
        {
            if (validation("bankLogo"))
            {
                EStatementEntities entities = new EStatementEntities();
                DateTime now = DateTime.Now;

                if (bankLogo_isUpdate)
                {
                    try
                    {
                        tbSystemSetting systemSetting = entities.tbSystemSetting.FirstOrDefault(x => x.ID == bankLogo_recordID);
                        if (systemSetting != null)
                        {
                            systemSetting.SettingValue = bankLogo_tb_path.Text.Trim();
                            systemSetting.StatementType = bankLogo_tb_statementType.Text.Trim();
                            systemSetting.EffectiveFrom = bankLogo_dtp_effectiveFrom.Value;
                            systemSetting.EffectiveTo = bankLogo_dtp_effectiveTo.Value;
                            systemSetting.LastUpdate = now;
                            systemSetting.ModifyBy = Properties.Settings.Default.AppName;

                            entities.SaveChanges();
                            refreshGridView("bankLogo");
                            gvSelectRow("bankLogo", bankLogo_recordID);
                        }
                        bankLogo_isUpdate = false;
                    }
                    catch (Exception ex)
                    {
                        Util.Log(string.Format("Error in updating bank logo. Error message:{0}", ex.Message));
                    }
                }
                else
                {
                    try
                    {
                        tbSystemSetting systemSetting = new tbSystemSetting();
                        systemSetting.SettingName = "header";
                        systemSetting.SettingValue = bankLogo_tb_path.Text.Trim();
                        systemSetting.StatementType = bankLogo_tb_statementType.Text.Trim();
                        systemSetting.EffectiveFrom = bankLogo_dtp_effectiveFrom.Value;
                        systemSetting.EffectiveTo = bankLogo_dtp_effectiveTo.Value;
                        systemSetting.Active = "Y";
                        systemSetting.CreateDate = now;
                        systemSetting.LastUpdate = now;
                        systemSetting.CreateBy = Properties.Settings.Default.AppName;
                        systemSetting.ModifyBy = Properties.Settings.Default.AppName;

                        entities.AddTotbSystemSetting(systemSetting);
                        entities.SaveChanges();
                        refreshGridView("bankLogo");
                    }
                    catch (Exception ex)
                    {
                        Util.Log(string.Format("Error in adding bank logo. Error message:{0}", ex.Message));
                    }
                }
                clearAll("bankLogo");
                EnableControls("bankLogo", false);
            }
        }

        private void bankLogo_btn_cancel_Click(object sender, EventArgs e)
        {
            clearAll("bankLogo");
            EnableControls("bankLogo", false);
            clearValidation("bankLogo");
        }

        #endregion

        #region Terms and Condition

        private void termsAndCondtion_btn_pathSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                termsAndCondition_tb_path.Text = openFileDialog.InitialDirectory + openFileDialog.FileName;
            }
        }

        private void termsAndCondition_bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            EnableControls("termsAndCondition", true);
        }

        private void termsAndCondition_bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to delete this record?", "Warning", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    int ID = ((SystemSettingTool.tbSystemSetting)(termsAndConditionBindingSource.Current)).ID;
                    EStatementEntities entities = new EStatementEntities();
                    tbSystemSetting SystemSetting = entities.tbSystemSetting.FirstOrDefault(x => x.ID == ID);

                    SystemSetting.Active = "N";
                    entities.SaveChanges();
                    refreshGridView("termsAndCondition");
                }
                catch (Exception ex)
                {
                    Util.Log(string.Format("Error in deleting terms and condition. Error message:{0}", ex.Message));
                }
            }
        }

        private void termsAndCondition_bindingNavigatorEditItem_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = ((SystemSettingTool.tbSystemSetting)(termsAndConditionBindingSource.Current)).ID;
                EStatementEntities entities = new EStatementEntities();
                tbSystemSetting SystemSetting = entities.tbSystemSetting.FirstOrDefault(x => x.ID == ID);

                termsAndCondition_isUpdate = true;
                termsAndCondition_recordID = SystemSetting.ID;
                termsAndCondition_tb_path.Text = SystemSetting.SettingValue;
                termsAndCondition_tb_statementType.Text = SystemSetting.StatementType;
                termsAndCondition_dtp_effectiveFrom.Value = SystemSetting.EffectiveFrom;
                termsAndCondition_dtp_effectiveTo.Value = SystemSetting.EffectiveTo;
                EnableControls("termsAndCondition", true);
            }
            catch (Exception ex)
            {
                Util.Log(string.Format("Error in editing terms and condition. Error message:{0}", ex.Message));
            }
        }

        private void termsAndCondition_btn_save_Click(object sender, EventArgs e)
        {
            if (validation("termsAndCondition"))
            {
                EStatementEntities entities = new EStatementEntities();
                DateTime now = DateTime.Now;

                if (termsAndCondition_isUpdate)
                {
                    try
                    {
                        tbSystemSetting systemSetting = entities.tbSystemSetting.FirstOrDefault(x => x.ID == termsAndCondition_recordID);
                        if (systemSetting != null)
                        {
                            systemSetting.SettingValue = termsAndCondition_tb_path.Text.Trim();
                            systemSetting.StatementType = termsAndCondition_tb_statementType.Text.Trim();
                            systemSetting.EffectiveFrom = termsAndCondition_dtp_effectiveFrom.Value;
                            systemSetting.EffectiveTo = termsAndCondition_dtp_effectiveTo.Value;
                            systemSetting.LastUpdate = now;
                            systemSetting.ModifyBy = Properties.Settings.Default.AppName;

                            entities.SaveChanges();
                            refreshGridView("termsAndCondition");
                            gvSelectRow("termsAndCondition", termsAndCondition_recordID);
                        }
                        termsAndCondition_isUpdate = false;
                    }
                    catch (Exception ex)
                    {
                        Util.Log(string.Format("Error in updating terms and condition. Error message:{0}", ex.Message));
                    }
                }
                else
                {
                    try
                    {
                        tbSystemSetting systemSetting = new tbSystemSetting();
                        systemSetting.SettingName = "tc";
                        systemSetting.SettingValue = termsAndCondition_tb_path.Text.Trim();
                        systemSetting.StatementType = termsAndCondition_tb_statementType.Text.Trim();
                        systemSetting.EffectiveFrom = termsAndCondition_dtp_effectiveFrom.Value;
                        systemSetting.EffectiveTo = termsAndCondition_dtp_effectiveTo.Value;
                        systemSetting.Active = "Y";
                        systemSetting.CreateDate = now;
                        systemSetting.LastUpdate = now;
                        systemSetting.CreateBy = Properties.Settings.Default.AppName;
                        systemSetting.ModifyBy = Properties.Settings.Default.AppName;

                        entities.AddTotbSystemSetting(systemSetting);
                        entities.SaveChanges();
                        refreshGridView("termsAndCondition");
                    }
                    catch (Exception ex)
                    {
                        Util.Log(string.Format("Error in adding terms and condition. Error message:{0}", ex.Message));
                    }
                }
                clearAll("termsAndCondition");
                EnableControls("termsAndCondition", false);
            }
        }

        private void termsAndCondition_btn_cancel_Click(object sender, EventArgs e)
        {
            clearAll("termsAndCondition");
            EnableControls("termsAndCondition", false);
            clearValidation("termsAndCondition");
        }

        #endregion

        #region Market Insert

        private void marketInsert_btn_pathSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                marketInsert_tb_path.Text = openFileDialog.InitialDirectory + openFileDialog.FileName;
            }
        }

        private void marketInsert_bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            EnableControls("marketInsert", true);
        }

        private void marketInsert_bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to delete this record?", "Warning", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    int ID = ((SystemSettingTool.tbMarketInsert)(marketInsertBindingSource.Current)).ID;
                    EStatementEntities entities = new EStatementEntities();
                    tbMarketInsert marketInsert = entities.tbMarketInsert.FirstOrDefault(x => x.ID == ID);

                    marketInsert.Active = "N";
                    entities.SaveChanges();
                    refreshGridView("marketInsert");
                }
                catch (Exception ex)
                {
                    Util.Log(string.Format("Error in deleting market insert. Error message:{0}", ex.Message));
                }
            }
        }

        private void marketInsert_bindingNavigatorEditItem_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = ((SystemSettingTool.tbMarketInsert)(marketInsertBindingSource.Current)).ID;
                EStatementEntities entities = new EStatementEntities();
                tbMarketInsert marketInsert = entities.tbMarketInsert.FirstOrDefault(x => x.ID == ID);

                marketInsert_isUpdate = true;
                marketInsert_recordID = marketInsert.ID;
                marketInsert_tb_insertID.Text = marketInsert.MarketInsertID;
                marketInsert_tb_path.Text = marketInsert.MarketingPath;
                marketInsert_tb_statementType.Text = marketInsert.StatementType;
                marketInsert_dtp_effectiveFrom.Value = marketInsert.EffectiveFrom;
                marketInsert_dtp_effectiveTo.Value = marketInsert.EffectiveTo;
                EnableControls("marketInsert", true);
            }
            catch (Exception ex)
            {
                Util.Log(string.Format("Error in editing market insert. Error message:{0}", ex.Message));
            }
        }

        private void marketInsert_btn_save_Click(object sender, EventArgs e)
        {
            if (validation("marketInsert"))
            {
                EStatementEntities entities = new EStatementEntities();
                DateTime now = DateTime.Now;

                if (marketInsert_isUpdate)
                {
                    try
                    {
                        tbMarketInsert marketInsert = entities.tbMarketInsert.FirstOrDefault(x => x.ID == marketInsert_recordID);
                        if (marketInsert != null)
                        {
                            marketInsert.MarketInsertID = marketInsert_tb_insertID.Text.Trim();
                            marketInsert.MarketingPath = marketInsert_tb_path.Text.Trim();
                            marketInsert.StatementType = marketInsert_tb_statementType.Text.Trim();
                            marketInsert.EffectiveFrom = marketInsert_dtp_effectiveFrom.Value;
                            marketInsert.EffectiveTo = marketInsert_dtp_effectiveTo.Value;
                            marketInsert.LastUpdate = now;
                            marketInsert.ModifyBy = Properties.Settings.Default.AppName;

                            entities.SaveChanges();
                            refreshGridView("marketInsert");
                            gvSelectRow("marketInsert", marketInsert_recordID);
                        }
                        marketInsert_isUpdate = false;
                    }
                    catch (Exception ex)
                    {
                        Util.Log(string.Format("Error in updating market insert. Error message:{0}", ex.Message));
                    }
                }
                else
                {
                    try
                    {
                        tbMarketInsert marketInsert = new tbMarketInsert();
                        marketInsert.MarketInsertID = marketInsert_tb_insertID.Text.Trim();
                        marketInsert.MarketingPath = marketInsert_tb_path.Text.Trim();
                        marketInsert.StatementType = marketInsert_tb_statementType.Text.Trim();
                        marketInsert.EffectiveFrom = marketInsert_dtp_effectiveFrom.Value;
                        marketInsert.EffectiveTo = marketInsert_dtp_effectiveTo.Value;
                        marketInsert.Active = "Y";
                        marketInsert.CreateDate = now;
                        marketInsert.LastUpdate = now;
                        marketInsert.CreateBy = Properties.Settings.Default.AppName;
                        marketInsert.ModifyBy = Properties.Settings.Default.AppName;

                        entities.AddTotbMarketInsert(marketInsert);
                        entities.SaveChanges();
                        refreshGridView("marketInsert");
                    }
                    catch (Exception ex)
                    {
                        Util.Log(string.Format("Error in adding market insert. Error message:{0}", ex.Message));
                    }
                }
                clearAll("marketInsert");
                EnableControls("marketInsert", false);
            }
        }

        private void marketInsert_btn_cancel_Click(object sender, EventArgs e)
        {
            clearAll("marketInsert");
            EnableControls("marketInsert", false);
            clearValidation("marketInsert");
        }

        #endregion

        #region Bank Info Insert

        private void bankInfoInsert_btn_pathSelect_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bankInfoInsert_tb_path.Text = openFileDialog.InitialDirectory + openFileDialog.FileName;
            }
        }

        private void bankInfoInsert_bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            EnableControls("bankInfoInsert", true);
        }

        private void bankInfoInsert_bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to delete this record?", "Warning", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    int ID = ((SystemSettingTool.tbBankInformationInsert)(bankInfoInsertBindingSource.Current)).ID;
                    EStatementEntities entities = new EStatementEntities();
                    tbBankInformationInsert bankinfoInsert = entities.tbBankInformationInsert.FirstOrDefault(x => x.ID == ID);

                    bankinfoInsert.Active = "N";
                    entities.SaveChanges();
                    refreshGridView("bankInfoInsert");
                }
                catch (Exception ex)
                {
                    Util.Log(string.Format("Error in deleting bank info insert. Error message:{0}", ex.Message));
                }
            }
        }

        private void bankInfoInsert_bindingNavigatorEditItem_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = ((SystemSettingTool.tbBankInformationInsert)(bankInfoInsertBindingSource.Current)).ID;
                EStatementEntities entities = new EStatementEntities();
                tbBankInformationInsert bankInfoInsert = entities.tbBankInformationInsert.FirstOrDefault(x => x.ID == ID);

                bankInfoInsert_isUpdate = true;
                bankInfoInsert_recordID = bankInfoInsert.ID;
                bankInfoInsert_tb_insertID.Text = bankInfoInsert.BankInformationID;
                bankInfoInsert_tb_path.Text = bankInfoInsert.BankInformationPath;
                bankInfoInsert_tb_statementType.Text = bankInfoInsert.StatementType;
                bankInfoInsert_dtp_effectiveFrom.Value = bankInfoInsert.EffectiveFrom;
                bankInfoInsert_dtp_effectiveTo.Value = bankInfoInsert.EffectiveTo;
                EnableControls("bankInfoInsert", true);
            }
            catch (Exception ex)
            {
                Util.Log(string.Format("Error in editing bank info insert. Error message:{0}", ex.Message));
            }
        }

        private void bankInfoInsert_btn_save_Click(object sender, EventArgs e)
        {
            if (validation("bankInfoInsert"))
            {
                EStatementEntities entities = new EStatementEntities();
                DateTime now = DateTime.Now;

                if (bankInfoInsert_isUpdate)
                {
                    try
                    {
                        tbBankInformationInsert bankInfoInsert = entities.tbBankInformationInsert.FirstOrDefault(x => x.ID == bankInfoInsert_recordID);
                        if (bankInfoInsert != null)
                        {
                            bankInfoInsert.BankInformationID = bankInfoInsert_tb_insertID.Text.Trim();
                            bankInfoInsert.BankInformationPath = bankInfoInsert_tb_path.Text.Trim();
                            bankInfoInsert.StatementType = bankInfoInsert_tb_statementType.Text.Trim();
                            bankInfoInsert.EffectiveFrom = bankInfoInsert_dtp_effectiveFrom.Value;
                            bankInfoInsert.EffectiveTo = bankInfoInsert_dtp_effectiveTo.Value;
                            bankInfoInsert.LastUpdate = now;
                            bankInfoInsert.ModifyBy = Properties.Settings.Default.AppName;

                            entities.SaveChanges();
                            refreshGridView("bankInfoInsert");
                            gvSelectRow("bankInfoInsert", bankInfoInsert_recordID);
                        }
                        bankInfoInsert_isUpdate = false;
                    }
                    catch (Exception ex)
                    {
                        Util.Log(string.Format("Error in updating bank info insert. Error message:{0}", ex.Message));
                    }
                }
                else
                {
                    try
                    {
                        tbBankInformationInsert bankInfoInsert = new tbBankInformationInsert();
                        bankInfoInsert.BankInformationID = bankInfoInsert_tb_insertID.Text.Trim();
                        bankInfoInsert.BankInformationPath = bankInfoInsert_tb_path.Text.Trim();
                        bankInfoInsert.StatementType = bankInfoInsert_tb_statementType.Text.Trim();
                        bankInfoInsert.EffectiveFrom = bankInfoInsert_dtp_effectiveFrom.Value;
                        bankInfoInsert.EffectiveTo = bankInfoInsert_dtp_effectiveTo.Value;
                        bankInfoInsert.Active = "Y";
                        bankInfoInsert.CreateDate = now;
                        bankInfoInsert.LastUpdate = now;
                        bankInfoInsert.CreateBy = Properties.Settings.Default.AppName;
                        bankInfoInsert.ModifyBy = Properties.Settings.Default.AppName;

                        entities.AddTotbBankInformationInsert(bankInfoInsert);
                        entities.SaveChanges();
                        refreshGridView("bankInfoInsert");
                    }
                    catch (Exception ex)
                    {
                        Util.Log(string.Format("Error in adding bank info insert. Error message:{0}", ex.Message));
                    }
                }
                clearAll("bankInfoInsert");
                EnableControls("bankInfoInsert", false);
            }
        }

        private void bankInfoInsert_btn_cancel_Click(object sender, EventArgs e)
        {
            clearAll("bankInfoInsert");
            EnableControls("bankInfoInsert", false);
            clearValidation("bankInfoInsert");
        }

        #endregion

        #endregion

        #region Private method

        private void refreshGridView(string gridView)
        {
            try
            {
                EStatementEntities entities = new EStatementEntities();

                switch (gridView)
                {
                    case "bankLogo":
                        entities.Refresh(System.Data.Objects.RefreshMode.StoreWins, entities.tbSystemSetting.Where(c => c.SettingName.StartsWith("header") && c.Active == "Y"));
                        List<tbSystemSetting> bankLogoSystemSettingList = entities.tbSystemSetting.Where(c => c.SettingName.StartsWith("header") && c.Active == "Y").ToList();
                        if (bankLogoSystemSettingList != null)
                            this.bankLogoBindingSource.DataSource = bankLogoSystemSettingList;

                        bankLogo_bindingNavigatorDeleteItem.Enabled = bankLogoSystemSettingList.Count == 0 ? false : true;
                        bankLogo_bindingNavigatorEditItem.Enabled = bankLogoSystemSettingList.Count == 0 ? false : true;
                        break;
                    case "termsAndCondition":
                        entities.Refresh(System.Data.Objects.RefreshMode.StoreWins, entities.tbSystemSetting.Where(c => c.SettingName.StartsWith("tc") && c.Active == "Y"));
                        List<tbSystemSetting> tcSystemSettingList = entities.tbSystemSetting.Where(c => c.SettingName.StartsWith("tc") && c.Active == "Y").ToList();
                        if (tcSystemSettingList != null)
                            this.termsAndConditionBindingSource.DataSource = tcSystemSettingList;
                        this.gv_termsAndCondition.Columns[0].Visible = false;

                        termsAndCondition_bindingNavigatorDeleteItem.Enabled = tcSystemSettingList.Count == 0 ? false : true;
                        termsAndCondition_bindingNavigatorEditItem.Enabled = tcSystemSettingList.Count == 0 ? false : true;
                        break;
                    case "marketInsert":
                        entities.Refresh(System.Data.Objects.RefreshMode.StoreWins, entities.tbMarketInsert.Where(c => c.Active == "Y"));
                        List<tbMarketInsert> marketInsertList = entities.tbMarketInsert.Where(c => c.Active == "Y").ToList();
                        if (marketInsertList != null)
                            this.marketInsertBindingSource.DataSource = marketInsertList;
                        this.gv_marketInsert.Columns[0].Visible = false;

                        marketInsert_bindingNavigatorDeleteItem.Enabled = marketInsertList.Count == 0 ? false : true;
                        marketInsert_bindingNavigatorEditItem.Enabled = marketInsertList.Count == 0 ? false : true;
                        break;
                    case "bankInfoInsert":
                        entities.Refresh(System.Data.Objects.RefreshMode.StoreWins, entities.tbBankInformationInsert.Where(c => c.Active == "Y"));
                        List<tbBankInformationInsert> bankInfoInsertList = entities.tbBankInformationInsert.Where(c => c.Active == "Y").ToList();
                        if (bankInfoInsertList != null)
                            this.bankInfoInsertBindingSource.DataSource = bankInfoInsertList;
                        this.gv_bankInfoInsert.Columns[0].Visible = false;

                        bankInfoInsert_bindingNavigatorDeleteItem.Enabled = bankInfoInsertList.Count == 0 ? false : true;
                        bankInfoInsert_bindingNavigatorEditItem.Enabled = bankInfoInsertList.Count == 0 ? false : true;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.Log(string.Format("Error in refreshGridView. gridView:{0}. Error message:{1}", gridView, ex.Message));
            }
        }

        private void EnableControls(string gridView, Boolean isEnable)
        {
            try
            {
                switch (gridView)
                {
                    case "bankLogo":
                        bankLogo_tb_path.Enabled = isEnable ? true : false;
                        bankLogo_tb_statementType.Enabled = isEnable ? true : false;
                        bankLogo_dtp_effectiveFrom.Enabled = isEnable ? true : false;
                        bankLogo_dtp_effectiveTo.Enabled = isEnable ? true : false;
                        bankLogo_btn_pathSelect.Enabled = isEnable ? true : false;
                        bankLogo_btn_save.Enabled = isEnable ? true : false;
                        bankLogo_btn_cancel.Enabled = isEnable ? true : false;
                        break;
                    case "termsAndCondition":
                        termsAndCondition_tb_path.Enabled = isEnable ? true : false;
                        termsAndCondition_tb_statementType.Enabled = isEnable ? true : false;
                        termsAndCondition_dtp_effectiveFrom.Enabled = isEnable ? true : false;
                        termsAndCondition_dtp_effectiveTo.Enabled = isEnable ? true : false;
                        termsAndCondtion_btn_pathSelect.Enabled = isEnable ? true : false;
                        termsAndCondition_btn_save.Enabled = isEnable ? true : false;
                        termsAndCondition_btn_cancel.Enabled = isEnable ? true : false;
                        break;
                    case "marketInsert":
                        marketInsert_tb_insertID.Enabled = isEnable ? true : false;
                        marketInsert_tb_path.Enabled = isEnable ? true : false;
                        marketInsert_tb_statementType.Enabled = isEnable ? true : false;
                        marketInsert_dtp_effectiveFrom.Enabled = isEnable ? true : false;
                        marketInsert_dtp_effectiveTo.Enabled = isEnable ? true : false;
                        marketInsert_btn_pathSelect.Enabled = isEnable ? true : false;
                        marketInsert_btn_save.Enabled = isEnable ? true : false;
                        marketInsert_btn_cancel.Enabled = isEnable ? true : false;
                        break;
                    case "bankInfoInsert":
                        bankInfoInsert_tb_insertID.Enabled = isEnable ? true : false;
                        bankInfoInsert_tb_path.Enabled = isEnable ? true : false;
                        bankInfoInsert_tb_statementType.Enabled = isEnable ? true : false;
                        bankInfoInsert_dtp_effectiveFrom.Enabled = isEnable ? true : false;
                        bankInfoInsert_dtp_effectiveTo.Enabled = isEnable ? true : false;
                        bankInfoInsert_btn_pathSelect.Enabled = isEnable ? true : false;
                        bankInfoInsert_btn_save.Enabled = isEnable ? true : false;
                        bankInfoInsert_btn_cancel.Enabled = isEnable ? true : false;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.Log(string.Format("Error in EnableControls. gridView:{0}. Error message:{1}", gridView, ex.Message));
            }
        }

        private void clearAll(string gridView)
        {
            try
            {
                switch (gridView)
                {
                    case "bankLogo":
                        bankLogo_tb_path.Text = "";
                        bankLogo_tb_statementType.Text = "";
                        bankLogo_dtp_effectiveFrom.Value = DateTime.Now;
                        bankLogo_dtp_effectiveTo.Value = DateTime.Now;
                        break;
                    case "termsAndCondition":
                        termsAndCondition_tb_path.Text = "";
                        termsAndCondition_tb_statementType.Text = "";
                        termsAndCondition_dtp_effectiveFrom.Value = DateTime.Now;
                        termsAndCondition_dtp_effectiveTo.Value = DateTime.Now;
                        break;
                    case "marketInsert":
                        marketInsert_tb_insertID.Text = "";
                        marketInsert_tb_path.Text = "";
                        marketInsert_tb_statementType.Text = "";
                        marketInsert_dtp_effectiveFrom.Value = DateTime.Now;
                        marketInsert_dtp_effectiveTo.Value = DateTime.Now;
                        break;
                    case "bankInfoInsert":
                        bankInfoInsert_tb_insertID.Text = "";
                        bankInfoInsert_tb_path.Text = "";
                        bankInfoInsert_tb_statementType.Text = "";
                        bankInfoInsert_dtp_effectiveFrom.Value = DateTime.Now;
                        bankInfoInsert_dtp_effectiveTo.Value = DateTime.Now;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.Log(string.Format("Error in clearAll. gridView:{0}. Error message:{1}", gridView, ex.Message));
            }
        }

        private Boolean validation(string gridView)
        {
            Boolean result = true;
            try
            {
                EStatementEntities entities = new EStatementEntities();
                switch (gridView)
                {
                    case "bankLogo":
                        if (String.IsNullOrEmpty(this.bankLogo_tb_path.Text.Trim()))
                        {
                            this.errorProvider.SetError(this.bankLogo_tb_path, "File Path cannot be empty.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.bankLogo_tb_path, "");
                        }
                        if (String.IsNullOrEmpty(this.bankLogo_tb_statementType.Text.Trim()))
                        {
                            this.errorProvider.SetError(this.bankLogo_tb_statementType, "Statement Type cannot be empty.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.bankLogo_tb_statementType, "");
                        }

                        List<tbSystemSetting> systemSettingBankLogoList = entities.tbSystemSetting.Where(a => a.SettingName == "header" && a.Active == "Y" && a.StatementType == bankLogo_tb_statementType.Text.Trim()).ToList();
                        Boolean isOverlap_bankLogo = false;
                        foreach (var systemSetting in systemSettingBankLogoList)
                        {
                            if (bankLogo_dtp_effectiveFrom.Value < systemSetting.EffectiveTo && bankLogo_dtp_effectiveTo.Value > systemSetting.EffectiveFrom)
                            {
                                if (bankLogo_isUpdate)
                                {
                                    if (systemSetting.ID != bankLogo_recordID)
                                    {
                                        isOverlap_bankLogo = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    isOverlap_bankLogo = true;
                                    break;
                                }
                            }
                        }
                        if (isOverlap_bankLogo)
                        {
                            this.errorProvider.SetError(this.bankLogo_dtp_effectiveFrom, "Period Overlap.");
                            this.errorProvider.SetError(this.bankLogo_dtp_effectiveTo, "Period Overlap.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.bankLogo_dtp_effectiveFrom, "");
                            this.errorProvider.SetError(this.bankLogo_dtp_effectiveTo, "");
                        }

                        break;
                    case "termsAndCondition":
                        if (String.IsNullOrEmpty(this.termsAndCondition_tb_path.Text.Trim()))
                        {
                            this.errorProvider.SetError(this.termsAndCondition_tb_path, "File Path cannot be empty.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.termsAndCondition_tb_path, "");
                        }
                        if (String.IsNullOrEmpty(this.termsAndCondition_tb_statementType.Text.Trim()))
                        {
                            this.errorProvider.SetError(this.termsAndCondition_tb_statementType, "Statement Type cannot be empty.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.termsAndCondition_tb_statementType, "");
                        }

                        List<tbSystemSetting> systemSettingTcList = entities.tbSystemSetting.Where(a => a.SettingName == "tc" && a.Active == "Y" && a.StatementType == termsAndCondition_tb_statementType.Text.Trim()).ToList();
                        Boolean isOverlap_termsAndCondition = false;
                        foreach (var systemSetting in systemSettingTcList)
                        {
                            if (termsAndCondition_dtp_effectiveFrom.Value < systemSetting.EffectiveTo && termsAndCondition_dtp_effectiveTo.Value > systemSetting.EffectiveFrom)
                            {
                                if (termsAndCondition_isUpdate)
                                {
                                    if (systemSetting.ID != termsAndCondition_recordID)
                                    {
                                        isOverlap_termsAndCondition = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    isOverlap_termsAndCondition = true;
                                    break;
                                }
                            }
                        }
                        if (isOverlap_termsAndCondition)
                        {
                            this.errorProvider.SetError(this.termsAndCondition_dtp_effectiveFrom, "Period Overlap.");
                            this.errorProvider.SetError(this.termsAndCondition_dtp_effectiveTo, "Period Overlap.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.termsAndCondition_dtp_effectiveFrom, "");
                            this.errorProvider.SetError(this.termsAndCondition_dtp_effectiveTo, "");
                        }

                        break;
                    case "marketInsert":
                        if (String.IsNullOrEmpty(this.marketInsert_tb_insertID.Text.Trim()))
                        {
                            this.errorProvider.SetError(this.marketInsert_tb_insertID, "Insert ID cannot be empty.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.marketInsert_tb_insertID, "");
                        }
                        if (String.IsNullOrEmpty(this.marketInsert_tb_path.Text.Trim()))
                        {
                            this.errorProvider.SetError(this.marketInsert_tb_path, "File Path cannot be empty.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.marketInsert_tb_path, "");
                        }
                        if (String.IsNullOrEmpty(this.marketInsert_tb_statementType.Text.Trim()))
                        {
                            this.errorProvider.SetError(this.marketInsert_tb_statementType, "Statement Type cannot be empty.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.marketInsert_tb_statementType, "");
                        }

                        List<tbMarketInsert> marketInsertList = entities.tbMarketInsert.Where(a => a.Active == "Y" && a.StatementType == marketInsert_tb_statementType.Text.Trim()).ToList();
                        Boolean isOverlap_marketInsert = false;
                        foreach (var systemSetting in marketInsertList)
                        {
                            if (marketInsert_dtp_effectiveFrom.Value < systemSetting.EffectiveTo && marketInsert_dtp_effectiveTo.Value > systemSetting.EffectiveFrom)
                            {
                                if (marketInsert_isUpdate)
                                {
                                    if (systemSetting.ID != marketInsert_recordID)
                                    {
                                        isOverlap_marketInsert = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    isOverlap_marketInsert = true;
                                    break;
                                }
                            }
                        }
                        if (isOverlap_marketInsert)
                        {
                            this.errorProvider.SetError(this.marketInsert_dtp_effectiveFrom, "Period Overlap.");
                            this.errorProvider.SetError(this.marketInsert_dtp_effectiveTo, "Period Overlap.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.marketInsert_dtp_effectiveFrom, "");
                            this.errorProvider.SetError(this.marketInsert_dtp_effectiveTo, "");
                        }

                        break;
                    case "bankInfoInsert":
                        if (String.IsNullOrEmpty(this.bankInfoInsert_tb_insertID.Text.Trim()))
                        {
                            this.errorProvider.SetError(this.bankInfoInsert_tb_insertID, "Insert ID cannot be empty.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.bankInfoInsert_tb_insertID, "");
                        }
                        if (String.IsNullOrEmpty(this.bankInfoInsert_tb_path.Text.Trim()))
                        {
                            this.errorProvider.SetError(this.bankInfoInsert_tb_path, "File Path cannot be empty.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.bankInfoInsert_tb_path, "");
                        }
                        if (String.IsNullOrEmpty(this.bankInfoInsert_tb_statementType.Text.Trim()))
                        {
                            this.errorProvider.SetError(this.bankInfoInsert_tb_statementType, "Statement Type cannot be empty.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.bankInfoInsert_tb_statementType, "");
                        }

                        List<tbBankInformationInsert> bankInfoInsertList = entities.tbBankInformationInsert.Where(a => a.Active == "Y" && a.StatementType == bankInfoInsert_tb_statementType.Text.Trim()).ToList();
                        Boolean isOverlap_bankInfoInsert = false;
                        foreach (var systemSetting in bankInfoInsertList)
                        {
                            if (bankInfoInsert_dtp_effectiveFrom.Value < systemSetting.EffectiveTo && bankInfoInsert_dtp_effectiveTo.Value > systemSetting.EffectiveFrom)
                            {
                                if (bankInfoInsert_isUpdate)
                                {
                                    if (systemSetting.ID != bankInfoInsert_recordID)
                                    {
                                        isOverlap_bankInfoInsert = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    isOverlap_bankInfoInsert = true;
                                    break;
                                }
                            }
                        }
                        if (isOverlap_bankInfoInsert)
                        {
                            this.errorProvider.SetError(this.bankInfoInsert_dtp_effectiveFrom, "Period Overlap.");
                            this.errorProvider.SetError(this.bankInfoInsert_dtp_effectiveTo, "Period Overlap.");
                            result = false;
                        }
                        else
                        {
                            this.errorProvider.SetError(this.bankInfoInsert_dtp_effectiveFrom, "");
                            this.errorProvider.SetError(this.bankInfoInsert_dtp_effectiveTo, "");
                        }

                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.Log(string.Format("Error in validation. gridView:{0}. Error message:{1}", gridView, ex.Message));
            }
            return result;
        }

        private void clearValidation(string gridView)
        {
            try
            {
                switch (gridView)
                {
                    case "bankLogo":
                        this.errorProvider.SetError(this.bankLogo_tb_path, "");
                        this.errorProvider.SetError(this.bankLogo_tb_statementType, "");
                        this.errorProvider.SetError(this.bankLogo_dtp_effectiveFrom, "");
                        this.errorProvider.SetError(this.bankLogo_dtp_effectiveTo, "");
                        break;
                    case "termsAndCondition":
                        this.errorProvider.SetError(this.termsAndCondition_tb_path, "");
                        this.errorProvider.SetError(this.termsAndCondition_tb_statementType, "");
                        this.errorProvider.SetError(this.termsAndCondition_dtp_effectiveFrom, "");
                        this.errorProvider.SetError(this.termsAndCondition_dtp_effectiveTo, "");
                        break;
                    case "marketInsert":
                        this.errorProvider.SetError(this.marketInsert_tb_insertID, "");
                        this.errorProvider.SetError(this.marketInsert_tb_path, "");
                        this.errorProvider.SetError(this.marketInsert_tb_statementType, "");
                        this.errorProvider.SetError(this.marketInsert_dtp_effectiveFrom, "");
                        this.errorProvider.SetError(this.marketInsert_dtp_effectiveTo, "");
                        break;
                    case "bankInfoInsert":
                        this.errorProvider.SetError(this.bankInfoInsert_tb_insertID, "");
                        this.errorProvider.SetError(this.bankInfoInsert_tb_path, "");
                        this.errorProvider.SetError(this.bankInfoInsert_tb_statementType, "");
                        this.errorProvider.SetError(this.bankInfoInsert_dtp_effectiveFrom, "");
                        this.errorProvider.SetError(this.bankInfoInsert_dtp_effectiveTo, "");
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.Log(string.Format("Error in clearValidation. gridView:{0}. Error message:{1}", gridView, ex.Message));
            }
        }

        private void gvSelectRow(string gridView, int recordID)
        {
            try
            {
                int index = 0;
                switch (gridView)
                {
                    case "bankLogo":
                        for (; index < gv_bankLogo.Rows.Count; index++)
                        {
                            if (((SystemSettingTool.tbSystemSetting)(gv_bankLogo.Rows[index].DataBoundItem)).ID == recordID)
                                break;
                        }
                        this.gv_bankLogo.Rows[index].Selected = true;
                        break;
                    case "termsAndCondition":
                        for (; index < gv_termsAndCondition.Rows.Count; index++)
                        {
                            if (((SystemSettingTool.tbSystemSetting)(gv_termsAndCondition.Rows[index].DataBoundItem)).ID == recordID)
                                break;
                        }
                        this.gv_termsAndCondition.Rows[index].Selected = true;
                        break;
                    case "marketInsert":
                        for (; index < gv_marketInsert.Rows.Count; index++)
                        {
                            if (((SystemSettingTool.tbMarketInsert)(gv_marketInsert.Rows[index].DataBoundItem)).ID == recordID)
                                break;
                        }
                        this.gv_marketInsert.Rows[index].Selected = true;
                        break;
                    case "bankInfoInsert":
                        for (; index < gv_bankInfoInsert.Rows.Count; index++)
                        {
                            if (((SystemSettingTool.tbBankInformationInsert)(gv_bankInfoInsert.Rows[index].DataBoundItem)).ID == recordID)
                                break;
                        }
                        this.gv_bankInfoInsert.Rows[index].Selected = true;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Util.Log(string.Format("Error in gvSelectRow. gridView:{0},recordID:{1}. Error message:{2}", gridView, recordID.ToString(), ex.Message));
            }
        }

        #endregion

    }
}
