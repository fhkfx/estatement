﻿namespace SystemSettingTool
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;
    using System.Windows.Forms.VisualStyles;

    partial class FormSystemSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSystemSetting));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tp_bankLogo = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bankLogo_btn_cancel = new System.Windows.Forms.Button();
            this.bankLogo_btn_save = new System.Windows.Forms.Button();
            this.bankLogo_dtp_effectiveTo = new System.Windows.Forms.DateTimePicker();
            this.bankLogo_dtp_effectiveFrom = new System.Windows.Forms.DateTimePicker();
            this.bankLogo_tb_statementType = new System.Windows.Forms.TextBox();
            this.bankLogo_btn_pathSelect = new System.Windows.Forms.Button();
            this.bankLogo_tb_path = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gv_bankLogo = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.settingNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.settingValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statementTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectiveFromDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectiveToDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createByDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modifyDateDataGridViewImageColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.modifyByDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastUpdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bankLogoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bankLogoBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bankLogo_bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bankLogo_bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bankLogo_bindingNavigatorEditItem = new System.Windows.Forms.ToolStripButton();
            this.tp_tc = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.termsAndCondition_btn_cancel = new System.Windows.Forms.Button();
            this.termsAndCondition_btn_save = new System.Windows.Forms.Button();
            this.termsAndCondition_dtp_effectiveTo = new System.Windows.Forms.DateTimePicker();
            this.termsAndCondition_dtp_effectiveFrom = new System.Windows.Forms.DateTimePicker();
            this.termsAndCondition_tb_statementType = new System.Windows.Forms.TextBox();
            this.termsAndCondtion_btn_pathSelect = new System.Windows.Forms.Button();
            this.termsAndCondition_tb_path = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gv_termsAndCondition = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.settingNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.settingValueDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statementTypeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectiveFromDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectiveToDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createDateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createByDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modifyDateDataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.modifyByDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastUpdateDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.termsAndConditionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.termsAndConditionBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem1 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem1 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.termsAndCondition_bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.termsAndCondition_bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.termsAndCondition_bindingNavigatorEditItem = new System.Windows.Forms.ToolStripButton();
            this.tp_marketingInsert = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.marketInsert_tb_insertID = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.marketInsert_btn_cancel = new System.Windows.Forms.Button();
            this.marketInsert_btn_save = new System.Windows.Forms.Button();
            this.marketInsert_dtp_effectiveTo = new System.Windows.Forms.DateTimePicker();
            this.marketInsert_dtp_effectiveFrom = new System.Windows.Forms.DateTimePicker();
            this.marketInsert_tb_statementType = new System.Windows.Forms.TextBox();
            this.marketInsert_btn_pathSelect = new System.Windows.Forms.Button();
            this.marketInsert_tb_path = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.gv_marketInsert = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marketInsertIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marketingPathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statementTypeDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectiveFromDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectiveToDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createDateDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createByDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modifyDateDataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.modifyByDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastUpdateDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.marketInsertBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.marketInsertbindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem2 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem2 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem2 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.marketInsert_bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.marketInsert_bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.marketInsert_bindingNavigatorEditItem = new System.Windows.Forms.ToolStripButton();
            this.tp_bankInfoInsert = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.bankInfoInsert_tb_insertID = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.bankInfoInsert_btn_cancel = new System.Windows.Forms.Button();
            this.bankInfoInsert_btn_save = new System.Windows.Forms.Button();
            this.bankInfoInsert_dtp_effectiveTo = new System.Windows.Forms.DateTimePicker();
            this.bankInfoInsert_dtp_effectiveFrom = new System.Windows.Forms.DateTimePicker();
            this.bankInfoInsert_tb_statementType = new System.Windows.Forms.TextBox();
            this.bankInfoInsert_btn_pathSelect = new System.Windows.Forms.Button();
            this.bankInfoInsert_tb_path = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.gv_bankInfoInsert = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bankInformationIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bankInformationPathDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statementTypeDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectiveFromDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.effectiveToDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activeDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createDateDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.createByDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modifyDateDataGridViewImageColumn3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.modifyByDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastUpdateDataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bankInfoInsertBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bankInfoInsertbindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem3 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem3 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem3 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem3 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem3 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem3 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.bankInfoInsert_bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bankInfoInsert_bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bankInfoInsert_bindingNavigatorEditItem = new System.Windows.Forms.ToolStripButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_exit = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabControl1.SuspendLayout();
            this.tp_bankLogo.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_bankLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankLogoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankLogoBindingNavigator)).BeginInit();
            this.bankLogoBindingNavigator.SuspendLayout();
            this.tp_tc.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_termsAndCondition)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.termsAndConditionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.termsAndConditionBindingNavigator)).BeginInit();
            this.termsAndConditionBindingNavigator.SuspendLayout();
            this.tp_marketingInsert.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_marketInsert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.marketInsertBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.marketInsertbindingNavigator)).BeginInit();
            this.marketInsertbindingNavigator.SuspendLayout();
            this.tp_bankInfoInsert.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_bankInfoInsert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankInfoInsertBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankInfoInsertbindingNavigator)).BeginInit();
            this.bankInfoInsertbindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.tabControl1, 2);
            this.tabControl1.Controls.Add(this.tp_bankLogo);
            this.tabControl1.Controls.Add(this.tp_tc);
            this.tabControl1.Controls.Add(this.tp_marketingInsert);
            this.tabControl1.Controls.Add(this.tp_bankInfoInsert);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1050, 505);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Tag = "";
            // 
            // tp_bankLogo
            // 
            this.tp_bankLogo.Controls.Add(this.tableLayoutPanel2);
            this.tp_bankLogo.Location = new System.Drawing.Point(4, 25);
            this.tp_bankLogo.Name = "tp_bankLogo";
            this.tp_bankLogo.Padding = new System.Windows.Forms.Padding(3);
            this.tp_bankLogo.Size = new System.Drawing.Size(1042, 476);
            this.tp_bankLogo.TabIndex = 0;
            this.tp_bankLogo.Text = "Bank Logo";
            this.tp_bankLogo.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.40425F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.59575F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1036, 470);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.bankLogo_btn_cancel);
            this.panel1.Controls.Add(this.bankLogo_btn_save);
            this.panel1.Controls.Add(this.bankLogo_dtp_effectiveTo);
            this.panel1.Controls.Add(this.bankLogo_dtp_effectiveFrom);
            this.panel1.Controls.Add(this.bankLogo_tb_statementType);
            this.panel1.Controls.Add(this.bankLogo_btn_pathSelect);
            this.panel1.Controls.Add(this.bankLogo_tb_path);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 300);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1030, 167);
            this.panel1.TabIndex = 1;
            // 
            // bankLogo_btn_cancel
            // 
            this.bankLogo_btn_cancel.Location = new System.Drawing.Point(837, 59);
            this.bankLogo_btn_cancel.Name = "bankLogo_btn_cancel";
            this.bankLogo_btn_cancel.Size = new System.Drawing.Size(125, 66);
            this.bankLogo_btn_cancel.TabIndex = 11;
            this.bankLogo_btn_cancel.Text = "Cancel";
            this.bankLogo_btn_cancel.UseVisualStyleBackColor = true;
            this.bankLogo_btn_cancel.Click += new System.EventHandler(this.bankLogo_btn_cancel_Click);
            // 
            // bankLogo_btn_save
            // 
            this.bankLogo_btn_save.Location = new System.Drawing.Point(692, 60);
            this.bankLogo_btn_save.Name = "bankLogo_btn_save";
            this.bankLogo_btn_save.Size = new System.Drawing.Size(125, 66);
            this.bankLogo_btn_save.TabIndex = 10;
            this.bankLogo_btn_save.Text = "Save";
            this.bankLogo_btn_save.UseVisualStyleBackColor = true;
            this.bankLogo_btn_save.Click += new System.EventHandler(this.bankLogo_btn_save_Click);
            // 
            // bankLogo_dtp_effectiveTo
            // 
            this.bankLogo_dtp_effectiveTo.CustomFormat = "yyyy-MM-dd";
            this.bankLogo_dtp_effectiveTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.bankLogo_dtp_effectiveTo.Location = new System.Drawing.Point(186, 113);
            this.bankLogo_dtp_effectiveTo.Name = "bankLogo_dtp_effectiveTo";
            this.bankLogo_dtp_effectiveTo.Size = new System.Drawing.Size(200, 22);
            this.bankLogo_dtp_effectiveTo.TabIndex = 9;
            // 
            // bankLogo_dtp_effectiveFrom
            // 
            this.bankLogo_dtp_effectiveFrom.CustomFormat = "yyyy-MM-dd";
            this.bankLogo_dtp_effectiveFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.bankLogo_dtp_effectiveFrom.Location = new System.Drawing.Point(186, 85);
            this.bankLogo_dtp_effectiveFrom.Name = "bankLogo_dtp_effectiveFrom";
            this.bankLogo_dtp_effectiveFrom.Size = new System.Drawing.Size(200, 22);
            this.bankLogo_dtp_effectiveFrom.TabIndex = 8;
            // 
            // bankLogo_tb_statementType
            // 
            this.bankLogo_tb_statementType.Location = new System.Drawing.Point(186, 59);
            this.bankLogo_tb_statementType.Name = "bankLogo_tb_statementType";
            this.bankLogo_tb_statementType.Size = new System.Drawing.Size(303, 22);
            this.bankLogo_tb_statementType.TabIndex = 7;
            // 
            // bankLogo_btn_pathSelect
            // 
            this.bankLogo_btn_pathSelect.Location = new System.Drawing.Point(516, 33);
            this.bankLogo_btn_pathSelect.Name = "bankLogo_btn_pathSelect";
            this.bankLogo_btn_pathSelect.Size = new System.Drawing.Size(75, 23);
            this.bankLogo_btn_pathSelect.TabIndex = 6;
            this.bankLogo_btn_pathSelect.Text = "Select";
            this.bankLogo_btn_pathSelect.UseVisualStyleBackColor = true;
            this.bankLogo_btn_pathSelect.Click += new System.EventHandler(this.bankLogo_btn_pathSelect_Click);
            // 
            // bankLogo_tb_path
            // 
            this.bankLogo_tb_path.Location = new System.Drawing.Point(186, 33);
            this.bankLogo_tb_path.Name = "bankLogo_tb_path";
            this.bankLogo_tb_path.Size = new System.Drawing.Size(303, 22);
            this.bankLogo_tb_path.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Effective To";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Effective From";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Statement Type";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "File Path";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gv_bankLogo);
            this.panel2.Controls.Add(this.bankLogoBindingNavigator);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1030, 291);
            this.panel2.TabIndex = 2;
            // 
            // gv_bankLogo
            // 
            this.gv_bankLogo.AllowUserToAddRows = false;
            this.gv_bankLogo.AllowUserToDeleteRows = false;
            this.gv_bankLogo.AllowUserToResizeColumns = false;
            this.gv_bankLogo.AllowUserToResizeRows = false;
            this.gv_bankLogo.AutoGenerateColumns = false;
            this.gv_bankLogo.BackgroundColor = System.Drawing.Color.White;
            this.gv_bankLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gv_bankLogo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_bankLogo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.settingNameDataGridViewTextBoxColumn,
            this.settingValueDataGridViewTextBoxColumn,
            this.statementTypeDataGridViewTextBoxColumn,
            this.effectiveFromDataGridViewTextBoxColumn,
            this.effectiveToDataGridViewTextBoxColumn,
            this.createDateDataGridViewTextBoxColumn,
            this.createByDataGridViewTextBoxColumn,
            this.modifyDateDataGridViewImageColumn,
            this.modifyByDataGridViewTextBoxColumn,
            this.lastUpdateDataGridViewTextBoxColumn});
            this.gv_bankLogo.DataSource = this.bankLogoBindingSource;
            this.gv_bankLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gv_bankLogo.EnableHeadersVisualStyles = false;
            this.gv_bankLogo.Location = new System.Drawing.Point(0, 30);
            this.gv_bankLogo.MultiSelect = false;
            this.gv_bankLogo.Name = "gv_bankLogo";
            this.gv_bankLogo.ReadOnly = true;
            this.gv_bankLogo.RowHeadersVisible = false;
            this.gv_bankLogo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gv_bankLogo.RowTemplate.Height = 24;
            this.gv_bankLogo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gv_bankLogo.Size = new System.Drawing.Size(1030, 261);
            this.gv_bankLogo.TabIndex = 0;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // settingNameDataGridViewTextBoxColumn
            // 
            this.settingNameDataGridViewTextBoxColumn.DataPropertyName = "SettingName";
            this.settingNameDataGridViewTextBoxColumn.HeaderText = "SettingName";
            this.settingNameDataGridViewTextBoxColumn.Name = "settingNameDataGridViewTextBoxColumn";
            this.settingNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.settingNameDataGridViewTextBoxColumn.Visible = false;
            // 
            // settingValueDataGridViewTextBoxColumn
            // 
            this.settingValueDataGridViewTextBoxColumn.DataPropertyName = "SettingValue";
            this.settingValueDataGridViewTextBoxColumn.HeaderText = "File Path";
            this.settingValueDataGridViewTextBoxColumn.Name = "settingValueDataGridViewTextBoxColumn";
            this.settingValueDataGridViewTextBoxColumn.ReadOnly = true;
            this.settingValueDataGridViewTextBoxColumn.Width = 200;
            // 
            // statementTypeDataGridViewTextBoxColumn
            // 
            this.statementTypeDataGridViewTextBoxColumn.DataPropertyName = "StatementType";
            this.statementTypeDataGridViewTextBoxColumn.HeaderText = "Statement Type";
            this.statementTypeDataGridViewTextBoxColumn.Name = "statementTypeDataGridViewTextBoxColumn";
            this.statementTypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.statementTypeDataGridViewTextBoxColumn.Width = 120;
            // 
            // effectiveFromDataGridViewTextBoxColumn
            // 
            this.effectiveFromDataGridViewTextBoxColumn.DataPropertyName = "EffectiveFrom";
            this.effectiveFromDataGridViewTextBoxColumn.HeaderText = "Effective From";
            this.effectiveFromDataGridViewTextBoxColumn.Name = "effectiveFromDataGridViewTextBoxColumn";
            this.effectiveFromDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // effectiveToDataGridViewTextBoxColumn
            // 
            this.effectiveToDataGridViewTextBoxColumn.DataPropertyName = "EffectiveTo";
            this.effectiveToDataGridViewTextBoxColumn.HeaderText = "Effective To";
            this.effectiveToDataGridViewTextBoxColumn.Name = "effectiveToDataGridViewTextBoxColumn";
            this.effectiveToDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // createDateDataGridViewTextBoxColumn
            // 
            this.createDateDataGridViewTextBoxColumn.DataPropertyName = "CreateDate";
            this.createDateDataGridViewTextBoxColumn.HeaderText = "Create Date";
            this.createDateDataGridViewTextBoxColumn.Name = "createDateDataGridViewTextBoxColumn";
            this.createDateDataGridViewTextBoxColumn.ReadOnly = true;
            this.createDateDataGridViewTextBoxColumn.Width = 120;
            // 
            // createByDataGridViewTextBoxColumn
            // 
            this.createByDataGridViewTextBoxColumn.DataPropertyName = "CreateBy";
            this.createByDataGridViewTextBoxColumn.HeaderText = "CreateBy";
            this.createByDataGridViewTextBoxColumn.Name = "createByDataGridViewTextBoxColumn";
            this.createByDataGridViewTextBoxColumn.ReadOnly = true;
            this.createByDataGridViewTextBoxColumn.Visible = false;
            // 
            // modifyDateDataGridViewImageColumn
            // 
            this.modifyDateDataGridViewImageColumn.DataPropertyName = "ModifyDate";
            this.modifyDateDataGridViewImageColumn.HeaderText = "ModifyDate";
            this.modifyDateDataGridViewImageColumn.Name = "modifyDateDataGridViewImageColumn";
            this.modifyDateDataGridViewImageColumn.ReadOnly = true;
            this.modifyDateDataGridViewImageColumn.Visible = false;
            // 
            // modifyByDataGridViewTextBoxColumn
            // 
            this.modifyByDataGridViewTextBoxColumn.DataPropertyName = "ModifyBy";
            this.modifyByDataGridViewTextBoxColumn.HeaderText = "ModifyBy";
            this.modifyByDataGridViewTextBoxColumn.Name = "modifyByDataGridViewTextBoxColumn";
            this.modifyByDataGridViewTextBoxColumn.ReadOnly = true;
            this.modifyByDataGridViewTextBoxColumn.Visible = false;
            // 
            // lastUpdateDataGridViewTextBoxColumn
            // 
            this.lastUpdateDataGridViewTextBoxColumn.DataPropertyName = "LastUpdate";
            this.lastUpdateDataGridViewTextBoxColumn.HeaderText = "Last Update";
            this.lastUpdateDataGridViewTextBoxColumn.Name = "lastUpdateDataGridViewTextBoxColumn";
            this.lastUpdateDataGridViewTextBoxColumn.ReadOnly = true;
            this.lastUpdateDataGridViewTextBoxColumn.Width = 120;
            // 
            // bankLogoBindingSource
            // 
            this.bankLogoBindingSource.DataSource = typeof(SystemSettingTool.tbSystemSetting);
            this.bankLogoBindingSource.Filter = "";
            // 
            // bankLogoBindingNavigator
            // 
            this.bankLogoBindingNavigator.AddNewItem = null;
            this.bankLogoBindingNavigator.BindingSource = this.bankLogoBindingSource;
            this.bankLogoBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.bankLogoBindingNavigator.DeleteItem = null;
            this.bankLogoBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bankLogo_bindingNavigatorAddNewItem,
            this.bankLogo_bindingNavigatorDeleteItem,
            this.bankLogo_bindingNavigatorEditItem});
            this.bankLogoBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bankLogoBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bankLogoBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bankLogoBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bankLogoBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bankLogoBindingNavigator.Name = "bankLogoBindingNavigator";
            this.bankLogoBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.bankLogoBindingNavigator.Size = new System.Drawing.Size(1030, 30);
            this.bankLogoBindingNavigator.TabIndex = 12;
            this.bankLogoBindingNavigator.Text = "bankLogoBindingNavigator";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(49, 27);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 30);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 30);
            // 
            // bankLogo_bindingNavigatorAddNewItem
            // 
            this.bankLogo_bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bankLogo_bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bankLogo_bindingNavigatorAddNewItem.Image")));
            this.bankLogo_bindingNavigatorAddNewItem.Name = "bankLogo_bindingNavigatorAddNewItem";
            this.bankLogo_bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bankLogo_bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 27);
            this.bankLogo_bindingNavigatorAddNewItem.Text = "Add new";
            this.bankLogo_bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bankLogo_bindingNavigatorAddNewItem_Click);
            // 
            // bankLogo_bindingNavigatorDeleteItem
            // 
            this.bankLogo_bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bankLogo_bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bankLogo_bindingNavigatorDeleteItem.Image")));
            this.bankLogo_bindingNavigatorDeleteItem.Name = "bankLogo_bindingNavigatorDeleteItem";
            this.bankLogo_bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bankLogo_bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 27);
            this.bankLogo_bindingNavigatorDeleteItem.Text = "Delete";
            this.bankLogo_bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bankLogo_bindingNavigatorDeleteItem_Click);
            // 
            // bankLogo_bindingNavigatorEditItem
            // 
            this.bankLogo_bindingNavigatorEditItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bankLogo_bindingNavigatorEditItem.Image = ((System.Drawing.Image)(resources.GetObject("bankLogo_bindingNavigatorEditItem.Image")));
            this.bankLogo_bindingNavigatorEditItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bankLogo_bindingNavigatorEditItem.Name = "bankLogo_bindingNavigatorEditItem";
            this.bankLogo_bindingNavigatorEditItem.Size = new System.Drawing.Size(23, 27);
            this.bankLogo_bindingNavigatorEditItem.Text = "Edit";
            this.bankLogo_bindingNavigatorEditItem.Click += new System.EventHandler(this.bankLogo_bindingNavigatorEditItem_Click);
            // 
            // tp_tc
            // 
            this.tp_tc.Controls.Add(this.tableLayoutPanel3);
            this.tp_tc.Location = new System.Drawing.Point(4, 25);
            this.tp_tc.Name = "tp_tc";
            this.tp_tc.Padding = new System.Windows.Forms.Padding(3);
            this.tp_tc.Size = new System.Drawing.Size(1042, 476);
            this.tp_tc.TabIndex = 1;
            this.tp_tc.Text = "T&C";
            this.tp_tc.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.panel4, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.19149F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.80851F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1036, 470);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.termsAndCondition_btn_cancel);
            this.panel4.Controls.Add(this.termsAndCondition_btn_save);
            this.panel4.Controls.Add(this.termsAndCondition_dtp_effectiveTo);
            this.panel4.Controls.Add(this.termsAndCondition_dtp_effectiveFrom);
            this.panel4.Controls.Add(this.termsAndCondition_tb_statementType);
            this.panel4.Controls.Add(this.termsAndCondtion_btn_pathSelect);
            this.panel4.Controls.Add(this.termsAndCondition_tb_path);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 300);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1030, 167);
            this.panel4.TabIndex = 2;
            // 
            // termsAndCondition_btn_cancel
            // 
            this.termsAndCondition_btn_cancel.Location = new System.Drawing.Point(837, 59);
            this.termsAndCondition_btn_cancel.Name = "termsAndCondition_btn_cancel";
            this.termsAndCondition_btn_cancel.Size = new System.Drawing.Size(125, 66);
            this.termsAndCondition_btn_cancel.TabIndex = 11;
            this.termsAndCondition_btn_cancel.Text = "Cancel";
            this.termsAndCondition_btn_cancel.UseVisualStyleBackColor = true;
            this.termsAndCondition_btn_cancel.Click += new System.EventHandler(this.termsAndCondition_btn_cancel_Click);
            // 
            // termsAndCondition_btn_save
            // 
            this.termsAndCondition_btn_save.Location = new System.Drawing.Point(692, 60);
            this.termsAndCondition_btn_save.Name = "termsAndCondition_btn_save";
            this.termsAndCondition_btn_save.Size = new System.Drawing.Size(125, 66);
            this.termsAndCondition_btn_save.TabIndex = 10;
            this.termsAndCondition_btn_save.Text = "Save";
            this.termsAndCondition_btn_save.UseVisualStyleBackColor = true;
            this.termsAndCondition_btn_save.Click += new System.EventHandler(this.termsAndCondition_btn_save_Click);
            // 
            // termsAndCondition_dtp_effectiveTo
            // 
            this.termsAndCondition_dtp_effectiveTo.CustomFormat = "yyyy-MM-dd";
            this.termsAndCondition_dtp_effectiveTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.termsAndCondition_dtp_effectiveTo.Location = new System.Drawing.Point(186, 113);
            this.termsAndCondition_dtp_effectiveTo.Name = "termsAndCondition_dtp_effectiveTo";
            this.termsAndCondition_dtp_effectiveTo.Size = new System.Drawing.Size(200, 22);
            this.termsAndCondition_dtp_effectiveTo.TabIndex = 9;
            // 
            // termsAndCondition_dtp_effectiveFrom
            // 
            this.termsAndCondition_dtp_effectiveFrom.CustomFormat = "yyyy-MM-dd";
            this.termsAndCondition_dtp_effectiveFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.termsAndCondition_dtp_effectiveFrom.Location = new System.Drawing.Point(186, 85);
            this.termsAndCondition_dtp_effectiveFrom.Name = "termsAndCondition_dtp_effectiveFrom";
            this.termsAndCondition_dtp_effectiveFrom.Size = new System.Drawing.Size(200, 22);
            this.termsAndCondition_dtp_effectiveFrom.TabIndex = 8;
            // 
            // termsAndCondition_tb_statementType
            // 
            this.termsAndCondition_tb_statementType.Location = new System.Drawing.Point(186, 59);
            this.termsAndCondition_tb_statementType.Name = "termsAndCondition_tb_statementType";
            this.termsAndCondition_tb_statementType.Size = new System.Drawing.Size(303, 22);
            this.termsAndCondition_tb_statementType.TabIndex = 7;
            // 
            // termsAndCondtion_btn_pathSelect
            // 
            this.termsAndCondtion_btn_pathSelect.Location = new System.Drawing.Point(516, 33);
            this.termsAndCondtion_btn_pathSelect.Name = "termsAndCondtion_btn_pathSelect";
            this.termsAndCondtion_btn_pathSelect.Size = new System.Drawing.Size(75, 23);
            this.termsAndCondtion_btn_pathSelect.TabIndex = 6;
            this.termsAndCondtion_btn_pathSelect.Text = "Select";
            this.termsAndCondtion_btn_pathSelect.UseVisualStyleBackColor = true;
            this.termsAndCondtion_btn_pathSelect.Click += new System.EventHandler(this.termsAndCondtion_btn_pathSelect_Click);
            // 
            // termsAndCondition_tb_path
            // 
            this.termsAndCondition_tb_path.Location = new System.Drawing.Point(186, 33);
            this.termsAndCondition_tb_path.Name = "termsAndCondition_tb_path";
            this.termsAndCondition_tb_path.Size = new System.Drawing.Size(303, 22);
            this.termsAndCondition_tb_path.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Effective To";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Effective From";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Statement Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(32, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "File Path";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.gv_termsAndCondition);
            this.panel3.Controls.Add(this.termsAndConditionBindingNavigator);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1030, 291);
            this.panel3.TabIndex = 0;
            // 
            // gv_termsAndCondition
            // 
            this.gv_termsAndCondition.AllowUserToAddRows = false;
            this.gv_termsAndCondition.AllowUserToDeleteRows = false;
            this.gv_termsAndCondition.AllowUserToResizeColumns = false;
            this.gv_termsAndCondition.AllowUserToResizeRows = false;
            this.gv_termsAndCondition.AutoGenerateColumns = false;
            this.gv_termsAndCondition.BackgroundColor = System.Drawing.Color.White;
            this.gv_termsAndCondition.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gv_termsAndCondition.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_termsAndCondition.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn1,
            this.settingNameDataGridViewTextBoxColumn1,
            this.settingValueDataGridViewTextBoxColumn1,
            this.statementTypeDataGridViewTextBoxColumn1,
            this.effectiveFromDataGridViewTextBoxColumn1,
            this.effectiveToDataGridViewTextBoxColumn1,
            this.activeDataGridViewTextBoxColumn,
            this.createDateDataGridViewTextBoxColumn1,
            this.createByDataGridViewTextBoxColumn1,
            this.modifyDateDataGridViewImageColumn1,
            this.modifyByDataGridViewTextBoxColumn1,
            this.lastUpdateDataGridViewTextBoxColumn1});
            this.gv_termsAndCondition.DataSource = this.termsAndConditionBindingSource;
            this.gv_termsAndCondition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gv_termsAndCondition.EnableHeadersVisualStyles = false;
            this.gv_termsAndCondition.Location = new System.Drawing.Point(0, 30);
            this.gv_termsAndCondition.MultiSelect = false;
            this.gv_termsAndCondition.Name = "gv_termsAndCondition";
            this.gv_termsAndCondition.ReadOnly = true;
            this.gv_termsAndCondition.RowHeadersVisible = false;
            this.gv_termsAndCondition.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gv_termsAndCondition.RowTemplate.Height = 24;
            this.gv_termsAndCondition.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gv_termsAndCondition.Size = new System.Drawing.Size(1030, 261);
            this.gv_termsAndCondition.TabIndex = 1;
            // 
            // iDDataGridViewTextBoxColumn1
            // 
            this.iDDataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn1.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn1.Name = "iDDataGridViewTextBoxColumn1";
            this.iDDataGridViewTextBoxColumn1.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn1.Visible = false;
            // 
            // settingNameDataGridViewTextBoxColumn1
            // 
            this.settingNameDataGridViewTextBoxColumn1.DataPropertyName = "SettingName";
            this.settingNameDataGridViewTextBoxColumn1.HeaderText = "SettingName";
            this.settingNameDataGridViewTextBoxColumn1.Name = "settingNameDataGridViewTextBoxColumn1";
            this.settingNameDataGridViewTextBoxColumn1.ReadOnly = true;
            this.settingNameDataGridViewTextBoxColumn1.Visible = false;
            // 
            // settingValueDataGridViewTextBoxColumn1
            // 
            this.settingValueDataGridViewTextBoxColumn1.DataPropertyName = "SettingValue";
            this.settingValueDataGridViewTextBoxColumn1.HeaderText = "File Path";
            this.settingValueDataGridViewTextBoxColumn1.Name = "settingValueDataGridViewTextBoxColumn1";
            this.settingValueDataGridViewTextBoxColumn1.ReadOnly = true;
            this.settingValueDataGridViewTextBoxColumn1.Width = 200;
            // 
            // statementTypeDataGridViewTextBoxColumn1
            // 
            this.statementTypeDataGridViewTextBoxColumn1.DataPropertyName = "StatementType";
            this.statementTypeDataGridViewTextBoxColumn1.HeaderText = "Statement Type";
            this.statementTypeDataGridViewTextBoxColumn1.Name = "statementTypeDataGridViewTextBoxColumn1";
            this.statementTypeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.statementTypeDataGridViewTextBoxColumn1.Width = 120;
            // 
            // effectiveFromDataGridViewTextBoxColumn1
            // 
            this.effectiveFromDataGridViewTextBoxColumn1.DataPropertyName = "EffectiveFrom";
            this.effectiveFromDataGridViewTextBoxColumn1.HeaderText = "Effective From";
            this.effectiveFromDataGridViewTextBoxColumn1.Name = "effectiveFromDataGridViewTextBoxColumn1";
            this.effectiveFromDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // effectiveToDataGridViewTextBoxColumn1
            // 
            this.effectiveToDataGridViewTextBoxColumn1.DataPropertyName = "EffectiveTo";
            this.effectiveToDataGridViewTextBoxColumn1.HeaderText = "Effective To";
            this.effectiveToDataGridViewTextBoxColumn1.Name = "effectiveToDataGridViewTextBoxColumn1";
            this.effectiveToDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // activeDataGridViewTextBoxColumn
            // 
            this.activeDataGridViewTextBoxColumn.DataPropertyName = "Active";
            this.activeDataGridViewTextBoxColumn.HeaderText = "Active";
            this.activeDataGridViewTextBoxColumn.Name = "activeDataGridViewTextBoxColumn";
            this.activeDataGridViewTextBoxColumn.ReadOnly = true;
            this.activeDataGridViewTextBoxColumn.Visible = false;
            // 
            // createDateDataGridViewTextBoxColumn1
            // 
            this.createDateDataGridViewTextBoxColumn1.DataPropertyName = "CreateDate";
            this.createDateDataGridViewTextBoxColumn1.HeaderText = "Create Date";
            this.createDateDataGridViewTextBoxColumn1.Name = "createDateDataGridViewTextBoxColumn1";
            this.createDateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.createDateDataGridViewTextBoxColumn1.Width = 120;
            // 
            // createByDataGridViewTextBoxColumn1
            // 
            this.createByDataGridViewTextBoxColumn1.DataPropertyName = "CreateBy";
            this.createByDataGridViewTextBoxColumn1.HeaderText = "CreateBy";
            this.createByDataGridViewTextBoxColumn1.Name = "createByDataGridViewTextBoxColumn1";
            this.createByDataGridViewTextBoxColumn1.ReadOnly = true;
            this.createByDataGridViewTextBoxColumn1.Visible = false;
            // 
            // modifyDateDataGridViewImageColumn1
            // 
            this.modifyDateDataGridViewImageColumn1.DataPropertyName = "ModifyDate";
            this.modifyDateDataGridViewImageColumn1.HeaderText = "ModifyDate";
            this.modifyDateDataGridViewImageColumn1.Name = "modifyDateDataGridViewImageColumn1";
            this.modifyDateDataGridViewImageColumn1.ReadOnly = true;
            this.modifyDateDataGridViewImageColumn1.Visible = false;
            // 
            // modifyByDataGridViewTextBoxColumn1
            // 
            this.modifyByDataGridViewTextBoxColumn1.DataPropertyName = "ModifyBy";
            this.modifyByDataGridViewTextBoxColumn1.HeaderText = "ModifyBy";
            this.modifyByDataGridViewTextBoxColumn1.Name = "modifyByDataGridViewTextBoxColumn1";
            this.modifyByDataGridViewTextBoxColumn1.ReadOnly = true;
            this.modifyByDataGridViewTextBoxColumn1.Visible = false;
            // 
            // lastUpdateDataGridViewTextBoxColumn1
            // 
            this.lastUpdateDataGridViewTextBoxColumn1.DataPropertyName = "LastUpdate";
            this.lastUpdateDataGridViewTextBoxColumn1.HeaderText = "Last Update";
            this.lastUpdateDataGridViewTextBoxColumn1.Name = "lastUpdateDataGridViewTextBoxColumn1";
            this.lastUpdateDataGridViewTextBoxColumn1.ReadOnly = true;
            this.lastUpdateDataGridViewTextBoxColumn1.Width = 120;
            // 
            // termsAndConditionBindingSource
            // 
            this.termsAndConditionBindingSource.DataSource = typeof(SystemSettingTool.tbSystemSetting);
            // 
            // termsAndConditionBindingNavigator
            // 
            this.termsAndConditionBindingNavigator.AddNewItem = null;
            this.termsAndConditionBindingNavigator.BindingSource = this.termsAndConditionBindingSource;
            this.termsAndConditionBindingNavigator.CountItem = this.bindingNavigatorCountItem1;
            this.termsAndConditionBindingNavigator.DeleteItem = null;
            this.termsAndConditionBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem1,
            this.bindingNavigatorMovePreviousItem1,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPositionItem1,
            this.bindingNavigatorCountItem1,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorMoveNextItem1,
            this.bindingNavigatorMoveLastItem1,
            this.bindingNavigatorSeparator5,
            this.termsAndCondition_bindingNavigatorAddNewItem,
            this.termsAndCondition_bindingNavigatorDeleteItem,
            this.termsAndCondition_bindingNavigatorEditItem});
            this.termsAndConditionBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.termsAndConditionBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem1;
            this.termsAndConditionBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem1;
            this.termsAndConditionBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem1;
            this.termsAndConditionBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem1;
            this.termsAndConditionBindingNavigator.Name = "termsAndConditionBindingNavigator";
            this.termsAndConditionBindingNavigator.PositionItem = this.bindingNavigatorPositionItem1;
            this.termsAndConditionBindingNavigator.Size = new System.Drawing.Size(1030, 30);
            this.termsAndConditionBindingNavigator.TabIndex = 0;
            this.termsAndConditionBindingNavigator.Text = "termsAndConditionBindingNavigator";
            // 
            // bindingNavigatorCountItem1
            // 
            this.bindingNavigatorCountItem1.Name = "bindingNavigatorCountItem1";
            this.bindingNavigatorCountItem1.Size = new System.Drawing.Size(49, 27);
            this.bindingNavigatorCountItem1.Text = "of {0}";
            this.bindingNavigatorCountItem1.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem1
            // 
            this.bindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem1.Image")));
            this.bindingNavigatorMoveFirstItem1.Name = "bindingNavigatorMoveFirstItem1";
            this.bindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem1.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveFirstItem1.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem1
            // 
            this.bindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem1.Image")));
            this.bindingNavigatorMovePreviousItem1.Name = "bindingNavigatorMovePreviousItem1";
            this.bindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem1.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMovePreviousItem1.Text = "Move previous";
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem1
            // 
            this.bindingNavigatorPositionItem1.AccessibleName = "Position";
            this.bindingNavigatorPositionItem1.AutoSize = false;
            this.bindingNavigatorPositionItem1.Name = "bindingNavigatorPositionItem1";
            this.bindingNavigatorPositionItem1.Size = new System.Drawing.Size(50, 30);
            this.bindingNavigatorPositionItem1.Text = "0";
            this.bindingNavigatorPositionItem1.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorMoveNextItem1
            // 
            this.bindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem1.Image")));
            this.bindingNavigatorMoveNextItem1.Name = "bindingNavigatorMoveNextItem1";
            this.bindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem1.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveNextItem1.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem1
            // 
            this.bindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem1.Image")));
            this.bindingNavigatorMoveLastItem1.Name = "bindingNavigatorMoveLastItem1";
            this.bindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem1.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveLastItem1.Text = "Move last";
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 30);
            // 
            // termsAndCondition_bindingNavigatorAddNewItem
            // 
            this.termsAndCondition_bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.termsAndCondition_bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("termsAndCondition_bindingNavigatorAddNewItem.Image")));
            this.termsAndCondition_bindingNavigatorAddNewItem.Name = "termsAndCondition_bindingNavigatorAddNewItem";
            this.termsAndCondition_bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.termsAndCondition_bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 27);
            this.termsAndCondition_bindingNavigatorAddNewItem.Text = "Add new";
            this.termsAndCondition_bindingNavigatorAddNewItem.Click += new System.EventHandler(this.termsAndCondition_bindingNavigatorAddNewItem_Click);
            // 
            // termsAndCondition_bindingNavigatorDeleteItem
            // 
            this.termsAndCondition_bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.termsAndCondition_bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("termsAndCondition_bindingNavigatorDeleteItem.Image")));
            this.termsAndCondition_bindingNavigatorDeleteItem.Name = "termsAndCondition_bindingNavigatorDeleteItem";
            this.termsAndCondition_bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.termsAndCondition_bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 27);
            this.termsAndCondition_bindingNavigatorDeleteItem.Text = "Delete";
            this.termsAndCondition_bindingNavigatorDeleteItem.Click += new System.EventHandler(this.termsAndCondition_bindingNavigatorDeleteItem_Click);
            // 
            // termsAndCondition_bindingNavigatorEditItem
            // 
            this.termsAndCondition_bindingNavigatorEditItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.termsAndCondition_bindingNavigatorEditItem.Image = ((System.Drawing.Image)(resources.GetObject("termsAndCondition_bindingNavigatorEditItem.Image")));
            this.termsAndCondition_bindingNavigatorEditItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.termsAndCondition_bindingNavigatorEditItem.Name = "termsAndCondition_bindingNavigatorEditItem";
            this.termsAndCondition_bindingNavigatorEditItem.Size = new System.Drawing.Size(23, 27);
            this.termsAndCondition_bindingNavigatorEditItem.Text = "toolStripButton1";
            this.termsAndCondition_bindingNavigatorEditItem.Click += new System.EventHandler(this.termsAndCondition_bindingNavigatorEditItem_Click);
            // 
            // tp_marketingInsert
            // 
            this.tp_marketingInsert.Controls.Add(this.tableLayoutPanel4);
            this.tp_marketingInsert.Location = new System.Drawing.Point(4, 25);
            this.tp_marketingInsert.Name = "tp_marketingInsert";
            this.tp_marketingInsert.Padding = new System.Windows.Forms.Padding(3);
            this.tp_marketingInsert.Size = new System.Drawing.Size(1042, 476);
            this.tp_marketingInsert.TabIndex = 2;
            this.tp_marketingInsert.Text = "Marketing Insert";
            this.tp_marketingInsert.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.panel6, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.19149F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.80851F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1036, 470);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.marketInsert_tb_insertID);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.marketInsert_btn_cancel);
            this.panel6.Controls.Add(this.marketInsert_btn_save);
            this.panel6.Controls.Add(this.marketInsert_dtp_effectiveTo);
            this.panel6.Controls.Add(this.marketInsert_dtp_effectiveFrom);
            this.panel6.Controls.Add(this.marketInsert_tb_statementType);
            this.panel6.Controls.Add(this.marketInsert_btn_pathSelect);
            this.panel6.Controls.Add(this.marketInsert_tb_path);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 300);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1030, 167);
            this.panel6.TabIndex = 3;
            // 
            // marketInsert_tb_insertID
            // 
            this.marketInsert_tb_insertID.Location = new System.Drawing.Point(186, 21);
            this.marketInsert_tb_insertID.Name = "marketInsert_tb_insertID";
            this.marketInsert_tb_insertID.Size = new System.Drawing.Size(303, 22);
            this.marketInsert_tb_insertID.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(32, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 17);
            this.label13.TabIndex = 12;
            this.label13.Text = "Insert ID";
            // 
            // marketInsert_btn_cancel
            // 
            this.marketInsert_btn_cancel.Location = new System.Drawing.Point(837, 59);
            this.marketInsert_btn_cancel.Name = "marketInsert_btn_cancel";
            this.marketInsert_btn_cancel.Size = new System.Drawing.Size(125, 66);
            this.marketInsert_btn_cancel.TabIndex = 11;
            this.marketInsert_btn_cancel.Text = "Cancel";
            this.marketInsert_btn_cancel.UseVisualStyleBackColor = true;
            this.marketInsert_btn_cancel.Click += new System.EventHandler(this.marketInsert_btn_cancel_Click);
            // 
            // marketInsert_btn_save
            // 
            this.marketInsert_btn_save.Location = new System.Drawing.Point(692, 60);
            this.marketInsert_btn_save.Name = "marketInsert_btn_save";
            this.marketInsert_btn_save.Size = new System.Drawing.Size(125, 66);
            this.marketInsert_btn_save.TabIndex = 10;
            this.marketInsert_btn_save.Text = "Save";
            this.marketInsert_btn_save.UseVisualStyleBackColor = true;
            this.marketInsert_btn_save.Click += new System.EventHandler(this.marketInsert_btn_save_Click);
            // 
            // marketInsert_dtp_effectiveTo
            // 
            this.marketInsert_dtp_effectiveTo.CustomFormat = "yyyy-MM-dd";
            this.marketInsert_dtp_effectiveTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.marketInsert_dtp_effectiveTo.Location = new System.Drawing.Point(186, 128);
            this.marketInsert_dtp_effectiveTo.Name = "marketInsert_dtp_effectiveTo";
            this.marketInsert_dtp_effectiveTo.Size = new System.Drawing.Size(200, 22);
            this.marketInsert_dtp_effectiveTo.TabIndex = 9;
            // 
            // marketInsert_dtp_effectiveFrom
            // 
            this.marketInsert_dtp_effectiveFrom.CustomFormat = "yyyy-MM-dd";
            this.marketInsert_dtp_effectiveFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.marketInsert_dtp_effectiveFrom.Location = new System.Drawing.Point(186, 100);
            this.marketInsert_dtp_effectiveFrom.Name = "marketInsert_dtp_effectiveFrom";
            this.marketInsert_dtp_effectiveFrom.Size = new System.Drawing.Size(200, 22);
            this.marketInsert_dtp_effectiveFrom.TabIndex = 8;
            // 
            // marketInsert_tb_statementType
            // 
            this.marketInsert_tb_statementType.Location = new System.Drawing.Point(186, 74);
            this.marketInsert_tb_statementType.Name = "marketInsert_tb_statementType";
            this.marketInsert_tb_statementType.Size = new System.Drawing.Size(303, 22);
            this.marketInsert_tb_statementType.TabIndex = 7;
            // 
            // marketInsert_btn_pathSelect
            // 
            this.marketInsert_btn_pathSelect.Location = new System.Drawing.Point(516, 48);
            this.marketInsert_btn_pathSelect.Name = "marketInsert_btn_pathSelect";
            this.marketInsert_btn_pathSelect.Size = new System.Drawing.Size(75, 23);
            this.marketInsert_btn_pathSelect.TabIndex = 6;
            this.marketInsert_btn_pathSelect.Text = "Select";
            this.marketInsert_btn_pathSelect.UseVisualStyleBackColor = true;
            this.marketInsert_btn_pathSelect.Click += new System.EventHandler(this.marketInsert_btn_pathSelect_Click);
            // 
            // marketInsert_tb_path
            // 
            this.marketInsert_tb_path.Location = new System.Drawing.Point(186, 48);
            this.marketInsert_tb_path.Name = "marketInsert_tb_path";
            this.marketInsert_tb_path.Size = new System.Drawing.Size(303, 22);
            this.marketInsert_tb_path.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 128);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 17);
            this.label9.TabIndex = 4;
            this.label9.Text = "Effective To";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(32, 100);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 17);
            this.label10.TabIndex = 3;
            this.label10.Text = "Effective From";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(32, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 17);
            this.label11.TabIndex = 2;
            this.label11.Text = "Statement Type";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(32, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 17);
            this.label12.TabIndex = 1;
            this.label12.Text = "File Path";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.gv_marketInsert);
            this.panel5.Controls.Add(this.marketInsertbindingNavigator);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1030, 291);
            this.panel5.TabIndex = 0;
            // 
            // gv_marketInsert
            // 
            this.gv_marketInsert.AllowUserToAddRows = false;
            this.gv_marketInsert.AllowUserToDeleteRows = false;
            this.gv_marketInsert.AllowUserToOrderColumns = true;
            this.gv_marketInsert.AllowUserToResizeColumns = false;
            this.gv_marketInsert.AllowUserToResizeRows = false;
            this.gv_marketInsert.AutoGenerateColumns = false;
            this.gv_marketInsert.BackgroundColor = System.Drawing.Color.White;
            this.gv_marketInsert.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gv_marketInsert.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_marketInsert.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn2,
            this.marketInsertIDDataGridViewTextBoxColumn,
            this.marketingPathDataGridViewTextBoxColumn,
            this.statementTypeDataGridViewTextBoxColumn2,
            this.effectiveFromDataGridViewTextBoxColumn2,
            this.effectiveToDataGridViewTextBoxColumn2,
            this.activeDataGridViewTextBoxColumn1,
            this.createDateDataGridViewTextBoxColumn2,
            this.createByDataGridViewTextBoxColumn2,
            this.modifyDateDataGridViewImageColumn2,
            this.modifyByDataGridViewTextBoxColumn2,
            this.lastUpdateDataGridViewTextBoxColumn2});
            this.gv_marketInsert.DataSource = this.marketInsertBindingSource;
            this.gv_marketInsert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gv_marketInsert.EnableHeadersVisualStyles = false;
            this.gv_marketInsert.Location = new System.Drawing.Point(0, 30);
            this.gv_marketInsert.MultiSelect = false;
            this.gv_marketInsert.Name = "gv_marketInsert";
            this.gv_marketInsert.ReadOnly = true;
            this.gv_marketInsert.RowHeadersVisible = false;
            this.gv_marketInsert.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gv_marketInsert.RowTemplate.Height = 24;
            this.gv_marketInsert.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gv_marketInsert.Size = new System.Drawing.Size(1030, 261);
            this.gv_marketInsert.TabIndex = 1;
            // 
            // iDDataGridViewTextBoxColumn2
            // 
            this.iDDataGridViewTextBoxColumn2.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn2.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn2.Name = "iDDataGridViewTextBoxColumn2";
            this.iDDataGridViewTextBoxColumn2.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn2.Visible = false;
            // 
            // marketInsertIDDataGridViewTextBoxColumn
            // 
            this.marketInsertIDDataGridViewTextBoxColumn.DataPropertyName = "MarketInsertID";
            this.marketInsertIDDataGridViewTextBoxColumn.HeaderText = "Insert ID";
            this.marketInsertIDDataGridViewTextBoxColumn.Name = "marketInsertIDDataGridViewTextBoxColumn";
            this.marketInsertIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.marketInsertIDDataGridViewTextBoxColumn.Width = 75;
            // 
            // marketingPathDataGridViewTextBoxColumn
            // 
            this.marketingPathDataGridViewTextBoxColumn.DataPropertyName = "MarketingPath";
            this.marketingPathDataGridViewTextBoxColumn.HeaderText = "File Path";
            this.marketingPathDataGridViewTextBoxColumn.Name = "marketingPathDataGridViewTextBoxColumn";
            this.marketingPathDataGridViewTextBoxColumn.ReadOnly = true;
            this.marketingPathDataGridViewTextBoxColumn.Width = 160;
            // 
            // statementTypeDataGridViewTextBoxColumn2
            // 
            this.statementTypeDataGridViewTextBoxColumn2.DataPropertyName = "StatementType";
            this.statementTypeDataGridViewTextBoxColumn2.HeaderText = "Statement Type";
            this.statementTypeDataGridViewTextBoxColumn2.Name = "statementTypeDataGridViewTextBoxColumn2";
            this.statementTypeDataGridViewTextBoxColumn2.ReadOnly = true;
            this.statementTypeDataGridViewTextBoxColumn2.Width = 125;
            // 
            // effectiveFromDataGridViewTextBoxColumn2
            // 
            this.effectiveFromDataGridViewTextBoxColumn2.DataPropertyName = "EffectiveFrom";
            this.effectiveFromDataGridViewTextBoxColumn2.HeaderText = "Effective From";
            this.effectiveFromDataGridViewTextBoxColumn2.Name = "effectiveFromDataGridViewTextBoxColumn2";
            this.effectiveFromDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // effectiveToDataGridViewTextBoxColumn2
            // 
            this.effectiveToDataGridViewTextBoxColumn2.DataPropertyName = "EffectiveTo";
            this.effectiveToDataGridViewTextBoxColumn2.HeaderText = "Effective To";
            this.effectiveToDataGridViewTextBoxColumn2.Name = "effectiveToDataGridViewTextBoxColumn2";
            this.effectiveToDataGridViewTextBoxColumn2.ReadOnly = true;
            this.effectiveToDataGridViewTextBoxColumn2.Width = 95;
            // 
            // activeDataGridViewTextBoxColumn1
            // 
            this.activeDataGridViewTextBoxColumn1.DataPropertyName = "Active";
            this.activeDataGridViewTextBoxColumn1.HeaderText = "Active";
            this.activeDataGridViewTextBoxColumn1.Name = "activeDataGridViewTextBoxColumn1";
            this.activeDataGridViewTextBoxColumn1.ReadOnly = true;
            this.activeDataGridViewTextBoxColumn1.Visible = false;
            // 
            // createDateDataGridViewTextBoxColumn2
            // 
            this.createDateDataGridViewTextBoxColumn2.DataPropertyName = "CreateDate";
            this.createDateDataGridViewTextBoxColumn2.HeaderText = "Create Date";
            this.createDateDataGridViewTextBoxColumn2.Name = "createDateDataGridViewTextBoxColumn2";
            this.createDateDataGridViewTextBoxColumn2.ReadOnly = true;
            this.createDateDataGridViewTextBoxColumn2.Width = 105;
            // 
            // createByDataGridViewTextBoxColumn2
            // 
            this.createByDataGridViewTextBoxColumn2.DataPropertyName = "CreateBy";
            this.createByDataGridViewTextBoxColumn2.HeaderText = "CreateBy";
            this.createByDataGridViewTextBoxColumn2.Name = "createByDataGridViewTextBoxColumn2";
            this.createByDataGridViewTextBoxColumn2.ReadOnly = true;
            this.createByDataGridViewTextBoxColumn2.Visible = false;
            // 
            // modifyDateDataGridViewImageColumn2
            // 
            this.modifyDateDataGridViewImageColumn2.DataPropertyName = "ModifyDate";
            this.modifyDateDataGridViewImageColumn2.HeaderText = "ModifyDate";
            this.modifyDateDataGridViewImageColumn2.Name = "modifyDateDataGridViewImageColumn2";
            this.modifyDateDataGridViewImageColumn2.ReadOnly = true;
            this.modifyDateDataGridViewImageColumn2.Visible = false;
            // 
            // modifyByDataGridViewTextBoxColumn2
            // 
            this.modifyByDataGridViewTextBoxColumn2.DataPropertyName = "ModifyBy";
            this.modifyByDataGridViewTextBoxColumn2.HeaderText = "ModifyBy";
            this.modifyByDataGridViewTextBoxColumn2.Name = "modifyByDataGridViewTextBoxColumn2";
            this.modifyByDataGridViewTextBoxColumn2.ReadOnly = true;
            this.modifyByDataGridViewTextBoxColumn2.Visible = false;
            // 
            // lastUpdateDataGridViewTextBoxColumn2
            // 
            this.lastUpdateDataGridViewTextBoxColumn2.DataPropertyName = "LastUpdate";
            this.lastUpdateDataGridViewTextBoxColumn2.HeaderText = "Last Update";
            this.lastUpdateDataGridViewTextBoxColumn2.Name = "lastUpdateDataGridViewTextBoxColumn2";
            this.lastUpdateDataGridViewTextBoxColumn2.ReadOnly = true;
            this.lastUpdateDataGridViewTextBoxColumn2.Width = 105;
            // 
            // marketInsertBindingSource
            // 
            this.marketInsertBindingSource.DataSource = typeof(SystemSettingTool.tbMarketInsert);
            // 
            // marketInsertbindingNavigator
            // 
            this.marketInsertbindingNavigator.AddNewItem = null;
            this.marketInsertbindingNavigator.BindingSource = this.marketInsertBindingSource;
            this.marketInsertbindingNavigator.CountItem = this.bindingNavigatorCountItem2;
            this.marketInsertbindingNavigator.DeleteItem = null;
            this.marketInsertbindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem2,
            this.bindingNavigatorMovePreviousItem2,
            this.bindingNavigatorSeparator6,
            this.bindingNavigatorPositionItem2,
            this.bindingNavigatorCountItem2,
            this.bindingNavigatorSeparator7,
            this.bindingNavigatorMoveNextItem2,
            this.bindingNavigatorMoveLastItem2,
            this.bindingNavigatorSeparator8,
            this.marketInsert_bindingNavigatorAddNewItem,
            this.marketInsert_bindingNavigatorDeleteItem,
            this.marketInsert_bindingNavigatorEditItem});
            this.marketInsertbindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.marketInsertbindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem2;
            this.marketInsertbindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem2;
            this.marketInsertbindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem2;
            this.marketInsertbindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem2;
            this.marketInsertbindingNavigator.Name = "marketInsertbindingNavigator";
            this.marketInsertbindingNavigator.PositionItem = this.bindingNavigatorPositionItem2;
            this.marketInsertbindingNavigator.Size = new System.Drawing.Size(1030, 30);
            this.marketInsertbindingNavigator.TabIndex = 0;
            this.marketInsertbindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem2
            // 
            this.bindingNavigatorCountItem2.Name = "bindingNavigatorCountItem2";
            this.bindingNavigatorCountItem2.Size = new System.Drawing.Size(49, 27);
            this.bindingNavigatorCountItem2.Text = "of {0}";
            this.bindingNavigatorCountItem2.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem2
            // 
            this.bindingNavigatorMoveFirstItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem2.Image")));
            this.bindingNavigatorMoveFirstItem2.Name = "bindingNavigatorMoveFirstItem2";
            this.bindingNavigatorMoveFirstItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem2.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveFirstItem2.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem2
            // 
            this.bindingNavigatorMovePreviousItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem2.Image")));
            this.bindingNavigatorMovePreviousItem2.Name = "bindingNavigatorMovePreviousItem2";
            this.bindingNavigatorMovePreviousItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem2.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMovePreviousItem2.Text = "Move previous";
            // 
            // bindingNavigatorSeparator6
            // 
            this.bindingNavigatorSeparator6.Name = "bindingNavigatorSeparator6";
            this.bindingNavigatorSeparator6.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem2
            // 
            this.bindingNavigatorPositionItem2.AccessibleName = "Position";
            this.bindingNavigatorPositionItem2.AutoSize = false;
            this.bindingNavigatorPositionItem2.Name = "bindingNavigatorPositionItem2";
            this.bindingNavigatorPositionItem2.Size = new System.Drawing.Size(50, 30);
            this.bindingNavigatorPositionItem2.Text = "0";
            this.bindingNavigatorPositionItem2.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator7
            // 
            this.bindingNavigatorSeparator7.Name = "bindingNavigatorSeparator7";
            this.bindingNavigatorSeparator7.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorMoveNextItem2
            // 
            this.bindingNavigatorMoveNextItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem2.Image")));
            this.bindingNavigatorMoveNextItem2.Name = "bindingNavigatorMoveNextItem2";
            this.bindingNavigatorMoveNextItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem2.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveNextItem2.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem2
            // 
            this.bindingNavigatorMoveLastItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem2.Image")));
            this.bindingNavigatorMoveLastItem2.Name = "bindingNavigatorMoveLastItem2";
            this.bindingNavigatorMoveLastItem2.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem2.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveLastItem2.Text = "Move last";
            // 
            // bindingNavigatorSeparator8
            // 
            this.bindingNavigatorSeparator8.Name = "bindingNavigatorSeparator8";
            this.bindingNavigatorSeparator8.Size = new System.Drawing.Size(6, 30);
            // 
            // marketInsert_bindingNavigatorAddNewItem
            // 
            this.marketInsert_bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.marketInsert_bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("marketInsert_bindingNavigatorAddNewItem.Image")));
            this.marketInsert_bindingNavigatorAddNewItem.Name = "marketInsert_bindingNavigatorAddNewItem";
            this.marketInsert_bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.marketInsert_bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 27);
            this.marketInsert_bindingNavigatorAddNewItem.Text = "Add new";
            this.marketInsert_bindingNavigatorAddNewItem.Click += new System.EventHandler(this.marketInsert_bindingNavigatorAddNewItem_Click);
            // 
            // marketInsert_bindingNavigatorDeleteItem
            // 
            this.marketInsert_bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.marketInsert_bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("marketInsert_bindingNavigatorDeleteItem.Image")));
            this.marketInsert_bindingNavigatorDeleteItem.Name = "marketInsert_bindingNavigatorDeleteItem";
            this.marketInsert_bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.marketInsert_bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 27);
            this.marketInsert_bindingNavigatorDeleteItem.Text = "Delete";
            this.marketInsert_bindingNavigatorDeleteItem.Click += new System.EventHandler(this.marketInsert_bindingNavigatorDeleteItem_Click);
            // 
            // marketInsert_bindingNavigatorEditItem
            // 
            this.marketInsert_bindingNavigatorEditItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.marketInsert_bindingNavigatorEditItem.Image = ((System.Drawing.Image)(resources.GetObject("marketInsert_bindingNavigatorEditItem.Image")));
            this.marketInsert_bindingNavigatorEditItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.marketInsert_bindingNavigatorEditItem.Name = "marketInsert_bindingNavigatorEditItem";
            this.marketInsert_bindingNavigatorEditItem.Size = new System.Drawing.Size(23, 27);
            this.marketInsert_bindingNavigatorEditItem.Text = "marketInsert_bindingNavigatorEditItem";
            this.marketInsert_bindingNavigatorEditItem.Click += new System.EventHandler(this.marketInsert_bindingNavigatorEditItem_Click);
            // 
            // tp_bankInfoInsert
            // 
            this.tp_bankInfoInsert.Controls.Add(this.tableLayoutPanel5);
            this.tp_bankInfoInsert.Location = new System.Drawing.Point(4, 25);
            this.tp_bankInfoInsert.Name = "tp_bankInfoInsert";
            this.tp_bankInfoInsert.Size = new System.Drawing.Size(1042, 476);
            this.tp_bankInfoInsert.TabIndex = 3;
            this.tp_bankInfoInsert.Text = "Bank-info Insert";
            this.tp_bankInfoInsert.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.panel7, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.panel8, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.19149F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.80851F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1036, 470);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.bankInfoInsert_tb_insertID);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.bankInfoInsert_btn_cancel);
            this.panel7.Controls.Add(this.bankInfoInsert_btn_save);
            this.panel7.Controls.Add(this.bankInfoInsert_dtp_effectiveTo);
            this.panel7.Controls.Add(this.bankInfoInsert_dtp_effectiveFrom);
            this.panel7.Controls.Add(this.bankInfoInsert_tb_statementType);
            this.panel7.Controls.Add(this.bankInfoInsert_btn_pathSelect);
            this.panel7.Controls.Add(this.bankInfoInsert_tb_path);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.label17);
            this.panel7.Controls.Add(this.label18);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 300);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1030, 167);
            this.panel7.TabIndex = 3;
            // 
            // bankInfoInsert_tb_insertID
            // 
            this.bankInfoInsert_tb_insertID.Location = new System.Drawing.Point(186, 21);
            this.bankInfoInsert_tb_insertID.Name = "bankInfoInsert_tb_insertID";
            this.bankInfoInsert_tb_insertID.Size = new System.Drawing.Size(303, 22);
            this.bankInfoInsert_tb_insertID.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(32, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 17);
            this.label14.TabIndex = 12;
            this.label14.Text = "Insert ID";
            // 
            // bankInfoInsert_btn_cancel
            // 
            this.bankInfoInsert_btn_cancel.Location = new System.Drawing.Point(837, 59);
            this.bankInfoInsert_btn_cancel.Name = "bankInfoInsert_btn_cancel";
            this.bankInfoInsert_btn_cancel.Size = new System.Drawing.Size(125, 66);
            this.bankInfoInsert_btn_cancel.TabIndex = 11;
            this.bankInfoInsert_btn_cancel.Text = "Cancel";
            this.bankInfoInsert_btn_cancel.UseVisualStyleBackColor = true;
            this.bankInfoInsert_btn_cancel.Click += new System.EventHandler(this.bankInfoInsert_btn_cancel_Click);
            // 
            // bankInfoInsert_btn_save
            // 
            this.bankInfoInsert_btn_save.Location = new System.Drawing.Point(692, 60);
            this.bankInfoInsert_btn_save.Name = "bankInfoInsert_btn_save";
            this.bankInfoInsert_btn_save.Size = new System.Drawing.Size(125, 66);
            this.bankInfoInsert_btn_save.TabIndex = 10;
            this.bankInfoInsert_btn_save.Text = "Save";
            this.bankInfoInsert_btn_save.UseVisualStyleBackColor = true;
            this.bankInfoInsert_btn_save.Click += new System.EventHandler(this.bankInfoInsert_btn_save_Click);
            // 
            // bankInfoInsert_dtp_effectiveTo
            // 
            this.bankInfoInsert_dtp_effectiveTo.CustomFormat = "yyyy-MM-dd";
            this.bankInfoInsert_dtp_effectiveTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.bankInfoInsert_dtp_effectiveTo.Location = new System.Drawing.Point(186, 128);
            this.bankInfoInsert_dtp_effectiveTo.Name = "bankInfoInsert_dtp_effectiveTo";
            this.bankInfoInsert_dtp_effectiveTo.Size = new System.Drawing.Size(200, 22);
            this.bankInfoInsert_dtp_effectiveTo.TabIndex = 9;
            // 
            // bankInfoInsert_dtp_effectiveFrom
            // 
            this.bankInfoInsert_dtp_effectiveFrom.CustomFormat = "yyyy-MM-dd";
            this.bankInfoInsert_dtp_effectiveFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.bankInfoInsert_dtp_effectiveFrom.Location = new System.Drawing.Point(186, 100);
            this.bankInfoInsert_dtp_effectiveFrom.Name = "bankInfoInsert_dtp_effectiveFrom";
            this.bankInfoInsert_dtp_effectiveFrom.Size = new System.Drawing.Size(200, 22);
            this.bankInfoInsert_dtp_effectiveFrom.TabIndex = 8;
            // 
            // bankInfoInsert_tb_statementType
            // 
            this.bankInfoInsert_tb_statementType.Location = new System.Drawing.Point(186, 74);
            this.bankInfoInsert_tb_statementType.Name = "bankInfoInsert_tb_statementType";
            this.bankInfoInsert_tb_statementType.Size = new System.Drawing.Size(303, 22);
            this.bankInfoInsert_tb_statementType.TabIndex = 7;
            // 
            // bankInfoInsert_btn_pathSelect
            // 
            this.bankInfoInsert_btn_pathSelect.Location = new System.Drawing.Point(516, 48);
            this.bankInfoInsert_btn_pathSelect.Name = "bankInfoInsert_btn_pathSelect";
            this.bankInfoInsert_btn_pathSelect.Size = new System.Drawing.Size(75, 23);
            this.bankInfoInsert_btn_pathSelect.TabIndex = 6;
            this.bankInfoInsert_btn_pathSelect.Text = "Select";
            this.bankInfoInsert_btn_pathSelect.UseVisualStyleBackColor = true;
            this.bankInfoInsert_btn_pathSelect.Click += new System.EventHandler(this.bankInfoInsert_btn_pathSelect_Click);
            // 
            // bankInfoInsert_tb_path
            // 
            this.bankInfoInsert_tb_path.Location = new System.Drawing.Point(186, 48);
            this.bankInfoInsert_tb_path.Name = "bankInfoInsert_tb_path";
            this.bankInfoInsert_tb_path.Size = new System.Drawing.Size(303, 22);
            this.bankInfoInsert_tb_path.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(32, 128);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 17);
            this.label15.TabIndex = 4;
            this.label15.Text = "Effective To";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(32, 100);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 17);
            this.label16.TabIndex = 3;
            this.label16.Text = "Effective From";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(32, 74);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(108, 17);
            this.label17.TabIndex = 2;
            this.label17.Text = "Statement Type";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(32, 48);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(63, 17);
            this.label18.TabIndex = 1;
            this.label18.Text = "File Path";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.gv_bankInfoInsert);
            this.panel8.Controls.Add(this.bankInfoInsertbindingNavigator);
            this.panel8.Controls.Add(this.dataGridView1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1030, 291);
            this.panel8.TabIndex = 0;
            // 
            // gv_bankInfoInsert
            // 
            this.gv_bankInfoInsert.AllowUserToAddRows = false;
            this.gv_bankInfoInsert.AllowUserToDeleteRows = false;
            this.gv_bankInfoInsert.AllowUserToResizeColumns = false;
            this.gv_bankInfoInsert.AllowUserToResizeRows = false;
            this.gv_bankInfoInsert.AutoGenerateColumns = false;
            this.gv_bankInfoInsert.BackgroundColor = System.Drawing.Color.White;
            this.gv_bankInfoInsert.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gv_bankInfoInsert.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gv_bankInfoInsert.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn3,
            this.bankInformationIDDataGridViewTextBoxColumn,
            this.bankInformationPathDataGridViewTextBoxColumn,
            this.statementTypeDataGridViewTextBoxColumn3,
            this.effectiveFromDataGridViewTextBoxColumn3,
            this.effectiveToDataGridViewTextBoxColumn3,
            this.activeDataGridViewTextBoxColumn2,
            this.createDateDataGridViewTextBoxColumn3,
            this.createByDataGridViewTextBoxColumn3,
            this.modifyDateDataGridViewImageColumn3,
            this.modifyByDataGridViewTextBoxColumn3,
            this.lastUpdateDataGridViewTextBoxColumn3});
            this.gv_bankInfoInsert.DataSource = this.bankInfoInsertBindingSource;
            this.gv_bankInfoInsert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gv_bankInfoInsert.EnableHeadersVisualStyles = false;
            this.gv_bankInfoInsert.Location = new System.Drawing.Point(0, 30);
            this.gv_bankInfoInsert.MultiSelect = false;
            this.gv_bankInfoInsert.Name = "gv_bankInfoInsert";
            this.gv_bankInfoInsert.ReadOnly = true;
            this.gv_bankInfoInsert.RowHeadersVisible = false;
            this.gv_bankInfoInsert.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gv_bankInfoInsert.RowTemplate.Height = 24;
            this.gv_bankInfoInsert.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gv_bankInfoInsert.Size = new System.Drawing.Size(1030, 261);
            this.gv_bankInfoInsert.TabIndex = 3;
            // 
            // iDDataGridViewTextBoxColumn3
            // 
            this.iDDataGridViewTextBoxColumn3.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn3.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn3.Name = "iDDataGridViewTextBoxColumn3";
            this.iDDataGridViewTextBoxColumn3.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn3.Visible = false;
            // 
            // bankInformationIDDataGridViewTextBoxColumn
            // 
            this.bankInformationIDDataGridViewTextBoxColumn.DataPropertyName = "BankInformationID";
            this.bankInformationIDDataGridViewTextBoxColumn.HeaderText = "Insert ID";
            this.bankInformationIDDataGridViewTextBoxColumn.Name = "bankInformationIDDataGridViewTextBoxColumn";
            this.bankInformationIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.bankInformationIDDataGridViewTextBoxColumn.Width = 75;
            // 
            // bankInformationPathDataGridViewTextBoxColumn
            // 
            this.bankInformationPathDataGridViewTextBoxColumn.DataPropertyName = "BankInformationPath";
            this.bankInformationPathDataGridViewTextBoxColumn.HeaderText = "File Path";
            this.bankInformationPathDataGridViewTextBoxColumn.Name = "bankInformationPathDataGridViewTextBoxColumn";
            this.bankInformationPathDataGridViewTextBoxColumn.ReadOnly = true;
            this.bankInformationPathDataGridViewTextBoxColumn.Width = 160;
            // 
            // statementTypeDataGridViewTextBoxColumn3
            // 
            this.statementTypeDataGridViewTextBoxColumn3.DataPropertyName = "StatementType";
            this.statementTypeDataGridViewTextBoxColumn3.HeaderText = "Statement Type";
            this.statementTypeDataGridViewTextBoxColumn3.Name = "statementTypeDataGridViewTextBoxColumn3";
            this.statementTypeDataGridViewTextBoxColumn3.ReadOnly = true;
            this.statementTypeDataGridViewTextBoxColumn3.Width = 125;
            // 
            // effectiveFromDataGridViewTextBoxColumn3
            // 
            this.effectiveFromDataGridViewTextBoxColumn3.DataPropertyName = "EffectiveFrom";
            this.effectiveFromDataGridViewTextBoxColumn3.HeaderText = "Effective From";
            this.effectiveFromDataGridViewTextBoxColumn3.Name = "effectiveFromDataGridViewTextBoxColumn3";
            this.effectiveFromDataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // effectiveToDataGridViewTextBoxColumn3
            // 
            this.effectiveToDataGridViewTextBoxColumn3.DataPropertyName = "EffectiveTo";
            this.effectiveToDataGridViewTextBoxColumn3.HeaderText = "Effective To";
            this.effectiveToDataGridViewTextBoxColumn3.Name = "effectiveToDataGridViewTextBoxColumn3";
            this.effectiveToDataGridViewTextBoxColumn3.ReadOnly = true;
            this.effectiveToDataGridViewTextBoxColumn3.Width = 95;
            // 
            // activeDataGridViewTextBoxColumn2
            // 
            this.activeDataGridViewTextBoxColumn2.DataPropertyName = "Active";
            this.activeDataGridViewTextBoxColumn2.HeaderText = "Active";
            this.activeDataGridViewTextBoxColumn2.Name = "activeDataGridViewTextBoxColumn2";
            this.activeDataGridViewTextBoxColumn2.ReadOnly = true;
            this.activeDataGridViewTextBoxColumn2.Visible = false;
            // 
            // createDateDataGridViewTextBoxColumn3
            // 
            this.createDateDataGridViewTextBoxColumn3.DataPropertyName = "CreateDate";
            this.createDateDataGridViewTextBoxColumn3.HeaderText = "Create Date";
            this.createDateDataGridViewTextBoxColumn3.Name = "createDateDataGridViewTextBoxColumn3";
            this.createDateDataGridViewTextBoxColumn3.ReadOnly = true;
            this.createDateDataGridViewTextBoxColumn3.Width = 105;
            // 
            // createByDataGridViewTextBoxColumn3
            // 
            this.createByDataGridViewTextBoxColumn3.DataPropertyName = "CreateBy";
            this.createByDataGridViewTextBoxColumn3.HeaderText = "CreateBy";
            this.createByDataGridViewTextBoxColumn3.Name = "createByDataGridViewTextBoxColumn3";
            this.createByDataGridViewTextBoxColumn3.ReadOnly = true;
            this.createByDataGridViewTextBoxColumn3.Visible = false;
            // 
            // modifyDateDataGridViewImageColumn3
            // 
            this.modifyDateDataGridViewImageColumn3.DataPropertyName = "ModifyDate";
            this.modifyDateDataGridViewImageColumn3.HeaderText = "ModifyDate";
            this.modifyDateDataGridViewImageColumn3.Name = "modifyDateDataGridViewImageColumn3";
            this.modifyDateDataGridViewImageColumn3.ReadOnly = true;
            this.modifyDateDataGridViewImageColumn3.Visible = false;
            // 
            // modifyByDataGridViewTextBoxColumn3
            // 
            this.modifyByDataGridViewTextBoxColumn3.DataPropertyName = "ModifyBy";
            this.modifyByDataGridViewTextBoxColumn3.HeaderText = "ModifyBy";
            this.modifyByDataGridViewTextBoxColumn3.Name = "modifyByDataGridViewTextBoxColumn3";
            this.modifyByDataGridViewTextBoxColumn3.ReadOnly = true;
            this.modifyByDataGridViewTextBoxColumn3.Visible = false;
            // 
            // lastUpdateDataGridViewTextBoxColumn3
            // 
            this.lastUpdateDataGridViewTextBoxColumn3.DataPropertyName = "LastUpdate";
            this.lastUpdateDataGridViewTextBoxColumn3.HeaderText = "Last Update";
            this.lastUpdateDataGridViewTextBoxColumn3.Name = "lastUpdateDataGridViewTextBoxColumn3";
            this.lastUpdateDataGridViewTextBoxColumn3.ReadOnly = true;
            this.lastUpdateDataGridViewTextBoxColumn3.Width = 105;
            // 
            // bankInfoInsertBindingSource
            // 
            this.bankInfoInsertBindingSource.DataSource = typeof(SystemSettingTool.tbBankInformationInsert);
            // 
            // bankInfoInsertbindingNavigator
            // 
            this.bankInfoInsertbindingNavigator.AddNewItem = null;
            this.bankInfoInsertbindingNavigator.BindingSource = this.bankInfoInsertBindingSource;
            this.bankInfoInsertbindingNavigator.CountItem = this.bindingNavigatorCountItem3;
            this.bankInfoInsertbindingNavigator.DeleteItem = null;
            this.bankInfoInsertbindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem3,
            this.bindingNavigatorMovePreviousItem3,
            this.bindingNavigatorSeparator9,
            this.bindingNavigatorPositionItem3,
            this.bindingNavigatorCountItem3,
            this.bindingNavigatorSeparator10,
            this.bindingNavigatorMoveNextItem3,
            this.bindingNavigatorMoveLastItem3,
            this.bindingNavigatorSeparator11,
            this.bankInfoInsert_bindingNavigatorAddNewItem,
            this.bankInfoInsert_bindingNavigatorDeleteItem,
            this.bankInfoInsert_bindingNavigatorEditItem});
            this.bankInfoInsertbindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.bankInfoInsertbindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem3;
            this.bankInfoInsertbindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem3;
            this.bankInfoInsertbindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem3;
            this.bankInfoInsertbindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem3;
            this.bankInfoInsertbindingNavigator.Name = "bankInfoInsertbindingNavigator";
            this.bankInfoInsertbindingNavigator.PositionItem = this.bindingNavigatorPositionItem3;
            this.bankInfoInsertbindingNavigator.Size = new System.Drawing.Size(1030, 30);
            this.bankInfoInsertbindingNavigator.TabIndex = 2;
            this.bankInfoInsertbindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem3
            // 
            this.bindingNavigatorCountItem3.Name = "bindingNavigatorCountItem3";
            this.bindingNavigatorCountItem3.Size = new System.Drawing.Size(49, 27);
            this.bindingNavigatorCountItem3.Text = "of {0}";
            this.bindingNavigatorCountItem3.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem3
            // 
            this.bindingNavigatorMoveFirstItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem3.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem3.Image")));
            this.bindingNavigatorMoveFirstItem3.Name = "bindingNavigatorMoveFirstItem3";
            this.bindingNavigatorMoveFirstItem3.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem3.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveFirstItem3.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem3
            // 
            this.bindingNavigatorMovePreviousItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem3.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem3.Image")));
            this.bindingNavigatorMovePreviousItem3.Name = "bindingNavigatorMovePreviousItem3";
            this.bindingNavigatorMovePreviousItem3.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem3.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMovePreviousItem3.Text = "Move previous";
            // 
            // bindingNavigatorSeparator9
            // 
            this.bindingNavigatorSeparator9.Name = "bindingNavigatorSeparator9";
            this.bindingNavigatorSeparator9.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorPositionItem3
            // 
            this.bindingNavigatorPositionItem3.AccessibleName = "Position";
            this.bindingNavigatorPositionItem3.AutoSize = false;
            this.bindingNavigatorPositionItem3.Name = "bindingNavigatorPositionItem3";
            this.bindingNavigatorPositionItem3.Size = new System.Drawing.Size(50, 30);
            this.bindingNavigatorPositionItem3.Text = "0";
            this.bindingNavigatorPositionItem3.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator10
            // 
            this.bindingNavigatorSeparator10.Name = "bindingNavigatorSeparator10";
            this.bindingNavigatorSeparator10.Size = new System.Drawing.Size(6, 30);
            // 
            // bindingNavigatorMoveNextItem3
            // 
            this.bindingNavigatorMoveNextItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem3.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem3.Image")));
            this.bindingNavigatorMoveNextItem3.Name = "bindingNavigatorMoveNextItem3";
            this.bindingNavigatorMoveNextItem3.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem3.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveNextItem3.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem3
            // 
            this.bindingNavigatorMoveLastItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem3.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem3.Image")));
            this.bindingNavigatorMoveLastItem3.Name = "bindingNavigatorMoveLastItem3";
            this.bindingNavigatorMoveLastItem3.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem3.Size = new System.Drawing.Size(23, 27);
            this.bindingNavigatorMoveLastItem3.Text = "Move last";
            // 
            // bindingNavigatorSeparator11
            // 
            this.bindingNavigatorSeparator11.Name = "bindingNavigatorSeparator11";
            this.bindingNavigatorSeparator11.Size = new System.Drawing.Size(6, 30);
            // 
            // bankInfoInsert_bindingNavigatorAddNewItem
            // 
            this.bankInfoInsert_bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bankInfoInsert_bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bankInfoInsert_bindingNavigatorAddNewItem.Image")));
            this.bankInfoInsert_bindingNavigatorAddNewItem.Name = "bankInfoInsert_bindingNavigatorAddNewItem";
            this.bankInfoInsert_bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bankInfoInsert_bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 27);
            this.bankInfoInsert_bindingNavigatorAddNewItem.Text = "Add new";
            this.bankInfoInsert_bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bankInfoInsert_bindingNavigatorAddNewItem_Click);
            // 
            // bankInfoInsert_bindingNavigatorDeleteItem
            // 
            this.bankInfoInsert_bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bankInfoInsert_bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bankInfoInsert_bindingNavigatorDeleteItem.Image")));
            this.bankInfoInsert_bindingNavigatorDeleteItem.Name = "bankInfoInsert_bindingNavigatorDeleteItem";
            this.bankInfoInsert_bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bankInfoInsert_bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 27);
            this.bankInfoInsert_bindingNavigatorDeleteItem.Text = "Delete";
            this.bankInfoInsert_bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bankInfoInsert_bindingNavigatorDeleteItem_Click);
            // 
            // bankInfoInsert_bindingNavigatorEditItem
            // 
            this.bankInfoInsert_bindingNavigatorEditItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bankInfoInsert_bindingNavigatorEditItem.Image = ((System.Drawing.Image)(resources.GetObject("bankInfoInsert_bindingNavigatorEditItem.Image")));
            this.bankInfoInsert_bindingNavigatorEditItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bankInfoInsert_bindingNavigatorEditItem.Name = "bankInfoInsert_bindingNavigatorEditItem";
            this.bankInfoInsert_bindingNavigatorEditItem.Size = new System.Drawing.Size(23, 27);
            this.bankInfoInsert_bindingNavigatorEditItem.Text = "bankInfoInsert_bindingNavigatorEditItem";
            this.bankInfoInsert_bindingNavigatorEditItem.Click += new System.EventHandler(this.bankInfoInsert_bindingNavigatorEditItem_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1030, 291);
            this.dataGridView1.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.40534F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.59465F));
            this.tableLayoutPanel1.Controls.Add(this.btn_exit, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tabControl1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.94138F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.058615F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1056, 563);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(3, 514);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(119, 44);
            this.btn_exit.TabIndex = 1;
            this.btn_exit.Text = "Exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // FormSystemSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 563);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FormSystemSetting";
            this.Text = "EStatement System Setting Tool";
            this.Load += new System.EventHandler(this.FormSystemSetting_Load);
            this.tabControl1.ResumeLayout(false);
            this.tp_bankLogo.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_bankLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankLogoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankLogoBindingNavigator)).EndInit();
            this.bankLogoBindingNavigator.ResumeLayout(false);
            this.bankLogoBindingNavigator.PerformLayout();
            this.tp_tc.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_termsAndCondition)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.termsAndConditionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.termsAndConditionBindingNavigator)).EndInit();
            this.termsAndConditionBindingNavigator.ResumeLayout(false);
            this.termsAndConditionBindingNavigator.PerformLayout();
            this.tp_marketingInsert.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_marketInsert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.marketInsertBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.marketInsertbindingNavigator)).EndInit();
            this.marketInsertbindingNavigator.ResumeLayout(false);
            this.marketInsertbindingNavigator.PerformLayout();
            this.tp_bankInfoInsert.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv_bankInfoInsert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankInfoInsertBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankInfoInsertbindingNavigator)).EndInit();
            this.bankInfoInsertbindingNavigator.ResumeLayout(false);
            this.bankInfoInsertbindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tp_bankLogo;
        private System.Windows.Forms.TabPage tp_tc;
        private System.Windows.Forms.TabPage tp_marketingInsert;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btn_exit;
        private TableLayoutPanel tableLayoutPanel2;
        private DataGridView gv_bankLogo;
        private Panel panel1;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
        private Button bankLogo_btn_pathSelect;
        private TextBox bankLogo_tb_path;
        private DateTimePicker bankLogo_dtp_effectiveTo;
        private DateTimePicker bankLogo_dtp_effectiveFrom;
        private TextBox bankLogo_tb_statementType;
        private Button bankLogo_btn_cancel;
        private Button bankLogo_btn_save;
        private ErrorProvider errorProvider;
        private TableLayoutPanel tableLayoutPanel3;
        private Panel panel2;
        private BindingNavigator bankLogoBindingNavigator;
        private ToolStripLabel bindingNavigatorCountItem;
        private ToolStripButton bindingNavigatorMoveFirstItem;
        private ToolStripButton bindingNavigatorMovePreviousItem;
        private ToolStripSeparator bindingNavigatorSeparator;
        private ToolStripTextBox bindingNavigatorPositionItem;
        private ToolStripSeparator bindingNavigatorSeparator1;
        private ToolStripButton bindingNavigatorMoveNextItem;
        private ToolStripButton bindingNavigatorMoveLastItem;
        private ToolStripSeparator bindingNavigatorSeparator2;
        private ToolStripButton bankLogo_bindingNavigatorAddNewItem;
        private ToolStripButton bankLogo_bindingNavigatorDeleteItem;
        private ToolStripButton bankLogo_bindingNavigatorEditItem;
        private Panel panel3;
        private BindingNavigator termsAndConditionBindingNavigator;
        private ToolStripButton termsAndCondition_bindingNavigatorAddNewItem;
        private ToolStripLabel bindingNavigatorCountItem1;
        private ToolStripButton termsAndCondition_bindingNavigatorDeleteItem;
        private ToolStripButton bindingNavigatorMoveFirstItem1;
        private ToolStripButton bindingNavigatorMovePreviousItem1;
        private ToolStripSeparator bindingNavigatorSeparator3;
        private ToolStripTextBox bindingNavigatorPositionItem1;
        private ToolStripSeparator bindingNavigatorSeparator4;
        private ToolStripButton bindingNavigatorMoveNextItem1;
        private ToolStripButton bindingNavigatorMoveLastItem1;
        private ToolStripSeparator bindingNavigatorSeparator5;
        private DataGridView gv_termsAndCondition;
        private DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn settingNameDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn settingValueDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn statementTypeDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn effectiveFromDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn effectiveToDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn createDateDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn createByDataGridViewTextBoxColumn;
        private DataGridViewImageColumn modifyDateDataGridViewImageColumn;
        private DataGridViewTextBoxColumn modifyByDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn lastUpdateDataGridViewTextBoxColumn;
        private BindingSource bankLogoBindingSource;
        private BindingSource termsAndConditionBindingSource;
        private Panel panel4;
        private Button termsAndCondition_btn_cancel;
        private Button termsAndCondition_btn_save;
        private DateTimePicker termsAndCondition_dtp_effectiveTo;
        private DateTimePicker termsAndCondition_dtp_effectiveFrom;
        private TextBox termsAndCondition_tb_statementType;
        private Button termsAndCondtion_btn_pathSelect;
        private TextBox termsAndCondition_tb_path;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private ToolStripButton termsAndCondition_bindingNavigatorEditItem;
        private DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn settingNameDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn settingValueDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn statementTypeDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn effectiveFromDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn effectiveToDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn activeDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn createDateDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn createByDataGridViewTextBoxColumn1;
        private DataGridViewImageColumn modifyDateDataGridViewImageColumn1;
        private DataGridViewTextBoxColumn modifyByDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn lastUpdateDataGridViewTextBoxColumn1;
        private TableLayoutPanel tableLayoutPanel4;
        private Panel panel5;
        private DataGridView gv_marketInsert;
        private BindingNavigator marketInsertbindingNavigator;
        private ToolStripButton marketInsert_bindingNavigatorAddNewItem;
        private ToolStripLabel bindingNavigatorCountItem2;
        private ToolStripButton marketInsert_bindingNavigatorDeleteItem;
        private ToolStripButton bindingNavigatorMoveFirstItem2;
        private ToolStripButton bindingNavigatorMovePreviousItem2;
        private ToolStripSeparator bindingNavigatorSeparator6;
        private ToolStripTextBox bindingNavigatorPositionItem2;
        private ToolStripSeparator bindingNavigatorSeparator7;
        private ToolStripButton bindingNavigatorMoveNextItem2;
        private ToolStripButton bindingNavigatorMoveLastItem2;
        private ToolStripSeparator bindingNavigatorSeparator8;
        private Panel panel6;
        private Button marketInsert_btn_cancel;
        private Button marketInsert_btn_save;
        private DateTimePicker marketInsert_dtp_effectiveTo;
        private DateTimePicker marketInsert_dtp_effectiveFrom;
        private TextBox marketInsert_tb_statementType;
        private Button marketInsert_btn_pathSelect;
        private TextBox marketInsert_tb_path;
        private Label label9;
        private Label label10;
        private Label label11;
        private Label label12;
        private TextBox marketInsert_tb_insertID;
        private Label label13;
        private ToolStripButton marketInsert_bindingNavigatorEditItem;
        private BindingSource marketInsertBindingSource;
        private DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn marketInsertIDDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn marketingPathDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn statementTypeDataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn effectiveFromDataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn effectiveToDataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn activeDataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn createDateDataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn createByDataGridViewTextBoxColumn2;
        private DataGridViewImageColumn modifyDateDataGridViewImageColumn2;
        private DataGridViewTextBoxColumn modifyByDataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn lastUpdateDataGridViewTextBoxColumn2;
        private TabPage tp_bankInfoInsert;
        private TableLayoutPanel tableLayoutPanel5;
        private Panel panel7;
        private TextBox bankInfoInsert_tb_insertID;
        private Label label14;
        private Button bankInfoInsert_btn_cancel;
        private Button bankInfoInsert_btn_save;
        private DateTimePicker bankInfoInsert_dtp_effectiveTo;
        private DateTimePicker bankInfoInsert_dtp_effectiveFrom;
        private TextBox bankInfoInsert_tb_statementType;
        private Button bankInfoInsert_btn_pathSelect;
        private TextBox bankInfoInsert_tb_path;
        private Label label15;
        private Label label16;
        private Label label17;
        private Label label18;
        private Panel panel8;
        private DataGridView dataGridView1;
        private DataGridView gv_bankInfoInsert;
        private BindingNavigator bankInfoInsertbindingNavigator;
        private ToolStripButton bankInfoInsert_bindingNavigatorAddNewItem;
        private ToolStripLabel bindingNavigatorCountItem3;
        private ToolStripButton bankInfoInsert_bindingNavigatorDeleteItem;
        private ToolStripButton bindingNavigatorMoveFirstItem3;
        private ToolStripButton bindingNavigatorMovePreviousItem3;
        private ToolStripSeparator bindingNavigatorSeparator9;
        private ToolStripTextBox bindingNavigatorPositionItem3;
        private ToolStripSeparator bindingNavigatorSeparator10;
        private ToolStripButton bindingNavigatorMoveNextItem3;
        private ToolStripButton bindingNavigatorMoveLastItem3;
        private ToolStripSeparator bindingNavigatorSeparator11;
        private BindingSource bankInfoInsertBindingSource;
        private DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn bankInformationIDDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn bankInformationPathDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn statementTypeDataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn effectiveFromDataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn effectiveToDataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn activeDataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn createDateDataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn createByDataGridViewTextBoxColumn3;
        private DataGridViewImageColumn modifyDateDataGridViewImageColumn3;
        private DataGridViewTextBoxColumn modifyByDataGridViewTextBoxColumn3;
        private DataGridViewTextBoxColumn lastUpdateDataGridViewTextBoxColumn3;
        private ToolStripButton bankInfoInsert_bindingNavigatorEditItem;

    }
}

