﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemSettingTool
{
    class Util
    {
        public static void Log(string content)
        {
            try
            {
                DateTime now = DateTime.Now;

                EStatementEntities entities = new EStatementEntities();
                tbLog log = new tbLog();
                log.Content = content;
                log.CreateBy = Properties.Settings.Default.AppName;
                log.ModifyBy = Properties.Settings.Default.AppName;
                log.CreateDate = now;
                log.LastUpdate = now;

                entities.AddTotbLog(log);
                entities.SaveChanges();
            }
            catch (Exception ex)
            {
            }
        }
    }
}
