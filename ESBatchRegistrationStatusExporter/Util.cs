﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ESBatchRegistrationStatusExporter
{
    class Util
    {
        static EStatementEntities Entity = new EStatementEntities();
        static String logFilename = String.Format("{0}.log", Properties.Settings.Default.AppName);
        static String logPath = Path.Combine(Directory.GetCurrentDirectory(), "Log", logFilename);

        public static void Log(logType type, String content)
        {
            try
            {
                DateTime now = DateTime.Now;
                tbLog log = new tbLog();
                log.Type = Enum.GetName(typeof(logType), type);
                log.Content = content;
                log.CreateBy = Properties.Settings.Default.AppName;
                log.ModifyBy = Properties.Settings.Default.AppName;
                log.CreateDate = now;
                log.LastUpdate = now;
                Entity.AddTotbLog(log);
                Entity.SaveChanges();

                Console.WriteLine(content);

                if (!File.GetLastWriteTime(logPath).ToString("yyyyMMdd").Equals(DateTime.Now.ToString("yyyyMMdd")))
                {
                    String newLogPath = String.Format("{0}_{1:yyyyMMdd}.log", logPath.Replace(".log", ""), File.GetLastWriteTime(logPath).ToString("yyyyMMdd"));
                    File.Delete(newLogPath);
                    File.Move(logPath, newLogPath);
                }

                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    sw.Close();
                }
            }
            catch (Exception) { }
        }

        public static void writeLog(String content)
        {
            try
            {
                if (File.Exists(logPath))
                {
                    if (!File.GetLastWriteTime(logPath).ToString("yyyyMMdd").Equals(DateTime.Now.ToString("yyyyMMdd")))
                    {
                        String newLogPath = String.Format("{0}_{1:yyyyMMdd}.log", logPath.Replace(".log", ""), File.GetLastWriteTime(logPath).ToString("yyyyMMdd"));
                        File.Move(logPath, newLogPath);
                    }
                }
                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    sw.Close();
                }
            }
            catch (Exception) { }
        }

        public class account
        {
            public List<String> rmid;
            public String priAcctNo;
            public String acctNo;
            public String prodCode;
            public String statementType;
            public String acctCategory;

            public account() { }
            public account(List<String> i_rmid, String i_priAcctNo, String i_acctNo, String i_prodCode, String i_statementType, String i_acctCategory)
            {
                rmid = i_rmid;
                priAcctNo = i_priAcctNo;
                acctNo = i_acctNo;
                prodCode = i_prodCode;
                statementType = i_statementType;
                acctCategory = i_acctCategory;
            }
        }
    }
}
