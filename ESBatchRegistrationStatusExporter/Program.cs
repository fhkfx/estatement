﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ESBatchRegistrationStatusExporter
{
    public enum logType { Info, Error, Warning };
    class Program
    {
        private static EStatementEntities entity = new EStatementEntities();
        private static String status = "SUCCESS";
        //add by jacky 2019-09-10
        private static String statementFlag = "N";
        private static String adviceFlag = "Y";
        //

        static void Main(string[] args)
        {
            try
            {
                Util.Log(logType.Info, "=========================================================");
                Util.Log(logType.Info, String.Format("ESBatchRegistrationStatusExporter start --- {0}\n", DateTime.Now));

                Util.Log(logType.Info, String.Format("{0} CiticFirst registration status export start", DateTime.Now.ToString("tt hh:mm:ss")));
                RegistrationStatusExport();
                Util.Log(logType.Info, String.Format("{0} CiticFirst registration status export finish\n", DateTime.Now.ToString("tt hh:mm:ss")));

                Util.Log(logType.Info, String.Format("{0} InfoCast registration status export start", DateTime.Now.ToString("tt hh:mm:ss")));
                InfoCastRegistrationStatusExport();
                Util.Log(logType.Info, String.Format("{0} InfoCast registration status export finish\n", DateTime.Now.ToString("tt hh:mm:ss")));

                Util.Log(logType.Info, String.Format("{0} SuperBank registration status export start", DateTime.Now.ToString("tt hh:mm:ss")));
                SuperBankRegistrationStatusExport();
                Util.Log(logType.Info, String.Format("{0} SuperBank registration status export finish\n", DateTime.Now.ToString("tt hh:mm:ss")));

                Util.Log(logType.Info, String.Format("{0} Conatiner registration status export start", DateTime.Now.ToString("tt hh:mm:ss")));
                ContainerRegistrationStatusExport();
                Util.Log(logType.Info, String.Format("{0} Conatiner registration status export finish\n", DateTime.Now.ToString("tt hh:mm:ss")));

                Util.Log(logType.Info, String.Format("{0} DirectBank registration status export start", DateTime.Now.ToString("tt hh:mm:ss")));
                DirectBankFileExport();
                Util.Log(logType.Info, String.Format("{0} DirectBank registration status export finish\n", DateTime.Now.ToString("tt hh:mm:ss")));

                Util.Log(logType.Info, String.Format("{0} estatement.txt export start", DateTime.Now.ToString("tt hh:mm:ss")));
                estatementTXTExport();
                Util.Log(logType.Info, String.Format("{0} estatement.txt export finish\n", DateTime.Now.ToString("tt hh:mm:ss")));

                Util.Log(logType.Info, String.Format("{0} SubAccount status export start", DateTime.Now.ToString("tt hh:mm:ss")));
                SubAccountStatusExport();
                Util.Log(logType.Info, String.Format("{0} SubAccount status export finish\n", DateTime.Now.ToString("tt hh:mm:ss")));

                Util.Log(logType.Info, String.Format("{0} Bank & Card status export start", DateTime.Now.ToString("tt hh:mm:ss")));
                BankAndCardStatusExport();
                Util.Log(logType.Info, String.Format("{0} Bank & Card status export finish\n", DateTime.Now.ToString("tt hh:mm:ss")));

                Util.Log(logType.Info, String.Format("{0} Non-Joint New Record export start", DateTime.Now.ToString("tt hh:mm:ss")));
                NewRecordExport();
                Util.Log(logType.Info, String.Format("{0} Non-Joint New Record export finish\n", DateTime.Now.ToString("tt hh:mm:ss")));

                Util.Log(logType.Info, String.Format("ESBatchRegistrationStatusExporter finish --- {0}\n", DateTime.Now));
                System.Threading.Thread.Sleep(5000);
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in main : {0}", ex.Message));
                System.Threading.Thread.Sleep(3000);
            }
        }
        private static void RegistrationStatusExport()
        {
            var allRMID = entity.tbAccountList.Where(a => a.Active == "Y" && a.RegistrationStatus == "S" && a.Category == "CiticFirst").Select(m => m.RMID).Distinct().ToList();
            StringBuilder consolidatedAccountList = new StringBuilder();
            int accountCount = 0;

            consolidatedAccountList.AppendLine(String.Format("{0};", DateTime.Now.ToString("yyyyMMdd")));
            foreach (var rmid in allRMID)
            {
                consolidatedAccountList.AppendLine(String.Format("{0}{1}E{2}", rmid, Properties.Settings.Default.Delimiter, Properties.Settings.Default.Delimiter));
                accountCount++;
            }
            consolidatedAccountList.AppendLine(String.Format("{0};", accountCount.ToString().PadLeft(10, '0')));
            try
            {
                using (StreamWriter sw = new StreamWriter(Properties.Settings.Default.AccountRegistrationStatusExportPath, false, Encoding.UTF8))
                {
                    sw.Write(consolidatedAccountList.ToString());
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                status = "FAILED";
                Util.Log(logType.Error, String.Format("Error in CiticFirst registration status export. Cannot write to registration file. Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("(CiticFirst) No. of records successfully extracted = {0}", accountCount.ToString()));
        }
        private static void InfoCastRegistrationStatusExport()
        {
            List<tbAccountList> accountsList = entity.tbAccountList.Where(a => a.Active == "Y" && (a.RegistrationStatus == "S" || a.RegistrationStatus == "U") && a.Category == "InfoCast").OrderBy(o => o.AccountNumber).Distinct().ToList();
            StringBuilder consolidatedAccountList = new StringBuilder();
            int accountCount = 0;

            consolidatedAccountList.AppendLine(String.Format("{0};", DateTime.Now.ToString("yyyyMMdd")));
            foreach (var account in accountsList)
            {
                if (account.RegistrationStatus == "S")
                    consolidatedAccountList.AppendLine(String.Format("{0}{1}E{2}", account.AccountNumber, Properties.Settings.Default.Delimiter, Properties.Settings.Default.Delimiter));
                else if (account.RegistrationStatus == "U")
                    consolidatedAccountList.AppendLine(String.Format("{0}{1}N{2}", account.AccountNumber, Properties.Settings.Default.Delimiter, Properties.Settings.Default.Delimiter));

                accountCount++;
            }
            consolidatedAccountList.AppendLine(String.Format("{0};", accountCount.ToString().PadLeft(10, '0')));
            try
            {
                using (StreamWriter sw = new StreamWriter(Properties.Settings.Default.InfoCastAccountRegistrationStatusExportPath, false, Encoding.ASCII))
                {
                    sw.Write(consolidatedAccountList.ToString());
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                status = "FAILED";
                Util.Log(logType.Error, String.Format("Error in InfoCast registration status export. Cannot write to registration file. Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("(InfoCast) No. of records successfully extracted = {0}", accountCount.ToString()));
        }
        private static void SuperBankRegistrationStatusExport()
        {
            List<tbAccountList> accountList = entity.tbAccountList.Where(a => a.Category == Properties.Settings.Default.CategorySuperBank).OrderBy(o => o.RMID).ToList();

            StringBuilder consolidatedAccountList = new StringBuilder();
            consolidatedAccountList.AppendLine(String.Format("{0};", DateTime.Now.ToString("yyyyMMdd")));

            #region Append SuperBank account registration status
            foreach (var account in accountList)
            {
                if (account.RegistrationStatus == "S")
                    consolidatedAccountList.AppendLine(String.Format("{0}{1}S", account.RMID, Properties.Settings.Default.Delimiter));
                else if (account.RegistrationStatus == "U")
                    consolidatedAccountList.AppendLine(String.Format("{0}{1}U", account.RMID, Properties.Settings.Default.Delimiter));
            }
            #endregion

            consolidatedAccountList.AppendLine(String.Format("{0};", accountList.Count.ToString().PadLeft(10, '0')));
            try
            {
                using (StreamWriter sw = new StreamWriter(Properties.Settings.Default.SuperBankAccountRegistrationStatusExportPath, false, Encoding.ASCII))
                {
                    sw.Write(consolidatedAccountList.ToString());
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                status = "FAILED";
                Util.Log(logType.Error, String.Format("Error in SuperBank registration status export. Cannot write to registration file. Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("(SuperBank) No. of records successfully extracted = {0}", accountList.Count.ToString()));
        }
        private static void ContainerRegistrationStatusExport()
        {
            List<tbAccountList> accountList = entity.tbAccountList.Where(a => a.Category == Properties.Settings.Default.CategoryContainer).OrderBy(o => o.AccountNumber).ToList();

            StringBuilder consolidatedAccountList = new StringBuilder();
            consolidatedAccountList.AppendLine(String.Format("{0};", DateTime.Now.ToString("yyyyMMdd")));

            #region Append Container account registration status
            foreach (var account in accountList)
            {
                if (account.RegistrationStatus == "S")
                    consolidatedAccountList.AppendLine(String.Format("{0}{1}S", account.AccountNumber, Properties.Settings.Default.Delimiter));
                else if (account.RegistrationStatus == "U")
                    consolidatedAccountList.AppendLine(String.Format("{0}{1}U", account.AccountNumber, Properties.Settings.Default.Delimiter));
            }
            #endregion

            consolidatedAccountList.AppendLine(String.Format("{0};", accountList.Count.ToString().PadLeft(10, '0')));
            try
            {
                using (StreamWriter sw = new StreamWriter(Properties.Settings.Default.ContainerAccountRegistrationStatusExportPath, false, Encoding.ASCII))
                {
                    sw.Write(consolidatedAccountList.ToString());
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                status = "FAILED";
                Util.Log(logType.Error, String.Format("Error in Conatiner registration status export. Cannot write to registration file. Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("(Conatiner) No. of records successfully extracted = {0}", accountList.Count.ToString()));
        }

        private static void DirectBankFileExport()
        {
            List<tbAccountList> accountList = entity.tbAccountList.Where(a => a.Category == Properties.Settings.Default.CategoryFixDeposit && a.StatementType == Properties.Settings.Default.StatementTypeFixDeposit).ToList();
            List<tbRmIDAccountNumber> rmidRmNumberMapList = entity.tbRmIDAccountNumber.Where(a => a.RMID != null && a.RMID != "" && a.RMNumber != null && a.RMNumber != "").ToList();
            StringBuilder consolidatedAccountList = new StringBuilder();
            Dictionary<String, String> rmidRmNumberMapDict = new Dictionary<String, String>();
            //List<tbAccountList> notMatchAccountList = new List<tbAccountList>();
            foreach (var rmidRmNumberMap in rmidRmNumberMapList)
            {
                if (!rmidRmNumberMapDict.ContainsKey(rmidRmNumberMap.RMID))
                    rmidRmNumberMapDict.Add(rmidRmNumberMap.RMID, rmidRmNumberMap.RMNumber);
            }

            foreach (var account in accountList)
            {
                if (rmidRmNumberMapDict.ContainsKey(account.RMID))
                    account.RMID = rmidRmNumberMapDict[account.RMID];
                else
                    account.RMID = "";
            }
            accountList = accountList.OrderBy(o => o.RMID).ToList();

            consolidatedAccountList.AppendLine(String.Format("{0};", DateTime.Now.ToString("yyyyMMdd")));

            int matchAccountCounter = 0;
            int notMatchAccountCounter = 0;
            #region Append accounts to consolidatedAccountList
            foreach (var account in accountList)
            {
                if (!String.IsNullOrEmpty(account.RMID))
                {
                    consolidatedAccountList.AppendLine(String.Format("{0,35}{1,20}{2,1}{3,100}{4,100}{5,100}{6,100}{7,100}", "00000" + account.RMID, account.AccountNumber, account.RegistrationStatus, "", "", "", "", ""));
                    matchAccountCounter++;
                }
                else
                {
                    //notMatchAccountList.Add(account);
                    notMatchAccountCounter++;
                }
            }
            #endregion

            consolidatedAccountList.AppendLine(String.Format("{0};", matchAccountCounter.ToString().PadLeft(10, '0')));
            try
            {
                using (StreamWriter sw = new StreamWriter(Properties.Settings.Default.directbankFDPath, false, Encoding.ASCII))
                {
                    sw.Write(consolidatedAccountList.ToString());
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                status = "FAILED";
                Util.Log(logType.Error, String.Format("Error in DirectBank registration status export. Cannot write to registration file. Error message:{0}", ex.Message));
            }

            Util.Log(logType.Info, String.Format("(DirectBank - FD) No. of records that can not match with RM number = {0}", notMatchAccountCounter));
            Util.Log(logType.Info, String.Format("(DirectBank - FD) No. of records successfully extracted = {0}", matchAccountCounter));
        }
        private static void estatementTXTExport()
        {
            Util.Log(logType.Info, String.Format("Truncate {0} start", Properties.Settings.Default.tbRefEstatementTXT_TableName));
            entity.ExecuteStoreCommand(String.Format("TRUNCATE TABLE {0}", Properties.Settings.Default.tbRefEstatementTXT_TableName));
            Util.Log(logType.Info, String.Format("Truncate {0} finish", Properties.Settings.Default.tbRefEstatementTXT_TableName));

            Util.Log(logType.Info, String.Format("Call store pro. {0} to update table {1} start", Properties.Settings.Default.spGenerateDataForEstatementTXT, Properties.Settings.Default.tbRefEstatementTXT_TableName));
            entity.ExecuteStoreCommand(String.Format("EXECUTE {0}", Properties.Settings.Default.spGenerateDataForEstatementTXT));
            Util.Log(logType.Info, String.Format("Call store pro. {0} to update table {1} finish", Properties.Settings.Default.spGenerateDataForEstatementTXT, Properties.Settings.Default.tbRefEstatementTXT_TableName));

            /****************************Modified by Jacky on 2019-02-19************************************/
            //Purpose : Remove duplicate record when get from database
            //List<tbRefEstatementTXT> accountList = entity.tbRefEstatementTXT.Where(a => a.RM_Number != null).OrderBy(o => o.RM_Number).ToList();
            List<tbRefEstatementTXT> accountList = entity.tbRefEstatementTXT.Where(a => a.RM_Number != null).OrderBy(o => o.RM_Number).GroupBy(b => b.RM_Number).Select(grp => grp.FirstOrDefault()).ToList();
            /*********************************************************************************************/
            List<tbStatementType> stmtDescriMList = entity.tbStatementType.Where(a => a.statementFreq == "M").ToList();
            Dictionary<String, String> stmtDescriMDict = new Dictionary<String, String>();
            foreach (var record in stmtDescriMList)
            {
                if (!stmtDescriMDict.ContainsKey(record.StatementType))
                    stmtDescriMDict.Add(record.StatementType, record.EngDesc);
            }
            tbStatementType FixDepositAcc = entity.tbStatementType.Where(a => a.statementFreq == "A" && a.StatementType == Properties.Settings.Default.StatementTypeFixDeposit).FirstOrDefault();

            StringBuilder consolidatedAccountList = new StringBuilder();
            int recordAcounter = 0;

            consolidatedAccountList.AppendLine(String.Format("{0};", DateTime.Now.ToString("yyyyMMdd")));

            #region Append accounts to consolidatedAccountList
            Util.Log(logType.Info, ("Map StatementType & EngDesc start"));
            foreach (var account in accountList)
            {
                /************************************original code of David************************************************************************************/
                /*if (!stmtDescriMDict.ContainsKey(account.StatementTypeEngDesc))
                {
                    if (account.StatementTypeEngDesc != Properties.Settings.Default.StatementTypeFixDeposit)
                        continue;
                    else
                    {
                        consolidatedAccountList.AppendLine(String.Format("{0,30}{1,100}{2,100}", account.RM_Number, FixDepositAcc.EngDesc, account.AccountNumberOrBPname));
                        recordAcounter++;
                    }
                }
                else
                {
                    consolidatedAccountList.AppendLine(String.Format("{0,30}{1,100}{2,100}", account.RM_Number, stmtDescriMDict[account.StatementTypeEngDesc], account.AccountNumberOrBPname));
                    recordAcounter++;
                }*/
                /**************************************************************************************************************************************************************************************/

                //*************************Modify by Jacky in 2018-09-10***************************************************************
                //Purpose :　Change the estatement.txt from 230 length to 300 length of adding statement flag, advice flag and filler
                if (account.StatementTypeEngDesc != Properties.Settings.Default.StatementTypeFixDeposit)
                    statementFlag = "Y";
                else
                    statementFlag = "N";

                if (account.Category.Equals(Properties.Settings.Default.CategoryFixDeposit) || account.Category.Equals(Properties.Settings.Default.CategoryContainer) || account.Category.Equals(Properties.Settings.Default.CategorySuperBank) || account.Category.Equals(Properties.Settings.Default.CategoryInfoCast))
                    adviceFlag = "Y";
                else
                    adviceFlag = "N";

                if (!stmtDescriMDict.ContainsKey(account.StatementTypeEngDesc))
                {
                    //As the statementFreq of FixDeposit is "A" in tbStatementType, therefore FixDeposit(FD) will not contain in stmtDescriMDict
                    if (account.StatementTypeEngDesc != Properties.Settings.Default.StatementTypeFixDeposit)
                        continue;
                    else
                    {
                        //checked all the category of record is FixDeposit
                        consolidatedAccountList.AppendLine(String.Format("{0,30}{1,100}{2,100}{3,1}{4,1}{5,68}", account.RM_Number, FixDepositAcc.EngDesc, account.AccountNumberOrBPname, statementFlag, adviceFlag, ""));
                        recordAcounter++;
                    }
                }
                else
                {
                    //checked all the category of record is not FixDeposit
                    consolidatedAccountList.AppendLine(String.Format("{0,30}{1,100}{2,100}{3,1}{4,1}{5,68}", account.RM_Number, stmtDescriMDict[account.StatementTypeEngDesc], account.AccountNumberOrBPname,statementFlag, adviceFlag, ""));
                    recordAcounter++;
                }
                /**************************************************************************************************************************************************************************************/
            }
            Util.Log(logType.Info, ("Map StatementType & EngDesc finish"));
            #endregion

            consolidatedAccountList.AppendLine(String.Format("{0};", recordAcounter.ToString().PadLeft(10, '0')));
            try
            {
                using (StreamWriter sw = new StreamWriter(Properties.Settings.Default.estatementTXTPath, false, Encoding.ASCII))
                {
                    sw.Write(consolidatedAccountList.ToString());
                    sw.Close();
                }
                /**********************2018/10/04 make a config file to make sure it is scuuess**************************/
                using (StreamWriter sw = new StreamWriter(Properties.Settings.Default.estatementTXTControlPath, false, Encoding.ASCII))
                {
                    sw.Write("Config Success!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    sw.Close();
                }
                /********************************************************************************************************/
            }
            catch (Exception ex)
            {
                status = "FAILED";
                Util.Log(logType.Error, String.Format("Error in estatement.txt export. Cannot write to registration file. Error message:{0}", ex.Message));
            }

            Util.Log(logType.Info, String.Format("(estatement.txt) No. of records successfully extracted = {0}", recordAcounter.ToString()));
        }

        private static void SubAccountStatusExport()
        {
            List<tbAccountList> SBSaccountList = entity.tbAccountList.Where(a => a.Category == Properties.Settings.Default.CategorySuperBank).ToList();
            List<tbSubAccountList> SBSsubAccountList = entity.tbSubAccountList.Where(a => a.Category == Properties.Settings.Default.CategorySuperBank).OrderBy(o => o.AccountNumber).ToList();
            List<tbAccountList> AVQaccountList = entity.tbAccountList.Where(a => a.Category == Properties.Settings.Default.CategoryContainer).ToList();
            List<tbSubAccountList> AVQsubAccountList = entity.tbSubAccountList.Where(a => a.Category == Properties.Settings.Default.CategoryContainer).OrderBy(o => o.AccountNumber).ToList();
            Dictionary<int, String> SBSidRegStatusDict = new Dictionary<int, String>();
            foreach (var record in SBSaccountList)
                SBSidRegStatusDict.Add(record.ID, record.RegistrationStatus);
            Dictionary<int, String> AVQidRegStatusDict = new Dictionary<int, String>();
            foreach (var record in AVQaccountList)
                AVQidRegStatusDict.Add(record.ID, record.RegistrationStatus);


            StringBuilder consolidatedAccountList = new StringBuilder();
            consolidatedAccountList.AppendLine(String.Format("{0};", DateTime.Now.ToString("yyyyMMdd")));
            int sbsAppendRecord = 0;
            int avqAppendRecord = 0;

            #region Append SuperBank sub account registration status
            foreach (var subacc in SBSsubAccountList)
            {
                if (SBSidRegStatusDict.ContainsKey(subacc.AccountID))
                {
                    consolidatedAccountList.AppendLine(String.Format("{0}|{1}", subacc.AccountNumber, SBSidRegStatusDict[subacc.AccountID]));
                    sbsAppendRecord++;
                }
                else
                    Util.Log(logType.Info, String.Format("(SuperBank subAccount) Cannot match any SuperBank records in tbAccountList with ID = {0}", subacc.AccountID));
            }
            #endregion

            #region Append Container sub account registration status
            foreach (var subacc in AVQsubAccountList)
            {
                if (AVQidRegStatusDict.ContainsKey(subacc.AccountID))
                {
                    consolidatedAccountList.AppendLine(String.Format("{0}|{1}", subacc.AccountNumber, AVQidRegStatusDict[subacc.AccountID]));
                    avqAppendRecord++;
                }
                else
                    Util.Log(logType.Info, String.Format("(Container subAccount) Cannot match any Container records in tbAccountList with ID = {0}", subacc.AccountID));
            }
            #endregion

            consolidatedAccountList.AppendLine(String.Format("{0};", (sbsAppendRecord + avqAppendRecord).ToString().PadLeft(10, '0')));
            try
            {
                using (StreamWriter sw = new StreamWriter(Properties.Settings.Default.SubAccountStatusExportPath, false, Encoding.ASCII))
                {
                    sw.Write(consolidatedAccountList.ToString());
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                status = "FAILED";
                Util.Log(logType.Error, String.Format("Error in SubAccount status export. Cannot write to registration file. Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("Total No. of records successfully extracted = {0}", (sbsAppendRecord + avqAppendRecord).ToString()));
        }

        private static void BankAndCardStatusExport()
        {
            List<tbAccountList> accountList = entity.tbAccountList.Where(a => (a.Category == Properties.Settings.Default.CategoryBankIndividual || a.Category == Properties.Settings.Default.CategoryBankCombine || a.Category == Properties.Settings.Default.CategoryCreditCard) && a.Active == "Y").ToList();

            StringBuilder consolidatedAccountList = new StringBuilder();
            consolidatedAccountList.AppendLine(String.Format("{0};", DateTime.Now.ToString("yyyyMMdd")));
            String datetime = DateTime.Now.ToString("yyyy-MM-dd 00:00:00.000");

            #region Append Bank & Card registration status
            foreach (var account in accountList)
            {
                consolidatedAccountList.AppendLine(String.Format("{0,-30}{1,-30}{2,-50}{3,-50}{4,1}{5,1}{6,-30}", account.RMID, account.AccountNumber, account.Category, account.StatementType, account.RegistrationStatus, account.Active, datetime));
            }
            #endregion

            consolidatedAccountList.AppendLine(String.Format("{0};", accountList.Count().ToString().PadLeft(10, '0')));
            try
            {
                using (StreamWriter sw = new StreamWriter(Properties.Settings.Default.BankAndCardRegistrationStatusExportPath, false, Encoding.ASCII))
                {
                    sw.Write(consolidatedAccountList.ToString());
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                status = "FAILED";
                Util.Log(logType.Error, String.Format("Error in Bank & Card status export. Cannot write to registration file. Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("(Bank & Card) Total No. of records successfully extracted = {0}", accountList.Count().ToString()));
        }

        private static void NewRecordExport()
        {
            List<tbAccountUpdateList> accountUpdateList = entity.tbAccountUpdateList.OrderBy(o => o.AccountNumber).ToList();

            List<tbAccountList> accountList = entity.tbAccountList.ToList();
            Dictionary<int, String> accountList_IDStatementTypeDict = new Dictionary<int, String>();
            foreach (var record in accountList)
                accountList_IDStatementTypeDict.Add(record.ID, record.StatementType);

            List<tbBPInfo> bpList = entity.tbBPInfo.ToList();
            Dictionary<String, String> bpList_bpidrmidDict = new Dictionary<String, String>();
            foreach (var record in bpList)
            {
                if ((record.no_of_acc_holder == 1) && (!String.IsNullOrEmpty(record.rmid)))
                    bpList_bpidrmidDict.Add(record.bpid, record.rmid);
                else   // exclude joint accounts
                    continue;
            }

            StringBuilder consolidatedAccountList = new StringBuilder();
            consolidatedAccountList.AppendLine(String.Format("H||{0}||{1}", DateTime.Now.ToString("yyyyMMddhhmmss"), DateTime.Now.ToString("yyyyMMdd")));

            #region Append New account records
            int SBSAVQcounter = 0;
            int normalCounter = 0;
            foreach (var account in accountUpdateList)
            {
                if ((accountList_IDStatementTypeDict[account.AccountID] == Properties.Settings.Default.StatementTypeSuperBank) || (accountList_IDStatementTypeDict[account.AccountID] == Properties.Settings.Default.StatementTypeContainer))
                {
                    if (bpList_bpidrmidDict.ContainsKey(account.RMID.Trim()))
                    {
                        consolidatedAccountList.AppendLine(String.Format("D||{0}||{1}||{2}||{3}", account.AccountNumber.Trim(), bpList_bpidrmidDict[account.RMID.Trim()], accountList_IDStatementTypeDict[account.AccountID], account.CreateDate?.ToString("yyyyMMdd")));
                        SBSAVQcounter++;
                    }
                }
                else
                {
                    consolidatedAccountList.AppendLine(String.Format("D||{0}||{1}||{2}||{3}", account.AccountNumber.Trim(), account.RMID.Trim(), accountList_IDStatementTypeDict[account.AccountID], account.CreateDate?.ToString("yyyyMMdd")));
                    normalCounter++;
                }
            }
            #endregion

            consolidatedAccountList.AppendLine(String.Format("T||{0}", SBSAVQcounter + normalCounter));

            try
            {
                using (StreamWriter sw = new StreamWriter(Properties.Settings.Default.NonJointAccountNewRecordExportPath, false, Encoding.ASCII))
                {
                    sw.Write(consolidatedAccountList.ToString());
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                status = "FAILED";
                Util.Log(logType.Error, String.Format("Error in Non-Joint New Record export. Cannot write to registration file. Error message:{0}", ex.Message));
            }
            Util.Log(logType.Info, String.Format("(Non-Joint New Record) No. of records successfully extracted = {0}", (SBSAVQcounter + normalCounter).ToString()));
        }
    }
}