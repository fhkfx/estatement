﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;

namespace ESSystemSettingTool
{
    public enum logType { Info, Error, Warning };
    public enum IndexFileFormat_GetData
    {
        Category = 0,
        StatementType = 1,
        StatementFreq = 2,
        FormatID = 3,
        Type = 4,
        TypeValue = 5,
        Column = 6,
        HasHeader = 7,
        DatePattern = 8,
    };
    public enum InfoArchiveFixConfig_GetData
    {
        Category = 0,
        StatementType = 1,
        StatementFreq = 2,
        SettingDescription = 3,
        SettingValue = 4,
    };
    public enum StatementFieldMapping_GetData
    {
        FormatID = 0,
        Field = 1,
        FieldNature = 2,
        Position = 3,
        Format = 4,
    };
    public enum PTYPE_GetData
    {
        ruleNumber = 0,
        metaType = 1,
        orderType = 2,
        assGroup = 3,
        assSubGroup = 4,
        productType = 5,
    };
    public enum PNAME_GetData
    {
        ruleNumber = 0,
        metaType = 1,
        orderType = 2,
        assGroup = 3,
        assSubGroup = 4,
        productName = 5,
    };
    public enum ADVICE_TITLE_GetData
    {
        ruleNumber = 0,
        metaType = 1,
        orderType = 2,
        assGroup = 3,
        assSubGroup = 4,
        adviceTitleEN = 5,
        adviceTitleTC = 6,
        adviceTitleSC = 7,
    };
    class Program
    {
        private static EStatementEntities entity = new EStatementEntities();
        private static Random rand = new Random();
        private static Int32 numOfFailed = 0;
        private static Int32 numOfProcessed = 0;

        static void Main(string[] args)
        {
            Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "Log"));
            Util.Log(logType.Info, "ESSystemSettingTool Start.");
            Util.writeLog(String.Format("{0:yyyyMMdd HHmmss} START ESSystem Tool", DateTime.Now));
            loadAdviceMap();
            if (args.Length != 0 && args[0] == "configtest")
            {
                loadInfoArchiveConfigToDB();
            }
            //C:\Users\alex_wong\Desktop\EStatement\SystemSettingToolInsert.csv
            foreach (var filePath in Directory.GetFiles(Properties.Settings.Default.SystemSettingCSVFolder, "*.csv"))
            {
                if (File.Exists(filePath))
                {
                    using (TextReader tr = File.OpenText(filePath))
                    {
                        List<String> lines = new List<String>();
                        String eachLine = String.Empty;
                        eachLine = tr.ReadLine();
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            lines.Add(eachLine);
                        }

                        foreach (var line in lines)
                        {
                            String[] arrColumns = null;
                            arrColumns = line.Split(',');

                            if (argsValidation(arrColumns, line, Path.GetFileName(filePath)))
                            {
                                if (arrColumns[0] == Properties.Settings.Default.OperationHeader)
                                    header(arrColumns, line, Path.GetFileName(filePath));
                                else if (arrColumns[0] == Properties.Settings.Default.OperationTC)
                                    tc(arrColumns, line, Path.GetFileName(filePath));
                                else if (arrColumns[0] == Properties.Settings.Default.OperationMarketInsert)
                                    marketInsert(arrColumns, line, Path.GetFileName(filePath));
                                else if (arrColumns[0] == Properties.Settings.Default.OperationBankInfoInsert)
                                    bankInfoInsert(arrColumns, line, Path.GetFileName(filePath));
                                else
                                    predefinedPeriod(arrColumns, line, Path.GetFileName(filePath));
                            }
                            else
                            {
                                numOfFailed++;
                            }
                        }
                    }
                }
            }
            Util.Log(logType.Info, "ESSystemSettingTool Finish.");
            Util.writeLog(String.Format("No. of records failed = {0}", numOfFailed.ToString()));
            Util.writeLog(String.Format("No. of records processed = {0}", numOfProcessed.ToString()));
            Util.writeLog(String.Format("Total no. of records in CSV = {0}", (numOfFailed + numOfProcessed).ToString()));
            Util.writeLog(String.Format("{0:yyyyMMdd HHmmss} END ESSystem Tool with status = {1}", DateTime.Now, numOfFailed == 0 ? "SUCCESS" : "FAIL"));
            System.Threading.Thread.Sleep(5000);
        }

        private static void loadAdviceMap()
        {
            Util.Log(logType.Info, "Load AdviceMapping Start.");
            try
            {
                Util.Log(logType.Info, "Truncate table tbAdviceMapping start.");
                entity.ExecuteStoreCommand("TRUNCATE TABLE [dbo].[tbAdviceMapping]");
                Util.Log(logType.Info, "Truncate table tbAdviceMapping end.");
                int lineCounter;

                #region product type mapping
                // read product type mapping logic
                Util.Log(logType.Info, "Read product type logic start.");
                if (File.Exists(Properties.Settings.Default.IndexFilePathProductType))
                {
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.IndexFilePathProductType))
                    {
                        String eachLine = String.Empty;
                        lineCounter = 0;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            lineCounter++;
                            if (eachLine.Split('|').Count() != 6)
                            {
                                Util.Log(logType.Error, String.Format("Product Type Mapping (PTYPEMAP.DAT), no. of column mismatch at line {0} : \n{1}", lineCounter, eachLine));
                                continue;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }

                    Util.Log(logType.Info, String.Format("Total No. of valid records in PTYPEMAP.DAT = {0}", lines.Count.ToString()));

                    int counter = 0;
                    DateTime now = DateTime.Now;
                    foreach (var line in lines)
                    {
                        String[] data = line.Split('|');

                        tbAdviceMapping record = new tbAdviceMapping();
                        record.RuleNumber = Convert.ToInt32(data[(int)PTYPE_GetData.ruleNumber]);
                        record.Meta_Type = data[(int)PTYPE_GetData.metaType];
                        record.Order_Type = data[(int)PTYPE_GetData.orderType];
                        record.Asset_Grp = data[(int)PTYPE_GetData.assGroup];
                        record.Asset_SubGrp = data[(int)PTYPE_GetData.assSubGroup];
                        record.Product_Type = data[(int)PTYPE_GetData.productType];
                        record.MappingCategory = "ProductType";
                        record.CreateDate = now;
                        record.CreateBy = Properties.Settings.Default.AppName;
                        record.ModifyDate = now;
                        record.ModifyBy = Properties.Settings.Default.AppName;
                        entity.AddTotbAdviceMapping(record);

                        if (counter++ % 2000 == 0)
                        {
                            entity.SaveChanges();
                        }
                    }
                    entity.SaveChanges();
                }
                Util.Log(logType.Info, "Read product type logic end.");
                #endregion

                #region product name mapping
                // read product name mapping logic
                Util.Log(logType.Info, "Read product name logic start.");
                if (File.Exists(Properties.Settings.Default.IndexFilePathProductName))
                {
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.IndexFilePathProductName))
                    {
                        String eachLine = String.Empty;
                        lineCounter = 0;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            lineCounter++;
                            if (eachLine.Split('|').Count() != 6)
                            {
                                Util.Log(logType.Error, String.Format("Product Name Mapping (PNAMEMAP.DAT), no. of column mismatch at line {0} : \n{1}", lineCounter, eachLine));
                                continue;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }

                    Util.Log(logType.Info, String.Format("Total No. of valid records in PNAMEMAP.DAT = {0}", lines.Count.ToString()));

                    int counter = 0;
                    DateTime now = DateTime.Now;
                    foreach (var line in lines)
                    {
                        String[] data = line.Split('|');

                        tbAdviceMapping record = new tbAdviceMapping();
                        record.RuleNumber = Convert.ToInt32(data[(int)PTYPE_GetData.ruleNumber]);
                        record.Meta_Type = data[(int)PTYPE_GetData.metaType];
                        record.Order_Type = data[(int)PTYPE_GetData.orderType];
                        record.Asset_Grp = data[(int)PTYPE_GetData.assGroup];
                        record.Asset_SubGrp = data[(int)PTYPE_GetData.assSubGroup];
                        record.Product_Name = data[(int)PNAME_GetData.productName];
                        record.MappingCategory = "ProductName";
                        record.CreateDate = now;
                        record.CreateBy = Properties.Settings.Default.AppName;
                        record.ModifyDate = now;
                        record.ModifyBy = Properties.Settings.Default.AppName;
                        entity.AddTotbAdviceMapping(record);

                        if (counter++ % 2000 == 0)
                        {
                            entity.SaveChanges();
                        }
                    }
                    entity.SaveChanges();
                }
                Util.Log(logType.Info, "Read product name logic end.");
                #endregion

                #region advice title mapping
                // read product name mapping logic
                Util.Log(logType.Info, "Read advice title logic start.");
                if (File.Exists(Properties.Settings.Default.IndexFilePathAdviceTitle))
                {
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.IndexFilePathAdviceTitle))
                    {
                        String eachLine = String.Empty;
                        lineCounter = 0;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            lineCounter++;
                            if (eachLine.Split('|').Count() != 8)
                            {
                                Util.Log(logType.Error, String.Format("Advice Title Mapping (ADVICEMAP.DAT), no. of column mismatch at line {0} : \n{1}", lineCounter, eachLine));
                                continue;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }

                    Util.Log(logType.Info, String.Format("Total No. of records in ADVICEMAP.DAT = {0}", lines.Count.ToString()));

                    int counter = 0;
                    DateTime now = DateTime.Now;
                    foreach (var line in lines)
                    {
                        String[] data = line.Split('|');

                        tbAdviceMapping record = new tbAdviceMapping();
                        record.RuleNumber = Convert.ToInt32(data[(int)PTYPE_GetData.ruleNumber]);
                        record.Meta_Type = data[(int)PTYPE_GetData.metaType];
                        record.Order_Type = data[(int)PTYPE_GetData.orderType];
                        record.Asset_Grp = data[(int)PTYPE_GetData.assGroup];
                        record.Asset_SubGrp = data[(int)PTYPE_GetData.assSubGroup];
                        record.AdviceTitleEN = data[(int)ADVICE_TITLE_GetData.adviceTitleEN];
                        record.AdviceTitleTC = data[(int)ADVICE_TITLE_GetData.adviceTitleTC];
                        record.AdviceTitleSC = data[(int)ADVICE_TITLE_GetData.adviceTitleSC];
                        record.MappingCategory = "AdviceTitle";
                        record.CreateDate = now;
                        record.CreateBy = Properties.Settings.Default.AppName;
                        record.ModifyDate = now;
                        record.ModifyBy = Properties.Settings.Default.AppName;
                        entity.AddTotbAdviceMapping(record);

                        if (counter++ % 2000 == 0)
                        {
                            entity.SaveChanges();
                        }
                    }
                    entity.SaveChanges();
                }
                Util.Log(logType.Info, "Read advice title logic end.");
                #endregion
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in function Load AdviceMapping : {0}", ex.Message));
            }
            Util.Log(logType.Info, "Load AdviceMapping End.");
        }

        private static void loadInfoArchiveConfigToDB()
        {
            try
            {
                int lineCounter;

                #region Add Index File Format to DB
                if (File.Exists(Properties.Settings.Default.FilePathIndexFileFormat))
                {
                    Util.Log(logType.Info, String.Format("Load Index File Format to table {0} Start", Properties.Settings.Default.IndexFileFormatTableName));
                    Util.Log(logType.Info, String.Format("Read Index File Format data file ({0}) start", Path.GetFileName(Properties.Settings.Default.FilePathIndexFileFormat)));
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.FilePathIndexFileFormat))
                    {
                        String eachLine = String.Empty;
                        lineCounter = 0;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            lineCounter++;
                            if (eachLine.Split('#').Count() != Convert.ToInt16(Properties.Settings.Default.FilePathIndexFileFormatValidColumn))
                            {
                                Util.Log(logType.Error, String.Format("Index File Format File({0}), no. of column mismatch at line {1} : \n{2}", Path.GetFileName(Properties.Settings.Default.FilePathIndexFileFormat), lineCounter, eachLine));
                                return;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }
                    Util.Log(logType.Info, String.Format("Read Index File Format data file ({0}) finish", Path.GetFileName(Properties.Settings.Default.FilePathIndexFileFormat)));
                    Util.Log(logType.Info, String.Format("Total No. of valid records in {0} = {1}", Path.GetFileName(Properties.Settings.Default.FilePathIndexFileFormat), lines.Count));

                    entity.ExecuteStoreCommand(String.Format("delete from {0}", Properties.Settings.Default.IndexFileFormatTableName));

                    Util.Log(logType.Info, String.Format("Insert record into {0} start", Properties.Settings.Default.IndexFileFormatTableName));
                    int counter = 0;
                    DateTime now = DateTime.Now;
                    foreach (var line in lines)
                    {
                        String[] data = line.Split('#');
                        tbIndexFileFormat record = new tbIndexFileFormat();
                        record.Category = data[(int)IndexFileFormat_GetData.Category].Trim();
                        record.StatementType = data[(int)IndexFileFormat_GetData.StatementType].Trim();
                        record.StatementFreq = data[(int)IndexFileFormat_GetData.StatementFreq].Trim();
                        record.FormatID = data[(int)IndexFileFormat_GetData.FormatID].Trim();
                        record.Type = data[(int)IndexFileFormat_GetData.Type].Trim();
                        record.TypeValue = data[(int)IndexFileFormat_GetData.TypeValue].Trim();
                        record.Column = Convert.ToInt32(data[(int)IndexFileFormat_GetData.Column].Trim());
                        record.HasHeader = data[(int)IndexFileFormat_GetData.HasHeader].Trim();
                        record.DatePattern = data[(int)IndexFileFormat_GetData.DatePattern].Trim();
                        record.CreateDate = now;
                        entity.AddTotbIndexFileFormat(record);

                        if (counter++ % 2000 == 0)
                        {
                            entity.SaveChanges();
                        }
                    }
                    entity.SaveChanges();
                    Util.Log(logType.Info, String.Format("Insert record into {0} finish", Properties.Settings.Default.IndexFileFormatTableName));
                    Util.Log(logType.Info, String.Format("Load Index File Format to table {0} Finish", Properties.Settings.Default.IndexFileFormatTableName));
                }
                else
                    Util.Log(logType.Info, "Index File Format data file not exist, skip 'Load Index File Format'");
                #endregion
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in Load Index File Format : {0}", ex.Message));
            }

            try
            {
                int lineCounter;

                #region Add InfoArchive Fix Config to DB
                if (File.Exists(Properties.Settings.Default.FilePathInfoArchiveFixConfig))
                {
                    Util.Log(logType.Info, String.Format("Load InfoArchive Fix Config to table {0} Start", Properties.Settings.Default.InfoArchiveFixConfigTableName));
                    Util.Log(logType.Info, String.Format("Read InfoArchive Fix Config data file ({0}) start", Path.GetFileName(Properties.Settings.Default.FilePathInfoArchiveFixConfig)));
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.FilePathInfoArchiveFixConfig))
                    {
                        String eachLine = String.Empty;
                        lineCounter = 0;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            lineCounter++;
                            if (eachLine.Split('|').Count() != Convert.ToInt16(Properties.Settings.Default.FilePathInfoArchiveFixConfigValidColumn))
                            {
                                Util.Log(logType.Error, String.Format("InfoArchive Fix Config File({0}), no. of column mismatch at line {1} : \n{2}", Path.GetFileName(Properties.Settings.Default.FilePathInfoArchiveFixConfig), lineCounter, eachLine));
                                return;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }
                    Util.Log(logType.Info, String.Format("Read InfoArchive Fix Config data file ({0}) finish", Path.GetFileName(Properties.Settings.Default.FilePathInfoArchiveFixConfig)));
                    Util.Log(logType.Info, String.Format("Total No. of valid records in {0} = {1}", Path.GetFileName(Properties.Settings.Default.FilePathInfoArchiveFixConfig), lines.Count));

                    entity.ExecuteStoreCommand(String.Format("delete from {0}", Properties.Settings.Default.InfoArchiveFixConfigTableName));

                    Util.Log(logType.Info, String.Format("Insert record into {0} start", Properties.Settings.Default.InfoArchiveFixConfigTableName));
                    int counter = 0;
                    DateTime now = DateTime.Now;
                    foreach (var line in lines)
                    {
                        String[] data = line.Split('|');
                        tbInfoArchiveFixConfig record = new tbInfoArchiveFixConfig();
                        record.Category = data[(int)InfoArchiveFixConfig_GetData.Category].Trim();
                        record.StatementType = data[(int)InfoArchiveFixConfig_GetData.StatementType].Trim();
                        record.StatementFreq = data[(int)InfoArchiveFixConfig_GetData.StatementFreq].Trim();
                        record.SettingDescription = data[(int)InfoArchiveFixConfig_GetData.SettingDescription].Trim();
                        record.SettingValue = data[(int)InfoArchiveFixConfig_GetData.SettingValue].Trim();
                        record.CreateDate = now;
                        entity.AddTotbInfoArchiveFixConfig(record);

                        if (counter++ % 2000 == 0)
                        {
                            entity.SaveChanges();
                        }
                    }
                    entity.SaveChanges();
                    Util.Log(logType.Info, String.Format("Insert record into {0} finish", Properties.Settings.Default.InfoArchiveFixConfigTableName));
                    Util.Log(logType.Info, String.Format("Load InfoArchive Fix Config to table {0} Finish", Properties.Settings.Default.InfoArchiveFixConfigTableName));
                }
                else
                    Util.Log(logType.Info, "InfoArchive Fix Config data file not exist, skip 'Load InfoArchive Fix Config'");
                #endregion
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in Load InfoArchive Fix Config : {0}", ex.Message));
            }

            try
            {
                int lineCounter;

                #region Statement Field Mapping
                if (File.Exists(Properties.Settings.Default.FilePathtbStatementFieldMappingConfig))
                {
                    Util.Log(logType.Info, String.Format("Load Statement Field Mapping to table {0} Start", Properties.Settings.Default.StatementFieldMappingTableName));
                    Util.Log(logType.Info, String.Format("Read Statement Field Mapping data file ({0}) start", Path.GetFileName(Properties.Settings.Default.FilePathtbStatementFieldMappingConfig)));
                    List<String> lines = new List<String>();
                    using (TextReader tr = File.OpenText(Properties.Settings.Default.FilePathtbStatementFieldMappingConfig))
                    {
                        String eachLine = String.Empty;
                        lineCounter = 0;
                        while ((eachLine = tr.ReadLine()) != null)
                        {
                            lineCounter++;
                            if (eachLine.Split('|').Count() != Convert.ToInt16(Properties.Settings.Default.FilePathStatementFieldMappingValidColumn))
                            {
                                Util.Log(logType.Error, String.Format("Statement Field Mapping File({0}), no. of column mismatch at line {1} : \n{2}", Path.GetFileName(Properties.Settings.Default.FilePathtbStatementFieldMappingConfig), lineCounter, eachLine));
                                return;
                            }
                            lines.Add(eachLine);
                        }
                        tr.Close();
                    }
                    Util.Log(logType.Info, String.Format("Read Statement Field Mapping data file ({0}) finish", Path.GetFileName(Properties.Settings.Default.FilePathtbStatementFieldMappingConfig)));
                    Util.Log(logType.Info, String.Format("Total No. of valid records in {0} = {1}", Path.GetFileName(Properties.Settings.Default.FilePathtbStatementFieldMappingConfig), lines.Count));

                    entity.ExecuteStoreCommand(String.Format("delete from {0}", Properties.Settings.Default.StatementFieldMappingTableName));

                    Util.Log(logType.Info, String.Format("Insert record into {0} start", Properties.Settings.Default.StatementFieldMappingTableName));
                    int counter = 0;
                    DateTime now = DateTime.Now;
                    foreach (var line in lines)
                    {
                        String[] data = line.Split('|');
                        tbStatementFieldMapping record = new tbStatementFieldMapping();
                        record.FormatID = data[(int)StatementFieldMapping_GetData.FormatID].Trim();
                        record.Field = data[(int)StatementFieldMapping_GetData.Field].Trim();
                        record.FieldNature = data[(int)StatementFieldMapping_GetData.FieldNature].Trim();
                        record.Position = Convert.ToInt32(data[(int)StatementFieldMapping_GetData.Position].Trim());
                        record.Format = data[(int)StatementFieldMapping_GetData.Format].Trim();
                        record.CreateDate = now;
                        entity.AddTotbStatementFieldMapping(record);

                        if (counter++ % 2000 == 0)
                        {
                            entity.SaveChanges();
                        }
                    }
                    entity.SaveChanges();
                    Util.Log(logType.Info, String.Format("Insert record into {0} finish", Properties.Settings.Default.StatementFieldMappingTableName));
                    Util.Log(logType.Info, String.Format("Load Statement Field Mapping to table {0} Finish", Properties.Settings.Default.StatementFieldMappingTableName));
                }
                else
                    Util.Log(logType.Info, "Statement Field Mapping data file not exist, skip 'Load Statement Field Mapping'");
                #endregion
            }
            catch (Exception ex)
            {
                Util.Log(logType.Error, String.Format("Error in Load Statement Field Mapping : {0}", ex.Message));
            }

        }

        // operation i/d category statementType effectiveFrom effectiveTo value HeaderPositionX HeaderPositionY
        private static Boolean argsValidation(String[] args, String line, String filename)
        {
            if (args == null)
            {
                Util.Log(logType.Warning, String.Format("argument is null. filename:{0}. line:{1}", filename, line));
                return false;
            }

            if (args.Length < 2)
            {
                Util.Log(logType.Warning, String.Format("Wrong number of arguments. filename:{0}. line:{1}", filename, line));
                return false;
            }

            if (args[0] != Properties.Settings.Default.OperationBankInfoInsert &&
                args[0] != Properties.Settings.Default.OperationHeader &&
                args[0] != Properties.Settings.Default.OperationMarketInsert &&
                args[0] != Properties.Settings.Default.OperationPredefinedPeriod &&
                args[0] != Properties.Settings.Default.OperationTC)
            {
                Util.Log(logType.Warning, String.Format("Invalid operation argument. filename:{0}. line:{1}", filename, line));
                return false;
            }

            if (!args[1].Equals("i") && !args[1].Equals("d"))
            {
                Util.Log(logType.Warning, String.Format("Wrong operation type argument. filename:{0}. line:{1}", filename, line));
                return false;
            }

            if (args[1].Equals("i"))
            {
                if (args[0] == Properties.Settings.Default.OperationHeader)
                {
                    if (args.Length < 9)
                    {
                        Util.Log(logType.Warning, String.Format("Wrong number of arguments. filename:{0}. line:{1}", filename, line));
                        return false;
                    }
                }
                else
                {
                    if (args.Length < 7)
                    {
                        Util.Log(logType.Warning, String.Format("Wrong number of arguments. filename:{0}. line:{1}", filename, line));
                        return false;
                    }
                }
            }
            if (args[1].Equals("d") && args.Length < 6)
            {
                Util.Log(logType.Warning, String.Format("Wrong number of arguments. filename:{0}. line:{1}", filename, line));
                return false;
            }

            if (args[2] != Properties.Settings.Default.CategoryIndividual &&
                    args[2] != Properties.Settings.Default.CategoryCiticFirst &&
                    args[2] != Properties.Settings.Default.CategoryCreditCard &&
                    args[2] != Properties.Settings.Default.CategoryCombine)
            {
                Util.Log(logType.Warning, String.Format("Invalid category argument. filename:{0}. line:{1}", filename, line));
                return false;
            }

            if (args[3].Length > 50)
            {
                Util.Log(logType.Warning, String.Format("Invalid type argument. filename:{0}. line:{1}", filename, line));
                return false;
            }

            DateTime effectiveFrom = DateTime.Now;
            DateTime effectiveTo = DateTime.Now;

            if (!isValidDate(args[4], ref effectiveFrom))
            {
                Util.Log(logType.Warning, String.Format("Invalid EffectiveFrom Date. filename:{0}. line:{1}", filename, line));
                return false;
            }
            if (!isValidDate(args[5], ref effectiveTo))
            {
                Util.Log(logType.Warning, String.Format("Invalid EffectiveTo Date. filename:{0}. line:{1}", filename, line));
                return false;
            }
            if (effectiveFrom > effectiveTo)
            {
                Util.Log(logType.Warning, String.Format("Effective Date From cannot larger than Effective Date To. filename:{0}. line:{1}", filename, line));
                return false;
            }

            if (args[1].Equals("i"))
            {
                if (args[0] == Properties.Settings.Default.OperationPredefinedPeriod)
                {
                    int value;
                    if (!int.TryParse(args[6], out value))
                    {
                        Util.Log(logType.Warning, String.Format("Invalid predefined period. filename:{0}. line:{1}", filename, line));
                        return false;
                    }
                }
                else
                {
                    if (!isValidPath(args[6]))
                    {
                        Util.Log(logType.Warning, String.Format("Invalid Path. filename:{0}. line:{1}", filename, line));
                        return false;
                    }
                }
            }

            if (args[1].Equals("i") && args[0] == Properties.Settings.Default.OperationHeader)
            {
                if (!isValidInt(args[7]))
                {
                    Util.Log(logType.Warning, String.Format("Invalid HeaderPositionX. filename:{0}. line:{1}", filename, line));
                    return false;
                }
                if (!isValidInt(args[8]))
                {
                    Util.Log(logType.Warning, String.Format("Invalid HeaderPositionY. filename:{0}. line:{1}", filename, line));
                    return false;
                }
            }

            return true;
        }

        private static void predefinedPeriod(String[] args, String line, String csvFilename)
        {
            EStatementEntities entities = new EStatementEntities();
            DateTime now = DateTime.Now;
            String[] formats = { "yyyy-MM-dd" };
            DateTime effectiveFrom = DateTime.ParseExact(args[4], "yyyy-MM-dd", null);
            DateTime effectiveTo = DateTime.ParseExact(args[5], "yyyy-MM-dd", null);
            String category = args[2];
            String statementType = args[3];

            if (args[1].Equals("i"))
            {
                List<tbSystemSetting> predefinedPeriodList = entities.tbSystemSetting.Where(a => a.SettingName == "PredefinedPeriod" && a.Category == category && a.StatementType == statementType && effectiveFrom <= a.EffectiveTo && effectiveTo >= a.EffectiveFrom).ToList();
                if (predefinedPeriodList.Count > 0)
                {
                    numOfFailed++;
                    Util.Log(logType.Warning, String.Format("Effective Date Overlap. filename:{0}. line:{1}", csvFilename, line));
                    return;
                }
                else
                {
                    tbSystemSetting systemSetting = new tbSystemSetting();
                    systemSetting.SettingName = "PredefinedPeriod";
                    systemSetting.SettingValue = args[6];
                    systemSetting.Category = category;
                    systemSetting.StatementType = statementType;
                    systemSetting.EffectiveFrom = effectiveFrom;
                    systemSetting.EffectiveTo = effectiveTo;
                    systemSetting.CreateDate = now;
                    systemSetting.LastUpdate = now;
                    systemSetting.CreateBy = Properties.Settings.Default.AppName;
                    systemSetting.ModifyBy = Properties.Settings.Default.AppName;

                    entities.AddTotbSystemSetting(systemSetting);
                    entities.SaveChanges();
                    numOfProcessed++;
                    Util.Log(logType.Info, String.Format("filename:{0}. line:{1}", csvFilename, line));
                }
            }
            else
            {
                tbSystemSetting record = entities.tbSystemSetting.Where(a => a.SettingName == "PredefinedPeriod" && a.Category == category && a.StatementType == statementType && a.EffectiveFrom == effectiveFrom && a.EffectiveTo == effectiveTo).FirstOrDefault();
                if (record != null)
                {
                    entities.tbSystemSetting.DeleteObject(record);
                    entities.SaveChanges();
                    numOfProcessed++;
                    Util.Log(logType.Info, String.Format("filename:{0}. line:{1}", csvFilename, line));
                }
                else
                {
                    numOfFailed++;
                    Util.Log(logType.Warning, String.Format("Record not found. filename:{0}. line:{1}", csvFilename, line));
                }
            }
        }

        private static void bankInfoInsert(String[] args, String line, String csvFilename)
        {
            EStatementEntities entities = new EStatementEntities();
            DateTime now = DateTime.Now;
            String[] formats = { "yyyy-MM-dd" };
            DateTime effectiveFrom = DateTime.ParseExact(args[4], "yyyy-MM-dd", null);
            DateTime effectiveTo = DateTime.ParseExact(args[5], "yyyy-MM-dd", null);
            String category = args[2];
            String statementType = args[3];

            if (args[1].Equals("i"))
            {
                List<tbBankInfoInsert> bankInfoInsertList = entities.tbBankInfoInsert.Where(a => a.Category == category && a.StatementType == statementType && effectiveFrom <= a.EffectiveTo && effectiveTo >= a.EffectiveFrom).ToList();
                if (bankInfoInsertList.Count > 0)
                {
                    numOfFailed++;
                    Util.Log(logType.Warning, String.Format("Effective Date Overlap. filename:{0}. line:{1}", csvFilename, line));
                    return;
                }
                else
                {
                    Directory.CreateDirectory(Properties.Settings.Default.BankInfoInsertPath);
                    String filename = String.Format("{0}_{1}_{2}", DateTime.Now.ToString("yyyyMMddHHmmssffff"), rand.Next(10000, 99999).ToString(), args[6]);
                    try
                    {
                        File.Copy(Path.Combine(Properties.Settings.Default.SystemSettingDataFolder, args[6]), Path.Combine(Properties.Settings.Default.BankInfoInsertPath, filename));
                    }
                    catch (Exception ex)
                    {
                        numOfFailed++;
                        Util.Log(logType.Error, String.Format("Error in copying file: {0}. Error message:{1}. filename:{2}. line:{3}", args[6], ex.Message, csvFilename, line));
                        return;
                    }

                    tbBankInfoInsert bankInfoInsert = new tbBankInfoInsert();
                    bankInfoInsert.InsertPath = Path.Combine(Properties.Settings.Default.BankInfoInsertPath, filename);
                    bankInfoInsert.Category = category;
                    bankInfoInsert.StatementType = statementType;
                    bankInfoInsert.EffectiveFrom = effectiveFrom;
                    bankInfoInsert.EffectiveTo = effectiveTo;
                    bankInfoInsert.CreateDate = now;
                    bankInfoInsert.LastUpdate = now;
                    bankInfoInsert.CreateBy = Properties.Settings.Default.AppName;
                    bankInfoInsert.ModifyBy = Properties.Settings.Default.AppName;

                    entities.AddTotbBankInfoInsert(bankInfoInsert);
                    entities.SaveChanges();
                    numOfProcessed++;
                    Util.Log(logType.Info, String.Format("filename:{0}. line:{1}", csvFilename, line));
                }
            }
            else
            {
                tbBankInfoInsert record = entities.tbBankInfoInsert.Where(a => a.Category == category && a.StatementType == statementType && a.EffectiveFrom == effectiveFrom && a.EffectiveTo == effectiveTo).FirstOrDefault();
                if (record != null)
                {
                    try
                    {
                        File.Delete(record.InsertPath);
                    }
                    catch (Exception) { }
                    entities.tbBankInfoInsert.DeleteObject(record);
                    entities.SaveChanges();
                    numOfProcessed++;
                    Util.Log(logType.Info, String.Format("filename:{0}. line:{1}", csvFilename, line));
                }
                else
                {
                    numOfFailed++;
                    Util.Log(logType.Warning, String.Format("Record not found. filename:{0}. line:{1}", csvFilename, line));
                }
            }
        }

        private static void marketInsert(String[] args, String line, String csvFilename)
        {
            EStatementEntities entities = new EStatementEntities();
            DateTime now = DateTime.Now;
            String[] formats = { "yyyy-MM-dd" };
            DateTime effectiveFrom = DateTime.ParseExact(args[4], "yyyy-MM-dd", null);
            DateTime effectiveTo = DateTime.ParseExact(args[5], "yyyy-MM-dd", null);
            String category = args[2];
            String statementType = args[3];

            if (args[1].Equals("i"))
            {
                List<tbMarketInsert> marketInsertList = entities.tbMarketInsert.Where(a => a.Category == category && a.StatementType == statementType && effectiveFrom <= a.EffectiveTo && effectiveTo >= a.EffectiveFrom).ToList();
                if (marketInsertList.Count > 0)
                {
                    numOfFailed++;
                    Util.Log(logType.Warning, String.Format("Effective Date Overlap. filename:{0}. line:{1}", csvFilename, line));
                    return;
                }
                else
                {
                    Directory.CreateDirectory(Properties.Settings.Default.MarketInsertPath);
                    String filename = String.Format("{0}_{1}_{2}", DateTime.Now.ToString("yyyyMMddHHmmssffff"), rand.Next(10000, 99999).ToString(), args[6]);
                    try
                    {
                        File.Copy(Path.Combine(Properties.Settings.Default.SystemSettingDataFolder, args[6]), Path.Combine(Properties.Settings.Default.MarketInsertPath, filename));
                    }
                    catch (Exception ex)
                    {
                        numOfFailed++;
                        Util.Log(logType.Error, String.Format("Error in copying file: {0}. Error message:{1}. filename:{2}. line:{3}", args[6], ex.Message, csvFilename, line));
                        return;
                    }

                    tbMarketInsert marketInsert = new tbMarketInsert();
                    marketInsert.InsertPath = Path.Combine(Properties.Settings.Default.MarketInsertPath, filename);
                    marketInsert.Category = category;
                    marketInsert.StatementType = statementType;
                    marketInsert.EffectiveFrom = effectiveFrom;
                    marketInsert.EffectiveTo = effectiveTo;
                    marketInsert.CreateDate = now;
                    marketInsert.LastUpdate = now;
                    marketInsert.CreateBy = Properties.Settings.Default.AppName;
                    marketInsert.ModifyBy = Properties.Settings.Default.AppName;

                    entities.AddTotbMarketInsert(marketInsert);
                    entities.SaveChanges();
                    numOfProcessed++;
                    Util.Log(logType.Info, String.Format("filename:{0}. line:{1}", csvFilename, line));
                }
            }
            else
            {
                tbMarketInsert record = entities.tbMarketInsert.Where(a => a.Category == category && a.StatementType == statementType && a.EffectiveFrom == effectiveFrom && a.EffectiveTo == effectiveTo).FirstOrDefault();
                if (record != null)
                {
                    try
                    {
                        File.Delete(record.InsertPath);
                    }
                    catch (Exception) { }
                    entities.tbMarketInsert.DeleteObject(record);
                    entities.SaveChanges();
                    numOfProcessed++;
                    Util.Log(logType.Info, String.Format("filename:{0}. line:{1}", csvFilename, line));
                }
                else
                {
                    numOfFailed++;
                    Util.Log(logType.Warning, String.Format("Record not found. filename:{0}. line:{1}", csvFilename, line));
                }
            }
        }

        private static void tc(String[] args, String line, String csvFilename)
        {
            EStatementEntities entities = new EStatementEntities();
            DateTime now = DateTime.Now;
            String[] formats = { "yyyy-MM-dd" };
            DateTime effectiveFrom = DateTime.ParseExact(args[4], "yyyy-MM-dd", null);
            DateTime effectiveTo = DateTime.ParseExact(args[5], "yyyy-MM-dd", null);
            String category = args[2];
            String statementType = args[3];

            if (args[1].Equals("i"))
            {
                List<tbSystemSetting> systemSettingTcList = entities.tbSystemSetting.Where(a => a.SettingName == "TermsAndCondition" && a.Category == category && a.StatementType == statementType && effectiveFrom <= a.EffectiveTo && effectiveTo >= a.EffectiveFrom).ToList();
                if (systemSettingTcList.Count > 0)
                {
                    numOfFailed++;
                    Util.Log(logType.Warning, String.Format("Effective Date Overlap. filename:{0}. line:{1}", csvFilename, line));
                    return;
                }
                else
                {
                    Directory.CreateDirectory(Properties.Settings.Default.TCPath);
                    String filename = String.Format("{0}_{1}_{2}", DateTime.Now.ToString("yyyyMMddHHmmssffff"), rand.Next(10000, 99999).ToString(), args[6]);
                    try
                    {
                        File.Copy(Path.Combine(Properties.Settings.Default.SystemSettingDataFolder, args[6]), Path.Combine(Properties.Settings.Default.TCPath, filename));
                    }
                    catch (Exception ex)
                    {
                        numOfFailed++;
                        Util.Log(logType.Error, String.Format("Error in copying file: {0}. Error message:{1}. filename:{2}. line:{3}", args[6], ex.Message, csvFilename, line));
                        return;
                    }

                    tbSystemSetting systemSetting = new tbSystemSetting();
                    systemSetting.SettingName = "TermsAndCondition";
                    systemSetting.SettingValue = Path.Combine(Properties.Settings.Default.HeaderPath, filename);
                    systemSetting.Category = category;
                    systemSetting.StatementType = statementType;
                    systemSetting.EffectiveFrom = effectiveFrom;
                    systemSetting.EffectiveTo = effectiveTo;
                    systemSetting.CreateDate = now;
                    systemSetting.LastUpdate = now;
                    systemSetting.CreateBy = Properties.Settings.Default.AppName;
                    systemSetting.ModifyBy = Properties.Settings.Default.AppName;

                    entities.AddTotbSystemSetting(systemSetting);
                    entities.SaveChanges();
                    numOfProcessed++;
                    Util.Log(logType.Info, String.Format("filename:{0}. line:{1}", csvFilename, line));
                }
            }
            else
            {
                tbSystemSetting record = entities.tbSystemSetting.Where(a => a.SettingName == "TermsAndCondition" && a.Category == category && a.StatementType == statementType && a.EffectiveFrom == effectiveFrom && a.EffectiveTo == effectiveTo).FirstOrDefault();
                if (record != null)
                {
                    try
                    {
                        File.Delete(record.SettingValue);
                    }
                    catch (Exception) { }
                    entities.tbSystemSetting.DeleteObject(record);
                    entities.SaveChanges();
                    numOfProcessed++;
                    Util.Log(logType.Info, String.Format("filename:{0}. line:{1}", csvFilename, line));
                }
                else
                {
                    numOfFailed++;
                    Util.Log(logType.Warning, String.Format("Record not found. filename:{0}. line:{1}", csvFilename, line));
                }
            }
        }

        private static void header(String[] args, String line, String csvFilename)
        {
            EStatementEntities entities = new EStatementEntities();
            DateTime now = DateTime.Now;
            String[] formats = { "yyyy-MM-dd" };
            DateTime effectiveFrom = DateTime.ParseExact(args[4], "yyyy-MM-dd", null);
            DateTime effectiveTo = DateTime.ParseExact(args[5], "yyyy-MM-dd", null);
            String category = args[2];
            String statementType = args[3];

            if (args[1].Equals("i"))
            {
                List<tbSystemSetting> systemSettingBankLogoList = entities.tbSystemSetting.Where(a => a.SettingName == "Header" && a.Category == category && a.StatementType == statementType && effectiveFrom <= a.EffectiveTo && effectiveTo >= a.EffectiveFrom).ToList();
                if (systemSettingBankLogoList.Count > 0)
                {
                    numOfFailed++;
                    Util.Log(logType.Warning, String.Format("Effective Date Overlap. filename:{0}. line:{1}", csvFilename, line));
                    return;
                }
                else
                {
                    Directory.CreateDirectory(Properties.Settings.Default.HeaderPath);
                    String filename = String.Format("{0}_{1}_{2}", DateTime.Now.ToString("yyyyMMddHHmmssffff"), rand.Next(10000, 99999).ToString(), args[6]);
                    try
                    {
                        File.Copy(Path.Combine(Properties.Settings.Default.SystemSettingDataFolder, args[6]), Path.Combine(Properties.Settings.Default.HeaderPath, filename));
                    }
                    catch (Exception ex)
                    {
                        numOfFailed++;
                        Util.Log(logType.Error, String.Format("Error in copying file: {0}. Error message:{1}. filename:{2}. line:{3}", args[6], ex.Message, csvFilename, line));
                        return;
                    }

                    tbSystemSetting systemSetting = new tbSystemSetting();
                    systemSetting.SettingName = "Header";
                    systemSetting.SettingValue = Path.Combine(Properties.Settings.Default.HeaderPath, filename);
                    systemSetting.Category = category;
                    systemSetting.StatementType = statementType;
                    systemSetting.HeaderPositionX = Convert.ToInt32(args[7]);
                    systemSetting.HeaderPositionY = Convert.ToInt32(args[8]);
                    systemSetting.EffectiveFrom = effectiveFrom;
                    systemSetting.EffectiveTo = effectiveTo;
                    systemSetting.CreateDate = now;
                    systemSetting.LastUpdate = now;
                    systemSetting.CreateBy = Properties.Settings.Default.AppName;
                    systemSetting.ModifyBy = Properties.Settings.Default.AppName;

                    entities.AddTotbSystemSetting(systemSetting);
                    entities.SaveChanges();
                    numOfProcessed++;
                    Util.Log(logType.Info, String.Format("filename:{0}. line:{1}", csvFilename, line));
                }
            }
            else
            {
                tbSystemSetting record = entities.tbSystemSetting.Where(a => a.SettingName == "Header" && a.Category == category && a.StatementType == statementType && a.EffectiveFrom == effectiveFrom && a.EffectiveTo == effectiveTo).FirstOrDefault();
                if (record != null)
                {
                    try
                    {
                        File.Delete(record.SettingValue);
                    }
                    catch (Exception) { }
                    entities.tbSystemSetting.DeleteObject(record);
                    entities.SaveChanges();
                    numOfProcessed++;
                    Util.Log(logType.Info, String.Format("filename:{0}. line:{1}", csvFilename, line));
                }
                else
                {
                    numOfFailed++;
                    Util.Log(logType.Warning, String.Format("Record not found. filename:{0}. line:{1}", csvFilename, line));
                }
            }
        }

        private static Boolean isValidDate(String date, ref DateTime output)
        {
            String[] formats = { "yyyy-MM-dd" };

            if (!DateTime.TryParseExact(date, formats,
                              CultureInfo.InvariantCulture,
                              DateTimeStyles.None,
                              out output))
                return false;
            return true;
        }

        private static Boolean isValidPath(String filePath)
        {
            if (!File.Exists(Path.Combine(Properties.Settings.Default.SystemSettingDataFolder, filePath)))
                return false;
            return true;
        }

        private static Boolean isValidInt(String position)
        {
            int temp = 0;
            return Int32.TryParse(position, out temp);
        }
    }
}
