﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ESSystemSettingTool
{
    class Util
    {
        static EStatementEntities Entity = new EStatementEntities();
        static String logFilename = String.Format("{0}.log", Properties.Settings.Default.AppName);
        static String logPath = Path.Combine(Directory.GetCurrentDirectory(), "Log", logFilename);

        public static void Log(logType type, String content)
        {
            try
            {
                DateTime now = DateTime.Now;
                tbLog log = new tbLog();
                log.Type = Enum.GetName(typeof(logType), type);
                log.Content = content;
                log.CreateBy = Properties.Settings.Default.AppName;
                log.ModifyBy = Properties.Settings.Default.AppName;
                log.CreateDate = now;
                log.LastUpdate = now;
                Entity.AddTotbLog(log);
                Entity.SaveChanges();

                Console.WriteLine(content);

                if (!type.Equals(logType.Info))
                {
                    writeLog(content);
                }
            }
            catch (Exception) { }
        }

        public static void writeLog(String content)
        {
            try
            {
                if (File.Exists(logPath))
                {
                    if (!File.GetLastWriteTime(logPath).ToString("yyyyMMdd").Equals(DateTime.Now.ToString("yyyyMMdd")))
                    {
                        String newLogPath = String.Format("{0}_{1:yyyyMMdd}.log", logPath.Replace(".log", ""), File.GetLastWriteTime(logPath).ToString("yyyyMMdd"));
                        File.Move(logPath, newLogPath);
                    }
                }
                using (StreamWriter sw = new StreamWriter(logPath, true, Encoding.UTF8))
                {
                    sw.WriteLine(content);
                    sw.Close();
                }
            }
            catch (Exception) { }
        }

        public class ProductTypeMapLogic
        {
            public String metaType;
            public String orderType;
            public String citicAssetGroup;
            public String citicAssetSubGroup;
            public String productType;
            public ProductTypeMapLogic(String i_metaType, String i_orderType, String i_citicAssetGroup, String i_citicAssetSubGroup, String i_productType)
            {
                metaType = i_metaType;
                orderType = i_orderType;
                citicAssetGroup = i_citicAssetGroup;
                citicAssetSubGroup = i_citicAssetSubGroup;
                productType = i_productType;
            }
        }
        public class ProductNameMapLogic
        {
            public String metaType;
            public String orderType;
            public String citicAssetGroup;
            public String citicAssetSubGroup;
            public String productName;
            public ProductNameMapLogic(String i_metaType, String i_orderType, String i_citicAssetGroup, String i_citicAssetSubGroup, String i_productName)
            {
                metaType = i_metaType;
                orderType = i_orderType;
                citicAssetGroup = i_citicAssetGroup;
                citicAssetSubGroup = i_citicAssetSubGroup;
                productName = i_productName;
            }
        }
        public class AdviceTitleMapLogic
        {
            public String metaType;
            public String orderType;
            public String citicAssetGroup;
            public String citicAssetSubGroup;
            public String adviceTitle;
            public AdviceTitleMapLogic(String i_metaType, String i_orderType, String i_citicAssetGroup, String i_citicAssetSubGroup, String i_adviceTitle)
            {
                metaType = i_metaType;
                orderType = i_orderType;
                citicAssetGroup = i_citicAssetGroup;
                citicAssetSubGroup = i_citicAssetSubGroup;
                adviceTitle = i_adviceTitle;
            }
        }
    }
}
